import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {GlobalVars} from '../global-vars';

@Injectable()
export class DriverProvider {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/";
  }

  getDrivers(filter = {}){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/drivers.json'  , {headers: this.headers,params: filter}).map(res => {
      return res;
    });
  }

  addDriver(params){
  	this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/register_as_driver.json',params, {headers: this.headers}).map(res => {
      return res;
    });
  }

  editDriver(params){
      this.setHeader();
      return this.http.post(this.baseUrl + 'fleet/drivers/register_as_driver.json',params, {headers: this.headers}).map(res => {
        return res;
      });
  }

  getDriver(id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/drivers/'+id+'.json', {headers: this.headers}).map(res => {
      return res;
    });
  }

  updateDriver(params,id){
    this.setHeader();
    return this.http.put(this.baseUrl + 'fleet/drivers/'+id+'.json', params  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  deleteDriver(id){
    this.setHeader();
    return this.http.delete(this.baseUrl + 'fleet/drivers/'+id+'.json' , {headers: this.headers}).map(res => {
      return res;
    });
  }

  upcomingShipments(page_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/upcoming_shipments.json?page='+page_id ,{}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  ongoingShipments(page_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/ongoing_shipments.json?page='+page_id ,{}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  completedShipments(page_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/completed_shipments.json?page='+page_id ,{}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  updateVehicleStatus(vehicle_id,status,shipment_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/update_vehicle_status.json' ,{vehicle_id,status,shipment_id}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  report(shipment_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/report_problem.json' ,{shipment_id}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  paymentReceived(shipment_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/drivers/payment_received.json' ,{shipment_id}, {headers: this.headers}).map(res => {
      return res;
    });
  }

  setHeader(){
    
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }
}

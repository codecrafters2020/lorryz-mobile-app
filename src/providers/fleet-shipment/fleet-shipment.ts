import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {GlobalVars} from '../global-vars';
import 'rxjs/add/operator/map';

@Injectable()
export class FleetShipment {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/fleet/";
  }

  update(shipment, shipment_id, company_id){
    this.setHeader();
    return this.http.put(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'.json' , {shipment},{headers: this.headers}).map(res => {
      return res;
    });
  }

  update_destination(change_destination_request, shipment_id, company_id){
    this.setHeader();
    return this.http.put(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/update_destination.json' , {change_destination_request},{headers: this.headers}).map(res => {
      return res;
    });
  }

  show(company_id, shipment_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  vehicles(company_id, shipment_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/vehicles.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  ongoing_vehicles(company_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/ongoing_vehicles.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  posted(company_id,page_id){
    console.log("page",page_id)
  	this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/posted.json?page='+page_id, {headers: this.headers}).map(res => {
        return res;
    });
  }

  active(company_id,page_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/active.json?page='+page_id  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  won(company_id,page_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/won.json?page='+page_id  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  ongoing(company_id,page_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/ongoing.json?page='+page_id  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  completed(company_id,page_id){
   this.setHeader();
   return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/completed.json?page='+page_id  , {headers: this.headers}).map(res => {
       return res;
   });
  }

  bidonShipment(shipment_id, company_id,bid){
      this.setHeader();
      return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/bids.json' , {bid},{headers: this.headers}).map(res => {
        return res;
      });
  }
  notInterested(shipment_id, company_id){
      this.setHeader();
      return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/not_interested.json' , {},{headers: this.headers}).map(res => {
        return res;
      });
  }

  quitBid(company_id,shipment_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/reject_bid.json' , {},{headers: this.headers}).map(res => {
      return res;
    });
  }
  updatePdfonShipment(shipment, shipment_id, company_id)
  {
    this.setHeader();
    return this.http.put(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'.json' , {shipment},{headers: this.headers}).map(res => {
      return res;
    });
  }
  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }

}

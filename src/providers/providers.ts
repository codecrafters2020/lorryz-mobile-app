import { GlobalVars } from './global-vars';
import { NetworkProvider } from '../providers/network/network';
import { Loader } from './loader';
import { Api } from './api/api';
import { Items } from '../mocks/providers/items';
import { Settings } from './settings/settings';
import { User } from './user/user';
import { Shipment } from './shipment/shipment';
import { FleetShipment } from './fleet-shipment/fleet-shipment';
import { CountryProvider } from './country/country';
import { DriverProvider } from '../providers/driver/driver';
import { VehicleProvider } from '../providers/vehicle/vehicle';
import { LocationTracker } from '../providers/location-tracker/location-tracker';
import { Rating } from '../providers/rating/rating';
import { Payment } from '../providers/payment/payment';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { InvoiceProvider } from '../providers/invoice/invoice';
export {
	GlobalVars,
    Api,
    NetworkProvider,
    Loader,
    Items,
    Settings,
    User,
    Shipment,
    FleetShipment,
    CountryProvider,
    DriverProvider,
    VehicleProvider,
    LocationTracker,
    Rating,
    Payment,
    GeocoderProvider,
    InvoiceProvider,
};

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import {GlobalVars} from '../global-vars';
import 'rxjs/add/operator/map';

@Injectable()
export class Payment {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public api: Api, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/";
  }

  cargo_payments(company_id){
  	this.setHeader();
    return this.http.get(this.baseUrl + 'cargo/companies/'+company_id+'/payments.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  fleet_payments(company_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/companies/'+company_id+'/payments.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }
}

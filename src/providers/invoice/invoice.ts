import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import {GlobalVars} from '../global-vars';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoiceProvider {
	public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public api: Api, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/invoices/";
  }

  send_in_email(shipment_id){
    this.setHeader();
    return this.http.post(this.baseUrl +shipment_id+ '/send_in_email.json',{}, {headers: this.headers}).map(res => {
      return res;
    });
  }
  
  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }
}

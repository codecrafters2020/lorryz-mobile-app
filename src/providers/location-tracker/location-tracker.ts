import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
import {VehicleProvider} from '../vehicle/vehicle';
import { Platform, ToastController} from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { BackgroundGeolocationConfig, BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';


@Injectable()
export class LocationTracker {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;

  constructor(public zone: NgZone,
   public backgroundGeolocation: BackgroundGeolocation,
   public geolocation: Geolocation,
   public platform: Platform,
   private diagnostic: Diagnostic,
   private locationAccuracy: LocationAccuracy,
   public vehicle: VehicleProvider,
   public toastCtrl: ToastController,) {


  }

  startTracking(vehicle_id) {
	  // Background Tracking
console.log("s")
	  const config: BackgroundGeolocationConfig = {
		desiredAccuracy: 0,
		stationaryRadius: 20,
		 distanceFilter: 10,
		 interval:8000,
		// debug:true,
		stopOnTerminate: true, // enable this to clear background location settings when the app terminates
};
console.log("a")
this.backgroundGeolocation.configure(config)
  .then(() => {
	console.log("m")
    this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {

	  // Run update inside of Angular's zone
	  this.zone.run(() => {
		this.lat = location.latitude;
		this.lng = location.longitude;
		let vehicle = {id: vehicle_id, latitude: this.lat, longitude: this.lng}
		this.vehicle.update_lat_lng(vehicle, vehicle_id).subscribe(data => {
			console.log("e")
			console.log(data)
		  });

	  });
      // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
      // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
      // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
    //  this.backgroundGeolocation.finish(); // FOR IOS ONLY
    });

  });

	//   this.backgroundGeolocation.configure(config).subscribe((location) => {


	//     console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

	//     // Run update inside of Angular's zone
	//     this.zone.run(() => {
	//       this.lat = location.latitude;
	//       this.lng = location.longitude;
	//       let vehicle = {id: vehicle_id, latitude: this.lat, longitude: this.lng}
	//       this.vehicle.update_lat_lng(vehicle, vehicle_id).subscribe(data => {
	// 	    });

	//     });

	//   }, (err) => {

	//     console.log(err);

	//   });

	  // Turn ON the background-geolocation system.
	this.backgroundGeolocation.start();


	  // Foreground Tracking
	 	this.foregroundGeolocation(vehicle_id)

	}

 	foregroundGeolocation(vehicle_id){
 		if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled){
        	this.locationAccuracy
           .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
         			this.loadForegroundLocation(vehicle_id);
          });
        }else{
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              () => {
                this.loadForegroundLocation(vehicle_id);
              },
              error => console.log('Error requesting location permissions', error)
            );
          });

        }
      })
    }else{
      this.loadForegroundLocation(vehicle_id);
    }
 	}

 	loadForegroundLocation(vehicle_id){
		var lastUpdateTime,

		minFrequency = 10*1000,
		options = {
			timeout : 30000,
			// maxAge: 0,
			enableHighAccuracy: true
		};
		var i =0;
	        console.log("loadForegroundLocation")
			this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
		  this.zone.run(() => {
			  console.log("loadForegroundLocationzone")
		    this.lat = position.coords.latitude;
		    this.lng = position.coords.longitude;
			let vehicle = {id: vehicle_id, latitude: this.lat, longitude: this.lng,accuracy:position.coords.accuracy,speed:(position.coords.speed*3.6)}
			if(i>=1 ){
				this.watch.unsubscribe().then(()=>{
					console.log("loadForegroundLocationzone"   + i +"here" );
					return;
				});}
			else
		{
      if(position.coords.accuracy<31)
      {
		i++;
		this.vehicle.update_lat_lng(vehicle, vehicle_id).subscribe(data => {
          console.log("updated lat lng"   + i +"addhere" );
            });
      }
    }
		  });

		});



 	}

  stopTracking() {
 		if (this.backgroundGeolocation){
	 		console.log('stopTracking');
	 		this.backgroundGeolocation.finish();
		  this.backgroundGeolocation.stop();
		  if (this.watch){
		  	this.watch.unsubscribe();
		  }
 		}
  }

}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import {GlobalVars} from '../global-vars';
import 'rxjs/add/operator/map';

@Injectable()
export class Shipment {
	public headers : any;
	private baseUrl: string;
	private baseurl: string;
  constructor(public http: HttpClient, public api: Api, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/cargo/";
    this.baseurl = globalVar.server_link + "/api/v1/shipments/";
  }

  create(shipment, company_id){
  	this.setHeader();
    return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments.json' , {shipment},{headers: this.headers})
    .map(res => {
      return res;
    });
  }

  posted(company_id,page_id){
  	this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/posted.json?page='+page_id  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  ongoing(company_id,page_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/ongoing.json?page='+page_id , {headers: this.headers}).map(res => {
        return res;
    });
  }

  upcoming(company_id,page_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/upcoming.json?page='+page_id  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  show(company_id, shipment_id){
  	this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }



  edit(company_id, shipment_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/edit.json'  , {headers: this.headers}).map(res => {
        return res;
    });
  }

  update(shipment, shipment_id, company_id){
  	this.setHeader();
    return this.http.put(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'.json' , {shipment},{headers: this.headers}).map(res => {
      return res;
    });
  }

  acceptBid(company_id,shipment_id ,bid_id){
      this.setHeader();
      return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/bids/'+bid_id+'/accept.json' , {},{headers: this.headers}).map(res => {
        return res;
      });
  }
  rejectBid(company_id,shipment_id, bid_id){
      this.setHeader();
      return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/'+shipment_id+'/bids/'+bid_id+'/reject.json' , {},{headers: this.headers}).map(res => {
        return res;
      });
  }


  calculate_fare(shipment, company_id){
    this.setHeader();
    return this.http.post(this.baseUrl + 'companies/'+company_id+'/shipments/calculate_fare.json' , {shipment},{headers: this.headers}).map(res => {
      return res;
    });
  }

  completed(company_id,page_id){
   this.setHeader();
   return this.http.get(this.baseUrl + 'companies/'+company_id+'/shipments/completed.json?page='+page_id  , {headers: this.headers}).map(res => {
       return res;
   });
  }

  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| '',
      "Access-Control-Allow-Origin": "*"
    })
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {GlobalVars} from '../global-vars';

@Injectable()
export class CountryProvider {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/";
  }

  locations(country_id){
  	this.setHeader();
    return this.http.get(this.baseUrl + 'countries/'+country_id+'/locations.json'  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  vehicles(country_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'countries/'+country_id+'/vehicles.json'  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  primaryvehicles(country_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'countries/'+country_id+'/primary_vehicles.json'  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  subCategoryVehicles(vehicle_id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'countries/'+vehicle_id+'/secondary_vehicles.json'  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }
}

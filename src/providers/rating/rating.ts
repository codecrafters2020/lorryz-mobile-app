import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {GlobalVars} from '../global-vars';
import 'rxjs/add/operator/map';

@Injectable()
export class Rating {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/";
  }
  rateShipment(company_rating){
    this.setHeader();
    return this.http.post(this.baseUrl + '/ratings.json' , {company_rating},{headers: this.headers}).map(res => {
      return res;
    });
  }

  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''
    })
  }

}

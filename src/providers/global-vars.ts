import { Injectable } from "@angular/core";

import "rxjs/add/operator/map";
/*
  Generated class for the GlobalVars provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class GlobalVars {
  public current_user;
  public server_link;
  public device_id;
  public os;
  public auth_token;
  public lang;
  rebid: any;
  location_initiazed_fleet:boolean;
  constructor() {
    this.current_user = "";
    this.device_id = "";
    this.os = "";
    this.lang="";
    this.rebid = "";
    this.location_initiazed_fleet=false;
    // this.server_link = "http://testlorryz.thetalent.games";

this.server_link = "https://staging.lorryz.com";
//this.server_link = "http://192.16.10.10:3000"
//this.server_link = "http://172.16.17.93:3000";
    // this.server_link = "https://4730091f.ngrok.io";
    //  this.server_link = "http://192.168.43.113:3000"
//this.server_link = "http://192.168.18.243:3000"
//this.server_link = "http://192.168.43.113:3000";
//this.server_link = "https://lorryz.com"
   // this.server_link = "http://172.16.17.93:3000"
  }

  //fleet: 030776553756
  //cargo:03002258890
}

import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';


@Injectable()
export class Company {
  _user: any;

  constructor(public api: Api) { }



  addCompanyInformation(params){
    let seq = this.api.post('companies/add_company_information.json', params).share();
    
    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  addIndividualInformation(params){
    let seq = this.api.post('companies/add_individual_information.json', params).share();
    
    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getIndividualInformation(){
    let seq = this.api.get('companies/get_individual_information.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq; 
  }

  getCompanyInformation(){
    
    let seq = this.api.get('companies/get_company_information.json', null).share();
    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq; 
  }

 
}

import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class Loader {
  loading: any;
  constructor(public loadingCtrl: LoadingController) {
  }

  show(text){
    if(this.loading){
      this.hide()
    }

    this.loading = this.loadingCtrl.create({
      content: text
    });
    this.loading.present();
  }

  hide(){
    this.loading.dismiss();
  }
}

import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;

  constructor(public api: Api) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */


  getCountries() {
    let seq = this.api.get('countries.json').share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  login(accountInfo: any) {
    let seq = this.api.post('users/sign_in.json', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {

    let seq = this.api.post('users', accountInfo).share();

    seq.subscribe((res: any) => {
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }


  updateSignUp(accountInfo: any,id) {

    let seq = this.api.put('users.json', accountInfo).share();

    seq.subscribe((res: any) => {
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  sendForgotPassword(accountInfo: any){


    let seq = this.api.post('users/reset_password.json', accountInfo).share();

    seq.subscribe((res: any) => {

    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  verfiyAccount(params){

    let seq = this.api.put('users/verify_phone_number.json', params).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  resentCode(){
    let seq = this.api.post('users/resent_code.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  registerAsFleetOwner(){
    let seq = this.api.post('users/register_as_fleet_owner.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  registerAsCargoOwner(){
    let seq = this.api.post('users/register_as_cargo_owner.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  registerAsIndividual(){
    let seq = this.api.post('users/register_as_individual.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  registerAsCompany(){
    let seq = this.api.post('users/register_as_company.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  notifications(user_id,page_id){
    let seq = this.api.get('users/'+user_id+'/notifications.json?page='+page_id , {}).share();
    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getCurrentUser(){
    let seq = this.api.post('users/current_user_detail.json', null).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  update(user){
    debugger;
    let seq = this.api.put('users/'+user.id+'.json', user).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  /**
   * Log the user out, which forgets the session
   */
  logout(user) {
    let seq = this.api.delete('users/'+user.id+'.json', user).share();

    seq.subscribe((res: any) => {
      console.log(res)
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }
}

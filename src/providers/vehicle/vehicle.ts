import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {GlobalVars} from '../global-vars';

@Injectable()
export class VehicleProvider {

  public headers : any;
	private baseUrl: string;
  constructor(public http: HttpClient, public globalVar: GlobalVars) {
    this.baseUrl = globalVar.server_link + "/api/v1/";
  }

  getVehicles(){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/vehicles.json', {headers: this.headers}).map(res => {
      return res;
    });
  }

  fetch_nearby(lat, lng){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/vehicles/fetch_nearby.json?lat='+lat+"&lng="+lng, {headers: this.headers}).map(res => {
      return res;
    });
  }

  getVehiclesForAssignment(filter){
    this.setHeader();
    
    return this.http.get(this.baseUrl + 'fleet/vehicles.json?', {headers: this.headers,params: filter}).map(res => {
      return res;
    });
  }

  getVehiclesByFilter(filter){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/vehicles.json?', {headers: this.headers,params: filter}).map(res => {
      return res;
    });
  }

  getVehicleType(){
   this.setHeader();
   return this.http.get(this.baseUrl + 'fleet/vehicles/vehicle_types.json', {headers: this.headers}).map(res => {
     return res;
   }); 
  }

  addVehicle(params){
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/vehicles.json', params  , {headers: this.headers}).map(res => {
      return res;
    });
  }
  getVehicle(id){
    this.setHeader();
    return this.http.get(this.baseUrl + 'fleet/vehicles/'+id+'.json', {headers: this.headers}).map(res => {
      return res;
    });
  }

  updateVehicle(params,id){
    this.setHeader();
    return this.http.put(this.baseUrl + 'fleet/vehicles/'+id+'.json', params  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  update_lat_lng(params,id){
    this.setHeader();
    return this.http.put(this.baseUrl + 'fleet/vehicles/'+id+'/update_lat_lng.json', params  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  deleteVehicle(id){
    this.setHeader();
    return this.http.delete(this.baseUrl + 'fleet/vehicles/'+id+'.json'  , {headers: this.headers}).map(res => {
      return res;
    });
  }

  changeDriver(driver_id,vehicle_id){
    
    this.setHeader();
    return this.http.post(this.baseUrl + 'fleet/vehicles/change_driver.json', {driver_id: driver_id, id: vehicle_id}  , {headers: this.headers}).map(res => {
      return res;
    });

  }

  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token
    })
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
declare var google;
@Injectable()
export class GeocoderProvider {

  constructor(public http: HttpClient) {
  }

  fetch_address_placeId(placeId): Promise<any>{
  	return new Promise((resolve, reject) =>
   	{
   		var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ "placeId": placeId }, function (results, status)
      {	
      	if (status == google.maps.GeocoderStatus.OK) {
	        if (results) {
	          let str : string = results[0].formatted_address;
	          resolve(str);
	        }
	      }
      })
      
   	});
  }

  fetch_address_latlng(latLng): Promise<any>{
    return new Promise((resolve, reject) =>
     {
       var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ "location": latLng }, function (results, status)
      {  
        if (status == google.maps.GeocoderStatus.OK) {
          if (results) {
            let str : string = results[0].formatted_address;
            resolve(str);
          }
        }
      })
      
     });
  }

  fetch_city_latlng(latLng): Promise<any>{
    return new Promise((resolve, reject) =>
     {
       var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ "location": latLng }, function (results, status)
      {  
        if (status == google.maps.GeocoderStatus.OK) {
          if (results) {
            let str = results[0].address_components
            resolve(str);
          }
        }
      })
      
     });
  }

  fetch_city_placeId(placeId): Promise<any>{
    return new Promise((resolve, reject) =>
     {
       var geocoder = new google.maps.Geocoder;
      geocoder.geocode({ "placeId": placeId }, function (results, status)
      {  
        if (status == google.maps.GeocoderStatus.OK) {
          if (results) {
            let str = results[0].address_components
            resolve(str);
          }
        }
      })
      
     });
  }
}

export class ValidationError{
  formErrors = {
    'first_name': [],
    'last_name': [],
    'email': [],
    'mobile': [],
    'password': [],
    'password_confirmation': [],
    'role_name': [],
    'terms_accepted': [],
    'country':[],
  };

  validationMessages = {
    'username': {
      'required':      'Username is required.',
      'minlength':     'Username must be at least 5 characters long.',
      'maxlength':     'Username cannot be more than 25 characters long.',
      'pattern':       'Your username must contain only numbers and letters.',
      'validUsername': 'Your username has already been taken.'
    },
    'first_name': {
      'required':      'First Name is required'
    },
    'last_name': {
      'required':      'Last Name is required'
    },
    'role_name': {
      'required':      'This field is required'
    },
    'terms_accepted': {
      'required':      'Please mark terms and conditions.'
    },
    'country':{
      'required':      'Country is required'
    },
    'email': {
      'required':      'Email is required',
      'pattern':       'Enter a valid email.'
    },
    'mobile': {
      'required':      'Mobile No is required',
      'pattern':       'Number is not valid',
    },
    'password': {
      'required':      'Password is required',
      'minlength':     'Password must be at least 6 characters long.'
    },
    'password_confirmation':{
      'required':      'Confirm password is required',
      'minlength':     'Confirm password must be at least 6 characters long.',
      'validateEqual': 'Password mismatch'
    },
  };

  populateErrorMessage(formObject: any){
      if (!formObject) {return;}
      const form = formObject;
      for(const field in this.formErrors){
          this.formErrors[field] = [];
          formObject[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid){
              const messages = this.validationMessages[field];
              for(const key in control.errors){
                  this.formErrors[field].push(messages[key])
              }
          }
      }
  }

  populateErrorMessageonSubmit(formObject: any){
      if (!formObject) {return;}
      const form = formObject;
      for(const field in this.formErrors){
          this.formErrors[field] = [];
          formObject[field] = '';
          const control = form.get(field);
          if (control){
              const messages = this.validationMessages[field];
              for(const key in control.errors){
                  this.formErrors[field].push(messages[key])
              }
          }
      }
  }
}

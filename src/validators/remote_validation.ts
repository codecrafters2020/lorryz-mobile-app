import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs';

@Injectable()

export class RemoteValidation{

    private baseUrl = 'http://mall.virtual-force.org/api/';

    constructor(private http: Http  ){};

    validUsername(fc: FormControl){
        return this.http.get(`${this.baseUrl}validate_username`);
    }

}
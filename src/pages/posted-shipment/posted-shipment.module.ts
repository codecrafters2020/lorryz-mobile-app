import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostedShipmentPage } from './posted-shipment';

@NgModule({
  declarations: [
    PostedShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(PostedShipmentPage),
  ],
})
export class PostedShipmentPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { PostedShipmentDetailPage } from '../pages';
import { Shipment, GlobalVars, Loader, CountryProvider} from '../../providers/providers';
import { interval } from 'rxjs/observable/interval';
//import { interval } from 'rxjs/observable/interval';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-posted-shipment',
  templateUrl: 'posted-shipment.html',
})
export class PostedShipmentPage {
  public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  public timeInterval = 10000;
  public subscriber: any;
  pickupnow;
  letsgo;
  vehiclevalue;
  position;
  src;
  vehicles;
  vehicletype;
  server;
  interval:any
  reload: any;
  bids: any;
  shipmentPickuptime: any;
  str: any;
  headers: any;
  baseurl: string;
  result: any;
  sentemail: any;
  resultstorage: any;
  constructor(
    public storage: Storage,
    public http: HttpClient,
    public navCtrl: NavController, public navParams: NavParams,
    public shipmentProvider: Shipment,
    private viewCtrl: ViewController,
    public loader: Loader,
    public alertCtrl: AlertController,
   public countryProvider: CountryProvider,
   public globalVar: GlobalVars) {
    if(this.globalVar.current_user.company_id == undefined)
    {
      this.navCtrl.setRoot("LoginPage")
    }
    this.bids=[];
    this.result=[];
    this.sentemail=[];
    this.resultstorage=[];
    this.headers=[];
    this.shipments = [];
    this.pickupnow = this.navParams.get("pickupnow");
    this.letsgo = this.navParams.get("letsgo");
    this.vehiclevalue = this.navParams.get("vehiclevalue");
    this.position = this.navParams.get("position");
    console.log(this.navParams.get("position"));
    console.log(this.navParams.get("pickupnow"));
    this.server = this.globalVar.server_link;
    this.interval=" "
    this.reload = this.navParams.get("reload");
    if(this.reload)
    {
       this.viewCtrl.dismiss("cancel")
      this.navCtrl.setRoot(PostedShipmentPage)
    }
    this.baseurl = globalVar.server_link + "/api/v1/email/send_shipment_email.json";
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.page = 1
   let self =this;
    this.fetchShipments()

  //  this.countryProvider
  //     .vehicles(this.globalVar.current_user.country_id)
  //     .subscribe(
  //       data => {
  //         this.vehicles = data;
  //         console.log(data);
  //       },
  //       err => {}
  //     );

      this.interval = setInterval(function () {

        self.automaticRefresh();

      }, 10000);
    }

    automaticRefresh()
    {
      this.page = 1
      this.fetchShipments()
    }


    doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.posted(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();
      console.log(data);

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
//        this.callEmail();
      }



      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });
  }
  delay() {
    return new Promise(resolve => setTimeout(resolve, 300));
  }

  async callEmail()
  {
    debugger;
    for(var i=0;i<this.shipments.length;i++)
    {
      debugger;
      await this.email_function(this.shipments[i]);
    }

  }
  async email_function(shipment)
  {
    debugger;
    var bids = shipment.bids
    var currDate = moment.utc().format('YYYY-MM-DD');
    await this.delay();

    if(currDate == shipment.pickup_date){
      this.shipmentPickuptime = shipment.pickup_time;
    this.str = this.shipmentPickuptime.split('T');
    console.log("pickup time "+this.str[1]);
    var h = this.str[1].split(':')
    var h1 = h[0];
    h1 = h1*3600;
    h1=parseInt(h1);
    var min = h[1];
    min = min*60;
    min=parseInt(min);
    var sec = h[2]
    sec=parseInt(sec);
    var shipmenttime = h1+min+sec-60;
    console.log("shipment pickuptime "+shipmenttime);
    var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
    console.log("local date "+date)
    var local = moment(date).format(' HH:mm:ss');
    console.log("local time "+local)
    var localtime = moment.duration(local).asSeconds()
    var time = localtime-shipmenttime;
    debugger;
    if(bids.length>0)
   {
    let self=this;
    if(time>=180){
      self.storage.get("email").then(result=>{
        debugger;
        this.resultstorage=result
        console.log(result)
       if(result!=null)
       {
        for(var i=0;i<result.length;i++)
        {
          if(result[i].shipmentid!=undefined)
          {
            if(result[i].shipmentid==shipment.id){
              if(result[i].email=="sent")
              {
                console.log("email sent")
              }
              else{
                debugger;
                self.send_email_no_bids_accepted(shipment.id);
              }
            }
            else{
              debugger;
              self.send_email_no_bids_accepted(shipment.id);
            }
          }
        }
       }
       else
          {
            debugger;
            self.send_email_no_bids_accepted(shipment.id);
          }



        });

    }
   }
   else
   {
     let self=this;
     console.log(time);
    if(time>=90){
      self.storage.get("email").then(result=>{
        this.resultstorage=result
        debugger;
        console.log(result)
        this.result=result;
       if(result!=null)
       {
        for(var z=0;z<this.result.length;z++)
        {
          if(result[z].shipmentid!=undefined)
          {
            debugger;

            if(result[z].shipmentid==shipment.id){
              if(result[z].email=="sent")
              {
                console.log("email sent")
              }
              else{
                self.send_email(shipment.id);
              }
            }
            else{
              debugger;
              self.send_email(shipment.id);
            }

          }
        }
       }
       else
          {
            debugger;
            self.send_email(shipment.id);
          }
        });
    }
   }
  }

}
async send_email_no_bids_accepted(id)
{
await this.delay();
debugger;
if(this.resultstorage)
  {
      for(var z=0;z<this.resultstorage.length;z++)
      {
        var loca = {email:this.resultstorage[z].email,
          shipmentid:this.resultstorage[z].shipmentid};
          this.sentemail.push(loca);
      }
  }

  this.setheaders();
  debugger;

  var localemail={email:"sent",shipmentid:id};
  this.sentemail.push(localemail);
  this.http.post(this.baseurl,{shipment_id:id,shipment_status:"no_bids_accepted"}
  ,{headers:this.headers}).subscribe((data: any) =>{
      console.log(data);
     this.storage.set("email",this.sentemail);
    });
}
async send_email(id)
{
  debugger;
  await this.delay();
  if(this.resultstorage)
  {
      for(var z=0;z<this.resultstorage.length;z++)
      {
        var loca = {email:this.resultstorage[z].email,
          shipmentid:this.resultstorage[z].shipmentid};
          this.sentemail.push(loca);
      }
  }


  this.setheaders();
  var localemail={email:"sent",shipmentid:id};
  this.sentemail.push(localemail);
  debugger;

  this.http.post(this.baseurl,{shipment_id:id,shipment_status:"no_bids"}
  ,{headers:this.headers}).subscribe((data: any) =>{
      console.log(data);
      this.storage.set("email",this.sentemail);
    });
}
setheaders()
  {

  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"

  });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }


  postedShipmentDetail(shipment_id){
    this.navCtrl.push("PostedShipmentDetailPage",{shipment_id: shipment_id,src:this.src});
}

  editShipment(shipment_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: 'All currently received bids will be cancelled',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {
          this.navCtrl.push("EditShipmentPage",{shipment_id: shipment_id, pickupnow:this.pickupnow,
          letsgo:this.letsgo, vehiclevalue:this.vehiclevalue, position:this.position});

        }
      }
    ]
    });
    alert.present();
  }

  openPosted(){
    this.navCtrl.setRoot("PostedShipmentPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }
  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  // ionViewDidLeave(){
  //   this.subscriber.unsubscribe();
  // }

  ngOnDestroy() {
    debugger;
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

}

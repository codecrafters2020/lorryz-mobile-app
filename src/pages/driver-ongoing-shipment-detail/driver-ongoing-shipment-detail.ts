import { Component } from "@angular/core";
import { Geolocation } from '@ionic-native/geolocation';

import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ToastController,
  
  Platform
} from "ionic-angular";
import { FleetShipment, GlobalVars } from "../../providers/providers";
import { User, DriverProvider, Loader } from "../../providers/providers";
import {
  Shipment,
  CountryProvider,
  VehicleProvider,
  LocationTracker
} from "../../providers/providers";

import { CallNumber } from "@ionic-native/call-number";

@IonicPage()
@Component({
  selector: "page-driver-ongoing-shipment-detail",
  templateUrl: "driver-ongoing-shipment-detail.html"
})
export class DriverOngoingShipmentDetailPage {
  public shipment: any;
  public vehicle_status: any;
  public current_driver_vehicle: any;
  public status: any;
  public shipments_data: any;
  public proceed: any;
  current_position: any;
  lat: any;
  lng: any;
  public loaded = false;
  public val: any;
  public _update_status: any;
  public shipments: any;
  interval: any;
  page: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public globalVar: GlobalVars,
    public driver: DriverProvider,
    public loader: Loader,
    public alertCtrl: AlertController,
    private callNumber: CallNumber,
    public toastCtrl: ToastController,
    public platform: Platform,
    private geolocation: Geolocation,
    public locationTracker: LocationTracker,
    public vehicle: VehicleProvider,
    public shipmentProvider: Shipment
    
    
  ) {
    this.shipment = {};
    this.shipments = [];
    this._update_status = true;
  }

  ionViewDidLoad() {
    this.shipment = this.navParams.get("shipment") || {};
    this.page = 1;
    this.automaticRefresh();
  }


  
  automaticRefresh() {

    let self = this
    this.interval = setInterval(function () {
     self.track_transport();
   }, 5000);
  }
  

  ionViewDidEnter() {
    this.loader.hide();
    this.loader.show("");
    this.shipmentProvider
      .show(this.globalVar.current_user.company_id, this.shipment.id)
      .subscribe(
        data => {
          this.loader.hide();
          this.shipment = data;
          console.log(this.shipment);
          this.status = 100;
          if (
            this.shipment.assigned_vehicle &&
            this.shipment.vehicle_shipments
          ) {
            if (this.shipment.assigned_vehicle.status == "booked") {
              this.status = 100;
            } else if (this.shipment.assigned_vehicle.status == "start") {
              this.track_transport();
              this.status = 200;
            } else if (this.shipment.assigned_vehicle.status == "loading") {
              this.track_transport();
              this.status = 300;
            } else if (this.shipment.assigned_vehicle.status == "enroute") {
              this.track_transport();
              this.status = 400;
            } else if (this.shipment.assigned_vehicle.status == "unloading") {
              this.track_transport();
              this.status = 500;
            } else if (this.shipment.assigned_vehicle.status == "finish") {
              if (this.platform.is("cordova")) {
                this.locationTracker.stopTracking();
              }
              this.status = 600;
            }
          }
        },
        err => {
          this.loader.hide();
        }
      );
  }

  textChanged(event) {
    console.log(" this is an status", this.status);
    console.log(" this is an status", event);
    if (this.status + 100 == event) {
      this.proceed = true;
    } else if (event - this.status == 100) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
    this.val = this.status;
  }

  changeStatus(event) {
    if (this.proceed) {
      debugger;
      if (this.status == 200) {
        this._update_status = true;
        this.vehicle_status = "start";
      } else if (this.status == 300) {
        debugger;
        this._update_status = true;
        this.vehicle_status = "loading";
      } else if (this.status == 400) {
        debugger;
        this._update_status = true;
        this.vehicle_status = "enroute";
      } else if (this.status == 500) {
        debugger;
        this._update_status = true;
        this.vehicle_status = "unloading";
      } else if (this.status == 600) {
        debugger;
        this._update_status = true;
        this.vehicle_status = "finish";
      } else if (this.status == 100) {
        this.vehicle_status = "booked";
      }
      debugger;
      if (this._update_status) {
        debugger;
        this.update_vehicle_status(event);
      }
    } else {
      debugger;
      this.status = this.val;
      event.setValue(this.status);
      console.log("you can not change state more than once", this.val);
    }
  }



  valid_payment_option() {
    return this.shipment.payment_option == "AfterDelivery";
  }

  update_vehicle_status(event) {
    debugger;
    this.loader.show("");
    console.log("i am in event", this.vehicle_status);
    

    this.driver
      .updateVehicleStatus(
        this.shipment.assigned_vehicle.vehicle_id,
        this.vehicle_status,
        this.shipment.id
      )
      .subscribe(
        data => {
          debugger;
          this.loader.hide();
          console.log(data);
          if (data && data["error"]) {
            let toast = this.toastCtrl.create({
              message: data["error"],
              duration: 6000,
              position: "top",
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
            this.status = this.val;
            console.log("val "+this.val);
            console.log("status "+this.status);
            event.setValue(this.status);
          } else {
            debugger;
            if (
              this.vehicle_status == "loading" ||
              this.vehicle_status == "enroute" ||
              this.vehicle_status == "unloading" ||
              this.vehicle_status == "start"
            ) {
              this.track_transport();
            }
            if (this.vehicle_status == "booked") {
              this.navCtrl.setRoot("DriverUpcomingShipmentListPage");
            } else if (this.vehicle_status == "start") {
              this.navCtrl.setRoot("DriverOngoingShipmentListPage");
            } else if (this.vehicle_status == "finish") {
              debugger;
              if (this.platform.is("cordova")) {
                debugger;
                this.locationTracker.stopTracking();
              } debugger;
              if (data["shipment"]["state"] != "ongoing") {
                if (
                  data["shipment"]["payment_option"] == "after_delivery" ||
                  data["shipment"]["payment_option"] == "before_pick_up"
                ) {
                  let toast = this.toastCtrl.create({
                    message:
                      "Shipment no " +
                      data["shipment"]["id"] +
                      " has been completed. Don't forget to collect " +
                      this.globalVar.current_user.country.currency +
                      " " +
                      data["shipment"]["amount"],
                    duration: 600000,
                    position: "top",
                    showCloseButton: true,
                    closeButtonText: "x",
                    cssClass: "toast-success"
                  });
                  toast.present();
                }
                debugger;
                this.navCtrl.setRoot("DriverCompletedShipmentListPage");
              }
            }
          }
        },
        err => {
          this.loader.hide();
        }
      );
  }

  report() {
    let alert = this.alertCtrl.create({
      title: "Attention!",
      message: "Are you sure? you want to report a problem to fleet owner",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Ok",
          handler: () => {
            this.loader.show("");

            this.driver.report(this.shipment.id).subscribe(
              data => {
                this.loader.hide();
              },
              err => {
                this.loader.hide();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  callToFleetOwner(mobile) {
    this.callNumber
      .callNumber(mobile, true)
      .then(res => console.log("Launched dialer!", res))
      .catch(err => console.log("Error launching dialer", err));
  }

  openMap() {
    this.navCtrl.push("MapLocationPage", { shipment: this.shipment });
  }

  showNotification() {
    this.navCtrl.push("NotificationPage");
  }

  track_transport() {
    if (this.platform.is("cordova")) {
      this.locationTracker.stopTracking();
      // this.locationTracker.startTracking(
      //   this.shipment.assigned_vehicle.vehicle_id
      // );

      
      this.geolocation.getCurrentPosition().then((position) => {
                      this.current_position = position;
                     this.lat = this.current_position.coords.latitude
                     this.lng = this.current_position.coords.longitude
                          let vehicle = {id: this.shipment.assigned_vehicle.vehicle_id,
                            latitude: this.lat,longitude: this.lng}
        
                          this.vehicle.update_lat_lng(vehicle, this.shipment.assigned_vehicle.vehicle_id)
                          .subscribe(data => {
                             console.log(data)
                          });
        
                      });
    }
  }
  
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
}

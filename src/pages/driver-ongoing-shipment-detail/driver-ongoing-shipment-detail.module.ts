import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverOngoingShipmentDetailPage } from './driver-ongoing-shipment-detail';

@NgModule({
  declarations: [
    DriverOngoingShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverOngoingShipmentDetailPage),
  ],
})
export class DriverOngoingShipmentDetailPageModule {}

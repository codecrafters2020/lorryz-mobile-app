import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentVehicleSelectPage } from './new-shipment-vehicle-select';

@NgModule({
  declarations: [
    NewShipmentVehicleSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(NewShipmentVehicleSelectPage),
  ],
})
export class NewShipmentVehicleSelectPageModule {}

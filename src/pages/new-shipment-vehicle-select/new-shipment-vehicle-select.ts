import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { GlobalVars } from '../../providers/providers';
declare var google;

@IonicPage()
@Component({
  selector: 'page-new-shipment-vehicle-select',
  templateUrl: 'new-shipment-vehicle-select.html',
})

export class NewShipmentVehicleSelectPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  shipment:any;
  modalreq: boolean;
  position: any;
  id: any;
  vehiclename: any;
  vehiclevalue=false;
  src: any;
  pickupnow: any;
  letsgo: any;
  unformated_pickup_time: any;
  input: any;
  sum: any;
  pakistan: any;
  subvehiclevalue=false;
  secondarysrc: any;
  secondaryid: any;
  secondaryvehiclename: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public globalVar: GlobalVars,
    private viewCtrl: ViewController,
    ) {
    this.shipment={};
    this.position = this.navParams.get("position");
    this.id = this.navParams.get("id");
    this.vehiclename = this.navParams.get("vehicle_name");
    this.vehiclevalue = this.navParams.get("value");
    this.src = this.navParams.get("src");
    this.pickupnow = this.navParams.get('pickup_now');
    this.letsgo = this.navParams.get('lets_go');
    this.unformated_pickup_time = this.navParams.get('time');
    this.shipment.no_of_vehicles = 1;
    this.input = this.navParams.get('cargodesc');
    this.modalreq = false;
    this.sum=this.navParams.get('sum')
    this.subvehiclevalue=this.navParams.get('subcategory');
    this.secondarysrc=this.navParams.get("secondarysrc");
    this.secondaryid=this.navParams.get("secondaryid");
    this.secondaryvehiclename=this.navParams.get("secondaryvehiclename");
  }

  ionViewDidLoad() {
    this.pakistan = this.globalVar.current_user.country.name;
    console.log('ionViewDidLoad NewShipmentVehicleSelectPage');
    this.shipment=this.navParams.get("shipment");
    this.load_map()
    this.shipment.no_of_vehicles = 1;
  }
  load_map(){
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();


    let source = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

    let mapOptions = {
      center: source,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }
    let self = this;

    directionsDisplay = new google.maps.DirectionsRenderer();

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(destination)
    this.addMarker(source)
    directionsDisplay.setMap(self.map);
    directionsDisplay.setOptions( { suppressMarkers: true } );

    var request = {
      origin: source,
      destination: destination,
      travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(self.map);
      } else {
        console.log("Directions Request failed: " + status);
      }
    });
  }
  addMarker(latlong){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latlong
    });
  }
openModal(category) {
  this.modalreq = true;
  debugger
  this.navCtrl.push('PagesVehicleTypePage', {pickupnow:this.pickupnow,letsgo:this.letsgo,
     position:this.position,shipment:this.shipment, vehcilevalue:this.vehiclevalue,edit:false,
       unformated_time:this.unformated_pickup_time,cargodesc:this.input,no_vehicles:this.shipment.no_of_vehicles,
       sum:this.sum,category:category,primaryvehicleid:this.id,vehicelvalue:this.vehiclevalue,vehiclename:this.vehiclename,
        primarysrc:this.src
  });
//   addModal.onDidDismiss(item => {
//     // this.navCtrl.setRoot('NewShipmentRequestPage');
//     // const startIndex = this.navCtrl.getActive().index - 2;
//     // this.navCtrl.remove(startIndex, 2);
// //    this.viewCtrl.dismiss();

//   })
//   addModal.present();

}
disabled()
{
    if(this.src && this.secondarysrc)
    {
      return false
    }
    else{
      return true;
    }
}
nextpage()
{
  debugger
    this.navCtrl.push("NewShipmentRequestPage",{  shipment:this.shipment,id:this.secondaryid,position:this.position,
    pickup_now:this.pickupnow,lets_go:this.letsgo, vehicle_name:this.secondaryvehiclename,src:this.secondarysrc,
     no_vehicles:this.shipment.no_of_vehicles,sum:this.sum})
}
}

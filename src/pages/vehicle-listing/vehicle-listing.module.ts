import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleListingPage } from './vehicle-listing';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    VehicleListingPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleListingPage),
    TranslateModule.forChild()
  ],
})
export class VehicleListingPageModule {}

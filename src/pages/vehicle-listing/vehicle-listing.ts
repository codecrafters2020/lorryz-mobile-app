import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController,NavParams,ModalController,ItemSliding,Item } from 'ionic-angular';

import { VehicleListing,DriverListing} from '../pages'
import { CallNumber } from '@ionic-native/call-number';
import { User,VehicleProvider, GlobalVars, Loader,DriverProvider} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-vehicle-listing',
  templateUrl: 'vehicle-listing.html',
})
export class VehicleListingPage {
  public vehicles: any;
  public _vehicle: any;
  public ascending= true;

  public status: '';
  public from_pickup_date:'';
  public to_pickup_date:'';
  public toggle = true;
  public load = true;
  activeItemSliding: ItemSliding = null;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public vehicle: VehicleProvider,
              public toastCtrl: ToastController,public loader: Loader,
              public modalCtrl: ModalController,
              private callNumber: CallNumber,
              public globalVar: GlobalVars
                            ) {
    this.vehicles = []
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.vehicle.getVehicles().subscribe((resp) => {
      this.loader.hide();
      this.vehicles= resp;


    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  loadVechile(){
  	this.navCtrl.setRoot(VehicleListing);
  }
  loadDriver(){
  	this.navCtrl.setRoot(DriverListing);
  }
  addNewVehicle(){
   this.navCtrl.push('FleetAddVehiclePage');
  }
  editVehicle(vehicle){
   this.navCtrl.push('FleetEditVehiclePage',{vehicle_id: vehicle.id});
  }

  changeDriver(vehicle) {

    let addModal = this.modalCtrl.create('FleetChangeDriverModalPage',{vehicle: vehicle});
    addModal.onDidDismiss(item => {
      this.navCtrl.setRoot('FleetVehicleDriverListingPage');
    })
    addModal.present();
  }
  deleteVehicle(id) {

    this.loader.show("");
    this.vehicle.deleteVehicle(id).subscribe((resp) => {
      this.loader.hide();
      this.navCtrl.setRoot('FleetVehicleDriverListingPage');


    }, (err) => {
      this.loader.hide();
    });
  }

  sortBy(){

       this.ascending=this.ascending ? false : true;


      this.ascending ?
        this.vehicles.sort((a, b) => a.vehicle_type.name !== b.vehicle_type.name ? a.vehicle_type.name < b.vehicle_type.name ? -1 : 1 : 0)
        :
        this.vehicles.sort((a, b) => a.vehicle_type.name !== b.vehicle_type.name ? a.vehicle_type.name < b.vehicle_type.name ? 1 : -1 : 0)
  }

  filter(){
    this.loader.show("");

    let filter = {state: this.status,not_available_from: this.from_pickup_date ? this.from_pickup_date : '' , not_available_to: this.to_pickup_date ? this.to_pickup_date : '' ,company_id: this.globalVar.current_user.company_id };
    this.vehicle.getVehiclesByFilter(filter).subscribe((resp) => {
      this.loader.hide();
      this.vehicles = resp
    }, (err) => {
      this.loader.hide();
    });
  }
  showHide(){

    if (this.toggle) {
      this.load = false;
      this.toggle = false

    }else{
      this.toggle = true
      this.load = false;
    }
  }

  clear(){
    this.status='';
    this.from_pickup_date='';
    this.to_pickup_date='';
  }

  openOption(itemSlide: ItemSliding, item: Item) {
    console.log('opening item slide..');

    if(this.activeItemSliding!==null) //use this if only one active sliding item allowed
     this.closeOption();

    this.activeItemSliding = itemSlide;

    let swipeAmount = 240; //set your required swipe amount
    itemSlide.startSliding(swipeAmount);
    itemSlide.moveSliding(swipeAmount);

    itemSlide.setElementClass('active-options-right', true);
    itemSlide.setElementClass('active-swipe-right', true);

    item.setElementStyle('transition', null);
    item.setElementStyle('transform', 'translate3d(-'+swipeAmount+'px, 0px, 0px)');
   }

   closeOption() {
    console.log('closing item slide..');

    if(this.activeItemSliding) {
     this.activeItemSliding.close();
     this.activeItemSliding = null;
    }
   }
}

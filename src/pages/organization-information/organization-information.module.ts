import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationInformationPage } from './organization-information';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    OrganizationInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizationInformationPage),
    TranslateModule.forChild()
  ],
})
export class OrganizationInformationPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { CompanyInformationPage,IndividualInformationPage } from '../pages';
import { User,GlobalVars, Loader } from '../../providers/providers';
/**
 * Generated class for the OrganizationInformationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-organization-information',
  templateUrl: 'organization-information.html',
})
export class OrganizationInformationPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public user: User,
              public loader: Loader,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganizationInformationPage');
    debugger
    if(this.navParams.get('fleet'))
    {
        this.registerAsIndividual();
    }
  }


  landToIndividualProfile(){

  }

  landToCompanyProfile(){

  }

  registerAsIndividual(){
     this.loader.show("");
     this.user.registerAsIndividual().subscribe((resp) => {
       this.loader.hide();
          // let msg = resp["success"] ? resp["success"] : resp["error"]

          // let toast = this.toastCtrl.create({
          //   message: msg,
          //   duration: 6000,
          //   position: 'top'
          // });
          // toast.present();

          this.navCtrl.push(IndividualInformationPage);
     }, (err) => {
       this.loader.hide();
       let toast = this.toastCtrl.create({
         message: "Some Went Wrong. Please Try Later",
         duration: 6000,
         position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
       });
       toast.present();
     });
  }

  registerAsCompany(){
    this.loader.show("");
    this.user.registerAsCompany().subscribe((resp) => {
      this.loader.hide();
         let msg = resp["success"] ? "" : resp["error"]

         if (resp["error"]) {
           let toast = this.toastCtrl.create({
             message: msg,
             duration: 6000,
             position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
           });
           toast.present();
         }


         this.navCtrl.push(CompanyInformationPage);

    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, Platform, Slides } from 'ionic-angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
declare var google;

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;
  map: any;
  current_position: any;
  latLng: any;
  count;
  country_name;
  constructor(public navCtrl: NavController,
    public platform: Platform,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    ) {}

  ngAfterViewInit()
  {
    console.log("a")
     this.initialize_autocomplete()
  }


     initialize_autocomplete() {
       if (this.platform.is('cordova')) {
         this.diagnostic.isLocationEnabled().then((enabled) => {
           if (enabled) {
             this.askForHighAccuracy();
           } else {
             this.locationAccuracy.canRequest().then((canRequest: boolean) => {
               this.askForHighAccuracy();
             });
           }
         })
       } else {
         this.loadMap();
       }
     }


     askForHighAccuracy() {
       this.locationAccuracy
         .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
           this.loadMap();
         });
     }

     loadMap() {

      if (this.map)
      {
        return this.map;
      }

      let self = this;
      this.geolocation.getCurrentPosition().then((position) => {

        this.current_position = position;
        // console.log(this.current_position);
        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var geocoder = new google.maps.Geocoder;
        const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({ 'location': this.latLng }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {

            if (results[1]) {
              var indice=0;
              for (var j=0; j<results.length; j++)
              {
                  if (results[j].types[0]=='locality')
                      {
                        indice=j;
                          break;
                      }
                  }

              for (var i=0; i<results[j].address_components.length; i++)
                  {

                      if (results[j].address_components[i].types[0] == "country") {
                              //this is the object you are looking for
                            this.count = results[j].address_components[i];
                            console.log(this.count.long_name)
                            this.country_name = this.count.long_name
                            if(this.country_name=="Pakistan")
                            {
           document.getElementById("country_name_pak").innerText="For any assistance, Please call us at +92 337 7131317 Or send us email us at contact@lorryz.com"
                            }
                            else{
           document.getElementById("country_name_pak").innerText="For any assistance, Please call us at +971 56 5466088 Or send us email us at contact@lorryz.com"

                            }

                          }




                        }

          } else {
         //   window.alert('Geocoder failed due to: ' + status);
          }}
        });


        });


      }

  login() {
    this.navCtrl.push('LoginPage');
  }

  signup() {
    this.navCtrl.push('SignupPage');
  }
}

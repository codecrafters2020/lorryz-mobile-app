import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
declare var require: any;
@IonicPage()
@Component({
  selector: 'page-fleet-completed-shipment-listing',
  templateUrl: 'fleet-completed-shipment-listing.html',
})
export class FleetCompletedShipmentListingPage {
  public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  apiKey: string;
  options = {

   };
  ongoing: any;
  id: any;
  company_category: string;
  company_type: string;
  pickupnow: boolean;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public globalVar: GlobalVars,) {
      this.company_type = this.globalVar.current_user.company.company_type;
      this.company_category = this.globalVar.current_user.company.category;

      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }
    this.shipments = [];
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };
    if((this.company_type=='individual')&&(this.company_category=='fleet'))
      {
        this.pickupnow=true;
      }

  }

  ionViewDidLoad() {
    this.ongoing = this.navParams.get("ongoing")
    this.id = this.navParams.get("id")
    this.loader.show("");
    this.page = 1
    this.fetchShipments()
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.completed(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
        let self=this
        debugger;
        if(self.pickupnow)
        {
          if(self.ongoing)
          {
            for(var i=0; i<self.shipments.length;i++)
            {
              if(self.shipments[i].id==self.id)
              {
                self.navCtrl.push("FleetCompletedShipmentDetailPage",{shipment: self.shipments[i]});
                break;
              }
            }

        }

        }
    }
      const self = this;

    if(this.globalVar.lang==true){
      for(let x=0;x<this.shipments.length;x++){
        var googleTranslate = require('google-translate')(this.apiKey, this.options);
        debugger;;
        googleTranslate.translate( self.shipments[x].drop_location, 'ur', function(err, translation) {
          self.shipments[x].drop_location = translation.translatedText; });
        googleTranslate.translate(self.shipments[x].pickup_location, 'ur', function(err, translation) {
          self.shipments[x].pickup_location = translation.translatedText; });
      console.log(this.shipments[x]);
      }
  }

      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompletedDetail(shipment){
    this.navCtrl.push("FleetCompletedShipmentDetailPage",{shipment: shipment});
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }


}

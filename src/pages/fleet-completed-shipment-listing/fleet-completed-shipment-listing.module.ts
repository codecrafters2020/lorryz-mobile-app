import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetCompletedShipmentListingPage } from './fleet-completed-shipment-listing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetCompletedShipmentListingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetCompletedShipmentListingPage),
    TranslateModule.forChild()
  ],
})
export class FleetCompletedShipmentListingPageModule {}

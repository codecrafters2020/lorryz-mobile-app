import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetOngoingShipmentDetailPage } from './fleet-ongoing-shipment-detail';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetOngoingShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetOngoingShipmentDetailPage),
    TranslateModule.forChild()
  ],
})
export class FleetOngoingShipmentDetailPageModule {}

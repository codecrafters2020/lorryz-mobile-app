import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController, Platform  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { FleetShipment, GlobalVars, Loader, DriverProvider, LocationTracker, VehicleProvider} from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
import { interval } from 'rxjs/observable/interval';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
declare var require: any;

declare var google;
var directionsService = new google.maps.DirectionsService();
var directionsRenderer = new google.maps.DirectionsRenderer();

@IonicPage()
@Component({
  selector: 'page-fleet-ongoing-shipment-detail',
  templateUrl: 'fleet-ongoing-shipment-detail.html',
})
export class FleetOngoingShipmentDetailPage {
  @ViewChild('map') mapElement: ElementRef;
  shipment: any;
  map: any;
  vehicles :any;
  markers :any;
  interval: any;
  infoWindows: any;
  companycategory: any;
  companytype: any;
  pickupnow: boolean;
  public vehicle_status: any;
  public status: any;
  proceed: any;
  val: any;
  _update_status: any;
  count: number;
  x: number;
  current_position: any;
  lat: any;
  lng: any;
  public timeInterval = 10000;
  public subscriber: any;
  map_line: any;
  ship: any;
  zoom_map: any;
  apiKey: string;
  options = {

   };
  status_code: string;
  headers: any;
  baseurl: any;
  showAmount: boolean=false;
  interval_2: number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public driver: DriverProvider,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private callNumber: CallNumber,
    public locationTracker: LocationTracker,
    public platform: Platform,
    private geolocation: Geolocation,
    public globalVar: GlobalVars,
    public vehicle: VehicleProvider,
    private locationAccuracy: LocationAccuracy,
    private diagnostic: Diagnostic,
    public http: HttpClient,
    private sanitizer: DomSanitizer
    ) {
      this.headers=[];
   this.baseurl = globalVar.server_link + "/api/v1/email/send_shipment_email.json";
    this.shipment = {};
    this.vehicles = [];
    this.markers = [];
    this.interval = "";
    this.infoWindows = [];
    this._update_status = true;
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };

 //   this.zoom_map = 25;
  }

  ionViewDidLoad() {
    this.companytype = this.globalVar.current_user.company.company_type;
    this.companycategory = this.globalVar.current_user.company.category;
    if(this.globalVar.location_initiazed_fleet)
    {
      this.locationTracker.stopTracking();
   //   clearInterval();
      this.globalVar.location_initiazed_fleet=false;
    }
    if(this.shipment.bids)
    {
      this.showAmount=true;
    }
    else
    {
      this.showAmount=false;
    }
    if((this.companytype=='individual')&&(this.companycategory=='fleet'))
    {
      this.pickupnow=true;
    //   var mapOptions = {
    //     mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     mapTypeControl: false,
    //     zoomControl: true,
    //     streetViewControl: false,
    //     zoom: 20,
    //     timeout:30000,
    //     maximumAge: 3000,
    //     enableHighAccuracy: true,
    //   }
    //   this.map_line = new google.maps.Map(document.getElementById('map'),mapOptions)

    //  directionsRenderer.setMap(this.map_line);
      // directionsRenderer.setPanel(document.getElementById('directionsPanel'));
      }
    this.ship = this.navParams.get('ship')
    let self = this;
    if(this.ship)
    { var shipmentid = this.navParams.get('shipment_id')
      var vehicleid = this.navParams.get('vehcileid')
      this.vehicle_status = "start";
      this.setheaders();

      this.http.post(this.baseurl,{shipment_id:shipmentid,shipment_status:"started"}
      ,{headers:this.headers}).subscribe((data: any) =>{
          console.log(data);
          this.globalVar.rebid=false;
        });

      this.driver
      .updateVehicleStatus(
        vehicleid,
        this.vehicle_status,
        shipmentid
        ).subscribe(data=>{
         console.log(data);
          this.load_shipment();
          this.loadVehicles();

        })
     }
     else
     {


    this.load_shipment();
    this.loadVehicles();

  }
  var killId = setTimeout(function() {
    for (var i = killId; i > 0; i--) clearInterval(i)
  }, 3000);
    this.interval = setInterval(function () {
      console.log("a")
      if(!(self.pickupnow))
     {
      self.load_shipment();
      self.loadVehicles();
     }
      if(self.pickupnow){
        if(self.shipment != undefined){
          debugger
          self.locationTracker.foregroundGeolocation(
//            self.shipment.vehicle_shipments[0].vehicle_id
//      self.shipment.vehicles[0].id
              self.globalVar.current_user.vehicle.id
);}
      }

    }, 60000);
    this.interval_2 = setInterval(function () {
      console.log("interval_2")
      self.load_shipment();
      self.loadVehicles();

    }, 10000);
    this.locationTracker.foregroundGeolocation(self.globalVar.current_user.vehicle.id);
  }




  ngOnDestroy() {
    if (this.interval_2) {
      clearInterval(this.interval_2);
    }
    // this.locationTracker.stopTracking();


  }
  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
}
  openPodPage()
  {
    this.navCtrl.push('PdfUploadPage',{pdfdoc:this.shipment['documents'],shipment:this.shipment,transitionfrom:'ongoing'});
  }
  load_shipment(){
    // this.loader.show("");
    this.shipmentProvider.show(this.globalVar.current_user.company_id, this.navParams.get('shipment_id'))
    .subscribe(data => {
      const self  = this;
      this.shipment = data;
      if(this.globalVar.lang==true){
        var googleTranslate = require('google-translate')(this.apiKey, this.options);
        googleTranslate.translate(self.shipment.drop_location, 'ur', function(err, translation) {
          self.shipment.drop_location = translation.translatedText; });
        googleTranslate.translate(self.shipment.pickup_location, 'ur', function(err, translation) {
          self.shipment.pickup_location = translation.translatedText; });
      console.log(this.shipment);
    }
      if (
        this.shipment.vehicles &&
        this.shipment.vehicle_shipments
      ) {
          for(var i=0;i<this.shipment.vehicles.length;i++)
            {
              if(this.shipment.vehicles[i].id==this.globalVar.current_user.vehicle.id)
                {

              if (this.shipment.vehicles[i].status == "booked") {
                this.status = 100;
              } else if (this.shipment.vehicles[i].status == "start") {
                this.status = 200;
                this.track_transport("start");
                this.vehicle_status = "start";
              } else if (this.shipment.vehicles[i].status == "loading") {
                this.status = 300;
                this.track_transport("loading");
                this.vehicle_status = "loading";
              } else if (this.shipment.vehicles[i].status == "enroute") {
                this.status = 400;
                this.vehicle_status = "enroute";
                this.track_transport("enroute");
              } else if (this.shipment.vehicles[i].status == "unloading") {
                this.status = 500;
                this.vehicle_status = "unloading";
                this.track_transport("unloading");
              } else if (this.shipment.vehicles[i].status== "finish") {
                this.vehicle_status = "finish";
                this.status = 600;
              }

                }
             }
          }
    },(err) => {
      // this.loader.hide();
    });


  }

  send_email()
  {
    this.setheaders();

    this.http.post(this.baseurl,{shipment_id:this.shipment.id,shipment_status:"no_bids"}
    ,{headers:this.headers}).subscribe((data: any) =>{
        console.log(data);
      });
  }
  setheaders()
  {

  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"

  });

  }

  loadVehicles(){
    if(this.shipment.state=='completed')
    {
      this.locationTracker.stopTracking();
      if(this.pickupnow)
      {
        this.navCtrl.setRoot("FleetCompletedShipmentListingPage",{ongoing:true,id:this.shipment.id});      }
      else
      {
        this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
      }
    }
    this.shipmentProvider.vehicles(this.globalVar.current_user.company_id, this.navParams.get('shipment_id'))
    .subscribe(data1 => {
      console.log(data1)
      this.vehicles = data1;
      this.load_map();
    },(err) => {
    });

  }

  load_map(){

    if(!(this.pickupnow))
    {
      let latLng = ""
      if (this.vehicles.length > 0){
        latLng = new google.maps.LatLng(this.vehicles[0]["latitude"], this.vehicles[0]["longitude"]);
      }
      else{
        latLng = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
      }
      let mapOptions = {
        center: latLng,
         zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false
      }
      if (!this.map) {
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      }
      this.loadMarkers();

    }
    else{
        // if (this.platform.is('cordova')) {
        //   this.diagnostic.isLocationEnabled().then((enabled) => {
        //     if (enabled) {
        //       this.askForHighAccuracy();
        //     } else {
        //       this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        //         this.askForHighAccuracy();
        //       });
        //     }
        //   })
        // } else {
        //   this.currentline();
        // }

    }


  }


  askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
        this.currentline();
      });
  }

  openGoogleMap()
  {
    let destination
    if (this.status==200)
     destination = this.shipment.pickup_lat + ',' + this.shipment.pickup_lng;
    else
    destination = this.shipment.drop_lat + ',' + this.shipment.drop_lng;


      if(this.platform.is('ios')){
        window.open('maps://?q=' + destination, '_system');
      } else {
        let label = encodeURI('My Label');
        window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
      }
  }
  currentline()
  {
  //  debugger;

    // this.geolocation.getCurrentPosition().then((position) => {
    // //  debugger;

    // this.current_position = position;
      var oceanBeach ;
    //   this.lat = this.current_position.coords.latitude
    //   this.lng = this.current_position.coords.longitude
    //   this.map_line.setCenter({lat:this.lat,lng:this.lng})
      //  var directionsService = new google.maps.DirectionsService();
    //  var directionsRenderer = new google.maps.DirectionsRenderer();

    var haight = new google.maps.LatLng(this.vehicles[0]["latitude"], this.vehicles[0]["longitude"]);
     const self=this;

     if (this.status==200)
     oceanBeach = new google.maps.LatLng(self.shipment.pickup_lat, self.shipment.pickup_lng);
     else
     oceanBeach = new google.maps.LatLng(self.shipment.drop_lat, self.shipment.drop_lng);
  //  //  debugger;
  //     console.log(self.map_line.getZoom());

  //     console.log(self.map_line.getZoom());
  //   // debugger;
  //    google.maps.event.addListener(self.map_line,"zoom_changed", function() {
  //      if(self.map_line != undefined){
  //    ///   debugger;

  //        self.zoom_map = self.map_line.getZoom()
  //     }
  //    });
      this.loadMarkers();
       directionsService.route(
           {
             origin:haight,
             destination: oceanBeach,
             travelMode: 'DRIVING',
             optimizeWaypoints: true
           },
           function(response, status) {
             if (status == 'OK') {
          //    debugger;
               directionsRenderer.setDirections(response);
             } else {
  //             window.alert('Directions request failed due to ' + status);
             }
           })

      // });

  }



  textChanged(event) {
    console.log(" this is an status", this.status);
    console.log(" this is an status", event);
    if (this.status + 100 == event) {
      this.proceed = true;
    } else if (event - this.status == 100) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
    this.val = this.status;
  }

  changeStatus(event,id) {
    if (this.proceed) {
      if (this.status == 200) {
        this._update_status = true;
        this.vehicle_status = "start";
      } else if (this.status == 300) {
        this._update_status = true;
        this.vehicle_status = "loading";
      } else if (this.status == 400) {
        this._update_status = true;
        this.vehicle_status = "enroute";
      } else if (this.status == 500) {
        this._update_status = true;
        this.vehicle_status = "unloading";
      } else if (this.status == 600) {
        this._update_status = true;
        this.vehicle_status = "finish";
      } else if (this.status == 100) {
        this.vehicle_status = "booked";
      }

      if (this._update_status) {
        this.update_vehicle_status(event,id);
      }
    } else {
      this.status = this.val;
      event.setValue(this.status);
      console.log("you can not change state more than once", this.val);
    }
  }

  update_vehicle_status(event,id) {
    this.loader.show("");
    console.log("i am in event", this.vehicle_status);
    this.driver
      .updateVehicleStatus(
        id,
      //  this.shipment.vehicle_shipments[0].vehicle_id,
        this.vehicle_status,
        this.shipment.id
      )
      .subscribe(
        data => {
          this.loader.hide();
          console.log(data);
          if (data && data["error"]) {
            let toast = this.toastCtrl.create({
              message: data["error"],
              duration: 6000,
              position: "top",
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
            this.status = this.val;
            event.setValue(this.val);
          } else {
            if (
              this.vehicle_status == "loading" ||
              this.vehicle_status == "enroute" ||
              this.vehicle_status == "unloading" ||
              this.vehicle_status == "start"
            ) {
             // debugger;
              this.track_transport(this.vehicle_status);
            }
              if (this.vehicle_status == "finish") {
              if (this.platform.is("cordova")) {
                this.locationTracker.stopTracking();
             }
              if (data["shipment"]["state"] != "ongoing") {
                if (
                  data["shipment"]["payment_option"] == "after_delivery" ||
                  data["shipment"]["payment_option"] == "before_pick_up"
                ) {
                  let toast = this.toastCtrl.create({
                    message:
                      "Shipment no " +
                      data["shipment"]["id"] +
                      " has been completed. Don't forget to collect " +
                      this.globalVar.current_user.country.currency +
                      " " +
                      data["shipment"]["amount"],
                    duration: 600000,
                    position: "top",
                    showCloseButton: true,
                    closeButtonText: "x",
                    cssClass: "toast-success"
                  });
                  toast.present();
                }
                if(this.pickupnow)
                {
                  var killId = setTimeout(function() {
                    for (var i = killId; i > 0; i--) clearInterval(i)
                  }, 3000);
           this.locationTracker.stopTracking();
           this.navCtrl.setRoot("FleetCompletedShipmentListingPage",{ongoing:true,id:this.shipment.id});
                  }
                  else
                  {
                    this.navCtrl.setRoot("DriverCompletedShipmentListingPage");
                  }
              }
            }
          }
        },
        err => {
          this.loader.hide();
        }
      );
  }

  track_transport(vehicle_status) {

    console.log(this.shipment.vehicle_shipments[0].vehicle_id )
    if (this.platform.is("cordova")) {

       console.log()
    //  this.locationTracker.foregroundGeolocation(
    //     this.shipment.vehicle_shipments[0].vehicle_id
    //   );
    }
    // let status;
    switch (vehicle_status) {
      case "start":
        this.status_code =  '(On its way to Cargo Pick Up Location)'
        break;
      case "loading":
        this.status_code  =  '(Cargo Loading in progress)'
        break;
      case "enroute":
        this.status_code  =  '(On its way to cargo Drop-Off location)'
        break;
      case "unloading":
        this.status_code  =  '(Cargo Unloading in progress)'
        break;
    }
    // if(this.ship)
    // {
    //   this.lat = this.current_position.coords.latitude;
    //   this.lng = this.current_position.coords.longitude;
    //   let vehicle = {id: this.shipment.vehicle_shipments[0].vehicle_id, latitude: this.lat, longitude: this.lng}
    //   this.vehicle.update_lat_lng(vehicle, this.shipment.vehicle_shipments[0].vehicle_id)
    //   .subscribe(async data => {


    //   console.log(data)
    // });

  //  }

  }
  delay(ms: number) {

    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  loadMarkers(){
    this.deleteMarkers();
   if(this.pickupnow)
   {
      let updatelocation = new google.maps.LatLng(this.lat,this.lng);
      this.addMarker(updatelocation, this.vehicles[0]);
    this.setMapOnAll(this.map_line);

   }
   else
   {
    for (let i = 0; i < this.vehicles.length; i++) {
      let updatelocation = new google.maps.LatLng(this.vehicles[i]["latitude"], this.vehicles[i]["longitude"]);
      this.addMarker(updatelocation, this.vehicles[i]);
    }
    this.setMapOnAll(this.map);

   }

    }

  addMarker(location, vehicle) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: 'assets/img/truck_pin.png'
    });
    this.markers.push(marker);
    let content = "<div class='map-pin-holder'>"+
          "<h1>Shipment No."+vehicle.shipment_id+"</h1>"+
          "<div class='pin-row clearfix'>"+
            "<div class='fleet-col-left'>"+
              "<div class='truck-row'>"+
                "<span><img src='assets/img/truck.png' alt='' ></span>"+
                "<span>"+vehicle.vehicle_type+"</span>"+
              "</div>"+
              "<div class='truck-number'>"+vehicle.registration_number+"</div>"+
            "</div>"+
            "<div class='fleet-col-right'>"+
                "<div class='address-outer'>"+
                    "<div class='address-one'>"+
                        vehicle.pickup_location+
                    "</div>"+
                    "<div class='address-two'>"+
                        vehicle.drop_location+
                    "</div>"+
                "</div>"+
          "</div>"+
          "</div>"+
        "</div>";

    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
       this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });

    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }


  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  callDriver(vehicle){
    this.callNumber.callNumber(vehicle.driver_mobile, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  callCargo(){
    if(this.pickupnow)
    {
      this.callNumber.callNumber(this.shipment.cargo_owner_number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
      console.log("individual")
    }
    else
    {
      var country = this.globalVar.current_user.country.name
      if(country=="Pakistan")
      {
        this.callNumber.callNumber("+923377131317", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("pakistan")
      }
      else
      {
        this.callNumber.callNumber("+971521250605", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("dubai")

      }
    }
  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  // changeDestination(){
  //   let addModal = this.modalCtrl.create('FleetChangeDestinationPage', {shipment: this.shipment});
  //   addModal.onDidDismiss(item => {
  //     this.shipment = item;
  //   })
  //   addModal.present();
  // }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  getVehicleStatus(vehicle){
    let status;
    switch (vehicle.status) {
      case "start":
        status =  '(On its way to Cargo Pick Up Location)'
        break;
      case "loading":
        status =  '(Cargo Loading in progress)'
        break;
      case "enroute":
        status =  '(On its way to cargo Drop-Off location)'
        break;
      case "unloading":
        status =  '(Cargo Unloading in progress)'
        break;
    }
    return status
  }
}

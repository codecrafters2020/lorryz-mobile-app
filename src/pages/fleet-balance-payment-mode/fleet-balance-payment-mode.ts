import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Payment, GlobalVars, Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-fleet-balance-payment-mode',
  templateUrl: 'fleet-balance-payment-mode.html',
})
export class FleetBalancePaymentModePage {

  public shipments: any;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
    public paymentProvider: Payment,
    public loader: Loader,
    public globalVar: GlobalVars,) {
    this.shipments = []
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.paymentProvider.fleet_payments(this.globalVar.current_user.company_id).subscribe(data => {
      this.loader.hide();
      this.shipments = data
      console.log(this.shipments)
    },(err) => {
      this.loader.hide();
    });
  }

  openBalance(){
    this.navCtrl.setRoot("FleetBalancePaymentModePage");
  }
  openWithDraw(){
    this.navCtrl.setRoot("WithdrawPaymentModePage");
  }
  openCreditCard(){
    this.navCtrl.setRoot("CreditCardPaymentModePage");
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

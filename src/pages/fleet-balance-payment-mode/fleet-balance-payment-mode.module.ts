import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetBalancePaymentModePage } from './fleet-balance-payment-mode';

@NgModule({
  declarations: [
    FleetBalancePaymentModePage,
  ],
  imports: [
    IonicPageModule.forChild(FleetBalancePaymentModePage),
  ],
})
export class FleetBalancePaymentModePageModule {}

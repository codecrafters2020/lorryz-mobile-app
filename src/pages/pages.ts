// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'TutorialPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';


export const ActivateAccountPage = 'ActivateAccountPage'; 



export const OwnerInformationPage = 'OwnerInformationPage';
export const CompanyInformationPage = 'CompanyInformationPage';
export const IndividualInformationPage = 'IndividualInformationPage';
export const OrganizationInformationPage = 'OrganizationInformationPage';


export const SignUpPage = 'SignupPage';
export const LoginPage = 'LoginPage';


export const LandingPage = 'LandingPage';

export const NewShipmentPage = 'NewShipmentPage';

export const NewShipmentLocationPage = 'NewShipmentLocationPage';


export const NewShipmentDropOffPage = 'NewShipmentDropOffPage';
export const NewShipmentDropOffLocationPage = 'NewShipmentDropOffLocationPage';


export const NewShipmentRequestPage = 'NewShipmentRequestPage';

export const PostedShipmentPage = 'PostedShipmentPage';

export const PostedShipmentDetailPage = 'PostedShipmentDetailPage';

export const OngoingShipmentPage = 'OngoingShipmentPage';
export const OngoingShipmentListPage = 'OngoingShipmentListPage';

export const ForgotPasswordPage = 'ForgotPasswordPage';



export const FleetWonShipmentBidding ='FleetWonShipmentBiddingPage'
export const FleetOngoingShipmentListing ='FleetOngoingShipmentListingPage'
export const FleetOngoingShipmentDetail ='FleetOngoingShipmentDetailPage'
export const FleetAddDriver ='FleetAddDriverPage'
export const FleetAddVehicle ='FleetAddVehiclePage'
export const FleetVehicleDriverListing ='FleetVehicleDriverListingPage'
export const FleetCompletedShipmentListing ='FleetCompletedShipmentListingPage'


export const FleetNewShipmentBidding = 'FleetNewShipmentBiddingPage'
export const FleetActiveShipmentBidding = 'FleetActiveShipmentBiddingPage'

export const FleetNewShipmentListing = 'FleetNewShipmentListingPage'



export const EditCompanyInformationPage = 'EditCompanyInformationPage';
export const EditIndividualInformationPage = 'EditIndividualInformationPage';


export const VehicleListing = 'VehicleListingPage'
export const DriverListing = 'DriverListingPage'


export const DriverUpcomingShipmentListPage = 'DriverUpcomingShipmentListPage'

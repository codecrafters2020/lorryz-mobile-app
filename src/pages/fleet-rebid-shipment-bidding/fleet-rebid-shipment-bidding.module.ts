import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetRebidShipmentBiddingPage } from './fleet-rebid-shipment-bidding';

@NgModule({
  declarations: [
    FleetRebidShipmentBiddingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetRebidShipmentBiddingPage),
  ],
})
export class FleetRebidShipmentBiddingPageModule {}

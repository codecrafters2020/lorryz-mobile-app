import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController,AlertController, Platform  } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
import { FleetNewShipmentListing } from '../pages';
import * as moment from 'moment';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { Media,MediaObject } from '@ionic-native/media';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

declare var google;


@IonicPage()
@Component({
  selector: 'page-fleet-rebid-shipment-bidding',
  templateUrl: 'fleet-rebid-shipment-bidding.html',
})
export class FleetRebidShipmentBiddingPage {
  @ViewChild('map') mapElement: ElementRef;

  shipment: any;
	bid: any;
  lock: boolean;
  distance: string;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  time: number;
  x: number;
  pickupTime: any;
  str: any;
  map: any;
  current_position: any;
  lat: any;
  lng: any;
  shipments: any;
  countid: number;
  audioFile:MediaObject;
  audioPlay: boolean=false;
  constructor(public navCtrl: NavController,
  				public navParams: NavParams,
  				public modalCtrl: ModalController,
  				public shipmentProvider: FleetShipment,
          public loader: Loader,
          public toastCtrl: ToastController,public alertCtrl: AlertController,
          public globalVar: GlobalVars,
          public platform: Platform,
          private diagnostic: Diagnostic,
          private locationAccuracy: LocationAccuracy,
          private geolocation: Geolocation,
          private media: Media,
          private sanitizer: DomSanitizer

          ) {
  	this.shipment = {};
  	this.bid = {};
    this.lock = false;
    this.audioPlay=false;
  }
  ionViewWillEnter()
  {
    document.getElementById("timess").innerText = "0:00";
    var el;
    el = <HTMLInputElement>document.getElementById("butt")
    el.disabled = true;

  }

   ionViewDidEnter() {
     if (this.platform.is('cordova')) {
       this.diagnostic.isLocationEnabled().then((enabled) => {
         if (enabled) {
           this.askForHighAccuracy();
         } else {
           this.locationAccuracy.canRequest().then((canRequest: boolean) => {
             this.askForHighAccuracy();
           });
         }
       })
     } else {
       this.loadMap();
     }
   }

   askForHighAccuracy() {
     this.locationAccuracy
       .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
         this.loadMap();
       });
   }



   loadMap() {

     if (this.map) {
       return this.map;
     }

     this.geolocation.getCurrentPosition().then((position) => {

       this.current_position = position;
       console.log(this.current_position);
      this.lat=this.current_position.coords.latitude
      this.lng=this.current_position.coords.longitude
      console.log(this.current_position.coords.latitude);
       console.log(this.current_position.coords.longitude);

       this.calcRoute();

       });

     }

      calcRoute() {
       var directionsService = new google.maps.DirectionsService();
       var directionsRenderer = new google.maps.DirectionsRenderer();
       var haight = new google.maps.LatLng(this.current_position.coords.latitude, this.current_position.coords.longitude);
       var oceanBeach = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
       var request = {
           origin: haight,
           destination: oceanBeach,

           travelMode: 'DRIVING'
       };
       directionsService.route(request, function(response, status) {
         if (status == 'OK') {
           directionsRenderer.setDirections(response);
         }
         console.log(response);
         this.distance = response.routes[0].legs[0].duration.text;
         document.getElementById("dis").innerText = this.distance;
     //    console.log(this.distance)
       });
     }

  ionViewDidLoad() {
  	console.log("working",this.navParams);
    this.shipment = this.navParams.get('shipment') || {};
    if (this.shipment.is_contractual || this.shipment.process == "prorate"){
      console.log(this.shipment)
      this.bid.amount = this.shipment.amount
      this.bid.detention = this.shipment.detention_per_vehicle
      this.lock = true
    }
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;

    if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {
      this.pickupnow = true;
        this.timersec();

    }


  }
  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
}
  timersec()
  {
    console.log(this.shipment.is_pickup_now)

    const self =this;
   var date1 = moment.utc().format('YYYY-MM-DD');

   const  els = <HTMLInputElement>document.getElementById("butt")
       if(date1 == this.shipment.pickup_date)
       {
        this.pickupTime = this.shipment.pickup_time;
        this.str = this.pickupTime.split('T');
        var s = this.str[1].split('.')
        var shipmenttime = moment.duration(s[0]).asSeconds();
      //    var h = this.str[1].split(':')
      //  var h1 = h[0];
      //  h1 = h1*3600;
      //  h1=parseInt(h1);
      //  var min = h[1];
      //  min = min*60;
      //  min=parseInt(min);
      //  var sec = h[2]
      //  sec=parseInt(sec);
       //.split('.');
//       var shipmenttime = h1+min+sec-60;

       var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');

       var local = moment(date).add(15, 'minutes').format(' HH:mm:ss');

       var localtime = moment.duration(local).asSeconds()
       var time = localtime-shipmenttime;
        time= 90-time;
       if(time >90 ){
         time = 90;

       }


       this.time = time;

       this.x = setInterval(function () {
       this.time = time;
       document.getElementById("timess").innerText = this.time;
       var el;
       el = <HTMLInputElement>document.getElementById("butt")
       el.disabled = false;
       if( --time < 0 )
       {

         this.buttonss = true;
         clearInterval(this.x);
         // var el;
         el = <HTMLInputElement>document.getElementById("butt")
         if(self.shipment !=undefined)
         {
          if(self.shipment.is_pickup_now)
             el.disabled=true;
         }
          else
              el.disabled=false;
            document.getElementById("timess").innerText = "0:00";
       }
       },1000);

     }

     else
     {

         if(self.shipment !=undefined)
          if( this.shipment.is_pickup_now)
             els.disabled=true;
          else
              els.disabled=false;
         document.getElementById("timess").innerText = "0:00";
         console.log("else clear interval")
       }

 }

 play(myFile) {
  this.audioFile = this.media.create(myFile);
  this.audioFile.play();
  this.audioPlay=true;
}
pauseAudio()
{
this.audioFile.pause();
this.audioPlay=false;
}
  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }


  addItem() {

    if(this.pickupnow){

      this.bid.detention=0;
    }


    if(parseFloat(this.bid.amount) <= 0 || this.bid.amount == null || isNaN(parseFloat(this.bid.amount)) ){

      let alert = this.alertCtrl.create({
      title: 'Alert',
      message: `Bid amount can't be zero, negative or empty`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
      });
      alert.present();
    }else if(parseFloat(this.bid.detention) < 0 || this.bid.detention == null || isNaN(parseFloat(this.bid.amount))){
      let alert = this.alertCtrl.create({
      title: 'Alert',
      message: `Detention can't be negative or empty`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
      });
      alert.present();
    }else{
      let addModal = this.modalCtrl.create('FleetNewShipmentBiddingModalPage',{shipment: this.shipment,bid: this.bid,active: true,
        lat:this.lat,lng:this.lng,bidactive:true});
      addModal.onDidDismiss(item => {
        if(item != "cancel"){
       //   this.globalVar.rebid = true
          this.navCtrl.setRoot("FleetActiveShipmentBiddingPage",{rebid:true});
        }
      })
      addModal.present();


    }

    }


  postedShipmentDetail(){

  }

  notInterested(shipment_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: "Do you want to remove this shipment from your list?" ,
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.notInterested(shipment_id,this.globalVar.current_user.company_id).subscribe(data => {
            this.loader.hide();
            this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
            let toast = this.toastCtrl.create({
              message: data["success"],
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();

          },(err) => {
            this.loader.hide();
          });
        }
      },
    ]
    });
    alert.present();
  }
    // bidonShipment(shipment_id){
    // 	let _bid = {amount: this.bid.amount,detention: this.bid.detention}
    // 	// debugger
    //   this.shipmentProvider.bidonShipment(shipment_id,this.globalVar.current_user.company_id,_bid).subscribe(data => {

    //     let toast = this.toastCtrl.create({
    //       message: "Bid Successfully Posted",
    //       duration: 3000,
    //       position: 'top'
    //     });
    //     toast.present();

    //     this.navCtrl.setRoot(FleetNewShipmentListing);

    //     // this.bids = data;
    //   },(err) => {

    //   });
    // }

    reject(shipment_id){
      // this.shipmentProvider.notInterested(this.globalVar.current_user.company_id,shipment_id).subscribe(data => {
      //   console.log(data);
      //   // this.shipments = data;
      // },(err) => {

      // });
    }
    showNotification(){
      this.navCtrl.push("NotificationPage")
    }

    ionViewDidLeave()
    {
        clearInterval(this.x);
        this.audioFile.stop();
    }
}

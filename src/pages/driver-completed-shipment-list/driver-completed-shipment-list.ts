import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, Platform } from 'ionic-angular';
import { User,DriverProvider,Loader, LocationTracker,GlobalVars } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-driver-completed-shipment-list',
  templateUrl: 'driver-completed-shipment-list.html',
})
export class DriverCompletedShipmentListPage {

  public shipments : any
  public ascending= true;
  public loaded = false;
  public page = 1;
    constructor(public navCtrl: NavController, 
                public navParams: NavParams,
                public driver: DriverProvider,
                public toastCtrl: ToastController,
                public platform: Platform,
                public globalVar: GlobalVars,
                public locationTracker: LocationTracker,
                public loader: Loader) {
      this.shipments = []
    }

  fetchShipments(infiniteScroll = null){
    
    this.driver.completedShipments(this.page).subscribe((data) => {
        this.loader.hide();
        
        if (infiniteScroll) {
        for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }

        this.page = this.page+1;
        this.loaded = true
        this.infiniteScrollComplete(infiniteScroll)

      }, (err) => {

        this.loader.hide();
        this.infiniteScrollComplete(infiniteScroll)

        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  ionViewDidEnter() {    
    this.loader.show("");
    this.page = 1
    this.fetchShipments()
    if (this.platform.is('cordova')) {
      this.locationTracker.stopTracking()
    }
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
  upcoming(){
  	this.navCtrl.setRoot("DriverUpcomingShipmentListPage");
  }
  ongoing(){
  	this.navCtrl.setRoot("DriverOngoingShipmentListPage");
  }
  completed(){
  	this.navCtrl.setRoot("DriverCompletedShipmentListPage");	
  }

  openOngoingDetail(shipment){
    this.navCtrl.push("DriverCompletedShipmentDetailPage", {shipment: shipment});  
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)

    

  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  
  
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverCompletedShipmentListPage } from './driver-completed-shipment-list';

@NgModule({
  declarations: [
    DriverCompletedShipmentListPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverCompletedShipmentListPage),
  ],
})
export class DriverCompletedShipmentListPageModule {}

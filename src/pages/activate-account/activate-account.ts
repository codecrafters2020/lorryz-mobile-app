import { Component, Input, Output, EventEmitter,ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, Platform, Slides } from 'ionic-angular';
import { OwnerInformationPage } from '../pages';
import { Geolocation } from '@ionic-native/geolocation';

import { User,GlobalVars, Loader } from '../../providers/providers';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

declare var google;

/**
 * Generated class for the ActivateAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activate-account',
  templateUrl: 'activate-account.html',
})
export class ActivateAccountPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;

  verification_code: string

  @Input() digit_1;
  disabled = true;

  @ViewChild('otp1') myInput ;

  map: any;
  current_position: any;
  latLng: any;
  count;
  country_name;




  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public globalVar: GlobalVars,
              public user: User,
              public toastCtrl: ToastController,
              public loader: Loader,
              public platform: Platform,
              private diagnostic: Diagnostic,
              private geolocation: Geolocation,
              private locationAccuracy: LocationAccuracy,
          ) {
  }

  ngAfterViewInit()
  {
    console.log("a")
     this.initialize_autocomplete()
  }


     initialize_autocomplete() {
       if (this.platform.is('cordova')) {
         this.diagnostic.isLocationEnabled().then((enabled) => {
           if (enabled) {
             this.askForHighAccuracy();
           } else {
             this.locationAccuracy.canRequest().then((canRequest: boolean) => {
               this.askForHighAccuracy();
             });
           }
         })
       } else {
         this.loadMap();
       }
     }


     askForHighAccuracy() {
       this.locationAccuracy
         .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
           this.loadMap();
         });
     }

     loadMap() {

      if (this.map)
      {
        return this.map;
      }

      let self = this;
      this.geolocation.getCurrentPosition().then((position) => {

        this.current_position = position;
        // console.log(this.current_position);
        this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var geocoder = new google.maps.Geocoder;
        const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({ 'location': this.latLng }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {

            if (results[1]) {
              var indice=0;
              for (var j=0; j<results.length; j++)
              {
                  if (results[j].types[0]=='locality')
                      {
                        indice=j;
                          break;
                      }
                  }

              for (var i=0; i<results[j].address_components.length; i++)
                  {

                      if (results[j].address_components[i].types[0] == "country") {
                              //this is the object you are looking for
                            this.count = results[j].address_components[i];
                            console.log(this.count.long_name)
                            this.country_name = this.count.long_name
                            if(this.country_name=="Pakistan")
                            {
           document.getElementById("country_name_pak").innerText=" For any assistance, Please call us at +92 337 7131317 Or send us email us at contact@lorryz.com"
                            }
                            else{
           document.getElementById("country_name_pak").innerText=" For any assistance, Please call us at +971 56 5466088 Or send us email us at contact@lorryz.com"

                            }

                          }




                        }

          } else {
         //   window.alert('Geocoder failed due to: ' + status);
          }}
        });


        });


      }

  ngOnInit() {
    console.log('ionViewDidLoad ActivateAccountPage');
  }

  verfiyAccount(){
    // debugger
    let code = this.digit_1;
    console.log(code)
    this.loader.show("");
    this.user.verfiyAccount({code: code}).subscribe((resp) => {
      this.loader.hide();
         if (resp["verified"]) {
           this.navCtrl.setRoot(OwnerInformationPage);
         }else{
           let toast = this.toastCtrl.create({
             message: resp["error"],
             duration: 3000,
             position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
           });
           toast.present();
         }
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 3000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });



  }

  resentCode(){
    this.loader.show("");
    this.user.resentCode().subscribe((resp) => {
         this.loader.hide();
         let msg = resp["success"] ? resp["success"] : resp["error"]

         let toast = this.toastCtrl.create({
           message: msg,
           duration: 3000,
           position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
         });
         toast.present();
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 3000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  next(el) {
    el.setFocus();
  }

  goBack(){
    this.navCtrl.setRoot("EditSignupDetailPage")
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

import { ActivateAccountPage } from './activate-account';

@NgModule({
  declarations: [
    ActivateAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivateAccountPage),
    TranslateModule.forChild()
  ],
})
export class ActivateAccountPageModule {}

import { Component, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ToastController,
  Keyboard,
  App,
  ViewController,
  LoadingController
} from "ionic-angular";
import {
  Shipment,
  GlobalVars,
  Loader,
  CountryProvider
} from "../../providers/providers";
import { Geolocation } from "@ionic-native/geolocation";
import moment from "moment";
import { Storage } from "@ionic/storage";
import { MediaCapture, CaptureError } from "@ionic-native/media-capture";
import { MediaObject, Media } from "@ionic-native/media";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { AwsProvider } from "../../providers/aws/aws";
// let mom =  moment()

declare var google;
@IonicPage()
@Component({
  selector: "page-edit-shipment",
  templateUrl: "edit-shipment.html"
})
export class EditShipmentPage {
  shipment: any;
  vehicles: any;
  minDate: any;
  lock: boolean;
  locations: any;
  fare: any;
  distance: any;
  pickupnow;
  letsgo;
  vehiclevalue;
  modalreq;
  position;
company_type;
company_category;
server;
shipmentId;
public input: string = '';
  public countries: string[] = [];
 public list:string[] = [
   "AC Airconditioning",
"Air conditioning",
"Bed & mattress",
  "Bedframes",
  "Bedroom set",
"Bed",
"Cabinets",
"Carpet",
"Center table with side tables",
"Chair",
"Clothes",
"Computer Table",
  "Cooking range",
  "Cupboard",
  "Curtains",
  "Desks",
  "Dining table",
  "Dishwasher",
  "Dressing table",
  "Fan",
  "Food Processor",
  "Freezer",
  "Fridge",
  "Furniture",
  "General",
  "General Cargo",
  "Geyser",
  "Home Theatre",
  "Hotel Scrap",
  "House Goods Shifting",
  "Iron Scrap",
  "Kitchen Appliance",
  "Mattress",
  "Microwave ovens",
  "Mirrors",
  "Plywood sheets",
  "Printer",
  "Printer & Scanner",
  "Refrigerators",
  "Sanitary",
  "Sewing Machine",
  "Slabs",
  "Sofas",
  "Table",
  "Television",
  "Tiles",
  "Tires",
  "TV",
  "Tyres",
  "Vacuum cleaners",
  "Vacuum pump",
  "Wardrobes",
  "Washing Machine",
  "Water Purifier & Dispenser",
  "Water tank (Plastic / Fibre)",
  "Wheel",
  "Wooden logs"
  ];
  pakistan: any;
  id: any;
  vehiclename: any;
  src: any;
  is_edit: any;
  mediaFiles:any;
  recData: any;
  document: any;
  audioFile: MediaObject;
  blob: any;
  updateVoiceNote: boolean;
  loading: any;
  constructor(
    public zone: NgZone,
    public storage: Storage,
    private viewCtrl: ViewController,
    public modalCtrl:ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: Shipment,
    public toastCtrl: ToastController,
    public countryProvider: CountryProvider,
    public loader: Loader,
    private geolocation: Geolocation,
    public globalVar: GlobalVars,
    public keyboard:Keyboard,
    public appCtrl:App,
    private mediaCapture: MediaCapture,
    private media: Media,
    private sanitizer: DomSanitizer,
    private awsProvider: AwsProvider,
    private loadingController: LoadingController,
  ) {
    this.shipment = {};
    this.vehicles = [];
    this.lock = false;
    this.locations = [];
    this.fare = 0;
    this.distance = 0;
    this.minDate = new Date().toISOString();
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
  //  console.log(this.company_type);
   // console.log(this.company_category);
   this.id = this.navParams.get("id");
    this.vehiclename = this.navParams.get("vehicle_name");
    this.src = this.navParams.get("src");
    this.is_edit = this.navParams.get("is_edit");

//    console.log(this.is_edit)
    this.server = this.globalVar.server_link;

//    console.log(this.navParams.get("shipmentId"));

  //  this.company_category = navParams.get("company_category");
  //  this.company_type = navParams.get("company_type");

    if((this.company_category == 'cargo')&&(this.company_type == 'company'))
    {
      this.letsgo = true;
    }


    if((this.company_category == 'cargo')&&(this.company_type == 'individual'))
    {
      this.shipment.cargo_packing_type = "other";
      this.shipment.payment_option = "before_pick_up";
       if(this.pickupnow == true)
       {
        this.shipment.no_of_vehicles = 1;

    //this.shipment.payment_option = "before_pick_up";
       }
    }

  }

  ionViewDidLoad() {
    this.pakistan = this.globalVar.current_user.country.name;
    if(this.navParams.get("shipmentId"))
    {
      this.shipmentId = this.navParams.get("shipmentId");
    }
    else
    {
        this.shipmentId = this.navParams.get("shipment_id");
    }
    this.loader.show("");
    this.shipmentProvider
      .edit(
        this.globalVar.current_user.company_id,
       this.shipmentId
      )
      .subscribe(
        data => {
          let time = moment(data["pickup_time"]);
          data["pickup_time"] = time.format();
          this.loader.hide();
          this.shipment = data;
          if (this.shipment.location_id && this.shipment.location_id > 0) {
            this.lock = true;
          } else {
//            this.shipment.location_id = "abc";
            this.calculateFare();
          }
          console.log(data);
          this.pickupnow = this.shipment.is_pickup_now;
          console.log(this.shipment.cargo_description);
          this.input = this.shipment.cargo_description;

        },
        err => {
          this.loader.hide();
        }
      );
    this.countryProvider
      .locations(this.globalVar.current_user.country_id)
      .subscribe(data => {
        this.locations = data;
      });


  }

  ngAfterViewInit() {
    let input_pickup = <HTMLInputElement>(
      document.getElementById("auto_pickup").getElementsByTagName("input")[0]
    );
    let input_drop = <HTMLInputElement>(
      document.getElementById("auto_drop").getElementsByTagName("input")[0]
    );
    let options = {
      componentRestrictions: {
        // country: this.globalVar.current_user.country.short_name,
        // types: ['(regions)']
      }
    };
    let self = this;
    let autoComplete_pickup = new google.maps.places.Autocomplete(
      input_pickup,
      options
    );
    google.maps.event.addListener(autoComplete_pickup, "place_changed", () => {
      var place = autoComplete_pickup.getPlace();

      self.shipment.pickup_lat = place.geometry.location.lat();
      self.shipment.pickup_lng = place.geometry.location.lng();
      self.shipment.pickup_location = input_pickup.value;
      self.shipment.pickup_building_name = place.name;
   console.log(
        "place_changed",
        place.geometry.location.lat(),
        place.geometry.location.lng()
      );
    });

    let autoComplete_drop = new google.maps.places.Autocomplete(
      input_drop,
      options
    );
    google.maps.event.addListener(autoComplete_drop, "place_changed", () => {
      var place = autoComplete_drop.getPlace();

      self.shipment.drop_lat = place.geometry.location.lat();
      self.shipment.drop_lng = place.geometry.location.lng();
      self.shipment.drop_location = input_drop.value;
      self.shipment.drop_building_name = place.name;
      console.log(
        "place_changed",
        place.geometry.location.lat(),
        place.geometry.location.lng()
      );
    });

    this.geolocation.getCurrentPosition().then(
      position => {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });

        autoComplete_pickup.setBounds(circle.getBounds());
        autoComplete_drop.setBounds(circle.getBounds());
      },
      err => {
        console.log(err);
      }
    );
  }

  openModal() {
    this.modalreq = true;
    // this.navCtrl.push('PagesVehicleTypePage',{edit:true, shipment:this.shipment,
    //   company_category:this.company_category,company_type:this.company_type,
    //   shipmentId:this.shipmentId
    // });
    let addModal = this.modalCtrl.create('PagesVehicleTypePage', {edit:true, shipment:this.shipment,
         company_category:this.company_category,company_type:this.company_type,
         shipmentId:this.shipmentId});

         addModal.present();

  }
  updateAudio()
  {
    this.updateVoiceNote=!this.updateVoiceNote
  }
  deleteAudio()
  {
      this.shipment.cargo_description_url='';
  }
  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
  }
  add(item: string) {
    this.input = item;
    this.countries = [];
  }

  removeFocus() {
    this.keyboard.close();
  }

  search() {
    if (!this.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
    return;
    }
    this.countries = this.list.filter(item => item.toUpperCase().includes(this.input.toUpperCase()));
  }

  updateShipment() {

    this.storage.get("email").then(result=>{
      console.log(result)
      var array = result;
      if(result!=null)
      {
       for(var i=0;i<=result.length;i++)
       {
         if(result[i].shipmentid!=undefined)
         {
           if(result[i].shipmentid==this.shipment.id){
             if(result[i].email=="sent")
             {
                this.zone.run(()=>{
                this.storage.remove("email");
                array.splice(i,1)
                // delete array[i];
                this.storage.set("email",array);
               });
             }
           }
         }
       }
      }

      // if(result.shipmentid==this.shipment.id){
      //   if(result.email=="sent")
      //   {
      //       this.storage.remove("email");
      //   }
      // }
      });
    if(this.id)
    {
      this.shipment.vehicle_type_id = this.id;
    }
    var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
    var stillUtc = moment.utc(date).toDate();
    var local = moment(date).add(15, 'minutes').format(' HH:mm:ss')
    console.log("local"+local);

    if(this.pickupnow == true)
    {
      this.shipment.pickup_date = date
      this.shipment.pickup_time = local
      console.log("Shipment"+this.shipment.pickup_time)
      if(this.shipment.pickup_building_name=="")
      {
        this.shipment.pickup_building_name="e";
      }
      if(this.shipment.drop_building_name=="")
      {
        this.shipment.drop_building_name="e";
      }
    }
    this.shipment.cargo_description= this.input;
    // this.loader.show("");
    this.shipmentProvider
      .update(
        this.shipment,
        this.shipment.id,
        this.globalVar.current_user.company_id
      )
      .subscribe(
        data => {
          // this.loader.hide();
          this.loading.dismiss();
          this.viewCtrl.dismiss().then(() => this.appCtrl.getRootNav().push('PostedShipmentPage',{reload: 1}));

//          this.navCtrl.setRoot("PostedShipmentPage");
        },
        err => {
          this.loading.dismiss();
          // this.loader.hide();
          let toast = this.toastCtrl.create({
            message: err.error.errors,
            duration: 6000,
            position: "top",
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        }
      );
  }
  format_pickup_time() {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    const day = today.getDate();
    const hour = parseInt(this.shipment.pickup_time.split(":")[0]);
    const min = parseInt(this.shipment.pickup_time.split(":")[1]);
    return new Date(year, month, day, hour, min);
  }

  pad(n) {
    return n < 10 ? "0" + n : n;
  }

  populateLocation(value): void {
    this.lock = true;
    this.shipment.pickup_building_name = value.pickup_building_name;
    this.shipment.pickup_location = value.pickup_location;
    this.shipment.pickup_lat = value.pickup_lat;
    this.shipment.pickup_lng = value.pickup_lng;
    this.shipment.pickup_city = value.pickup_city;
    this.shipment.pickup_country = value.pickup_country;

    this.shipment.drop_building_name = value.drop_building_name;
    this.shipment.drop_location = value.drop_location;
    this.shipment.drop_lat = value.drop_lat;
    this.shipment.drop_lng = value.drop_lng;
    this.shipment.drop_city = value.drop_city;
    this.shipment.drop_country = value.drop_country;

    this.shipment.amount = value.rate;
    this.shipment.vehicle_type_id = value.vehicle_type_id;
    this.shipment.loading_time = value.loading_time;
    this.shipment.unloading_time = value.unloading_time;
  }

  unselectLocation() {
    this.lock = false;
    this.shipment.amount = "";
  }

  calculateFare() {
    if (
      this.globalVar.current_user.process == "prorate" &&
      this.shipment.vehicle_type_id &&
      this.shipment.unloading_time &&
      this.shipment.loading_time
    ) {
      this.shipmentProvider
        .calculate_fare(this.shipment, this.globalVar.current_user.company_id)
        .subscribe(
          data => {
            this.fare = data["fare"];
            this.distance = data["distance"];
          },
          err => {}
        );
    }
  }

  showNotification() {
    this.navCtrl.push("NotificationPage");
  }
  captureAudio() {
    this.mediaCapture.captureAudio().then(res => {
    this.mediaFiles = res[0];
    this.recData=res[0]['fullPath'];
  }, (err: CaptureError) => console.error(err));
  }
  play(myFile) {
    if (myFile.name.indexOf('.mp3') > -1) {
      this.audioFile = this.media.create(myFile.fullPath);
      this.audioFile.play();
    }
  }
  async uploadDocumentResource()
{
  this.loading = this.loadingController.create({
  });
  this.loading.present();
    let that=this;
    if(this.recData)
    {
            that.zone.run(() => {
              let request = new XMLHttpRequest();
              request.open('GET', this.recData, true);
              request.responseType = 'blob';
              request.timeout = 360000;
              request.onload = function() {
                that.zone.run(() => {
                  let reader = new FileReader();
                  reader.readAsDataURL(request.response);
                  console.log("Step 1 reader",reader);
                  console.log("Step 1 got stuck in request.response",request)

                  reader.onload =  function(){
                    that.zone.run(async () => {
                      console.log("Step 2 Loaded",reader.result);
                      that.blob = reader.result.split(',')[1];
                      that.shipment.cargo_description_url=that.blob;
                      that.updateShipment();
                         // loading.dismiss();
                    });
                  }

                  reader.onerror =  function(e){
                    console.log('We got an error in file reader:::::::: ', e);
                  };

                  reader.onabort = function(event) {
                    console.log("reader onabort",event)
                  }
                })
              };

              request.onerror = function (e) {
                that.loading.dismiss();
                console.log("** I got an error in XML Http REquest",e);
              };

              request.ontimeout = function(event) {
                that.loading.dismiss();
                 console.log("xmlhttp ontimeout",event)
              }

              request.onabort = function(event) {
                that.loading.dismiss();
                 console.log("xmlhttp onabort",event);
              }


              request.send();
            })
    }
    else
    {
      this.updateShipment();
//      this.loading.dismiss();
    }
}
}

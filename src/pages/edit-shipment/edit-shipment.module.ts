import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditShipmentPage } from './edit-shipment';

@NgModule({
  declarations: [
    EditShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(EditShipmentPage),
  ],
})
export class EditShipmentPageModule {}

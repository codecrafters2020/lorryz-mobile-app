import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelShipmentModalPage } from './cancel-shipment-modal';

@NgModule({
  declarations: [
    CancelShipmentModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelShipmentModalPage),
  ],
})
export class CancelShipmentModalPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OngoingShipmentDetailPage } from './ongoing-shipment-detail';

@NgModule({
  declarations: [
    OngoingShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OngoingShipmentDetailPage),
  ],
})
export class OngoingShipmentDetailPageModule {}

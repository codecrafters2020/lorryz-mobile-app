import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';
import { Clipboard } from '@ionic-native/clipboard';
import { Geolocation } from '@ionic-native/geolocation';
import { Slides } from 'ionic-angular';

declare var google;
@IonicPage()
@Component({
  selector: 'page-ongoing-shipment-detail',
  templateUrl: 'ongoing-shipment-detail.html',
})
export class OngoingShipmentDetailPage {
  @ViewChild('slides1') slides: Slides;
  @ViewChild('map') mapElement: ElementRef;
  shipment: any;
  map: any;
  vehicles :any;
  markers :any;
  interval: any;
  infoWindows: any;
  companytype: any;
  companycategory: any;
  pickupnow: boolean;
  current_position: any;
  lat: any;
  lng: any;
  latLng
  line_map: any;
  distance;
  dropmarkers: any;
  currentIndex=0;
  swipperIstrue=false;
  fromNotification: boolean;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private callNumber: CallNumber,
    private clipboard: Clipboard,
    public globalVar: GlobalVars,
    private geolocation: Geolocation,
    ) {
    this.shipment = {};
    this.vehicles = [];
    this.markers = [];
    this.interval = "";
    this.infoWindows = [];
  }

  ionViewDidLoad() {
    this.companytype = this.globalVar.current_user.company.company_type;
    this.companycategory = this.globalVar.current_user.company.category;

    if((this.companytype=='individual')&&(this.companycategory=='cargo'))
    {
      this.pickupnow=true;

    }
    let mapOptions = {
      zoom:19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.fromNotification=this.navParams.get('fromNotification');
    this.load_shipment();
    this.loadVehicles();
    let self = this;
    this.interval = setInterval(function () {
      // console.log("here")
      self.load_shipment();
      self.loadVehicles();
    }, 10000);

  }
  openPodPage()
  {
    this.navCtrl.push('PdfUploadPage',{pdfdoc:this.shipment['documents'],shipment:this.shipment,transitionfrom:'ongoing'});
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  load_shipment(){
    // this.loader.show("");
    this.shipmentProvider.show(this.globalVar.current_user.company_id, this.navParams.get('shipment_id')).subscribe(data => {
      // this.loader.hide();
      this.shipment = data;
      if(this.shipment.state == "completed" && this.fromNotification)
      {
        let toast = this.toastCtrl.create({
          message:"Shipment # "+this.shipment.id+" status has "+this.shipment.state,
          duration: 5000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });
        toast.present();
        this.navCtrl.pop();
      }

    },(err) => {
      // this.loader.hide();
    });
  }

  loadVehicles(){
      let currentIndex = this.slides.getActiveIndex();
      console.log(currentIndex);
      if(currentIndex>=0 )
      {
        this.currentIndex = currentIndex;
      }
      this.shipmentProvider.vehicles(this.globalVar.current_user.company_id, this.navParams.get('shipment_id')).subscribe(data => {
        console.log(data)
        this.vehicles = data;
        if(this.vehicles.length>0)
        {
          this.latLng = new google.maps.LatLng(this.vehicles[this.currentIndex]["latitude"], this.vehicles[this.currentIndex]["longitude"]);
          this.load_map();
        }
      },(err) => {
      });

  }

  load_map(){

    // if(this.pickupnow)
    // {
        if(this.shipment.state == "completed" && !this.fromNotification)
        {
          this.navCtrl.setRoot("CompletedShipmentPage",{ongoing:true,id:this.shipment.id});
        }
        else
        {
          this.currentline();
        }
    //     this.currentline();
    // }

    // else
    // {

    //   this.currentline();
  //     let latLng = ""
  // if (this.vehicles.length > 0){
  //   latLng = new google.maps.LatLng(this.vehicles[0]["latitude"], this.vehicles[0]["longitude"]);
  // }
  // else{
  //   latLng = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
  // }
  // let mapOptions = {
  //   center: latLng,
  //   zoom:19,
  //   mapTypeId: google.maps.MapTypeId.ROADMAP,
  //   mapTypeControl: false,
  //   zoomControl: false,
  //   streetViewControl: false
  // }
  // if (!this.map) {
  //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  // }
  // this.loadMarkers();

//}
    }

    currentline()
    {

      this.map.setCenter({lat:this.vehicles[this.currentIndex]["latitude"], lng:this.vehicles[this.currentIndex]["longitude"]})

      var directionsService = new google.maps.DirectionsService();
      var directionsRenderer = new google.maps.DirectionsRenderer();
      var haight = new google.maps.LatLng(this.vehicles[this.currentIndex]["latitude"], this.vehicles[this.currentIndex]["longitude"]);


      const self=this;
      if(this.shipment.vehicles[this.currentIndex].status_code == 200)
      var oceanBeach = new google.maps.LatLng(self.shipment.pickup_lat, self.shipment.pickup_lng);
      else
      var oceanBeach = new google.maps.LatLng(self.shipment.drop_lat, self.shipment.drop_lng);

      directionsRenderer.setMap(this.map);
      var request = {
        origin: haight,
        destination: oceanBeach,

        travelMode: 'DRIVING'
    };

    if((this.companytype=='individual') && (this.companycategory=='cargo'))
    {
      directionsService.route(request, function(response, status) {
        if (status == 'OK') {
         // directionsRenderer.setDirections(response);
        }
//        console.log(response);
        this.distance = response.routes[0].legs[0].duration.text;
       document.getElementById("fleet_distance").innerText = this.distance;
      //   console.log(this.distance)

      });

    }



//         directionsService.route(
//             {
//               origin:haight,
//               destination: oceanBeach,
//               travelMode: 'DRIVING'
//             },
//             function(response, status) {
//               if (status === 'OK') {
// //                directionsRenderer.setDirections(response);
//               } else {
//                 // window.alert('Directions request failed due to ' + status);
//               }
//             })

         this.loadMarkers();
        this.loadDropOffMarker();
    }

    loadDropOffMarker(){
//      this.deleteMarkers();
if(this.shipment.vehicles[this.currentIndex].status_code == 200)
{
    let updatelocation = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    this.addDropOffMarker(updatelocation);
        this.setMapOnAll(this.map);
}
else
{
  let updatelocation = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);
  this.addDropOffMarker(updatelocation);
        this.setMapOnAll(this.map);
}


    }
    addDropOffMarker(location) {
      let marker = new google.maps.Marker({
        position: location,
        map: this.map,
        animation: google.maps.Animation.DROP,
        // icon: 'assets/img/truck_pin.png'
      });
      this.markers.push(marker);
    }

   loadMarkers(){
    this.deleteMarkers();
    if(this.pickupnow)
    {
        let updatelocation = new google.maps.LatLng(this.vehicles[this.currentIndex]["latitude"], this.vehicles[this.currentIndex]["longitude"]);
        this.addMarker(updatelocation, this.vehicles[this.currentIndex]);
      this.setMapOnAll(this.map);

    }
    else{
      for (let i = 0; i < this.vehicles.length; i++) {
        let updatelocation = new google.maps.LatLng(this.vehicles[i]["latitude"], this.vehicles[i]["longitude"]);
        this.addMarker(updatelocation, this.vehicles[i]);
      }
      this.setMapOnAll(this.map);

    }
  }

  addMarker(location, vehicle) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: 'assets/img/truck_pin.png'
    });
    this.markers.push(marker);
    let content = "<div class='fleet-card'>"+
        "<h1>Shipment No. "+vehicle.shipment_id+"</h1>"+
        "<div class='fleet-row clearfix'>"+
          "<div class='fleet-col-left'>"+
            "<div class='truck-row'>"+
                "<span><img src='assets/img/truck.png'></span>"+
                "<span>"+vehicle.vehicle_type+"</span>"+
                "<span>"+vehicle.registration_number+"</span>"+
            "</div>"+
          "</div>"+
          "<div class='fleet-col-right'>"+
            "<div class='address-outer'>"+
              "<div class='address-one'>"+
                  vehicle.pickup_location+
              "</div>"+
              "<div class='address-two'>"+
                  vehicle.drop_location+
              "</div>"+
            "</div>"+
          "</div>"+
        "</div>"+
      "</div>";

    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
       this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });

    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
      // this.dropmarkers[0].setMap(map);

  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
 //   this.dropmarkers=
  }

  openPosted(){
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }

  callFleet(){
    if(this.pickupnow)
    {
      this.callNumber.callNumber(this.shipment.fleet_owner_number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
      console.log("individual"+this.shipment.fleet_owner_number)
    }
    else
    {
      var country = this.globalVar.current_user.country.name
      if(country=="Pakistan")
      {
        this.callNumber.callNumber("+923377131317", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("pakistan")
      }
      else
      {
        this.callNumber.callNumber("+971565466088", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("dubai")

      }
    }

  }

  copyTrackingCode(){
    console.log(this.shipment.tracking_code)
    this.clipboard.copy(this.shipment.tracking_code);
    let toast = this.toastCtrl.create({
        message: "Tracking code copied to clipboard",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success"
      });
      toast.present();
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }

  changeDestination(){
    let addModal = this.modalCtrl.create('FleetChangeDestinationPage', {shipment: this.shipment});
    addModal.onDidDismiss(item => {
      this.shipment = item;
    })
    addModal.present();
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  getVehicleStatus(vehicle){
    let status;
    switch (vehicle.status) {
      case "start":
        status =  '(On its way to Cargo Pick Up Location)'
        break;
      case "loading":
        status =  '(Cargo Loading in progress)'
        break;
      case "enroute":
        status =  '(On its way to cargo Drop-Off location)'
        break;
      case "unloading":
        status =  '(Cargo Unloading in progress)'
        break;
    }
    return status
  }
}

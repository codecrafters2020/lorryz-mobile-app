import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetChangeVehicleModalPage } from './fleet-change-vehicle-modal';

@NgModule({
  declarations: [
    FleetChangeVehicleModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetChangeVehicleModalPage),
  ],
})
export class FleetChangeVehicleModalPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { FleetShipment,VehicleProvider,GlobalVars, Loader } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-fleet-change-vehicle-modal',
  templateUrl: 'fleet-change-vehicle-modal.html',
})
export class FleetChangeVehicleModalPage {
	public shipment: any;
	public shipment_obj: any;
	public vehicles : any;
  public loaded : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	private viewCtrl: ViewController,
  	public vehicle: VehicleProvider,
  	public shipmentProvider: FleetShipment,
		public toastCtrl: ToastController,
		public loader: Loader,
    public alertCtrl: AlertController,
    public globalVar: GlobalVars) {
  	this.shipment = this.navParams.get('shipment') || {};
  	this.vehicles = [];
  	this.shipment_obj = {}
    this.loaded = false;
  }

  ionViewDidLoad() {
    console.log(this.shipment.vehicle_ids)
    this.shipment_obj = Object.assign(this.shipment_obj, {id: this.shipment.id, vehicle_ids: this.shipment.vehicle_ids});
    this.vehicle.getVehicles().subscribe((resp) => {
      console.log(resp)
      this.vehicles= resp;
      this.loaded = true
    }, (err) => {
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  assignVehicle(){
    if (this.shipment_obj.vehicle_ids.length == 0){
      this.shipment_obj = Object.assign(this.shipment_obj, {state_event: "accepted"});	
    }
    this.loader.show("");
    this.shipmentProvider.update(this.shipment_obj, this.shipment.id, this.globalVar.current_user.company_id).subscribe(data => {
      console.log(data)
      this.loader.hide();
      this.viewCtrl.dismiss();
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err.error.errors[0],
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present(); 
    });
  }

  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }

}

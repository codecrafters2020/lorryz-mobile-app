import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController  } from 'ionic-angular';
import { OrganizationInformationPage } from '../pages';
import { User,Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-owner-information',
  templateUrl: 'owner-information.html',
})
export class OwnerInformationPage {

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   public toastCtrl: ToastController,
   public loader: Loader,
   public user: User,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OwnerInformationPage');
  }

  selectOrganization(){
  	this.navCtrl.push(OrganizationInformationPage);
  }

  registerAsFleetOwner(){
    this.loader.show("");
    this.user.registerAsFleetOwner().subscribe((resp) => {
         // let msg = resp["success"] ? resp["success"] : resp["error"]

         // let toast = this.toastCtrl.create({
         //   message: msg,
         //   duration: 6000,
         //   position: 'top'
         // });
         // toast.present();
        //  this.loader.hide();
        this.user.registerAsIndividual().subscribe((resp) => {
          this.loader.hide();
             // let msg = resp["success"] ? resp["success"] : resp["error"]

             // let toast = this.toastCtrl.create({
             //   message: msg,
             //   duration: 6000,
             //   position: 'top'
             // });
             // toast.present();

             this.navCtrl.push('IndividualInformationPage');
        }, (err) => {
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Some Went Wrong. Please Try Later",
            duration: 6000,
            position: 'top',
             showCloseButton: true,
             closeButtonText: "x",
             cssClass: "toast-danger"
          });
          toast.present();
        });
        //  this.navCtrl.push(OrganizationInformationPage,{fleet:true});

    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  registerAsCargoOwner(){
    this.loader.show("");
     this.user.registerAsCargoOwner().subscribe((resp) => {
          // let msg = resp["success"] ? resp["success"] : resp["error"]

          // let toast = this.toastCtrl.create({
          //   message: msg,
          //   duration: 6000,
          //   position: 'top'
          // });
          // toast.present();
          this.loader.hide();
          this.navCtrl.push(OrganizationInformationPage);
     }, (err) => {
       this.loader.hide();
       let toast = this.toastCtrl.create({
         message: "Some Went Wrong. Please Try Later",
         duration: 6000,
         position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
       });
       toast.present();
     });
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

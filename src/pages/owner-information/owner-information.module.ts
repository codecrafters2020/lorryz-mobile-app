import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OwnerInformationPage } from './owner-information';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    OwnerInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(OwnerInformationPage),
    TranslateModule.forChild()
  ],
})
export class OwnerInformationPageModule {}

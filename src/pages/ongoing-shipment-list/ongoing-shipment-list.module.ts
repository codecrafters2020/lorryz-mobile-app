import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OngoingShipmentListPage } from './ongoing-shipment-list';

@NgModule({
  declarations: [
    OngoingShipmentListPage,
  ],
  imports: [
    IonicPageModule.forChild(OngoingShipmentListPage),
  ],
})
export class OngoingShipmentListPageModule {}

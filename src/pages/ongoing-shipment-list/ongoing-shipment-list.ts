import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Shipment, GlobalVars, Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-ongoing-shipment-list',
  templateUrl: 'ongoing-shipment-list.html',
})
export class OngoingShipmentListPage {

  public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  interval: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public shipmentProvider: Shipment,
    public loader: Loader,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public globalVar: GlobalVars) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }

    this.shipments = [];
    this.interval = " ";
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.page = 1
    let self = this;
    this.fetchShipments()
    this.interval = setInterval(function () {

      self.automaticRefresh();

    }, 10000);

  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  automaticRefresh()
  {
    this.page = 1
    this.fetchShipments()
  }
  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.ongoing(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }


      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }


  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  openPosted(){
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }

  openOngoingDetail(shipment_id){
    this.navCtrl.push("OngoingShipmentDetailPage", {shipment_id: shipment_id});
  }
  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

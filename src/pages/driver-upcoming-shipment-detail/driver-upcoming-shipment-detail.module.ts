import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverUpcomingShipmentDetailPage } from './driver-upcoming-shipment-detail';

@NgModule({
  declarations: [
    DriverUpcomingShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverUpcomingShipmentDetailPage),
  ],
})
export class DriverUpcomingShipmentDetailPageModule {}

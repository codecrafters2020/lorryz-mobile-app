import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditvehicletypePage } from './editvehicletype';

@NgModule({
  declarations: [
    EditvehicletypePage,
  ],
  imports: [
    IonicPageModule.forChild(EditvehicletypePage),
  ],
})
export class EditvehicletypePageModule {}

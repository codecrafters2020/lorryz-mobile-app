import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVars, Loader } from '../../providers/providers';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

/**
 * Generated class for the EditvehicletypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editvehicletype',
  templateUrl: 'editvehicletype.html',
})
export class EditvehicletypePage {

  viewdata=false
  From_date
  to_date
  driverdate
  driverdateshow
  time:any;
  time1;
  public headers : any;
  baseurl: string;
  finalDate;
  dateArrayShow;
  showTime: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public globalVar: GlobalVars,
    public http: HttpClient,
    public loader: Loader,
    ) {
      this.driverdateshow = [];
      this.driverdate = [];
      this.time = [];
      this.time1 = [];
      this.headers = [];
      this.baseurl = globalVar.server_link + "/api/v1/reports/";
      this.dateArrayShow=[];
      this.showTime=[];
  }

  ionViewDidLoad() {
    this.setheaders();
    console.log('ionViewDidLoad EditvehicletypePage');
  }
  setheaders()
  {
  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"
  });
  }
  disabled()
  {
    if (this.From_date && this.to_date) {
      return false
    }
    else {
      return true
    }
  }
  DriverViewData()
  {
    this.loader.show("");
    this.finalDate=[];
    this.dateArrayShow=[];
    console.log(this.globalVar.current_user.id)
    var userid= this.globalVar.current_user.id
    this.driverdate = [];
    var date1 = new Date(this.From_date);
    var date2 = new Date(this.to_date);
    let diff = date2.getTime() - date1.getTime();
    var Difference_In_Days = diff / (1000 * 3600 * 24);
    if(Difference_In_Days<0)
    {
      this.loader.hide()
    }
    // console.log(Difference_In_Days)
    this.http.post(this.baseurl+'logs_report.json',{user_id:userid,start_date:
    date1,end_date:date2},{headers:this.headers}).subscribe((data) => {

        let array1;
        let array2=[];
        array1=[];

        for (let obj of data["reports"]) {
            // console.log("heading:", obj["heading"] );
          for (let key in obj["report"]) {
              // console.log("key:", key, "value:", obj["report"][key]);
              array1={heading:obj["heading"],value:obj["report"][key],key: key};
              array2.push(array1);
            }
        }
        // console.log(array2);
          var s=0;
         for(var z=0;z<=(array2.length)-1;z++)
        {
          var x= data["dates"][s];
          if(x==array2[z].key)
          {
//            console.log("new array2"+array2[z].key+array2[z].heading+array2[z].value);
              if(array2[z].heading=="Total Time")
              {
                var ttime = array2[z].value;
                var totaltime = moment.utc(ttime*1000).format('HH:mm:ss');
                var date = array2[z].key;
              }
              else if(array2[z].heading=="Bids Placed")
              {
                var bidsPlaced = array2[z].value;
                var date = array2[z].key;
              }
              else if(array2[z].heading=="Completed")
              {
                var completed = array2[z].value;
                var date = array2[z].key;
              }
              else if(array2[z].heading=="Won")
              {
                var won = array2[z].value;
                var date = array2[z].key;
              }
              else if(array2[z].heading=="Recieved")
              {
                var received = array2[z].value;
                var date = array2[z].key;
              }
              else if(array2[z].heading=="Time")
              {
                var time:any = array2[z].value
                this.tConvert(time);
                var date = array2[z].key;
              }
          }
          if(z==(array2.length)-1)
          {
              this.finalDate = {Totaltime:totaltime,bidsplaced:bidsPlaced,completed:completed,won:won,
                received:received,time:this.showTime,date:date};
              this.dateArrayShow.push(this.finalDate)
              totaltime=" ";
              bidsPlaced=" ";
              completed=" ";
              won=" ";
              received=" ";
              time;
              date="";
              z=-1;
              s=s+1;
              this.showTime=[];
              this.time=[];
              this.time1=[];
              this.loader.hide();
          }
          if(s==(data["dates"].length))
          {
            break;

          }
        }
    },(err) => {
      this.loader.hide();
    });
  }
  tConvert (time) {

      var len = time.length;
      for(var i=0;i<len;i++)
      {
        var h = time[i].split("..");
        var localtime=moment.utc(h[0]).local().format("HH:mm:ss");
        var local=moment.utc(h[1]).local().format("HH:mm:ss");
        this.time={starttime:localtime,endtime:local};
        this.time1.push(this.time);
      }
      this.showTime.push(this.time1);

  }
  toggleSection(i)
  {
      this.dateArrayShow[i].open = !this.dateArrayShow[i].open;
  }

  toggleItem(i,j)
  {
      this.dateArrayShow[i].time[j].open = !this.dateArrayShow[i].time[j].open;
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentLocationPage } from './new-shipment-location';

@NgModule({
  declarations: [
    NewShipmentLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(NewShipmentLocationPage),
  ],
})
export class NewShipmentLocationPageModule {}

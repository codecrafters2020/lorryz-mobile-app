import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { NewShipmentDropOffPage } from '../pages';
import { NewShipmentPage } from '../pages';

declare var google;
@IonicPage()
@Component({
  selector: 'page-new-shipment-location',
  templateUrl: 'new-shipment-location.html',
})
export class NewShipmentLocationPage {
  lat: any;
  lng: any;
  address: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private cd: ChangeDetectorRef,public alertCtrl: AlertController) {
    this.lat = "";
    this.lng = "";
    this.address = "";
  }

  ionViewDidEnter() {
  }

  ngAfterViewInit() {
    let input = <HTMLInputElement>document.getElementById("auto");
    let options = {
      componentRestrictions: {
        // country: 'ae',
        // types: ['(regions)']
      }
    }
    let self = this;
    let autoComplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autoComplete, 'place_changed', () => {
      var place =  autoComplete.getPlace();
      
      self.cd.detectChanges();
      self.lat = place.geometry.location.lat();
      self.lng = place.geometry.location.lng();
      self.address = place.formatted_address;
      console.log('place_changed', place.geometry.location.lat(), place.geometry.location.lng());
    });
  }

  newShipmentDropOff(){
    if(this.lat && this.lng && this.address){
      this.navCtrl.push(NewShipmentDropOffPage, {start_lat: this.lat, start_lng: this.lng, pickup_location: this.address});
    }else{
      let alert = this.alertCtrl.create({
        title: 'ERROR',
        subTitle: 'Please add address.',
        buttons: ['Dismiss']
      });
      alert.present();
    }  
  }

  newShipmentmap(){
    this.navCtrl.pop();  
  }

  openPosted(){
    this.navCtrl.setRoot("PostedShipmentPage");  
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");  
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");  
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");  
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");  
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

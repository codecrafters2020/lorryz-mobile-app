import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetOngoingShipmentMapPage } from './fleet-ongoing-shipment-map';

@NgModule({
  declarations: [
    FleetOngoingShipmentMapPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetOngoingShipmentMapPage),
  ],
})
export class FleetOngoingShipmentMapPageModule {}

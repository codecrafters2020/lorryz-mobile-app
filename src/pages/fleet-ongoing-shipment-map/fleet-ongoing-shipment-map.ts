import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController, Platform  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

declare var google;
@IonicPage()
@Component({
  selector: 'page-fleet-ongoing-shipment-map',
  templateUrl: 'fleet-ongoing-shipment-map.html',
})
export class FleetOngoingShipmentMapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  vehicles :any;
  markers :any;
  interval: any;
  infoWindows: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private callNumber: CallNumber,
    public platform: Platform,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    public globalVar: GlobalVars) {
    this.vehicles = [];
    this.markers = [];
    this.interval = "";
    this.infoWindows = [];
  }

  ionViewDidLoad() {
    this.loadVehicles();
    let self = this;
    this.interval = setInterval(function () {
      console.log("here")
      self.loadMarkers();;
    }, 10000);
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  loadVehicles(){
    this.shipmentProvider.ongoing_vehicles(this.globalVar.current_user.company_id).subscribe(data => {
      this.vehicles = data;
      if (this.platform.is('cordova')) {
        this.diagnostic.isLocationEnabled().then((enabled) => {
          if (enabled){
            this.askForHighAccuracy();
          }else{
            this.locationAccuracy.canRequest().then((canRequest: boolean) => {
              this.askForHighAccuracy();
            });
          }
        })
      }else{
        this.load_map();
      }
    },(err) => {
    });
  }

  askForHighAccuracy() {
   this.locationAccuracy
     .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
       this.load_map();
     });
  }

  load_map(){
    if (this.map) {
      return this.map;
    }
    
    let latLng = ""
    if (this.vehicles.length > 0){
      latLng = new google.maps.LatLng(this.vehicles[0]["latitude"], this.vehicles[0]["longitude"]);
      let mapOptions = {
        center: latLng,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }else{
      this.loader.show("Loading the Map");
      this.geolocation.getCurrentPosition().then((position) => {
        this.loader.hide();     
        latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          zoomControl: false,
          streetViewControl: false
        }
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      }, (err) => {
        console.log(err);
      });
    }
    this.loadMarkers();
  }

  loadMarkers(){
    this.deleteMarkers();
    for (let i = 0; i < this.vehicles.length; i++) {
      let updatelocation = new google.maps.LatLng(this.vehicles[i]["latitude"], this.vehicles[i]["longitude"]);
      this.addMarker(updatelocation, this.vehicles[i]);
    }
    this.setMapOnAll(this.map);
  }

  addMarker(location, vehicle) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: 'assets/img/truck_pin.png'
    });
    this.markers.push(marker);
      let content = "<div class='map-pin-holder'>"+
            "<h1>Shipment No."+vehicle.shipment_id+"</h1>"+
            "<div class='pin-row clearfix'>"+
              "<div class='fleet-col-left'>"+
                "<div class='truck-row'>"+
                  "<span><img src='assets/img/truck.png' alt='' ></span>"+
                  "<span>"+vehicle.vehicle_type+"</span>"+
                "</div>"+
                "<div class='truck-number'>"+vehicle.registration_number+"</div>"+
              "</div>"+
              "<div class='fleet-col-right'>"+
                  "<div class='address-outer'>"+
                      "<div class='address-one'>"+
                          vehicle.pickup_location+
                      "</div>"+
                      "<div class='address-two'>"+
                          vehicle.drop_location+
                      "</div>"+
                  "</div>"+
            "</div>"+
            "</div>"+
          "</div>";

    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
       this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });

    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");  
  }
  
  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");  
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");  
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");  
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");  
  }

  ongoingShipmentList(){
  	this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

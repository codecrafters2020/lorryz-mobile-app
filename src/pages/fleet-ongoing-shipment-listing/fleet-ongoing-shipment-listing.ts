import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader, VehicleProvider} from '../../providers/providers';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-fleet-ongoing-shipment-listing',
  templateUrl: 'fleet-ongoing-shipment-listing.html',
})
export class FleetOngoingShipmentListingPage {

  public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  interval: any;
  current_position: any;
  lat: any;
  lng: any;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public globalVar: GlobalVars,
    private geolocation: Geolocation,
    public vehicle: VehicleProvider) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }
   this.shipments = [];
    this.interval = " "
  }

  ionViewDidLoad() {
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
    this.loader.show("");
    this.page = 1
    this.fetchShipments()
    if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {

     // self.trackTransport();
      this.pickupnow = true;
    //  this.automaticRefresh();

    }

  }
   delay(ms: number) {

  return new Promise( resolve => setTimeout(resolve, ms) );
}


   trackTransport()
   {
     console.log("data")

          this.geolocation.getCurrentPosition().then((position) => {
     console.log("datas")

             this.current_position = position;
            this.lat = this.current_position.coords.latitude
            this.lng = this.current_position.coords.longitude
                 let vehicle = {id: this.shipments[0].vehicle_shipments[0].vehicle_id,
                   latitude: this.lat,longitude: this.lng}
                   console.log("datass")

                 this.vehicle.update_lat_lng(vehicle, this.shipments[0].vehicle_shipments[0].vehicle_id)
                 .subscribe(async data => {
                    console.log(data)

                 });

             });
   }

  // automaticRefresh() {

  //   let self = this
  //   this.interval = setInterval(function () {
  //     self.trackTransport();
  //  }, 20000);
  // }


    ngOnDestroy() {
      if (this.interval) {
        clearInterval(this.interval);
      }
    }


  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.ongoing(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }


      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }
  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  openOngoingDetail(shipment_id){
    this.navCtrl.push("FleetOngoingShipmentDetailPage", {shipment_id: shipment_id});
  }

  ongoingShipmentMap(){
    this.navCtrl.setRoot("FleetOngoingShipmentMapPage");
  }
  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

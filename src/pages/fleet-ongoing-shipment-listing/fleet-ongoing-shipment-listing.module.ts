import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetOngoingShipmentListingPage } from './fleet-ongoing-shipment-listing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetOngoingShipmentListingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetOngoingShipmentListingPage),
    TranslateModule.forChild()
  ],
})
export class FleetOngoingShipmentListingPageModule {}

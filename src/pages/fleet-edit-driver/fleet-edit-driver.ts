import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,ActionSheetController } from 'ionic-angular';
import { User,DriverProvider,Loader,GlobalVars } from '../../providers/providers';

import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AwsProvider } from './../../providers/aws/aws';


import { Storage } from '@ionic/storage';
import b64toBlob from "b64-to-blob";

import { Company } from '../../providers/company/company';

import { parseNumber } from 'libphonenumber-js'
import list from 'country-list';
let countries = list();


@IonicPage()
@Component({
  selector: 'page-fleet-edit-driver',
  templateUrl: 'fleet-edit-driver.html',
})
export class FleetEditDriverPage {

  public mobile: string
  	public full_name: string
  	public national_id: string
  	public license: string
  	public expiry_date: string
    public license_expiry: string
  	public _countries: any
  	public country_code: any
  	public driver_detail: any
    license_selected_documents = [];
    license_documents = [];

    emirate_selected_documents = [];
    emirate_documents = [];

    avatar: String
    selected_avatar: String;
    minDate: any;
    constructor(
                    public navCtrl: NavController, 
                    public navParams: NavParams,
                    public company: Company,
                    public storage: Storage,
                    public toastCtrl: ToastController,
                    public globalVar: GlobalVars,
                    private awsProvider: AwsProvider,
                    private file: File, 
                    private camera: Camera,
                    private actionSheetCtrl: ActionSheetController,
                    public loader: Loader,
                    public user: User, 
                    public driver: DriverProvider,
                    public zone: NgZone,
                    

                    ) {
    	this._countries = [];
      this.minDate = new Date().toISOString()
    }

    ionViewDidLoad() {
     
     this.user.getCountries().subscribe(data => {
       this._countries = data["countries"];
     },(err) => {
     });

     let id = this.navParams.get('driver_id')
     this.loader.show("");
     this.driver.getDriver(id).subscribe((resp) => {
       this.loader.hide();
       
       this.mobile=resp["mobile"];
       this.full_name= resp["first_name"]+' '+resp["last_name"];
       this.national_id= resp["driver"]["national_id"];
       this.license= resp["driver"]["license"];
       this.expiry_date= resp["driver"]["expiry_date"];
       this.license_expiry= resp["driver"]["license_expiry"];
       this.country_code=resp["country"]["dialing_code"];

       this.avatar = resp["driver"]["avatar"]  
       this.selected_avatar = resp["driver"]["avatar"]
       this.license_documents = resp["driver"]["license_documents"].slice();
       this.license_selected_documents = resp["driver"]["license_documents"].slice();

       this.emirate_documents = resp["driver"]["emirate_documents"].slice();
       this.emirate_selected_documents = resp["driver"]["emirate_documents"].slice();
       

       this.driver_detail = resp;
     }, (err) => {
       this.loader.hide();
       let toast = this.toastCtrl.create({
         message: err && err.error.errors ? err.error.errors[0] : 'Something Went wrong',
         duration: 6000,
         position: 'top',
         showCloseButton: true,
         closeButtonText: "x",
         cssClass: "toast-danger"
       });
       toast.present();
     });

    }

    remove(doc){
      
      this.license_documents.splice( this.license_documents.indexOf(doc), 1 );  
      this.license_selected_documents.splice( this.license_selected_documents.indexOf(doc), 1 ); 
      
      this.emirate_documents.splice( this.emirate_documents.indexOf(doc), 1 );  
      this.emirate_selected_documents.splice( this.emirate_selected_documents.indexOf(doc), 1 ); 
      

      
    }

    verifyPhone(){
       let _mobile =  this.mobile
       
       let that = this;

       let country = this._countries.filter(obj => {
         return obj.dialing_code == that.country_code
       })[0]
    
       let country_initial = countries.getCode(country.name)

       let _parse_number = parseNumber('Phone: '+_mobile, country_initial, { extended: true })
       return _parse_number
     }

    updateDriver(id) {


      let num = this.verifyPhone()

      if (!num["valid"]) {

        let toast = this.toastCtrl.create({
          message: "Phone number is not valid",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      }else{

      if(this.validateDoc()){
        return false;
      }
      
        let account = {};
        let _mobile = '+'+num["countryCallingCode"] + num["phone"]
        account['driver'] = {
                              mobile: _mobile,
                              full_name: this.full_name,
                              national_id: this.national_id,
                              license: this.license,
                              expiry_date: this.expiry_date,
                              license_expiry: this.license_expiry,
                            	country_code: this.country_code,
                              avatar: this.avatar,
                              license_documents: this.license_documents,
                              emirate_documents: this.emirate_documents
                            }


                            
                            
                            
                            
                            
        this.loader.show("");
        this.driver.updateDriver(account,id).subscribe((resp) => {
          this.loader.hide();
          if (resp && resp["error"]) {
            let toast = this.toastCtrl.create({
                      message: resp["error"] ? resp["error"] : 'Something Went wrong',
                      duration: 6000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-danger"
                    });
                    toast.present();
          }else{
            this.navCtrl.setRoot('DriverListingPage'); 
          }
          

        }, (err) => {
          this.loader.hide();
        });
      }
    }

    
    validateDoc(){

      
      if (this.license_documents.length == 0) {
        let toast = this.toastCtrl.create({
          message: "License documents is mandatory",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        return true
      }else if(this.emirate_documents.length == 0){
        let toast = this.toastCtrl.create({
          message: "ID documents is mandatory",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        return true
      }
    }

    takePicture(sourceType) {
      const options: CameraOptions = {
        quality: 100,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: sourceType
      }
       
      this.camera.getPicture(options).then((imageData) => {
        this.selected_avatar = imageData;
        let that = this;
        this.loader.show("uploading...");
        
        let ext = 'jpg';
        let type = 'image/jpeg'
        let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
        
        this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {
          console.log("Success in getSignedUploadRequest",data)
          that.avatar =  data["public_url"]
          console.log("i am in public url",data["public_url"])

            that.zone.run(() => {
              let request = new XMLHttpRequest();
              request.open('GET', imageData, true);
              request.responseType = 'blob';
              request.timeout = 360000;
              request.onload = function() {

                let reader = new FileReader();
                reader.readAsDataURL(request.response);
                
                reader.onload =  function(e){

                  that.zone.run(() => {
                    console.log("error",e);
                    let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                    that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                       that.loader.hide();
                       
                       console.log("result that i got after uploading to aws",_result)
                    });
                  })

                }

                reader.onerror =  function(e){
                  console.log('We got an error in file reader:::::::: ', e);
                };

                reader.onabort = function(event) {
                  console.log("reader onabort",event)
                }

              };

              request.onerror = function (e) {
                that.loader.hide();
                console.log("** I got an error in XML Http REquest",e);
              };

              request.ontimeout = function(event) {
                 that.loader.hide();
                 console.log("xmlhttp ontimeout",event)
              }

              request.onabort = function(event) {
                 that.loader.hide();
                 console.log("xmlhttp onabort",event);
              }


              request.send();
            });
        }, (err) => {
          that.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
          console.log('getSignedUploadRequest timeout error: ', err);
        });

      }, (err) => {
        let toast = this.toastCtrl.create({
          message: "Unable to Access Gallery. Please Try Later",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();

        console.log('err: ', err);
      });
    }


    takeDocuments(sourceType,license) {

        const options: CameraOptions = {
          quality: 100,
          correctOrientation: true,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: sourceType
        }
        let that = this;
        this.camera.getPicture(options).then((imageData) => {
          if (license) {
            that.license_selected_documents.push(imageData);  
          }else{
            that.emirate_selected_documents.push(imageData);  
          }
          
          
          that.loader.show("uploading...");
          
            let ext = 'jpg';
            let type = 'image/jpeg'
            let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
            
            this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {
              
              if (license) {
                that.license_documents.push(data["public_url"]);
              }else{
                that.emirate_documents.push(data["public_url"]);
              }

              that.zone.run(() => {
                let request = new XMLHttpRequest();
                request.open('GET', imageData, true);
                request.responseType = 'blob';
                request.onload = function() {

                  let reader = new FileReader();
                  reader.readAsDataURL(request.response);
                  
                  reader.onload =  function(e){

                    that.zone.run(() => {
                      let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                      that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                         that.loader.hide();
                         console.log("result that i got after uploading to aws",_result)
                        }, (err) => {
                          that.loader.hide();
                          console.log('err: ', err);
                      });
                     });
                  }
                }
                request.onerror = function (e) {
                  that.loader.hide();
                  console.log("** I got an error in XML Http REquest",e);
                };

                request.ontimeout = function(event) {
                   that.loader.hide();
                   console.log("xmlhttp ontimeout",event)
                }

                request.onabort = function(event) {
                   that.loader.hide();
                   console.log("xmlhttp onabort",event);
                }

                request.send();
              })
            }, (err) => {
              that.loader.hide();
              let toast = this.toastCtrl.create({
                message: "Unable to send request. Check wifi connection",
                duration: 6000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-danger"
              });
              toast.present();
              console.log('getSignedUploadRequest timeout error: ', err);
            });
          }, (err) => {

            let toast = this.toastCtrl.create({
              message: "Unable to Access Gallery. Please Try Later",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

            console.log('err: ', err);
          });


      }

    presentActionSheet() {
       let actionSheet = this.actionSheetCtrl.create({
         title: 'Select Image Source',
         buttons: [
           {
             text: 'Load from Library',
             handler: () => {
               this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
             }
           },
           {
             text: 'Use Camera',
             handler: () => {
               this.takePicture(this.camera.PictureSourceType.CAMERA);
             }
           },
           {
             text: 'Cancel',
             role: 'cancel'
           }
         ]
       });
       actionSheet.present();
     }
     
    presentActionSheetForDocuments(license = false) {
        let actionSheet = this.actionSheetCtrl.create({
          title: 'Select Image Source',
          buttons: [
            {
              text: 'Load from Library',
              handler: () => {
                this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY,license);
              }
            },
            {
              text: 'Use Camera',
              handler: () => {
                this.takeDocuments(this.camera.PictureSourceType.CAMERA,license);
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        });
        actionSheet.present();
      }

    showNotification(){
      this.navCtrl.push("NotificationPage")
    }
    openDocument(doc){
      window.open(doc,'_system', 'location=yes');
      // const browser = this.iab.create(doc);
    }

    valid_format(file) {        
      return file.includes(".jpg") || file.includes(".jpeg") || file.includes(".png")        
    }  

  }


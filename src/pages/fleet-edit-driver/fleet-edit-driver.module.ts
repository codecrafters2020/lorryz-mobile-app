import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetEditDriverPage } from './fleet-edit-driver';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    FleetEditDriverPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetEditDriverPage),
    TranslateModule.forChild()
  ],
})
export class FleetEditDriverPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetActiveShipmentBiddingPage } from './fleet-active-shipment-bidding';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetActiveShipmentBiddingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetActiveShipmentBiddingPage),
    TranslateModule.forChild()
  ],
})
export class FleetActiveShipmentBiddingPageModule {}

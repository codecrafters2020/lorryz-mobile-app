import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController, AlertController } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader, VehicleProvider} from '../../providers/providers';
import * as moment from 'moment';
import { interval } from 'rxjs/observable/interval';
import { Geolocation } from '@ionic-native/geolocation';
declare var google;
declare var require: any;
@IonicPage()
@Component({
  selector: 'page-fleet-active-shipment-bidding',
  templateUrl: 'fleet-active-shipment-bidding.html',
})
export class FleetActiveShipmentBiddingPage {

  public shipments: any;
  public timeInterval = 10000;
  public subscriber: any;
  bid: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  pickuptime: any;
  str: any;
  temp_time: number;
  time: number;
  x: number;
  current_position: any;
  lat: any;
  lng: any;
  shipment_ongoing
  interval: any;
  apiKey: string;
  options = {

   };
   shipment_active_array=[];
  rebid: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public alertCtrl: AlertController,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    private geolocation: Geolocation,
    public vehicle: VehicleProvider) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }

    this.shipments = [];
    this.shipment_ongoing = [];
    this.interval = "";
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };
    this.shipment_active_array=[];
  }

  ionViewDidLoad() {
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
      // this.rebid =this.globalVar.rebid
      // console.log(this.globalVar.rebid)
      if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {
      this.pickupnow = true;
    }

    this.loader.show("");
    this.page = 1

    this.fetchShipments()

      this.automaticRefresh()

  }

  automaticRefresh() {
    let self = this
    this.interval = setInterval(function () {
      self.page=1
      self.fetchShipments();

    }, 10000);

  }


  cancelShipments(shipment)
  {
    if(shipment.is_pickup_now)
    {
      // this.globalVar.rebid = false
      this.shipmentProvider.quitBid(this.globalVar.current_user.company_id,shipment.id).subscribe(data => {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: "Bid Cancelled on Active Shipments",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });
        toast.present();
        // this.navCtrl.setRoot('FleetActiveShipmentBiddingPage')
      },(err) => {
        this.loader.hide();
      });
    }
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.active(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }
      const self = this;
      if(this.shipments.length!=0)
      {
        if(this.pickupnow)
        {
          this.pickuptime = this.shipments[0].pickup_time;
          this.str = this.pickuptime.split('T');
          var s = this.str[1].split('.')
          var shipmenttime = moment.duration(s[0]).asSeconds();

          // var h = this.str[1].split(':')
          // var h1 = h[0];
          // h1 = h1*3600;
          // h1=parseInt(h1);
          // var min = h[1];
          // min = min*60;
          // min=parseInt(min);
          // var sec = h[2]
          // sec=parseInt(sec);
          // var shipmenttime = h1+min+sec-60;
          // console.log("shipment pickuptime "+shipmenttime);

          var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');

          var local = moment(date).add(15, 'minutes').format(' HH:mm:ss');

          var localtime = moment.duration(local).asSeconds()
          var time = localtime-shipmenttime;

          if(time >= 180)
          {
            this.cancelShipments(this.shipments[0]);
          }

          if(this.globalVar.lang==true){
            if(this.shipment_active_array.length ==0 || this.shipment_active_array[0].shipment_id != this.shipments[0].shipment_id){
              var googleTranslate = require('google-translate')(this.apiKey, this.options);
              debugger;;
              googleTranslate.translate( self.shipments[0].drop_location, 'ur', function(err, translation) {
                self.shipments[0].drop_location = translation.translatedText; });
              googleTranslate.translate(self.shipments[0].pickup_location, 'ur', function(err, translation) {
                self.shipments[0].pickup_location = translation.translatedText; });
            console.log(this.shipments[0]);
            console.log("I AM TRANSALATED");
            }else{
              self.shipments[0].drop_location = this.shipment_active_array[0].drop_location;
              self.shipments[0].pickup_location = this.shipment_active_array[0].pickup_location;
              console.log("I AM NOT");
            }
        }
          this.shipment_active_array = this.shipments;
        }
      }

      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  addItem(shipment) {
    this.navCtrl.push("FleetRebidShipmentBiddingPage",{shipment: shipment});
  }

  getBidAmount(shipment){
    let that = this;
    return shipment.bids.filter(function(e){return e.company_id == that.globalVar.current_user.company_id})[0].amount
  }
  quitBid(shipment){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: "Are you sure you want to quit bidding?" ,
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },

      {
        text: 'Yes',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.quitBid(this.globalVar.current_user.company_id,shipment.id).subscribe(data => {
            this.loader.hide();
            let toast = this.toastCtrl.create({
              message: "Bid Quit Successfully",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();
            this.navCtrl.setRoot('FleetActiveShipmentBiddingPage')
          },(err) => {
            this.loader.hide();
          });
        }
      },
    ]
    });
    alert.present();
  }
  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  ngOnDestroy()
  {
    let self=this
    console.log("view leave");
      if (this.interval) {
        clearInterval(this.interval);
      }


  }

}

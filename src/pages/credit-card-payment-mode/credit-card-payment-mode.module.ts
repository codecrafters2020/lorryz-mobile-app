import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreditCardPaymentModePage } from './credit-card-payment-mode';

@NgModule({
  declarations: [
    CreditCardPaymentModePage,
  ],
  imports: [
    IonicPageModule.forChild(CreditCardPaymentModePage),
  ],
})
export class CreditCardPaymentModePageModule {}

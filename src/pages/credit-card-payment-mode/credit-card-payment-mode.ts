import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CreditCardPaymentModePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-credit-card-payment-mode',
  templateUrl: 'credit-card-payment-mode.html',
})
export class CreditCardPaymentModePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreditCardPaymentModePage');
  }

  openBalance(){
    this.navCtrl.setRoot("BalancePaymentModePage");
  }
  openWithDraw(){
    this.navCtrl.setRoot("WithdrawPaymentModePage");
  }
  openCreditCard(){
    this.navCtrl.setRoot("CreditCardPaymentModePage");
  }

}

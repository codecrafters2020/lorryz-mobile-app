import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetChangeDriverModalPage } from './fleet-change-driver-modal';

@NgModule({
  declarations: [
    FleetChangeDriverModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetChangeDriverModalPage),
  ],
})
export class FleetChangeDriverModalPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,ViewController,ModalController } from 'ionic-angular';
import { VehicleListing,DriverListing} from '../pages'
import { User,DriverProvider,Loader,VehicleProvider } from '../../providers/providers';


@IonicPage()
@Component({
  selector: 'page-fleet-change-driver-modal',
  templateUrl: 'fleet-change-driver-modal.html',
})
export class FleetChangeDriverModalPage {
	// public vehicle: any;
	public drivers: any;
  public _vehicle: any;
  public driver_id: any;
  public current_driver: any;
  public loaded: any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public driver: DriverProvider,
              public toastCtrl: ToastController,
              public loader: Loader,
              public vehicle: VehicleProvider,
              private viewCtrl: ViewController,
              public modalCtrl: ModalController,) {
  	// this.vehicle = {};
  	this.drivers = [];
    this.loaded = false;
  }

  ionViewDidLoad() {

    this._vehicle = this.navParams.get('vehicle') || {};
    console.log("vehcile",this._vehicle);

    this.driver_id = this._vehicle.driver ? this._vehicle.driver.id: null
    this.loader.show("");
    let filter = {assignment: true, driver_id: this.driver_id}
    this.driver.getDrivers(filter).subscribe((resp) => {
        this.loader.hide();
        this.drivers= resp;
        this.loaded = true;

        this.current_driver = this.drivers.filter(obj => {
       return obj.id === this.driver_id
     })[0]
        

      }, (err) => {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });

  }

  changeDriver(vehicle_id){
    this.loader.show("");
    this.vehicle.changeDriver(this.driver_id,vehicle_id).subscribe((resp) => {
        this.viewCtrl.dismiss();
        this.loader.hide();
      }, (err) => {
        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });
  }

 dismiss() {
    this.viewCtrl.dismiss();
  }


  onSelectChange(selectedValue: any) {
     console.log('Selected', selectedValue);

     this.current_driver = this.drivers.filter(obj => {
       return obj.id === selectedValue
     })[0]

   }


}

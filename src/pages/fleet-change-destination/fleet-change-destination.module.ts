import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetChangeDestinationPage } from './fleet-change-destination';

@NgModule({
  declarations: [
    FleetChangeDestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetChangeDestinationPage),
  ],
})
export class FleetChangeDestinationPageModule {}

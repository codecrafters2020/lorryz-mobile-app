import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;
@IonicPage()
@Component({
  selector: 'page-fleet-change-destination',
  templateUrl: 'fleet-change-destination.html',
})
export class FleetChangeDestinationPage {
	drop_location: any;
	shipment: any;
	shipment_obj: any;
  constructor(public navCtrl: NavController,
   	public navParams: NavParams,
   	public shipmentProvider: FleetShipment,
  	public toastCtrl: ToastController,
    public loader: Loader,
    public globalVar: GlobalVars,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
  	private viewCtrl: ViewController) {
  	this.drop_location = ""
  	this.shipment = this.navParams.get('shipment') || {}
  	this.shipment_obj = {shipment_id: this.shipment.id, lat: this.shipment.drop_lat, lng: this.shipment.drop_lng, address: this.shipment.drop_location, city: this.shipment.drop_city}
  }

  ionViewDidLoad() {
    console.log(this.shipment)
  }

  ngAfterViewInit() {  
    let input_drop = <HTMLInputElement>document.getElementById("auto_drop").getElementsByTagName('input')[0];
    let options = {
      componentRestrictions: {
        // country: this.globalVar.current_user.country.short_name,
        // types: ['(regions)']
      }
    }
    let self = this;
    let autoComplete_drop = new google.maps.places.Autocomplete(input_drop, options);
    google.maps.event.addListener(autoComplete_drop, 'place_changed', () => {
      var place =  autoComplete_drop.getPlace();
      self.shipment_obj.lat = place.geometry.location.lat();
      self.shipment_obj.lng = place.geometry.location.lng();
      self.shipment_obj.address = input_drop.value;
      self.shipment_obj.drop_building_name = place.name
      self.shipment_obj.city =  this.extractFromAdress(place.address_components, "locality")
    });

    this.geolocation.getCurrentPosition().then((position) => {       

      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });

      autoComplete_drop.setBounds(circle.getBounds());
      
    }, (err) => {
      console.log(err);
    });

  }

  updateShipment(){
    debugger
    if(this.shipment_obj.lat == this.shipment.drop_lat && this.shipment_obj.lng == this.shipment.drop_lng){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your old and new destination is same. Please enter a new destination to proceed.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.loader.show("");
      this.shipmentProvider.update_destination(this.shipment_obj, this.shipment_obj.shipment_id, this.globalVar.current_user.company_id).subscribe(data => {
        this.loader.hide();
        console.log(data)
        this.viewCtrl.dismiss(data);
      }, (err) => {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: err.error.errors,
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present(); 
      });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(this.shipment);
  }

  extractFromAdress(components, type){
    for (var i=0; i<components.length; i++)
      for (var j=0; j<components[i].types.length; j++)
        if (components[i].types[j]==type) return components[i].long_name;
    return "";
  }
}

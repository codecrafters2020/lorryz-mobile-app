import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompletedShipmentPage } from './completed-shipment';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    CompletedShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(CompletedShipmentPage),
    Ionic2RatingModule
  ],
})
export class CompletedShipmentPageModule {}

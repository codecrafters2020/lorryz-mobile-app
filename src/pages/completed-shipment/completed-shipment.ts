import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader,Shipment} from '../../providers/providers';
import { Ionic2Rating } from "ionic2-rating";
import { ForegroundService } from '@ionic-native/foreground-service/ngx';

@IonicPage()
@Component({
  selector: 'page-completed-shipment',
  templateUrl: 'completed-shipment.html',
})
export class CompletedShipmentPage {

  public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  ongoing: any;
  id: any;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
    public shipmentProvider: Shipment,
    public loader: Loader,
    public alertCtrl: AlertController,
    public globalVar: GlobalVars,
    public foregroundService: ForegroundService) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }
      this.company_type = this.globalVar.current_user.company.company_type
      this.company_category = this.globalVar.current_user.company.category
      if((this.company_type=='individual')&&(this.company_category=='cargo'))
      {
        this.pickupnow=true;
      }

    this.shipments = [];
  }

  ionViewDidLoad() {
    this.ongoing = this.navParams.get("ongoing")
    this.id = this.navParams.get("id")
    this.loader.show("");
    this.page = 1
    this.fetchShipments()
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.completed(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
        // if(this.pickupnow)
        // {
          if(this.ongoing)
          {
            for(var i=0; i<this.shipments.length;i++)
            {
              if(this.shipments[i].id==this.id)
              {
                this.navCtrl.push("CompletedShipmentDetailPage",{shipment: this.shipments[i]});
              }
            }
          }

        // }
      }


      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }
  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  openPosted(){
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }

  openCompletedDetail(shipment){
    this.navCtrl.push("CompletedShipmentDetailPage",{shipment: shipment});
  }
  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

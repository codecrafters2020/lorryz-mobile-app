import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetAddVehiclePage } from './fleet-add-vehicle';

@NgModule({
  declarations: [
    FleetAddVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(FleetAddVehiclePage),
  ],
})
export class FleetAddVehiclePageModule {}

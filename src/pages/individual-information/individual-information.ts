import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,ActionSheetController } from 'ionic-angular';

import { User,GlobalVars,Loader } from '../../providers/providers';
import { NewShipmentPage } from '../pages';
import { Company } from '../../providers/company/company';
import { Storage } from '@ionic/storage';

import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AwsProvider } from './../../providers/aws/aws';
import b64toBlob from "b64-to-blob";

@IonicPage()
@Component({
  selector: 'page-individual-information',
  templateUrl: 'individual-information.html',
})
export class IndividualInformationPage {
  public secondary_mobile_no: String;
  public landline_no: String;
  public national_id: String;
  public expiry_date: String;
  public news_update: any;
  public password: String;
  public confirm_password: String;
  public current_password: String;
  selected_documents = [];
  documents = [];
  avatar: String
  selected_avatar: String;
  minDate: any;
  tax_registration_no: any;
  cnicNotValid: boolean=false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public company: Company,
              public storage: Storage,
              public toastCtrl: ToastController,
              public globalVar: GlobalVars,
              private awsProvider: AwsProvider,
              private file: File,
              private camera: Camera,
              private actionSheetCtrl: ActionSheetController,
              public loader: Loader,
              public zone: NgZone,
              public user: User,) {
    this.minDate = new Date().toISOString()
    this.loader.show("");
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad IndividualInformationPage');
    this.landline_no = "null";
    this.secondary_mobile_no = "null";
    if(this.globalVar.current_user.role=='fleet_owner' || this.globalVar.current_user.role=='cargo_owner')
    {
      this.cnicNotValid=true;
    }
  }


  ionViewWillEnter(){
    console.log('ionViewDidLoad IndividualInformationPage1');
    this.updateCurrentUser()
    this.storage.get('currentUser').then(user => {
      console.log('ionViewDidLoad IndividualInformationPage2');
      if (user) {
        this.globalVar.current_user = user;
        if(this.globalVar.current_user.role=='fleet_owner' )
        {
          this.national_id=' ';
          this.cnicNotValid=true;
        }
        if(this.globalVar.current_user.role=='cargo_owner')
        {
          this.national_id=' ';
          this.cnicNotValid=true;
        }
        this.company.getIndividualInformation().timeout(360000).subscribe((resp) => {
          if(resp){
            this.loader.hide();
            console.log('ionViewDidLoad IndividualInformationPage3');
            this.secondary_mobile_no = resp["secondary_mobile_no"]
            this.landline_no = resp["landline_no"]
            this.national_id = resp["national_id"]
            this.expiry_date = resp["expiry_date"]
            this.news_update =  true;
            this.avatar = resp["avatar"]
            this.selected_avatar = resp["avatar"]
            this.documents = resp["documents"].slice();
            this.selected_documents = resp["documents"].slice();
            this.tax_registration_no = this.globalVar.current_user.company.tax_registration_no
          }else{
           this.loader.hide();
           console.log('ionViewDidLoad IndividualInformationPage4');
           if (resp && resp["error"]) {
             let toast = this.toastCtrl.create({
               message: resp && resp["error"] ? resp["error"] : 'Something went wrong',
               duration: 6000,
               position: 'top',
               showCloseButton: true,
               closeButtonText: "x",
               cssClass: "toast-danger"
             });
             toast.present();
           }

          }
        }, (err) => {
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Some Went Wrong. Please Try Later",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        });
      }

    });

  }


  updateCurrentUser(){
    // this.loader.show('');
    this.user.getCurrentUser().subscribe((resp) => {
      let user = resp["user"]
      this.storage.set('currentUser',user);
      this.globalVar.current_user = user;
      // this.loader.hide();
      console.log('ionViewDidLoad IndividualInformationPage5');
    }, (err) => {
      this.loader.hide();
    });

  }
  remove(doc){
    this.documents.splice( this.documents.indexOf(doc), 1 );
    this.selected_documents.splice( this.selected_documents.indexOf(doc), 1 );


  }

  landToMain(){
  if(!(this.globalVar.current_user.company.company_type=="individual"))
  {
      this.expiry_date=" ";

    if(this.validateDoc()){
      return false;
    }
  }
    let data = {individual_informations: {
        secondary_mobile_no: this.secondary_mobile_no,
        landline_no: this.landline_no,
        national_id: this.national_id,
        expiry_date: this.expiry_date,
        news_update: this.news_update,
        password: '',
        confirm_password: '',
        current_password: this.current_password,
        avatar: this.avatar,
        documents: this.documents,
        tax_registration_no: this.tax_registration_no
      }
    }

    console.log("i am in working on adding indiviudla information",data);
    this.loader.show("");
    this.company.addIndividualInformation(data).subscribe((resp) => {
      this.loader.hide();
      if(resp && resp["success"] ){
        this.storage.set('currentUser',resp["user"]);
        this.globalVar.current_user = resp["user"];
        if(this.globalVar.current_user.company.category == "cargo")
        {
            this.navCtrl.setRoot('NewShipmentPage');
        }
        else if(this.globalVar.current_user.company.company_type == "individual" &&
        this.globalVar.current_user.company.category == "fleet")
        {
          this.navCtrl.setRoot('FleetAddVehiclePage');
        }
        else{
          debugger;
          let toast = this.toastCtrl.create({
         // message: resp["success"],
         message: "Your account will be approved shortly by Lorryz Team",
         duration: 6000,
         position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
       });
       toast.present();
       this.logout();

        }}


    else{
        this.loader.hide();
       let toast = this.toastCtrl.create({
         message: resp["error"],
         duration: 6000,
         position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
       });
       toast.present();
    }}




    , (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });


  }


  logout() {
    this.loader.show("");
    this.user.logout(this.globalVar.current_user).subscribe(resp => {
      this.loader.hide();
    });
    this.globalVar.current_user = "";
    this.storage.remove("currentUser").then(() => {
      this.navCtrl.setRoot("LoginPage");
    });
  }


  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.selected_avatar = imageData;

      if (this.globalVar.current_user.information){
        this.globalVar.current_user.information.avatar = imageData;
        //imageData
      }

      let that = this;
      this.loader.show("uploading...");

      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {
        console.log("Success in getSignedUploadRequest",data)
        that.avatar =  data["public_url"]
        console.log("i am in public url",data["public_url"])

          that.zone.run(() => {
            let request = new XMLHttpRequest();
            request.open('GET', imageData, true);
            request.responseType = 'blob';
            request.timeout = 360000;
            request.onload = function() {

              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              reader.onload =  function(e){

                that.zone.run(() => {
                  console.log("error",e);
                  let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                     that.loader.hide();

                     console.log("result that i got after uploading to aws",_result)
                  });
                })

              }

              reader.onerror =  function(e){
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function(event) {
                console.log("reader onabort",event)
              }

            };

            request.onerror = function (e) {
              that.loader.hide();
              console.log("** I got an error in XML Http REquest",e);
            };

            request.ontimeout = function(event) {
               that.loader.hide();
               console.log("xmlhttp ontimeout",event)
            }

            request.onabort = function(event) {
               that.loader.hide();
               console.log("xmlhttp onabort",event);
            }


            request.send();
          });
      }, (err) => {
        that.loader.hide();
        let toast = this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
      });

    }, (err) => {
      let toast = this.toastCtrl.create({
        message: "Unable to Access Gallery. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();

      console.log('err: ', err);
    });
  }


  takeDocuments(sourceType) {

    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    let that = this;
    this.camera.getPicture(options).then((imageData) => {

    that.selected_documents.push("assets/img/avatar.jpeg");
    that.loader.show("uploading...");

      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **",newName);

      this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest",data);

        that.documents.push(data["public_url"]);
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', imageData, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function() {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader",reader);
              console.log("Step 1 got stuck in request.response",request)

              reader.onload =  function(){
                that.zone.run(() => {
                  console.log("Step 2 Loaded",reader.result);
                  let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                  console.log("blob",blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    that.loader.hide();
                    console.log("result that i got after uploading to aws",_result,that.documents)
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror =  function(e){
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function(event) {
                console.log("reader onabort",event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.hide();
            console.log("** I got an error in XML Http REquest",e);
          };

          request.ontimeout = function(event) {
             that.loader.hide();
             console.log("xmlhttp ontimeout",event)
          }

          request.onabort = function(event) {
             that.loader.hide();
             console.log("xmlhttp onabort",event);
          }


          request.send();
        })
      }, (err) => {
          that.loader.hide();
          console.log('getSignedUploadRequest timeout error: ', err);

          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();

        });

    }, (err) => {
      let toast = this.toastCtrl.create({
        message: "Unable to Access Gallery. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
      console.log('err: ', err);
    });
  }

  validateDoc(){
    if(this.documents.length == 0){
      let toast = this.toastCtrl.create({
        message: "ID Card is mandatory",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
      return true
    }
  }
presentActionSheet() {
   let actionSheet = this.actionSheetCtrl.create({
     title: 'Select Image Source',
     buttons: [
       {
         text: 'Load from Library',
         handler: () => {
           this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
         }
       },
       {
         text: 'Use Camera',
         handler: () => {
           this.takePicture(this.camera.PictureSourceType.CAMERA);
         }
       },
       {
         text: 'Cancel',
         role: 'cancel'
       }
     ]
   });
   actionSheet.present();
 }

 presentActionSheetForDocuments() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  openDocument(doc){
    window.open(doc,'_system', 'location=yes');
    // const browser = this.iab.create(doc);
  }

  valid_format(file) {
    return file.includes(".jpg") || file.includes(".jpeg") || file.includes(".png")
  }



}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndividualInformationPage } from './individual-information';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    IndividualInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(IndividualInformationPage),
    TranslateModule.forChild()
  ],
})
export class IndividualInformationPageModule {}

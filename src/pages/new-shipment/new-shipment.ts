import { Component, ViewChild, ElementRef, ChangeDetectorRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, Slides, AlertController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { NewShipmentDropOffPage } from '../pages';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { VehicleProvider, GeocoderProvider, GlobalVars, Loader } from '../../providers/providers';
import { Storage } from "@ionic/storage";
declare var google;
@IonicPage()
@Component({
  selector: 'page-new-shipment',
  templateUrl: 'new-shipment.html',
})
export class NewShipmentPage {
  @ViewChild('searchbar', {read: ElementRef}) searchbar: ElementRef;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;
  map: any;
  address: any;
  latLng: any;
  lat: any;
  lng: any;
  markers: any;
  infoWindows: any;
  marker: any;
  current_position: any;
  autoComplete: any;
  pickup_building_name: any;
  pickup_city: any;
  input: any;
  _place: string
  autocompleted_initialized: any;
  building;
    latitude;
  longitude;
  currentlocation: any;
  location: { building: any; address: any; };
  place: any;
  pickup_country: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public platform: Platform,
    private diagnostic: Diagnostic,
    public vehicle: VehicleProvider,
    public zone: NgZone,
    public geocoder: GeocoderProvider,
    private cd: ChangeDetectorRef,
    public globalVar: GlobalVars,
    public loader: Loader,
    private geolocation: Geolocation,
    public alertCtrl: AlertController,
    public keyboard: Keyboard,
    public storage: Storage,
    private locationAccuracy: LocationAccuracy) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }

    this.address = "";
    this.latLng = "";
    this.markers = "";
    this.infoWindows = [];
    this.current_position = "";
    this.autoComplete = "";
    this.pickup_building_name = "";
    this.pickup_city = "";
    this.pickup_country="";
    this.autocompleted_initialized = false
  }

  ngAfterViewInit() {

    this.initialize_autocomplete()


  }


  reInitializeAutocomplete(event) {
    debugger
    if (this.autocompleted_initialized == false) {
      this.autocompleted_initialized = true;
      // event.currentTarget.value=' ';
      this.initialize_autocomplete()
      this.fetch_nearest_autocomplete(this.current_position)
    }
    // else
    // {
    //   event.currentTarget.value=' ';
    // }
  }

  initialize_autocomplete() {
    this.input = this.searchbar.nativeElement.querySelector('.searchbar-input');
    let options = {}
    this.autoComplete = new google.maps.places.Autocomplete(this.input, options);
    let self = this;

    google.maps.event.addListener(this.autoComplete, 'place_changed', () => {
      let place = this.autoComplete.getPlace();
        this.place=place
      self.cd.detectChanges();
      this.zone.run(() => {
        self.lat = place.geometry.location.lat();
        self.lng = place.geometry.location.lng();
        self.address = this.input.value;
        self.pickup_building_name = place.name

        self.pickup_city = this.extractFromAdress(place.address_components, "locality");
        self.pickup_country = this.extractFromAdress(place.address_components, "country");
      });

      self.map.setCenter(place.geometry.location)
      self.marker.setPosition(place.geometry.location);

    });
  }

  ionViewDidEnter() {
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled) {
          this.askForHighAccuracy();
        } else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.askForHighAccuracy();
          });
        }
      })
    } else {
      this.loadMap();
    }
  }

  askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
        this.loadMap();
      });
  }

  loadMap() {

    if (this.map) {
      return this.map;
    }

    // this.loader.show("Loading the Map")
    this.geolocation.getCurrentPosition().then((position) => {
      // this.loader.hide()

      this.current_position = position;
      console.log(this.current_position);

      this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.currentlocation=this.latLng;
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
      var geocoder = new google.maps.Geocoder;
     this.latitude=this.lat
     this.longitude = this.lng

      geocoder.geocode({ 'location': this.latLng }, function (results, status) {
        console.log(results)
        if (status === google.maps.GeocoderStatus.OK) {
          if (results[0] && results[0].place_id && results[0].place_id !== '') {
            var request = {
              placeId: results[0].place_id,
              fields: ['name', 'formatted_address', 'place_id', 'geometry']
            };
              // console.log('City: ',results[0].address_components[4].short_name);
              // console.log('Country: ',results[0].address_components[8].long_name);
              // self.pickup_city=results[0].address_components[2].short_name;
              // self.pickup_country=results[0].address_components[8].long_name;
             self.pickup_city = self.extractFromAdress(results[0].address_components, "locality");

            self.geocoder.fetch_address_placeId(results[0].place_id)
            .then((data: any) => {
              self.address = data;
              })

            var service = new google.maps.places.PlacesService(self.map);

            service.getDetails(request, function (place, status) {
              if (status === google.maps.places.PlacesServiceStatus.OK) {
                self.input = place.formatted_address;
               // self.building = place.name;

                self.location = {building:self.building , address:self.input}

              }
              });

          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });

      let mapOptions = {
        center: this.latLng,
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false,
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let self = this;


      google.maps.event.addListenerOnce(this.map, 'idle', function () {
        self.loadTrucks(position.coords.latitude, position.coords.longitude)
        //Load the markers
        self.marker = new google.maps.Marker({
          map: this,
          // icon: 'assets/img/blue_pin_icon.png',
          animation: google.maps.Animation.DROP,
          position: self.latLng,
          // draggable:true
        });

        google.maps.event.addListener(self.map, 'click', function (e) {
          if (e.placeId && e.placeId !== '') {
            var request = {
              placeId: e.placeId,
              fields: ['name', 'formatted_address', 'place_id', 'geometry']
            };


            var service = new google.maps.places.PlacesService(self.map);

            service.getDetails(request, function (place, status) {
              if (status === google.maps.places.PlacesServiceStatus.OK) {
                self.pickup_building_name = place.name;
                self.building = place.name;
//                self.address = place.formatted_address;
              }
            });
          }

          self.building = "";
          self.pickup_building_name = "";
          self.keyboardCheck()
          self.focusButton()
          self.marker.setPosition(e.latLng); // set marker position to map center
          self.zone.run(() => {
            self.latLng = e.latLng;
            self.lat = e.latLng.lat();
            self.lng = e.latLng.lng();

            self.fetch_address(e);
          });
        });
        google.maps.event.addListener(self.map, 'drag', function (e) {
          self.keyboardCheck()
          self.focusButton()
        });
      });

      this.fetch_nearest_autocomplete(position)
    }, (err) => {
      console.log(err);
      this.loader.hide()
    });

  }

  keyboardCheck() {
    this.focusButton();
    setTimeout(() => {
      this.keyboard.hide();
    }, 500);
  }

  currentloc()
  {
  this.keyboardCheck();
  this.focusButton();
    this.building = '';
    this.pickup_building_name = '';
    this.address = this.location.address;
    this.lat=this.latitude;
    this.lng = this.longitude

    let mapOptions = {
      center: this.currentlocation,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    let self = this;

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      self.loadTrucks(self.latitude, self.longitude)
      //Load the markers
      self.marker = new google.maps.Marker({
        map: this,
        // icon: 'assets/img/blue_pin_icon.png',
        animation: google.maps.Animation.DROP,
        position: self.currentlocation,
        // draggable:true
      });

      google.maps.event.addListener(self.map, 'click', function (e) {
        self.focusButton()
        if (e.placeId && e.placeId !== '') {
          var request = {
            placeId: e.placeId,
            fields: ['name', 'formatted_address', 'place_id', 'geometry']
          };

          var service = new google.maps.places.PlacesService(self.map);

          service.getDetails(request, function (place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              self.pickup_building_name = place.name;
              self.building = place.name;
            }
          });
        }

        self.building = "";
        self.pickup_building_name = "";
         self.keyboardCheck()
         self.marker.setPosition(e.latLng); // set marker position to map center
        self.zone.run(() => {
          self.latLng = e.latLng;
          self.lat = e.latLng.lat();
          self.lng = e.latLng.lng();
          self.fetch_address(e);
        });
      });
      google.maps.event.addListener(self.map, 'drag', function (e) {
        self.keyboardCheck()
        self.focusButton()
      });
    });

  }
focusButton(): void {
  let self=this;
    setTimeout(() => {
//    self.searchbar.nativeElement.setFocus();
//    self.searchbar.nativeElement.blur();
    self.searchbar.nativeElement.onblur=true;
    }, 500);
  }
  fetch_nearest_autocomplete(position) {
    var geolocation = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    var circle = new google.maps.Circle({
      center: geolocation,
      radius: position.coords.accuracy
    });

    this.autoComplete.setBounds(circle.getBounds());
  }

  loadTrucks(lat, lng) {
    var i1 = 0.00150
    var i2 = 0.00160
    var i3 = 0.00140


    this.vehicle.fetch_nearby(lat, lng).subscribe((resp) => {
      this.markers = resp;
      for (let i = 0; i < this.markers.length; i++) {
        let location = new google.maps.LatLng(this.markers[i]["latitude"], this.markers[i]["longitude"]);
      // this.addMarker(location, this.markers[i]);
      }
      for (let z = 0; z < this.markers.length/2; z++) {
        var precision = 100000; // 2 decimals
        var randomnum = Math.floor(Math.random() * (0.01 * precision - 0.001 * precision) + 0.001 * precision) / (0.1*precision);
        let location = new google.maps.LatLng(this.markers[z]["latitude"]+randomnum, this.markers[z]["longitude"]+randomnum);
      //  this.addMarker(location, this.markers[z]);
      }

    }, (err) => {

    });
      let location = new google.maps.LatLng(lat+i1, lng+i3);
      let loc = new google.maps.LatLng(lat-i2, lng+i3);
      let locat = new google.maps.LatLng(lat+i3, lng-i3);
     // this.addDummyMarker(location);
     // this.addDummyMarker(locat);
     // this.addDummyMarker(loc);


  }
  addDummyMarker(location) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      animation: google.maps.Animation.DROP,
      icon: 'assets/img/truck_pin.png'
    });
    // let content = "<div>" + "Suzuki" + "</div><p>"
    //   + "xyz-123" + "</p>";

    // this.addInfoWindow(marker, content);
  }

  addMarker(location, vehicle) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      animation: google.maps.Animation.DROP,
      icon: 'assets/img/truck_pin.png'
    });
    // <img src='assets/img/truck.png'>
    let content = "<div>" + vehicle.vehicle_type.name + "</div><p>"
      + vehicle.registration_number + "</p>";

//    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });

    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for (let window of this.infoWindows) {
      window.close();
    }
  }

  fetch_address(e) {
    if (e.placeId) {
      this.geocoder.fetch_address_placeId(e.placeId)
        .then((data: any) => {
          this.address = data;
          console.log(data)
          console.log(this.address)
        })
        .catch((error: any) => {
          console.log(error);
        });
      this.geocoder.fetch_city_placeId(e.placeId)
        .then((data: any) => {
          this.pickup_city = this.extractFromAdress(data, "locality");
          this.pickup_country = this.extractFromAdress(data, "country");
          console.log(this.pickup_city+ this.pickup_country);
        })
    } else {
      this.geocoder.fetch_address_latlng(e.latLng)
        .then((data: any) => {
          this.address = data;
          console.log(data)
          console.log(this.address)
        })
        .catch((error: any) => {
          console.log(error);
        });

      this.geocoder.fetch_city_latlng(e.latLng)
        .then((data: any) => {
          this.pickup_city = this.extractFromAdress(data, "locality");
          this.pickup_country = this.extractFromAdress(data, "country");
          console.log(this.pickup_city+ this.pickup_country);
        })
    }

  }

  newShipmentLocation() {
    this.navCtrl.push("NewShipmentLocationPage");
  }

  newShipmentDropOff() {

    let user = this.globalVar.current_user;

    console.log(this.lat)
    console.log(this.lng)
    if (user.is_max_period_amount_overdue) {
      let alert = this.alertCtrl.create({
        title: 'Attention!',
        message: 'Your max credit period/amount is expired, please make payment to proceed further',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
        ]
      });
      alert.present();
    } else {
      this.navCtrl.push(NewShipmentDropOffPage, { start_lat: this.lat, start_lng: this.lng, pickup_location: this.address, position: this.current_position, pickup_building_name: this.pickup_building_name, pickup_city: this.pickup_city,pickup_country:this.pickup_country });
    }

  }


  disabled() {
    if (this.lat && this.address) {
      return false
    } else {
      return true
    }
  }

  extractFromAdress(components, type) {
    for (var i = 0; i < components.length; i++)
      for (var j = 0; j < components[i].types.length; j++)
        if (components[i].types[j] == type) return components[i].long_name;
    return "";
  }

  openPosted() {
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue) {
      let alert = this.alertCtrl.create({
        title: 'Attention!',
        message: 'Your max credit period/amount is expired, please make payment to proceed further',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
        ]
      });
      alert.present();
    } else {
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }

  openOngoing() {
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming() {
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew() {
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted() {
    this.navCtrl.setRoot("CompletedShipmentPage");
  }
  showNotification() {
    this.navCtrl.push("NotificationPage")
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentPage } from './new-shipment';

@NgModule({
  declarations: [
    NewShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(NewShipmentPage),
  ],
})
export class NewShipmentPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController, AlertController } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader, VehicleProvider} from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';
import { interval } from 'rxjs/observable/interval';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { Storage } from "@ionic/storage";

declare var require: any;

@IonicPage()
@Component({
  selector: 'page-fleet-won-shipment-bidding',
  templateUrl: 'fleet-won-shipment-bidding.html',
})
export class FleetWonShipmentBiddingPage {

  public shipments: any;
  public ascending= true;
  public shipment_obj = {};
  public loaded = false;
  public page = 1;
  public timeInterval = 10000;
  public subscriber: any;

  pickupnow;
  companytype: any;
  companycategory: any;
  public vehicles : any
  but=true;
  dsable: boolean;
  vehicle_array=[];
  ship;
  vehicle_length;
  apiKey: string;
  options = {

   };
  x: number;
  time;
  headers: any;
  baseurl: string;
  shipmentPickuptime: any;
  str: any;
  interval: any;
  fromNotification;
  shipmentid;
  constructor(
    public http: HttpClient,
    public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public globalVar: GlobalVars,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private callNumber: CallNumber,
    public modalCtrl: ModalController,
    public vehicle: VehicleProvider,
    public storage: Storage,
    ) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }
   this.baseurl = globalVar.server_link + "/api/v1/email/send_shipment_email.json";
    this.shipments = [];
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };

  }

  ionViewDidLoad() {
    this.loader.show("");
    this.page = 1
    this.companytype = this.globalVar.current_user.company.company_type;
    this.companycategory = this.globalVar.current_user.company.category;
    this.fromNotification=this.navParams.get('fromNotification');
    this.shipmentid=this.navParams.get('shipment_id');

     //  api/v1/email/send_shipment_email;
    // two paramete: shipmentid,not started

    let filter = {assignment: true}
    this.vehicle.getVehiclesForAssignment(filter).subscribe((resp) => {
      this.loader.hide();
      this.vehicles= resp;
      this.but = false;
      console.log(this.vehicles)
      this.vehicle_length=this.vehicles.length;
      this.loaded = true
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });

    if((this.companycategory == "fleet")&&(this.companytype == "individual"))
    {
      this.pickupnow=true;
      let self =this;
      this.interval = setInterval(function () {
        self.email_function();
        }, 10000);

     }

    this.fetchShipments()
  }

  shipmentnotstarted(){
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.setheaders();
    if(this.globalVar.rebid)
    {
      console.log("email sent");
    }
    else
    {
      this.http.post(this.baseurl,{shipment_id:this.shipments[0].id,shipment_status:"not_started"}
      ,{headers:this.headers}).subscribe((data: any) =>{
          console.log(data);
          this.globalVar.rebid=true;
        });
    }

  }
  setheaders()
  {

  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"

  });

  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  email_function()
  {
    if(this.shipments.length>0)
    {
        var currDate = moment.utc().format('YYYY-MM-DD');
        if(currDate == this.shipments[0].pickup_date){
          this.shipmentPickuptime = this.shipments[0].pickup_time;
        this.str = this.shipmentPickuptime.split('T');
        console.log("pickup time "+this.str[1]);
        var h = this.str[1].split(':')
        var h1 = h[0];
        h1 = h1*3600;
        h1=parseInt(h1);
        var min = h[1];
        min = min*60;
        min=parseInt(min);
        var sec = h[2]
        sec=parseInt(sec);
        var shipmenttime = h1+min+sec-60;
        console.log("shipment pickuptime "+shipmenttime);
        var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        console.log("local date "+date)
        var local = moment(date).format(' HH:mm:ss');
        console.log("local time "+local)
        var localtime = moment.duration(local).asSeconds()
        var time = localtime-shipmenttime;
      let self=this;
        if(time>300){
          self.shipmentnotstarted();
        }
      }
    }
  }
  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.won(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
          let self=this;
          debugger
          if(this.shipments.length>0)
          {
            self.email_function();
            if(this.fromNotification)
            {
              for(var z=0;z<this.shipments.length;z++)
              {
                if(this.shipmentid==this.shipments[z].id)
                {
                  break;
                }
                else{
                  if(z==(this.shipments.length-1))
                  {
                    let toast = this.toastCtrl.create({
                      message: 'Shipment # '+this.shipmentid+' has completed',
                      duration: 5000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-success"
                    });
                    toast.present();
                    this.navCtrl.pop();
                  }
                }
              }
            }
          }
          else{
            if(this.fromNotification)
            {
              let toast = this.toastCtrl.create({
                message: 'Shipment # '+this.shipmentid+' has completed',
                duration: 5000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-success"
              });
              toast.present();
              this.navCtrl.pop();
            }
          }


      }

      const self=this;
      if(this.globalVar.lang==true){
        debugger;;
        var googleTranslate = require('google-translate')(this.apiKey, this.options);
        debugger;;
        googleTranslate.translate(self.shipments[0].drop_location, 'ur', function(err, translation) {
          self.shipments[0].drop_location = translation.translatedText; });
        googleTranslate.translate(self.shipments[0].pickup_location, 'ur', function(err, translation) {
          self.shipments[0].pickup_location = translation.translatedText; });
      console.log(this.shipments);
    }
      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  fleetinidvidual(shipment){

    if(this.vehicle_length == 0){
       console.log(this.vehicles)
    let toast = this.toastCtrl.create({
      message: "No vehicle assigned to the driver ",
      duration: 6000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "x",
      cssClass: "toast-danger"
    });
    toast.present();
      this.dsable=false
    }
    else{

this.vehicle_array.push(this.vehicles[0].id)
this.shipment_obj = {state_event: "vehicle_assigned",
vehicle_ids:this.vehicle_array};

  this.loader.show("");
  this.shipmentProvider.update(this.shipment_obj, shipment.id, this.globalVar.current_user.company_id)
  .subscribe(data => {
    this.loader.hide();
    this.but=true;
    this.dsable = true;
    this.ship = true;
    console.log(data)
   this.navCtrl.setRoot('FleetOngoingShipmentDetailPage',{shipment_id:shipment.id,
   ship:this.ship,vehcileid:this.vehicles[0].id});
  }, (err) => {
    this.loader.hide();
    let toast = this.toastCtrl.create({
      message: err.error.errors,
      duration: 6000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "x",
      cssClass: "toast-danger"
    });
    toast.present();
    this.navCtrl.setRoot('FleetWonShipmentBiddingPage');
  });




  if(this.dsable==true){
    this.but=true;
  }
  else{
    this.but=false;
  }

    }

}

  cancelShipment(shipment){

    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: 'Are you sure? you want to cancel shipment',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {

           this.shipment_obj = {id: shipment.id, cancel_by: 'fleet_owner', state_event: 'cancel', cancel_reason: 'My customer cancelled the shipment'}
           this.loader.show("");
           this.shipmentProvider.update(this.shipment_obj, shipment.id, this.globalVar.current_user.company_id).subscribe(data => {
             this.loader.hide();
             this.navCtrl.setRoot('FleetWonShipmentBiddingPage');
           }, (err) => {
             this.loader.hide();
             let toast = this.toastCtrl.create({
               message: err.error.errors,
               duration: 6000,
               position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
             });
             toast.present();
           });
         }


        }]
    });
    alert.present();

  }

  callFleet(shipment){
    if(this.pickupnow)
    {
      this.callNumber.callNumber(shipment.cargo_owner_number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
      console.log("individual")
    }
    else
    {
      var country = this.globalVar.current_user.country.name
      if(country=="Pakistan")
      {
        this.callNumber.callNumber("+923377131317", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("pakistan")
      }
      else
      {
        this.callNumber.callNumber("+971521250605", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("dubai")

      }
    }

  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  assignVehicle(shipment){

    let addModal = this.modalCtrl.create('FleetAssignVehicleModalPage', {shipment: shipment});
    addModal.onDidDismiss(item => {
      if(item != "cancel"){
        this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
      }
    })
    addModal.present();
  }

  changeVehicle(shipment){
    let addModal = this.modalCtrl.create('FleetChangeVehicleModalPage', {shipment: shipment});
    addModal.onDidDismiss(item => {
      this.navCtrl.setRoot('FleetWonShipmentBiddingPage');
    })
    addModal.present();
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  ngOnDestroy()
  {
    let self=this
    console.log("view leave");
      if (this.interval) {
        clearInterval(this.interval);
      }


  }
}

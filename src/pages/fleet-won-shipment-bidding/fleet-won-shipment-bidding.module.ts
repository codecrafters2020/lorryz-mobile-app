import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetWonShipmentBiddingPage } from './fleet-won-shipment-bidding';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    FleetWonShipmentBiddingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetWonShipmentBiddingPage),
    TranslateModule.forChild()
  ],
})
export class FleetWonShipmentBiddingPageModule {}

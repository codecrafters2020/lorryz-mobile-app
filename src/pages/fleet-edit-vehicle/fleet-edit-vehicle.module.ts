import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetEditVehiclePage } from './fleet-edit-vehicle';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    FleetEditVehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(FleetEditVehiclePage),
    TranslateModule.forChild()
  ],
})
export class FleetEditVehiclePageModule {}

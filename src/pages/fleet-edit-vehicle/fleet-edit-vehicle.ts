import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,ActionSheetController } from 'ionic-angular';
import {User,GlobalVars,Loader,DriverProvider,VehicleProvider} from '../../providers/providers';


import { Company } from '../../providers/company/company';
import { Storage } from '@ionic/storage';

import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AwsProvider } from './../../providers/aws/aws';


import b64toBlob from "b64-to-blob";

@IonicPage()
@Component({
  selector: 'page-fleet-edit-vehicle',
  templateUrl: 'fleet-edit-vehicle.html',
})
export class FleetEditVehiclePage {

      
      

  from_pickup_date: String = new Date().toISOString();
  to_pickup_date: String = new Date().toISOString();

  public toggle = false;
  public registration_number: string
  public expiry_date: string
  public insurance_number: string
  public insurance_expiry_date: string
  public authorization_letter: string
  public vehicle_type_id: string
  public vehicle_types: any
  public vehicle_detail: any;
  selected_documents = [];
  documents = [];
  minDate: any;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  constructor(
            public navCtrl: NavController, 
            public navParams: NavParams,
            public company: Company,
            public storage: Storage,
            public toastCtrl: ToastController,
            public globalVar: GlobalVars,
            private awsProvider: AwsProvider,
            private file: File, 
            private camera: Camera,
            private actionSheetCtrl: ActionSheetController,
            public loader: Loader,
            public user: User,
            public vehicle: VehicleProvider,
            public zone: NgZone,) {
            this.vehicle_types = [];
            this.minDate = new Date().toISOString()
  }

  ionViewDidLoad() {
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
    if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {
      this.pickupnow = true;
    }

    this.vehicle.getVehicleType().subscribe((resp) => {
      
      this.vehicle_types = resp;

    }, (err) => {
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });


    let id = this.navParams.get('vehicle_id')
    this.loader.show("");
    this.vehicle.getVehicle(id).subscribe((resp) => {
      this.loader.hide();
      this.registration_number= resp["registration_number"]
      this.expiry_date= resp["expiry_date"]
      this.insurance_expiry_date= resp["insurance_expiry_date"]
      
      this.insurance_number= resp["insurance_number"]
      this.authorization_letter= resp["authorization_letter"]
      this.vehicle_type_id= resp["vehicle_type_id"]
      this.toggle = resp["available"]
      this.from_pickup_date = resp["not_available_from"]
      this.to_pickup_date = resp["not_available_to"]
      this.documents = resp["documents"].slice();
      this.selected_documents = resp["documents"].slice();
      this.vehicle_detail = resp

    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });



    
  }

  updateVehicle(id) {
    

    if(!(this.globalVar.current_user.company.company_type=="individual"))
    {
  
  
      if(this.validateDoc()){
        return false;
      }
    }
      

    let account = {};
    account['vehicle'] = {
                          registration_number: this.registration_number,
                          expiry_date: this.expiry_date,
                          insurance_expiry_date: this.insurance_expiry_date,
                          insurance_number: this.insurance_number,
                          authorization_letter: this.authorization_letter,
                          vehicle_type_id: this.vehicle_type_id,
                          available: this.toggle,
                          not_available_from: this.from_pickup_date,
                          not_available_to: this.to_pickup_date,
                          documents: this.documents

                        }
    this.loader.show("");
    this.vehicle.updateVehicle(account,id).subscribe((resp) => {
      this.loader.hide();
      this.navCtrl.setRoot('FleetVehicleDriverListingPage');

    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }

  notify(event){
    this.toggle = event.checked
  }

  remove(doc){
    
    this.documents.splice( this.documents.indexOf(doc), 1 );  
    this.selected_documents.splice( this.selected_documents.indexOf(doc), 1 ); 
    
    
  } 

  validateDoc(){
    if(this.documents.length == 0){
      let toast = this.toastCtrl.create({
        message: "Vehicle Documents is mandatory",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
      return true
    }
  }


  takeDocuments(sourceType) {

    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    let that = this;
    this.camera.getPicture(options).then((imageData) => {
      
    that.selected_documents.push(imageData);
    that.loader.show("uploading...");
      
      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **",newName);

      this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest",data);

        that.documents.push(data["public_url"]);
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', imageData, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function() {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);
              
              console.log("Step 1 reader",reader);
              console.log("Step 1 got stuck in request.response",request)

              reader.onload =  function(){
                that.zone.run(() => {
                  console.log("Step 2 Loaded",reader.result);
                  let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                  console.log("blob",blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    that.loader.hide();
                    console.log("result that i got after uploading to aws",_result,that.documents)
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror =  function(e){
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function(event) {
                console.log("reader onabort",event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.hide();
            console.log("** I got an error in XML Http REquest",e);
          };

          request.ontimeout = function(event) {
             that.loader.hide();
             console.log("xmlhttp ontimeout",event)
          }

          request.onabort = function(event) {
             that.loader.hide();
             console.log("xmlhttp onabort",event);
          }


          request.send();
        })
      }, (err) => {
          that.loader.hide();
          console.log('getSignedUploadRequest timeout error: ', err);

          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();

        });

    }, (err) => {
      let toast = this.toastCtrl.create({
        message: "Unable to Access Gallery. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
      console.log('err: ', err);
    });
  }

   
  presentActionSheetForDocuments() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  openDocument(doc){
    window.open(doc,'_system', 'location=yes');
    // const browser = this.iab.create(doc);
  } 

  valid_format(file) {        
    return file.includes(".jpg") || file.includes(".jpeg") || file.includes(".png")        
  }  


}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetAddsecondaryvehiclePage } from './fleet-addsecondaryvehicle';

@NgModule({
  declarations: [
    FleetAddsecondaryvehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(FleetAddsecondaryvehiclePage),
  ],
})
export class FleetAddsecondaryvehiclePageModule {}

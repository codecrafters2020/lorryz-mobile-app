import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FleetAddsecondaryvehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fleet-addsecondaryvehicle',
  templateUrl: 'fleet-addsecondaryvehicle.html',
})
export class FleetAddsecondaryvehiclePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FleetAddsecondaryvehiclePage');
  }

}

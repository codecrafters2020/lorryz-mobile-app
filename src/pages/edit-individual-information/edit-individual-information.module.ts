import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditIndividualInformationPage } from './edit-individual-information';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    EditIndividualInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(EditIndividualInformationPage),
    TranslateModule.forChild()
  ],
})
export class EditIndividualInformationPageModule {}

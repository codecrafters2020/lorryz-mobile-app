import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController,NavParams,AlertController,ItemSliding,Item } from 'ionic-angular';

import { VehicleListing,DriverListing} from '../pages'
import { User,DriverProvider,Loader,GlobalVars } from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';
@IonicPage()
@Component({
  selector: 'page-driver-listing',
  templateUrl: 'driver-listing.html',
})
export class DriverListingPage {
  public drivers : any
  public ascending= true;
  activeItemSliding: ItemSliding = null;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public driver: DriverProvider,
              public toastCtrl: ToastController,
              public loader: Loader,
              private callNumber: CallNumber,
              public globalVar: GlobalVars,
              public alertCtrl: AlertController,) {
    this.drivers = []
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.driver.getDrivers().subscribe((resp) => {
        this.loader.hide();
        this.drivers= resp;


      }, (err) => {
        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });
    }

  loadVechile(){
  	this.navCtrl.setRoot('FleetVehicleDriverListingPage');
  }
  loadDriver(){
  	this.navCtrl.setRoot(DriverListing);
  }
  addNewDriver(){
   this.navCtrl.push('FleetAddDriverPage');
  }
  editDriver(id){
    // event.preventDefault();
   this.navCtrl.push('FleetEditDriverPage',{driver_id: id});
  }

  deleteDriver(id) {
    let alert = this.alertCtrl.create({
    title: 'Confirm Cancel',
    message: 'Do you want to Delete this Driver?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {

          this.loader.show("");
          this.driver.deleteDriver(id).subscribe((resp) => {
            this.loader.hide();
            this.navCtrl.setRoot('DriverListingPage');
          }, (err) => {
            this.loader.hide();
          });
        }
      }
    ]
    });
    alert.present();


  }
  callToFleetOwner(driver){

    this.callNumber.callNumber(driver.mobile, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  sortBy(){

    // event.preventDefault();
    this.ascending=this.ascending ? false : true;

    this.ascending ?
      this.drivers.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)
      :
      this.drivers.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)


  }
  openOption(itemSlide: ItemSliding, item: Item) {
    console.log('opening item slide..');

    if(this.activeItemSliding!==null) //use this if only one active sliding item allowed
     this.closeOption();

    this.activeItemSliding = itemSlide;

    let swipeAmount = 200; //set your required swipe amount
    itemSlide.startSliding(swipeAmount);
    itemSlide.moveSliding(swipeAmount);

    itemSlide.setElementClass('active-options-right', true);
    itemSlide.setElementClass('active-swipe-right', true);

    item.setElementStyle('transition', null);
    item.setElementStyle('transform', 'translate3d(-'+swipeAmount+'px, 0px, 0px)');
   }

   closeOption() {
    console.log('closing item slide..');

    if(this.activeItemSliding) {
     this.activeItemSliding.close();
     this.activeItemSliding = null;
    }
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

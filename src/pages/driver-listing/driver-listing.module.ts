import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverListingPage } from './driver-listing';

@NgModule({
  declarations: [
    DriverListingPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverListingPage),
  ],
})
export class DriverListingPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import{
Shipment,
GlobalVars,
Loader,
CountryProvider
} from "../../providers/providers";
import { NewShipmentRequestPage } from '../new-shipment-request/new-shipment-request';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
/**
 * Generated class for the FleetAddprimaryvehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fleet-addprimaryvehicle',
  templateUrl: 'fleet-addprimaryvehicle.html',
})
export class FleetAddprimaryvehiclePage {

  value;
  vehicles:any;
  id;
  shipment;
  position;
  pickupnow;
  letsgo;
  vehicle_name;
  src;
  server;
  company_category
  company_type
  isedit = false;
  shipmentId;
  time;
  name;
  desc;
  public vehicleName:string[] = [];
  public vehicleDescription:string[] = [];
  cargodesc: any;
  no_vehicles: any;
  sum
  headers: any;
  baseurl: string;
  secondaryvehicles: any;
  primaryvehicleid: any;
  registerationNo: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    public shipmentProvider: Shipment,
    public countryProvider: CountryProvider,
    public loader: Loader,
    public globalVar: GlobalVars,
    public http: HttpClient
    ) {
      this.baseurl = globalVar.server_link + "/api/v1/";
      this.server = this.globalVar.server_link;
      this.vehicles = [];
      this.secondaryvehicles=[];
      this.company_category = navParams.get('company_category');
      this.company_type = navParams.get('company_type');
      this.registerationNo=this.navParams.get('registerationNo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FleetAddprimaryvehiclePage');
    debugger
    if(this.navParams.get('category')=='category')
    {
      this.countryProvider
      .primaryvehicles(this.globalVar.current_user.country_id)
      .subscribe(
        data => {
          this.vehicles = data;
        },
        err => {}
      );

    }
    else{
      this.countryProvider
      .subCategoryVehicles(this.navParams.get('primaryvehicleid'))
      .subscribe(
        data => {
          console.log(data);
          this.secondaryvehicles = data;
        },
        err => {}
      );
    }
      this.primaryvehicleid= this.navParams.get('primaryvehicleid');
      this.src=this.navParams.get("primarysrc");
      this.vehicle_name=this.navParams.get("vehiclename");
  }

  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }

  vehicleid(id,name,src){
   this.src = src;
    this.id = id;
  this.vehicle_name = name;
     this.value = true;
     if(this.isedit == true)
     {
      this.navCtrl.push('EditShipmentPage',{
        shipment:this.shipment,id:this.id,vehicle_name:this.vehicle_name,src:this.src,
         company_category:this.company_category,
          company_type:this.company_type,shipmentId:this.shipmentId,is_edit:true
      }).then(() => {
        this.viewCtrl.dismiss("cancel");
        const startIndex = this.navCtrl.getActive().index - 2;
    this.navCtrl.remove(startIndex, 3);

      });
     }
     else{
      this.navCtrl.push('FleetAddVehiclePage',{
        id:this.id,
        vehicle_name:this.vehicle_name, value:this.value,src:this.src,registrationNo:this.registerationNo
       });
     }



  }
  secondaryvehicleid(id,name,src)
  {
    debugger
    this.navCtrl.push('FleetAddVehiclePage',{
      id:this.primaryvehicleid,vehicle_name:this.vehicle_name, value:true,src:this.src,
      secondarysrc:src,secondaryid:id,secondaryvehiclename:name,registrationNo:this.registerationNo,subvehicle:true
     })
  }
  setheaders()
{
  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"
  })

}


}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetAddprimaryvehiclePage } from './fleet-addprimaryvehicle';

@NgModule({
  declarations: [
    FleetAddprimaryvehiclePage,
  ],
  imports: [
    IonicPageModule.forChild(FleetAddprimaryvehiclePage),
  ],
})
export class FleetAddprimaryvehiclePageModule {}

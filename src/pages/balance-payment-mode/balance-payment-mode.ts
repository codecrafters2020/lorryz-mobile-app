import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { Payment, GlobalVars, Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-balance-payment-mode',
  templateUrl: 'balance-payment-mode.html',
})
export class BalancePaymentModePage {

  public shipments: any;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
    public paymentProvider: Payment,
    public loader: Loader,
    public modalCtrl: ModalController,
    public globalVar: GlobalVars,) {
    this.shipments = []
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.paymentProvider.cargo_payments(this.globalVar.current_user.company_id).subscribe(data => {
      this.loader.hide();
      this.shipments = data
      console.log(this.shipments)
    },(err) => {
      this.loader.hide();
    });
  }

  openBalance(){
    this.navCtrl.setRoot("BalancePaymentModePage");
  }
  openWithDraw(){
    this.navCtrl.setRoot("WithdrawPaymentModePage");
  }
  openCreditCard(){
    this.navCtrl.setRoot("CreditCardPaymentModePage");
  }

  openPaymentDetail(){
    let addModal = this.modalCtrl.create('PaymentDetailModalPage',{});
      addModal.onDidDismiss(item => {
        
      })
      addModal.present();
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  
}

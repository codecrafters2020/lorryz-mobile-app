import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancePaymentModePage } from './balance-payment-mode';

@NgModule({
  declarations: [
    BalancePaymentModePage,
  ],
  imports: [
    IonicPageModule.forChild(BalancePaymentModePage),
  ],
})
export class BalancePaymentModePageModule {}

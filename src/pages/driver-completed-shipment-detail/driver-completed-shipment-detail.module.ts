import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverCompletedShipmentDetailPage } from './driver-completed-shipment-detail';

@NgModule({
  declarations: [
    DriverCompletedShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverCompletedShipmentDetailPage),
  ],
})
export class DriverCompletedShipmentDetailPageModule {}

import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ToastController,
  Platform
} from "ionic-angular";
import { FleetShipment, GlobalVars } from "../../providers/providers";
import { User, DriverProvider, Loader } from "../../providers/providers";
import {
  Shipment,
  CountryProvider,
  LocationTracker
} from "../../providers/providers";

import { CallNumber } from "@ionic-native/call-number";

@IonicPage()
@Component({
  selector: "page-driver-completed-shipment-detail",
  templateUrl: "driver-completed-shipment-detail.html"
})
export class DriverCompletedShipmentDetailPage {
  public shipment: any;
  public vehicle_status: any;
  public status: any;
  public shipments_data: any;
  public proceed: any;
  public val: any;
  public _update_status: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public globalVar: GlobalVars,
    public driver: DriverProvider,
    public loader: Loader,
    public alertCtrl: AlertController,
    private callNumber: CallNumber,
    public toastCtrl: ToastController,
    public platform: Platform,
    public locationTracker: LocationTracker,
    public shipmentProvider: Shipment
  ) {
    this.shipment = {};
  }

  ionViewDidLoad() {
    this.shipment = this.navParams.get("shipment") || {};
    this.loader.hide();
    this.loader.show("");
    this.shipmentProvider
      .show(this.globalVar.current_user.company_id, this.shipment.id)
      .subscribe(
        data => {
          this.loader.hide();
          this.shipment = data;
          this.status = 100;
          if (
            this.shipment.assigned_vehicle &&
            this.shipment.vehicle_shipments
          ) {
            if (this.shipment.assigned_vehicle.status == "booked") {
              this.status = 100;
            } else if (this.shipment.assigned_vehicle.status == "start") {
              this.status = 200;
            } else if (this.shipment.assigned_vehicle.status == "loading") {
              this.status = 300;
            } else if (this.shipment.assigned_vehicle.status == "enroute") {
              this.status = 400;
            } else if (this.shipment.assigned_vehicle.status == "unloading") {
              this.status = 500;
            } else if (this.shipment.assigned_vehicle.status == "finish") {
              this.status = 600;
            }
          }
        },
        err => {
          this.loader.hide();
        }
      );
  }

  textChanged(event) {
    console.log(" this is an status", this.status);
    console.log(" this is an status", event);
    if (this.status + 100 == event) {
      this.proceed = true;
    } else if (event - this.status == 100) {
      this.proceed = true;
    } else {
      this.proceed = false;
    }
    this.val = this.status;
  }

  changeStatus(event) {
    if (this.proceed) {
      if (this.status == 200) {
        this.vehicle_status = "start";
      } else if (this.status == 300) {
        this.shipment.payment_option == "BeforePickUp"
          ? this.pick_payment_before_loading(event)
          : (this.vehicle_status = "loading");
      } else if (this.status == 400) {
        this.vehicle_status = "enroute";
      } else if (this.status == 500) {
        this.vehicle_status = "unloading";
      } else if (this.status == 600) {
        this.shipment.payment_option == "AfterDelivery"
          ? this.pick_payment_on_finish(event)
          : (this.vehicle_status = "finish");
      } else if (this.status == 100) {
        this.vehicle_status = "booked";
      }

      if (this._update_status) {
        this.update_vehicle_status(event);
      }
    } else {
      this.status = this.val;
      event.setValue(this.status);
      console.log("you can not change state more than once", this.val);
    }
  }

  update_vehicle_status(event) {
    // this.loader.show("");
    this.driver
      .updateVehicleStatus(
        this.shipment.assigned_vehicle.vehicle_id,
        this.vehicle_status,
        this.shipment.id
      )
      .subscribe(
        data => {
          // this.loader.hide();
          if (data && data["error"]) {
            let toast = this.toastCtrl.create({
              message: data["error"],
              duration: 6000,
              position: "top",
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
            this.status = this.val;
            event.setValue(this.status);
          } else {
            if (this.vehicle_status == "enroute") {
              // this.locationTracker.startTracking(
              //   this.shipment.assigned_vehicle.vehicle_id
              // );
            }
            if (this.vehicle_status == "booked") {
              this.navCtrl.setRoot("DriverUpcomingShipmentListPage");
            } else if (this.vehicle_status == "start") {
              this.navCtrl.setRoot("DriverOngoingShipmentListPage");
            } else if (this.vehicle_status == "finish") {
              this.locationTracker.stopTracking();
              this.navCtrl.setRoot("DriverCompletedShipmentListPage");
            }
          }
        },
        err => {
          // this.loader.hide();
        }
      );
  }

  pick_payment_before_loading(event) {
    !(this.shipment.paid_by_cargo && this.shipment.paid_to_fleet)
      ? this.force_to_collect_payment(
          event,
          "Please mark above, that you've collected Cash on 'Pickup'"
        )
      : (this.vehicle_status = "loading");
  }

  pick_payment_on_finish(event) {
    !(this.shipment.paid_by_cargo && this.shipment.paid_to_fleet)
      ? this.force_to_collect_payment(
          event,
          "Please mark above, that you've collected Cash on 'Dropoff'"
        )
      : (this.vehicle_status = "finish");
  }

  force_to_collect_payment(event, msg) {
    let alert = this.alertCtrl.create({
      title: "Attention!",
      message: msg,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.status = this.val;
            event.setValue(this.status);
            this._update_status = false;
          }
        },
        {
          text: "Ok",
          handler: () => {
            this.status = this.val;
            event.setValue(this.status);
            this._update_status = false;
          }
        }
      ]
    });
    alert.present();
  }

  report() {
    let alert = this.alertCtrl.create({
      title: "Attention!",
      message: "Are you sure? you want to report a problem to fleet owner",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Ok",
          handler: () => {
            this.loader.show("");

            this.driver.report(this.shipment.id).subscribe(
              data => {
                this.loader.hide();
              },
              err => {
                this.loader.hide();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  confirmIt(event) {
    let alert = this.alertCtrl.create({
      title: "Attention!",
      message:
        "Please confirm you've collected " + this.shipment.amount + " amount",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
            this.shipment.paid_by_cargo = false;
            this.shipment.paid_to_fleet = false;
            alert.dismiss();
          }
        },
        {
          text: "Ok",
          handler: () => {
            this.loader.show("");
            this.shipment.paid_by_cargo = true;
            this.shipment.paid_to_fleet = true;
            this.driver.paymentReceived(this.shipment.id).subscribe(
              data => {
                this.loader.hide();
              },
              err => {
                this.loader.hide();
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  callToFleetOwner(mobile) {
    this.callNumber
      .callNumber(mobile, true)
      .then(res => console.log("Launched dialer!", res))
      .catch(err => console.log("Error launching dialer", err));
  }

  openMap() {
    this.navCtrl.push("MapLocationPage", { shipment: this.shipment });
  }

  showNotification() {
    this.navCtrl.push("NotificationPage");
  }
}

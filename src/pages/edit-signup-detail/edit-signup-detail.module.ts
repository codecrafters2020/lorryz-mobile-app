import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSignupDetailPage } from './edit-signup-detail';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    EditSignupDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(EditSignupDetailPage),
    TranslateModule.forChild()
  ],
})
export class EditSignupDetailPageModule {}

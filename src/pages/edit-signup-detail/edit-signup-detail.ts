import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User,GlobalVars, Loader } from '../../providers/providers';
import { MainPage,ActivateAccountPage,LoginPage,ForgotPasswordPage } from '../pages';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { ValidationError } from '../../validators/validators';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { parseNumber } from 'libphonenumber-js'
import list from 'country-list';
let countries = list();

@IonicPage()
@Component({
  selector: 'page-edit-signup-detail',
  templateUrl: 'edit-signup-detail.html',
})
export class EditSignupDetailPage {

  // The account fields for the login form.
    // If you're using the username field with or without email, make
    // sure to add it to the type
    // account: { name: string, email: string, password: string } = {
    //   name: 'Test Human',
    //   email: 'test@example.com',
    //   password: 'test'
    // };

    // Our translated text strings
    private signupErrorString: string;
    private account : FormGroup;
    serverError: any;
    public _countries: any
    constructor(public navCtrl: NavController,
      public user: User,
      public translateService: TranslateService,
      private formBuilder: FormBuilder,
      public globalVar: GlobalVars,
      public storage: Storage,
      public validationError: ValidationError,
      public loader: Loader,
      private iab: InAppBrowser,
      public toastCtrl: ToastController) {

      this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
        this.signupErrorString = value;
      })

      this.account = this.formBuilder.group({
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
        email: ['' ,''],
        country: ['', Validators.required],
        password: ['' , Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ])],
        mobile: ['' , Validators.compose([
          Validators.required,
          // Validators.pattern(/^(?:\+971|00971|0)(?:2|3|4|6|7|9|50|51|52|55|56)[0-9]{7}$/)
        ])]

      });

      this._countries = []

    }


    ionViewDidLoad() {

      this.loader.show("");
      this.user.getCountries().subscribe(data => {
        this._countries = data;
        this.loader.hide();
      },(err) => {
      });

      this.account.controls['first_name'].setValue(this.globalVar.current_user.first_name);
      this.account.controls['last_name'].setValue(this.globalVar.current_user.last_name);
      
      this.account.controls['email'].setValue(this.globalVar.current_user.email);
      this.account.controls['country'].setValue(this.globalVar.current_user.country.dialing_code);
      this.account.controls['password'].setValue(this.globalVar.current_user.password);


      let _mobile =  this.globalVar.current_user.mobile
      let _parse_number = parseNumber(_mobile)

      this.account.controls['mobile'].setValue(_parse_number["phone"]);


      

    }


    signIn(event){
      event.preventDefault();
      this.navCtrl.setRoot(LoginPage);
    }

    validate(){

      this.validationError.populateErrorMessageonSubmit(this.account)
    }

    verifyPhone(){
      let _mobile =  this.account["value"]["mobile"]
      
      let that = this;


      let country = this._countries.countries.filter(obj => {
        return obj.dialing_code == that.account["value"]["country"]
      })[0]
      
      console.log("country.code",country.name);

      let country_initial = countries.getCode(country.name)

      let _parse_number = parseNumber('Phone: '+_mobile, country_initial, { extended: true })
      return _parse_number
    }


    updateSignUp() {
      this.validationError.populateErrorMessageonSubmit(this.account)

      
      let num = this.verifyPhone()
      
      if (!num["valid"]) {

        let toast = this.toastCtrl.create({
          message: "Phone number is not valid",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      }else{

        this.loader.show("");
        let _account = {};
        _account['user'] = this.account["value"]

        _account['user']['mobile']= '+'+num["countryCallingCode"] + num["phone"]
        _account['user']['id']= this.globalVar.current_user.id
        _account['user'] = Object.assign({}, _account['user'], {device_id: this.globalVar.device_id, os: this.globalVar.os});

        this.user.updateSignUp(_account,this.globalVar.current_user.id).subscribe((resp) => {
          console.log("ressssssp",resp);
          this.globalVar.current_user = resp["user"];
          this.storage.set('currentUser' , resp["user"]);
          if(resp && !resp["errors"]){
            this.storage.ready().then(() => {
              this.globalVar.current_user = resp["user"];
              this.storage.set('currentUser' , resp["user"]);
              this.loader.hide();
              this.navCtrl.setRoot(ActivateAccountPage);
            });
          }else{
            this.loader.hide();
            this.serverError = resp["errors"][0];

            let toast = this.toastCtrl.create({
              message: resp["errors"][Object.keys(resp["errors"])[0]],
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
          }
        }, (err) => {
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Some Went Wrong. Please Try Later",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        });
      }
    }

    opemTermsConditions(){
      const browser = this.iab.create('https://lorryz.com/dashboard/terms_and_conditions/', '_blank', 'location=yes')
    }

    showNotification(){
      this.navCtrl.push("NotificationPage")
    }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OngoingShipmentPage } from './ongoing-shipment';

@NgModule({
  declarations: [
    OngoingShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(OngoingShipmentPage),
  ],
})
export class OngoingShipmentPageModule {}

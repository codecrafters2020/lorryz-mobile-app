import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OngoingShipmentListPage } from '../pages';
/**
 * Generated class for the OngoingShipmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ongoing-shipment',
  templateUrl: 'ongoing-shipment.html',
})
export class OngoingShipmentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OngoingShipmentPage');
  }

  ongoingShipmentList(){
  	this.navCtrl.setRoot(OngoingShipmentListPage);  
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

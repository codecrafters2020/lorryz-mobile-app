import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompletedShipmentDetailPage } from './completed-shipment-detail';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    CompletedShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CompletedShipmentDetailPage),
    Ionic2RatingModule
  ],
})
export class CompletedShipmentDetailPageModule {}

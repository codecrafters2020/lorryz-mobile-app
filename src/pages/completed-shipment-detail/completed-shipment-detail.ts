import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController, ViewController  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { FleetShipment,VehicleProvider,GlobalVars, Loader, InvoiceProvider } from '../../providers/providers';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import * as $ from 'jquery';
import { Ionic2Rating } from "ionic2-rating";
declare var google;

@IonicPage()
@Component({
  selector: 'page-completed-shipment-detail',
  templateUrl: 'completed-shipment-detail.html',
})
export class CompletedShipmentDetailPage {

  @ViewChild('map') mapElement: ElementRef;
  public shipment;
  public current_user_rating: any;
  public other_user_rating: any;
  public company_rating_length = 0;
  map: any;
  vehicles :any;
  markers :any;
  company_category: any;
  company_type: any;
  pickupnow: boolean;
  routes: any;
  apiKey = 'AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw'
  placeIdArray: any;
  snappedCoordinates: any;
  polylines: any;
  lengthofroute:boolean=false;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public shipmentProvider: FleetShipment,
              public loader: Loader,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              public invoice: InvoiceProvider,
              public globalVar: GlobalVars,
              private viewCtrl: ViewController,
              public http: HttpClient
   ) {

    this.shipment = this.navParams.get("shipment");
    this.vehicles = [];
    this.markers = [];
    this.routes=[];
  }


  ionViewDidLoad() {

     let that = this;

     this.company_type = this.globalVar.current_user.company.company_type
     this.company_category = this.globalVar.current_user.company.category
     if((this.company_type=='individual')&&(this.company_category=='cargo'))
     {
       this.pickupnow=true;
     }
     if (this.shipment['company_ratings']) {
       this.company_rating_length = this.shipment['company_ratings'].length

       // if (this.shipment['company_ratings'].length > 0) {
         this.current_user_rating = this.shipment['company_ratings'].map(function(rating) {
           if (rating.giver_id == that.globalVar.current_user.company_id){
             return rating;
           }

         }).filter(function(e){return e})[0];

         this.other_user_rating = this.shipment['company_ratings'].map(function(rating) {
           if (rating.giver_id != that.globalVar.current_user.company_id){
             return rating;
           }

         }).filter(function(e){return e})[0];

     }else{
       this.company_rating_length = 0
       this.current_user_rating = 0
       this.other_user_rating= 0
     }


     this.vehicles = this.shipment["vehicles"];
    //  if(this.shipment.route!=null)
    //  {
    //   if(this.shipment.route.length>0)
    //   {
    //    this.load_map_proper_routes();
    //   }
    //   else
    //   {
    //     this.load_map();
    //   }
    //  }
    //  else
    //  {
       this.load_map();
    //  }

     let self = this;
    //  if(this.pickupnow)
    //  {
      if(this.shipment.state != 'cancel')
      {
             if(!(this.current_user_rating))
         {
             self.openRating();
         }
     }

    //  }


   }

   openRating(){
  	let addModal = this.modalCtrl.create('FleetRateShipmentPage', {shipment: this.shipment});
    addModal.onDidDismiss(item => {
    if(item != "cancel"){
      this.navCtrl.setRoot("CompletedShipmentPage");
      this.viewCtrl.dismiss();

    }
    })
    addModal.present();
  }
  async load_map_proper_routes()
  {
    //let source = new google.maps.LatLng(parseFloat(this.shipment.route[100].lat),parseFloat(this.shipment.route[100].lng));
    let mapOptions = {
      center: {
        lat: parseFloat(this.shipment.route[0].lat),
        lng: parseFloat(this.shipment.route[0].lng)
      },
      zoom: 13,
    }
    // let self = this;
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    var data = {
      "type": "FeatureCollection",
      "features": [{
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "LineString",
          "coordinates": [
          ]
        }
      }]
    };
    this.snappedCoordinates = [];
    this.placeIdArray = [];
    this.polylines=[];
    var len = this.shipment.route.length;
    var L = Math.floor(len/100);
    var c=0;
    var b=100;
    var lines,coords;
    for(var j=0; j<L;j++)
      {
        await this.delay(1000);
        var data = {
          "type": "FeatureCollection",
          "features": [{
            "type": "Feature",
            "properties": {},
            "geometry": {
              "type": "LineString",
              "coordinates": [
              ]
            }
          }]
        };
          for(var i=c;i<b;i++)
          {
            // var route = {lat:parseFloat(this.shipment.route[i].lat),lng:parseFloat(this.shipment.route[i].lng)};
            // this.routes.push([parseFloat(this.shipment.route[i].lat),parseFloat(this.shipment.route[i].lng)]);
            var a=data.features;
            a[0].geometry.coordinates.push([parseFloat(this.shipment.route[i].lng),parseFloat(this.shipment.route[i].lat)]);
          }

          this.map.data.addGeoJson(data);
           lines = data.features;
           coords = [];
            for (var z = 0; z < lines[0].geometry.coordinates.length; z++) {
              coords.push([lines[0].geometry.coordinates[z][1], lines[0].geometry.coordinates[z][0]]);
            }
            await this.delay(1000);
          await this.runSnapToRoad(coords,j,L);
          // let source1 = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
          // let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

          // this.addMarker(source1);
          // this.addMarker(destination)
          // this.load_map_2(this.routes);
          let self=this;


         c=b;
         b=b+100;
        //  if(j==L)
        //  {
        //    this.lengthofroute = true;
        //  }

    }

 }
 delay(num) {
  return new Promise(resolve => setTimeout(resolve, num));
}
  async runSnapToRoad(path,j,L) {
      var latlng;
      //await this.delay(800);
      let self=this;
   await $.get('https://roads.googleapis.com/v1/snapToRoads', {
      interpolate: true,
      key: this.apiKey,
      path: path.join('|')
    }, function (data) {
      if (data.error) {
        alert("error" + data.error.message);
        return;
      }
      for (var i = 0; i < data.snappedPoints.length; i++) {
         latlng = new google.maps.LatLng(
            data.snappedPoints[i].location.latitude,
            data.snappedPoints[i].location.longitude);
      self.snappedCoordinates.push(latlng);
        self.placeIdArray.push(data.snappedPoints[i].placeId);
        // var snappedPolyline = new google.maps.Polyline({
        //   path: self.snappedCoordinates,
        //   strokeColor: '#FF0000',
        //   strokeWeight: 4,
        //   strokeOpacity: 0.9,
        // });

        // snappedPolyline.setMap(this.map);
      }
//      self.processSnapToRoadResponse(data);
// debugger
      //  if(j==L)
      //  {
        self.drawSnappedPolyline();
      //  }
    });
  }
  // Store snapped polyline returned by the snap-to-road service.
  processSnapToRoadResponse(data) {
      for (var i = 0; i < data.snappedPoints.length; i++) {
        var latlng = new google.maps.LatLng(
            data.snappedPoints[i].location.latitude,
            data.snappedPoints[i].location.longitude);
        this.snappedCoordinates.push(latlng);
        //this.placeIdArray.push(data.snappedPoints[i].placeId);
      }
    }

    // Draws the snapped polyline (after processing snap-to-road response).
  drawSnappedPolyline() {
      var snappedPolyline = new google.maps.Polyline({
        path: this.snappedCoordinates,
        strokeColor: '#FF0000',
        strokeWeight: 4,
        strokeOpacity: 0.9,
      });

      snappedPolyline.setMap(this.map);
      this.polylines.push(snappedPolyline);
    }

  load_map(){
    var s,s1,f,s2,l,f1,feed;
    this.snappedCoordinates=[];
    if(this.shipment.route)
    {
      for(var i=0;i<this.shipment.route.length;i++)
      {
         s = this.shipment.route[i].split('>');
         s1 = s[1].split(',');
         f = s1[0].split('"')
         s2 = s[2].split('}');
         l = s2[0].length;
         f1 = s2[0].slice(0,(l-1));
         var route = f + "," + f1;
        //  let latlng = new google.maps.LatLng({lat:parseFloat(f),lng:parseFloat(f1)});
         feed = {location: route, stopover: true};
         this.snappedCoordinates.push(feed);
      }
    }

    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();


    let source = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

    let mapOptions = {
      center: source,
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }
    let self = this;

    directionsDisplay = new google.maps.DirectionsRenderer();

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(destination)
    this.addMarker(source)
    directionsDisplay.setMap(self.map);
    directionsDisplay.setOptions( { suppressMarkers: true } );

    var request = {
      origin: source,
      destination: destination,
      travelMode: google.maps.TravelMode.DRIVING,
      waypoints:this.snappedCoordinates,
      optimizeWaypoints: true,
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(self.map);
      } else {
        console.log("Directions Request failed: " + status);
      }
    });
  }
  load_map_2(routes)
  {
    var line= new google.maps.Polyline({
      path: routes,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
  });
    line.setMap(this.map);


  }

  addMarker(latlong){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latlong
    });
  }

  openRatingPopup(rate){
  	let addModal = this.modalCtrl.create('FleetRateShipmentPage', {shipment: this.shipment, rate: rate});
    addModal.onDidDismiss(item => {
    if(item != "cancel"){
      if (this.globalVar.current_user.role == 'fleet_owner') {
        this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
      }else{
        this.navCtrl.setRoot("CompletedShipmentPage");
      }

    }
    })
    addModal.present();
  }
  onModelChange(event){
    console.log("changing event",event);
  }


  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }


  openPosted(){
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }

  openCompletedDetail(shipment){
    this.navCtrl.push("CompletedShipmentDetailPage",{shipment: shipment});
  }
  openPodPage()
  {
    this.navCtrl.push('PdfUploadPage',{pdfdoc:this.shipment['documents'],shipment:this.shipment,transitionfrom:'completed'});
  }
  download(shipment){
    this.loader.show("sending...");
    this.invoice.send_in_email(this.shipment.id).subscribe((resp) => {
      if (resp["success"]) {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: resp["success"],
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });
        toast.present();
      }else{
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: resp["error"],
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });

        toast.present();
      }



    });
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

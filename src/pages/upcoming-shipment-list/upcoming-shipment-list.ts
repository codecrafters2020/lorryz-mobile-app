import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController } from 'ionic-angular';
import { Shipment, GlobalVars, Loader} from '../../providers/providers';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-upcoming-shipment-list',
  templateUrl: 'upcoming-shipment-list.html',
})
export class UpcomingShipmentListPage {

	public shipments: any;
  public ascending= true;
  public loaded = false;
  public page = 1;
  country;
  interval: any;
  pickupnow: any;
  companycategory: string;
  companytype: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public shipmentProvider: Shipment,
    public loader: Loader,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private callNumber: CallNumber,
    public globalVar: GlobalVars) {
      if(this.globalVar.current_user.company_id == undefined)
      {
        this.navCtrl.setRoot("LoginPage")
      }

    this.shipments = [];
    this.interval = " "
  }

  ionViewDidLoad() {
    this.companytype = this.globalVar.current_user.company.company_type;
    this.companycategory = this.globalVar.current_user.company.category;

    if((this.companytype=='individual')&&(this.companycategory=='cargo'))
    {
      this.pickupnow=true;
    }

    this.loader.show("");
    this.page = 1
    let self=this;
    this.fetchShipments()
    this.interval = setInterval(function () {
      self.automaticRefresh();

    }, 10000);
  }

  automaticRefresh()
  {
    this.page = 1
    this.fetchShipments()
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  fetchShipments(infiniteScroll = null){

    this.shipmentProvider.upcoming(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }


      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll)

    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  cancelShipment(shipment){
    let addModal = this.modalCtrl.create('CancelShipmentModalPage', {shipment: shipment, cancel_by: "cargo_owner", state_event: "cancel"});
    addModal.onDidDismiss(item => {
      this.navCtrl.setRoot('UpcomingShipmentListPage');
    })
    addModal.present();
  }

  openPosted(){
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      this.navCtrl.setRoot("PostedShipmentPage");
    }
  }


  callFleet(shipment){
    if(this.pickupnow)
    {
      this.callNumber.callNumber(shipment.fleet_owner_number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
      console.log("individual")
    }
    else
    {
      var country = this.globalVar.current_user.country.name
      if(country=="Pakistan")
      {
        this.callNumber.callNumber("+923377131317", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("pakistan")
      }
      else
      {
        this.callNumber.callNumber("+971521250605", true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
        console.log("dubai")

      }
    }
   }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }
  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

}

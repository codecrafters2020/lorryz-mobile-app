import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpcomingShipmentListPage } from './upcoming-shipment-list';

@NgModule({
  declarations: [
    UpcomingShipmentListPage,
  ],
  imports: [
    IonicPageModule.forChild(UpcomingShipmentListPage),
  ],
})
export class UpcomingShipmentListPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverOngoingShipmentListPage } from './driver-ongoing-shipment-list';

@NgModule({
  declarations: [
    DriverOngoingShipmentListPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverOngoingShipmentListPage),
  ],
})
export class DriverOngoingShipmentListPageModule {}

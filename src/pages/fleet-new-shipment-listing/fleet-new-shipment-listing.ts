import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,AlertController, Platform} from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader, Shipment, VehicleProvider} from '../../providers/providers';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import { interval } from 'rxjs/observable/interval';
import * as moment from 'moment';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './../../app/app.component';

declare var google;

@IonicPage()
@Component({
  selector: 'page-fleet-new-shipment-listing',
  templateUrl: 'fleet-new-shipment-listing.html',
})
export class FleetNewShipmentListingPage {

  public timeInterval = 10000;
  public subscriber: any;

  public shipments: any;
  public onGoingShipments : any;
  toggle;
  time;
  public ascending= true;
  public loaded = false;
  public page = 1;
  company_type;
  company_category;
  baseurl;
  pickupnow;
  count:any ;
  shipmentid:any;
  pickuptime;
  str;
  bids:any;
  shipment:any;
  shipment_array:any=[];
  shipment_ongoing
  x;
  dsable;
  counter;
  shipmentPickuptime : any;
  public headers : any;
  button;
  unsub: boolean;
  i: number;
  count1: any;
  callme: boolean=false;
  refresh_counter: any;
  interval: any;
  current_position: any;
  lat: any;
  lng: any;
  ongoing_length = 0;
  ongoing_len: boolean;
  shipment_ongoing_len: any;
  shipment_array1: any;
  callbidsCount;
  shipment_1: any;
  shipmentidNotif='';
  expirepickuptime: any;
  expirestr: any;
  fromNotification: any;
  constructor(public http: HttpClient,
    public platform: Platform,
    public navCtrl: NavController, public navParams: NavParams,
    public shipmentProvider: FleetShipment,
    public loader: Loader,
    public appC: MyApp,
    public globalVar: GlobalVars,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private geolocation: Geolocation,
    public vehicle: VehicleProvider) {
    this.shipments = [];
    this.shipment_ongoing=[];
    this.onGoingShipments = [];
    this.count = [];
    this.shipment_array = [];
    this.shipmentid = [];
    this.shipment_array1=[];
    this.baseurl = globalVar.server_link + "/api/v1/";
    this.toggle = globalVar.current_user.is_online;
    this.counter=0;
    this.i=0;
    this.callme =true;
    this.interval="";
    this.callbidsCount = true
    this.shipmentidNotif=this.navParams.get('shipmentid');
  }

  ionViewDidLoad() {
    console.log(this.globalVar.current_user.notification_count);
    if(this.globalVar.current_user.company_id == undefined)
    {
      this.navCtrl.setRoot("LoginPage")
    }
    this.fromNotification=this.navParams.get('fromNotification');
    this.setheaders();
    this.loader.show("");
    this.callme =false;
    this.page = 1
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
    this.shipments=[];
    this.onGoingShipments = [];
    this.refresh_counter=1;
//    this.bids=[];
  if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {
      this.pickupnow = true;
      if(!this.globalVar.location_initiazed_fleet)
      {
          this.appC.inialize_location_Fleet();
      }
    }
    // let self=this;
    this.fetchShipments();
   this.automaticRefresh();
//      this.logoutseconddevice();
    // this.platform.resume.subscribe(async () => {
    //   this.logoutseconddevice();
    // })
  }

  logoutseconddevice()
  {
    var globalVarDeviceid=this.globalVar.device_id;
      this.http.post( this.globalVar.server_link+'/api/v1/users/db_device_id.json',null,{headers: this.headers})
      .subscribe(async (data)=>{
        if(data["user_device_id"]!=null)
        {
          if(globalVarDeviceid==data["user_device_id"])
          {
            // this.setRootPage();
            // let toast = this.toastCtrl.create({
            //   message: "Device Id match",
            //   duration: 3000,
            //   position: "top"
            // });
            // toast.present();
          }
          else
          {
            this.appC.logout();
              let toast = this.toastCtrl.create({
              message: "User Id already logged in on another device",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
          }

        }


      });

  }

  delay() {
    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  automaticRefresh() {
  let self = this
  self.shipment_array1=self.shipment_array;

  //  self.shipment_array=[];
 this.interval = setInterval(function () {
  // this.callbidsCount = true
    self.page=1
    self.shipment_array1=self.shipment_array;
    self.shipment_array=[];
    self.fetchShipments();

  }, 10000);

  }

  doRefresh(refresher) {
    this.page = 1;
  this.shipment_array1=this.shipment_array;
  this.shipment_array=[];
    // this.callbidsCount = false
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);


  }

  notify(event){
    this.setheaders();
    this.toggle = event.checked
    console.log("headers "+this.toggle);
  this.http.patch(this.baseurl+'users/change_online_status.json',{'is_online' : this.toggle,'device_id':this.globalVar.device_id},
  { headers:this.headers}).subscribe((data) => {

});
}


bidscount()
{
  console.log("bidscount");
  for (var i = 0 ; i < this.shipments.length;i++)
{
  var shipmnt=this.shipments[i];
  var date1 = moment.utc().format('YYYY-MM-DD');


  if(date1 == shipmnt.pickup_date){
        this.pickuptime = shipmnt.pickup_time;
        this.str = this.pickuptime.split('T');
        var s = this.str[1].split('.')
        var shipmenttime = moment.duration(s[0]).asSeconds()

        var h = this.str[1].split(':')
        var h1 = h[0];
        h1 = h1*3600;
        h1=parseInt(h1);
        var min = h[1];
        min = min*60;
        min=parseInt(min);
        var sec = h[2]
        sec=parseInt(sec);
        //.split('.');
        //var shipmenttime = h1+min+sec-60;

        var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');

        var local = moment(date).add(15, 'minutes').format(' HH:mm:ss')

        var localtime = moment.duration(local).asSeconds()
        var time = localtime-shipmenttime;
          let self =this
     if(time < 90 )
      {
        self.button=false
        console.log(time)
      }
      else
      {
        this.button=true
      }
      // if((this.ongoing_length == 0) && (this.shipment_ongoing_len == 0))
      // {
      //   this.ongoing_len=true;
      // }
      // else{
      //   this.ongoing_len=false;
      // }
  }


  else
  {
    // if((this.ongoing_length == 0) && (this.shipment_ongoing_len == 0))
    // {
    //   this.ongoing_len=true;
    // }
    // else{
    //   this.ongoing_len=false;
    // }

    this.time = "0:00";
    this.button = true;

  }

this.shipment_1={ shipment:shipmnt , timer:this.time , counter:this.x, button:this.button} ;
if(i>= this.shipment_array.length){

  this.shipment_array.push(this.shipment_1);

}

}


// if(this.shipments.length == this.shipment_array.length)
// {

//      this.settimer();
// }

}


//  delay(ms: number) {

//   return new Promise( resolve => setTimeout(resolve, ms) );
// }

// async settimer()
// {
//   this.count1= this.shipment_array.length;

//   while(this.count1)
//   {


//     this.count1= this.shipment_array.length;
//   for (var i =0 ;i < this.shipment_array.length;i++) {
//      if(this.shipment_array[i].timer > 0){


//            --this.shipment_array[i].timer;
//            await this.delay(1000);



//    }


//       else
//       {
//         this.count1--;
//         this.shipment_array[i].timer="0:00";
//         this.shipment_array[i].button = true;

//       }





//     }}

// }

setheaders()
{
  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"
  })

}
checkShipmentExpire(shipment)
{
  debugger
  // var shipmenttime=0,date='',local,localtime,time1;
        if(this.shipmentidNotif==shipment.id)
        {
          if(shipment.is_pickup_now)
          {
            debugger
            this.expirepickuptime = shipment.pickup_time;
            this.expirestr = this.expirepickuptime.split('T');
            var s1 = this.expirestr[1].split('.')
            var shipmenttime1 = moment.duration(s1[0]).asSeconds();

            var date1 = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            var local1 = moment(date1).add(15, 'minutes').format(' HH:mm:ss')
            var localtime1 = moment.duration(local1).asSeconds()
            var time1 = localtime1-shipmenttime1;
            if(time1 < 90 )
              {
                this.bidNow('abc',shipment);
                this.shipmentidNotif='';
              }
              else
              {
                if(shipment.is_pickup_now)
                {
                  let toast = this.toastCtrl.create({
                    message: 'Shipment # '+shipment.id+' has expired.',
                    duration: 4000,
                    position: 'top',
                    showCloseButton: true,
                    closeButtonText: "x",
                    cssClass: "toast-danger"
                  });
                  toast.present();
                  this.shipmentidNotif='';
                }
              }
          }
          else
          {
            this.shipmentidNotif='';
          }
        }

}
fetchShipments(infiniteScroll = null){


  this.shipmentProvider.active(this.globalVar.current_user.company_id,1).subscribe((data) => {
      this.onGoingShipments = data
      this.ongoing_length = this.onGoingShipments.length;
      this.removePickUpNowUnwantedShipments();
     },(err) => {
      this.loader.hide();
    })


  // this.shipmentProvider.ongoing(this.globalVar.current_user.company_id,1).subscribe((data) => {
  //   this.shipment_ongoing = data;
  //     this.shipment_ongoing_len = this.shipment_ongoing.length
  // })

    this.shipmentProvider.posted(this.globalVar.current_user.company_id,this.page).subscribe((data) => {
      this.loader.hide();

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;


      }
     if(this.pickupnow==true)
     {
        this.bidscount();

     }
     if(this.shipmentidNotif)
     {
        for(var i=0;i<this.shipments.length;i++)
        {
            // var shipment = this.shipments[i];
            debugger
            if(this.shipmentidNotif==this.shipments[i].id)
            {
              this.checkShipmentExpire(this.shipments[i]);
              break;
            }
            else
            {
              if(this.fromNotification)
              {
                let toast = this.toastCtrl.create({
                  message: 'Shipment # '+this.shipmentidNotif+' has completed or expired.',
                  duration: 4000,
                  position: 'top',
                  showCloseButton: true,
                  closeButtonText: "x",
                  cssClass: "toast-success"
                });
                toast.present();
                this.shipmentidNotif='';
                this.navCtrl.pop();
                break;
              }
              if(this.shipments[i].is_pickup_now && !this.fromNotification)
              {
                let toast = this.toastCtrl.create({
                  message: 'Shipment # '+this.shipmentidNotif+' has expired.',
                  duration: 4000,
                  position: 'top',
                  showCloseButton: true,
                  closeButtonText: "x",
                  cssClass: "toast-danger"
                });
                toast.present();
                this.shipmentidNotif='';
                break;
              }
            }
        }
     }
      this.page = this.page+1;
      this.loaded = true
      this.infiniteScrollComplete(infiniteScroll);


    },(err) => {
      this.loader.hide();
      this.infiniteScrollComplete(infiniteScroll)
    })
  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }

  postedShipmentDetail(){
    // event.preventDefault();
  }
  bidNow(event,shipment){
    if(this.pickupnow)
{
  //   if((this.ongoing_length == 0) && (this.shipment_ongoing_len == 0))
  //   {
      let user = this.globalVar.current_user;
      if (user.is_max_period_amount_overdue){
        let alert = this.alertCtrl.create({
        title: 'Attention!',
        message: 'Your max credit period/amount is expired, please make payment to proceed further',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
        ]
        });
        alert.present();
      }else{
        // event.preventDefault();
        console.log("i am in shipments",shipment)
        this.navCtrl.push("FleetNewShipmentBiddingPage",{shipment: shipment});
      }

    // }
//     else{
//       let alert = this.alertCtrl.create({
//         title: 'Attention!',
//         message: 'Shipment already in progress',
//         buttons: [
//           {
//             text: 'OK',
//             role: 'cancel',
//             handler: () => {
// //              console.log('Cancel clicked');
//             }
//           },
//         ]
//         });
//         alert.present();
//     }
  }
  else
  {
    let user = this.globalVar.current_user;
    if (user.is_max_period_amount_overdue){
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Your max credit period/amount is expired, please make payment to proceed further',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }else{
      // event.preventDefault();
      console.log("i am in shipments",shipment)
      this.navCtrl.push("FleetNewShipmentBiddingPage",{shipment: shipment});
    }
  }

}

removePickUpNowUnwantedShipments(){
console.log("functin called");
if(this.onGoingShipments!=null && this.onGoingShipments.length>0){
 for(var i =0; i<this.onGoingShipments.length; i++){
    if(this.onGoingShipments[i].is_pickup_now){
      var shipment = this.onGoingShipments[i];
      var currDate = moment.utc().format('YYYY-MM-DD');
      if(currDate == shipment.pickup_date){
        this.shipmentPickuptime = shipment.pickup_time;
        this.str = this.shipmentPickuptime.split('T');
        var s = this.str[1].split('.')
        var shipmenttime = moment.duration(s[0]).asSeconds();

        // this.shipmentPickuptime = shipment.pickup_time;
        // this.str = this.shipmentPickuptime.split('T');
        // var h = this.str[1].split(':')
        // var h1 = h[0];
        // h1 = h1*3600;
        // h1=parseInt(h1);
        // var min = h[1];
        // min = min*60;
        // min=parseInt(min);
        // var sec = h[2]
        // sec=parseInt(sec);
        // var shipmenttime = h1+min+sec-60;
        // console.log("shipment pickuptime "+shipmenttime);
        var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        var local = moment(date).add(15, 'minutes').format(' HH:mm:ss');
        var localtime = moment.duration(local).asSeconds()
        var time = localtime-shipmenttime;
      if(time>180){
          this.cancelShipments(shipment);
        }
      }else{
        this.cancelShipments(shipment);
      }
    }
  }
}
}


cancelShipments(shipment)
{
  clearInterval(this.x);
  if(shipment.is_pickup_now)
  {
//this.globalVar.rebid=false
  this.shipmentProvider.quitBid(this.globalVar.current_user.company_id,shipment.id).subscribe(data => {
    this.loader.hide();
    let toast = this.toastCtrl.create({
      message: "Bid Cancelled on Active Shipments",
      duration: 6000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "x",
      cssClass: "toast-success"
    });
    toast.present();
    // this.navCtrl.setRoot('FleetActiveShipmentBiddingPage')
  },(err) => {
    this.loader.hide();
  });

  }

}

  notInterested(shipment_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: "Do you want to remove this shipment from your list?" ,
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.notInterested(shipment_id,this.globalVar.current_user.company_id).subscribe(data => {
            this.loader.hide();
            this.navCtrl.setRoot("FleetNewShipmentListingPage");
            let toast = this.toastCtrl.create({
              message: data["success"],
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();

          },(err) => {
            this.loader.hide();
          });
        }
      },
    ]
    });
    alert.present();
  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)



  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  ngOnDestroy()
  {
    let self=this
    console.log("view leave");
      if (this.interval) {
        clearInterval(this.interval);
      }


  }

  // ionViewWillEnter(){

  //   let self=this
  //   console.log("view leave did leave");
  //     if (this.interval) {
  //       clearInterval(this.interval);
  //     }
  //}
}

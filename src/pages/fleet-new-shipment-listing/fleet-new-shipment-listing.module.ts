import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetNewShipmentListingPage } from './fleet-new-shipment-listing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetNewShipmentListingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetNewShipmentListingPage),
    TranslateModule.forChild()
  ],
})
export class FleetNewShipmentListingPageModule {}

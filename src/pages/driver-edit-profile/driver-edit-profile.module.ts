import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverEditProfilePage } from './driver-edit-profile';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    DriverEditProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(DriverEditProfilePage),
    TranslateModule.forChild()
  ],
})
export class DriverEditProfilePageModule {}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { FleetShipment,VehicleProvider,GlobalVars, Loader, InvoiceProvider } from '../../providers/providers';
import { Ionic2Rating } from "ionic2-rating";
declare var google;
declare var require: any;
@IonicPage()
@Component({
  selector: 'page-fleet-completed-shipment-detail',
  templateUrl: 'fleet-completed-shipment-detail.html',
})
export class FleetCompletedShipmentDetailPage {
  @ViewChild('map') mapElement: ElementRef;
  public shipment;
  public current_user_rating: any;
  public other_user_rating: any;
  public company_rating_length = 0;
  map: any;
  company_category: any;
  company_type: any;
  vehicles :any;
  apiKey: string;
  options = {

   };
  pickupnow: boolean;
  routes: any;
  snappedCoordinates: any;
  showAmount: boolean=false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public shipmentProvider: FleetShipment,
              public loader: Loader,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              public invoice: InvoiceProvider,
              public globalVar: GlobalVars) {
                  this.routes=[];
                  this.snappedCoordinates=[];
    this.shipment = this.navParams.get("shipment");
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };
    const self = this;
    if(this.globalVar.lang==true){
      debugger;;
      var googleTranslate = require('google-translate')(this.apiKey, this.options);
      debugger;;
      googleTranslate.translate(self.shipment.drop_location, 'ur', function(err, translation) {
        self.shipment.drop_location = translation.translatedText; });
      googleTranslate.translate(self.shipment.pickup_location, 'ur', function(err, translation) {
        self.shipment.pickup_location = translation.translatedText; });
    console.log(this.shipment);
  }
    this.vehicles = [];
  }

  ionViewDidLoad() {
    console.log(this.shipment)
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
    if(this.shipment.bids)
    {
      this.showAmount=true;
    }
    if((this.company_type=='individual')&&(this.company_category=='fleet'))
    {
      this.pickupnow=true;
    }

    let that = this;

    if (this.shipment['company_ratings']) {
      this.company_rating_length = this.shipment['company_ratings'].length

      // if (this.shipment['company_ratings'].length > 0) {
        this.current_user_rating = this.shipment['company_ratings'].map(function(rating) {
          if (rating.giver_id == that.globalVar.current_user.company_id){
            return rating;
          }

        }).filter(function(e){return e})[0];

        this.other_user_rating = this.shipment['company_ratings'].map(function(rating) {
          if (rating.giver_id != that.globalVar.current_user.company_id){
            return rating;
          }

        }).filter(function(e){return e})[0];

    }else{
      this.company_rating_length = 0
      this.current_user_rating = 0
      this.other_user_rating= 0
    }

     console.log("this.globalVar",this.globalVar.current_user);

    this.vehicles = this.shipment["vehicles"];

    // }
    // if(this.shipment.route!=null)
    // {
    //  if(this.shipment.route.length>0)
    //  {
    //   this.load_map_proper_routes();
    //  }
    //  else
    //  {
    //    this.load_map();
    //  }
    // }
    // else
    // {
      this.load_map();
    // }
   let self = this;
    if(this.pickupnow)
    {
      if(this.shipment.state != 'cancel')
      {
        if(!(self.current_user_rating))
        {
            self.openRating()
        }
      }

    }
  }
  openPodPage()
  {
    this.navCtrl.push('PdfUploadPage',{pdfdoc:this.shipment['documents'],shipment:this.shipment,transitionfrom:'completed'});
  }
  load_map_proper_routes()
  {
    for(var i=0;i<this.shipment.route.length;i++)
    {
      var route = {lat:parseFloat(this.shipment.route[i].lat),lng:parseFloat(this.shipment.route[i].lng)};
      this.routes.push(route);
    }
    let source = new google.maps.LatLng(parseFloat(this.shipment.route[0].lat),parseFloat(this.shipment.route[0].lng));
    let source1 = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

    let mapOptions = {
      center: source,
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }
    let self = this;
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(source1);
    this.addMarker(destination)
    this.load_map_2(this.routes);

  }


  load_map(){
    var s,s1,f,s2,l,f1,feed;
    this.snappedCoordinates=[];
    if(this.shipment.route)
    {
      for(var i=0;i<this.shipment.route.length;i++)
      {
        s = this.shipment.route[i].split('>');
        s1 = s[1].split(',');
        f = s1[0].split('"')
        s2 = s[2].split('}');
        l = s2[0].length;
        f1 = s2[0].slice(0,(l-1));
        var route = f + "," + f1;
        //  let latlng = new google.maps.LatLng({lat:parseFloat(f),lng:parseFloat(f1)});
        feed = {location: route, stopover: true};
        this.snappedCoordinates.push(feed);
      }
    }
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();


    let source = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

    let mapOptions = {
      center: source,
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }
    let self = this;

    directionsDisplay = new google.maps.DirectionsRenderer();

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(destination)
    this.addMarker(source)
    directionsDisplay.setMap(self.map);
    directionsDisplay.setOptions( { suppressMarkers: true } );

    var request = {
      origin: source,
      destination: destination,
      travelMode: google.maps.TravelMode.DRIVING,
      waypoints:this.snappedCoordinates,
      optimizeWaypoints: true,
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(self.map);
      } else {
        console.log("Directions Request failed: " + status);
      }
    });
  }
  load_map_2(routes)
  {
    var line= new google.maps.Polyline({
      path: routes,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
  });
    line.setMap(this.map);


  }

  addMarker(latlong){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latlong
    });
  }

  openRating(){
  	let addModal = this.modalCtrl.create('FleetRateShipmentPage', {shipment: this.shipment});
    addModal.onDidDismiss(item => {
    if(item != "cancel"){
      this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
    }
    })
    addModal.present();
  }

  openRatingPopup(rate){
  	let addModal = this.modalCtrl.create('FleetRateShipmentPage', {shipment: this.shipment, rate: rate});
    addModal.onDidDismiss(item => {
    if(item != "cancel"){
      this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
    }
    })
    addModal.present();
  }

  onModelChange(event){
    console.log("changing event",event);
  }

  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompletedDetail(shipment){
    this.navCtrl.push("FleetCompletedShipmentDetailPage",{shipment: this.shipment});
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }


  download(){
    this.loader.show("sending...");
    this.invoice.send_in_email(this.shipment.id).subscribe((resp) => {
      if (resp["success"]) {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: resp["success"],
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });
        toast.present();
      }else{
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: resp["error"],
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });

        toast.present();
      }



    });
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }


}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetCompletedShipmentDetailPage } from './fleet-completed-shipment-detail';
import { Ionic2RatingModule } from "ionic2-rating";
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    FleetCompletedShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetCompletedShipmentDetailPage),
    Ionic2RatingModule,
    TranslateModule.forChild()
  ],
})
export class FleetCompletedShipmentDetailPageModule {}

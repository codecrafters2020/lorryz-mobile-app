import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditCompanyInformationPage } from './edit-company-information';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    EditCompanyInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(EditCompanyInformationPage),
    TranslateModule.forChild()
  ],
})
export class EditCompanyInformationPageModule {}

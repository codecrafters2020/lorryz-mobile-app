import { Component, ViewChild, ElementRef } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { IonicPage, NavController, ToastController, Platform, Slides } from "ionic-angular";
import { Geolocation } from '@ionic-native/geolocation';

import { User, GlobalVars, Loader } from "../../providers/providers";
import {
  MainPage,
  ActivateAccountPage,
  LoginPage,
  ForgotPasswordPage
} from "../pages";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { ValidationError } from "../../validators/validators";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { parseNumber } from "libphonenumber-js";

import list from "country-list";
import { Diagnostic } from "@ionic-native/diagnostic";
import { LocationAccuracy } from "@ionic-native/location-accuracy";
let countries = list();
declare var google;


@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  // account: { name: string, email: string, password: string } = {
  //   name: 'Test Human',
  //   email: 'test@example.com',
  //   password: 'test'
  // };

  // Our translated text strings
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;
  private signupErrorString: string;
  public account: FormGroup;
  serverError: any;
  public _countries: any;
  map: any;
  current_position: any;
  latLng: any;
  count: any;
  abc: any;
  len: number;
  _country: any;
  country: string;
  constructor(
    public navCtrl: NavController,
    public user: User,
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    public globalVar: GlobalVars,
    public storage: Storage,
    public validationError: ValidationError,
    public loader: Loader,
    private iab: InAppBrowser,
    public toastCtrl: ToastController,
    public platform: Platform,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy

      ) {
    this.translateService.get("SIGNUP_ERROR").subscribe(value => {
      this.signupErrorString = value;
    });

    this.account = this.formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      terms_accepted: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/^(true)$/)
        ])
      ],
      email: ["", ""],
      country: ["", Validators.required],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      mobile: [
        "",
        Validators.compose([
          Validators.required
          // Validators.pattern(/^(?:\+971|00971|0)(?:2|3|4|6|7|9|50|51|52|55|56)[0-9]{7}$/)
        ])
      ]
    });

    this._countries = [];
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.user.getCountries().subscribe(
      data => {
        this._countries = data;
        this.len = this._countries.countries.length;
        this.loader.hide();
        this.initialize_autocomplete()
      },
      err => {}
    );
  }

  initialize_autocomplete() {
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled) {
          this.askForHighAccuracy();
        } else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.askForHighAccuracy();
          });
        }
      })
    } else {
      this.loadMap();
    }
  }


  askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
        this.loadMap();
      });
  }

  loadMap() {

   if (this.map) {
     return this.map;
   }

   let self = this;
   // this.loader.show("Loading the Map")
   this.geolocation.getCurrentPosition().then((position) => {
     // this.loader.hide()

     this.current_position = position;
     console.log(this.current_position);
     this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

     var geocoder = new google.maps.Geocoder;
     const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

     geocoder.geocode({ 'location': this.latLng }, function (results, status) {
       if (status === google.maps.GeocoderStatus.OK) {

         if (results[1]) {
           var indice=0;
           for (var j=0; j<results.length; j++)
           {
               if (results[j].types[0]=='locality')
                   {
                     indice=j;
                       break;
                   }
               }

           for (var i=0; i<results[j].address_components.length; i++)
               {

                   if (results[j].address_components[i].types[0] == "country") {
                           //this is the object you are looking for
                         self.count = results[j].address_components[i];
                         self.abc = self.count.long_name;
                         console.log(self.abc)

                       }




                     }
                     self.countree(self.abc)
       } else {
      //   window.alert('Geocoder failed due to: ' + status);
       }}
     });


     });


   }

   countree(abc){
     let self =this;
     // console.log(abc)

     for(var i=0; i<this.len;i++)
       {
         if(abc == this._countries.countries[i].name)
         {
             this.country = this._countries.countries[i].dialing_code;
             console.log(this.country)
             console.log(this.abc)
             document.getElementById("country_name").nodeValue = this.country;

           }
       }

       if(this.abc=="Pakistan")
       {
        document.getElementById("country_number").innerText="For any assistance, Please call us at +92 337 7131317 Or send us email us at contact@lorryz.com"

       }
       else
       {
        document.getElementById("country_number").innerText="For any assistance, Please call us at +971 56 5466088 Or send us email us at contact@lorryz.com"

       }



   }





  signIn(event) {
    event.preventDefault();
    this.navCtrl.setRoot(LoginPage);
  }

  validate() {
    this.validationError.populateErrorMessageonSubmit(this.account);
  }

  verifyPhone() {
    let _mobile = this.account["value"]["mobile"];

    let that = this;

    let country = this._countries.countries.filter(obj => {
      return obj.dialing_code == that.account["value"]["country"];
    })[0];

    console.log("country.code", country.name);

    let country_initial = countries.getCode(country.name);

    let _parse_number = parseNumber("Phone: " + _mobile, country_initial, {
      extended: true
    });
    return _parse_number;
  }

  doSignup() {
    let num = this.verifyPhone();

    if (!num["valid"]) {
      let toast = this.toastCtrl.create({
        message: "Phone number is not valid",
        duration: 6000,
        position: "top",
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    } else {
      this.validationError.populateErrorMessageonSubmit(this.account);
      this.loader.show("");
      let _account = {};
      _account["user"] = this.account["value"];

      _account["user"]["mobile"] =
        "+" + num["countryCallingCode"] + num["phone"];

      _account["user"] = Object.assign({}, _account["user"], {
        device_id: this.globalVar.device_id,
        os: this.globalVar.os
      });

      this.user.signup(_account).subscribe(
        resp => {
          if (resp && !resp["errors"]) {
            this.storage.ready().then(() => {
              this.globalVar.current_user = resp["user"];
              this.storage.set("currentUser", resp["user"]);
              this.loader.hide();
              this.navCtrl.setRoot(ActivateAccountPage);
            });
          } else {
            this.loader.hide();
            this.serverError = resp["errors"][0];

            let toast = this.toastCtrl.create({
              message: resp["errors"][Object.keys(resp["errors"])[0]],
              duration: 6000,
              position: "top",
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();
          }
        },
        err => {
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Some Went Wrong. Please Try Later",
            duration: 6000,
            position: "top",
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        }
      );
    }
  }

  openTermsConditions() {
    const browser = this.iab.create(
      "http://lorryz.com/dashboard/terms_and_conditions/",
      "_blank",
      "location=yes"
    );
  }

  openPrivacy() {
    const browser = this.iab.create(
      "https://lorryz.com/dashboard/privacy_policy",
      "_blank",
      "location=yes"
    );
  }
}

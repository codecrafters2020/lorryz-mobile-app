import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostedShipmentDetailPage } from './posted-shipment-detail';
import { Ionic2RatingModule } from "ionic2-rating";
@NgModule({
  declarations: [
    PostedShipmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PostedShipmentDetailPage),
    Ionic2RatingModule
  ],
})
export class PostedShipmentDetailPageModule {}

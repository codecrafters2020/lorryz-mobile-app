import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController, ViewController  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { Shipment, GlobalVars, Loader} from '../../providers/providers';
import { Ionic2Rating } from "ionic2-rating";
import { interval } from 'rxjs/observable/interval';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from "@ionic/storage";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
declare var google;


@IonicPage()
@Component({
  selector: 'page-posted-shipment-detail',
  templateUrl: 'posted-shipment-detail.html',
})
export class PostedShipmentDetailPage {

  public timeInterval = 10000;
  public subscriber: any;
  shipment: any;
  bids: any;
  ascending=true;
  src;
  companytype;
  companycategory;
  server;
  button
  x
  time
  pickuptime;
  pickupnow: boolean;
  str;
  y;
  shipment_obj: any;
  cancel_by: any;
	state_event: any;
  cancel_reason: any;
  temp_time;
  t: number;
  time2: any;
  doc;
  pick: any;
  array_bids :any;
  arr_bid = [];
  distance;
  arr_bids: any[];
  source: any;
  headers: any;
  baseurl: string;
  email: any;
  sentemail: any;
  resultstorage: any;
  fromNotification: boolean;
    constructor(    public http: HttpClient,
      public storage: Storage,
      public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    public shipmentProvider: Shipment,
    public loader: Loader,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public globalVar: GlobalVars,
    private sanitizer: DomSanitizer) {
    this.shipment = {};
    this.sentemail=[];
    this.resultstorage=[];
    this.bids = [];
    this.arr_bid = [];
      this.email=[];
    // this.src= this.navParams.get('src');
    // console.log( this.navParams.get("src"));
    this.baseurl = globalVar.server_link + "/api/v1/email/send_shipment_email.json";
    this.companytype = this.globalVar.current_user.company.company_type;
    this.companycategory = this.globalVar.current_user.company.category;
    this.server = this.globalVar.server_link;
    this.cancel_reason = "My customer cancelled the shipment"
    this.fromNotification=this.navParams.get('fromNotification');
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.shipmentProvider.show(this.globalVar.current_user.company_id, this.navParams.get('shipment_id')).subscribe(data => {
      this.loader.hide();
      console.log(data)
      this.shipment = data;
      this.bids = this.shipment.bids;
      this.pickupdate();
     this.calcRoute();
    },(err) => {
      this.loader.hide();
    });

     this.source = interval(10000);
    this.subscriber = this.source.subscribe(val => {

      this.shipmentProvider.show(this.globalVar.current_user.company_id,this.navParams.get('shipment_id'))
      .subscribe((data) => {
        console.log('data');
        console.log(data);
        this.loader.hide();
        this.shipment = data;
        this.arr_bids= this.arr_bid;
        this.arr_bid=[]
        this.bids = this.shipment.bids;

        this.calcRoute();

      });
    });

      this.cancelShipments();

    if((this.companycategory == 'cargo')&&(this.companytype == 'individual'))
    {
      this.pickupnow = true;

    }




  }

  async calcRoute() {

    // console.log(this.bids[0].lat+this.bids[0].lng)
    this.bids = this.shipment.bids;
    var bidd;

    let self = this;
    for (const bidd of self.bids)
    {
     //  bidd = self.bids[i]
     await this.calculate(bidd)
    }
    // console.log("array_bids"+this.array_bids);
    // console.log("array_bids"+this.arr_bid);
  }

   delay() {
    return new Promise(resolve => setTimeout(resolve, 300));
  }
  async calculate(bidd)
  {
    await this.delay();
     console.log(bidd)
    let self = this;

    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer();
    console.log("bidd"+bidd.lat+bidd.lng)
    var haight = new google.maps.LatLng(bidd.lat, bidd.lng);
    var oceanBeach = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);

    var request = {
      origin: haight,
      destination: oceanBeach,
      travelMode: 'DRIVING'
  };
  directionsService.route(request, function(response, status) {
    if (status == 'OK') {

      directionsRenderer.setDirections(response);
        //      console.log(response);
    self.distance = response.routes[0].legs[0].duration.text;
  // document.getElementById("dis").innerText = this.distance;
    console.log(this.distance)
  //  console.log(self.i)
 //   self.i++;
  // self.array_bids = {bids:bidd, duration:this.distance};
  //this.distances+;
    self.arr_bid.push(self.distance);
  }



  });

  }


  pickupdate()
  {
    if((this.shipment.state=='completed' || this.shipment.state=='ongoing' || this.shipment.state=='accepted') && this.fromNotification)
    {
      let toast = this.toastCtrl.create({
        message:"Shipment # "+this.shipment.id+" status has "+ this.shipment.state,
        duration: 5000,
        position: "top",
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success"
      });
      toast.present();
      this.navCtrl.pop();
    }

    this.pick = this.shipment.is_pickup_now

    if(this.pickupnow == true){

      if(this.pick == true){

        const sel = this;

        var date1 = moment.utc().format('YYYY-MM-DD');

      if(date1 == this.shipment.pickup_date)
      {

      this.pickuptime = this.shipment.pickup_time;
    this.str = this.pickuptime.split('T');
    console.log("pickup time "+this.str[1]);
    var h = this.str[1].split(':')
    var h1 = h[0];
    h1 = h1*3600;
    h1=parseInt(h1);
    var min = h[1];
    min = min*60;
    min=parseInt(min);
    var sec = h[2]
    sec=parseInt(sec);
    var shipmenttime = h1+min+sec-60;
    console.log("shipment pickuptime "+shipmenttime);

    var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
    console.log("local date "+date)

    var local = moment(date).format(' HH:mm:ss');
    console.log("local time "+local)

    var localtime = moment.duration(local).asSeconds()

    console.log("local time "+localtime)
    var time = localtime-shipmenttime;
    var time1 =180- time;
    this.temp_time=time1;

    if(time1 >180 )
     time1 = 180;
    console.log("] time "+time1)
    this.time = time1;
    console.log("] this.time "+this.temp_time)
    const self = this;

  this.x = setInterval(function () {

    this.time = time1;
    this.temp_time=this.time;
    if(this.time >90)
    this.temp_time = this.time-90;
    document.getElementById("time").innerText = this.temp_time;
    if( --time1 < 0 )
    {
     clearInterval(this.x);
     if(self.bids.length>0)
     {
      self.storage.get("email").then(result=>{
        self.resultstorage=result;
        console.log(result)
        self.email=result;
        var e=0;
        if(result!=null && result.length!=0)
        {
         for(var i=0;i<result.length;i++)
         {
          if(result[i].shipmentid!=undefined)
           {
            if(result[i].shipmentid==self.shipment.id){
//                if(result[i].email=="sent")
//                {
//                  console.log("email sent");
//                  break;
//                }
//                else{
//                  e++;
// //                 self.send_email_no_bids_accepted();
//                }
                  e=0;
                  break;
             }
             else{
               e++;
//              self.send_email_no_bids_accepted();
             }
           }
         }
         if(e)
         {
          self.send_email_no_bids_accepted();
         }
        }
        else
           {
            self.send_email_no_bids_accepted();
           }

        });

     }
     else
     {
      self.storage.get("email").then(result=>{
        self.resultstorage=result;
        console.log(result)
        var e=0;
       if(result!=null  && result.length!=0)
       {
        for(var i=0;i<result.length;i++)
            {
              if(result[i].shipmentid==self.shipment.id){
  //             if(result[i].email=="sent")
  //             {
  //               console.log("email sent")
  //               break;
  //             }
  //             else{
  //               e++;
  // //              self.send_email();
  //             }
              e=0;
              break;
            }
            else{
              e++;
//                self.send_email();
            }

          }
          if(e)
          {
            self.send_email();
          }
       }
       else
          {
            self.send_email();
          }
        });

     }
//     self.cancelShipments();
    console.log("clear interval")
    document.getElementById("time").innerText = "0:00";
    }
    if(time1 <= 90)
    {
    document.getElementById("secondtimer").innerText = "Time left to accept bids";
    this.doc =  true;
    if(self.bids.length == 0 && time1>0)
    {
      // for(var i=0;i<this.email.length;i++)
      // {
        self.storage.get("email").then(result=>{
          self.resultstorage=result;
          console.log(result)
          var e =0;
          if(result!=null  && result.length!=0)
         {
          for(var i=0;i<result.length;i++)
          {
            if(result[i].shipmentid!=undefined)
            {
              if(result[i].shipmentid==self.shipment.id){
                // if(result[i].email=="sent")
                // {
                //   console.log("email sent")
                //   break;
                // }
//                 else{
//                   e++;
// //                  self.send_email();
//                 }
                  e=0;
                  break;
              }
              else{
                e++
//                self.send_email();
              }

            }
          }
          if(e)
          {
            self.send_email();
          }
         }
         else
            {
              self.send_email();
            }



          });
      // }
//      self.cancelShipments();
    }


  }



},1000);



}
  else
      {
//        sel.cancelShipments();
          var t1 = "0:00"
          document.getElementById("time").innerText = t1 ;
      }
    }
  }

  }

  send_email()
  {
    var localemail={email:"sent",shipmentid:this.shipment.id};
    this.sentemail.push(localemail);
    if(this.resultstorage)
    {
        for(var i=0;i<this.resultstorage.length;i++)
        {
          var loca = {email:this.resultstorage[i].email,
            shipmentid:this.resultstorage[i].shipmentid};
            this.sentemail.push(loca);
        }
    }
    // this.sentemail.push(this.resultstorage);

    this.storage.set("email",this.sentemail);
    this.setheaders();
    this.http.post(this.baseurl,{shipment_id:this.shipment.id,shipment_status:"no_bids"}
    ,{headers:this.headers}).subscribe((data: any) =>{
        console.log(data);
      });
  }
  send_email_no_bids_accepted()
  {
    var localemail={email:"sent",shipmentid:this.shipment.id};
    this.sentemail.push(localemail);
    if(this.resultstorage)
    {
      for(var i=0;i<this.resultstorage.length;i++)
        {
          var loca = {email:this.resultstorage[i].email,
            shipmentid:this.resultstorage[i].shipmentid};
            this.sentemail.push(loca);
        }
    }

    // this.sentemail.push(this.resultstorage);
    this.setheaders();
    this.storage.set("email",this.sentemail);
    this.http.post(this.baseurl,{shipment_id:this.shipment.id,shipment_status:"no_bids_accepted"}
    ,{headers:this.headers}).subscribe((data: any) =>{
        console.log(data);
      });
  }
  cancelShipments(){

    clearInterval(this.x);
    if(!(this.shipment.state == "completed"))
    {
        let toast = this.toastCtrl.create({
          message:"Lorryz is connecting you to a driver soon.",
          duration: 10000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });
        toast.present();
    }


    // let addMod = this.modalCtrl.create('CancelShipmentPage', {shipment: this.shipment,
    //   cancel_by: "cargo_owner", state_event: "cancel"});
    // addMod.onDidDismiss(item => {
    //   this.navCtrl.setRoot('PostedShipmentPage');
    // })
    // addMod.present();

  }

  setheaders()
  {

  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"

  });

  }




acceptBid(bid_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: 'Are you sure you want to accept this bid?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.acceptBid(this.globalVar.current_user.company_id, this.shipment.id,bid_id).subscribe(data => {
            console.log(data);
            this.loader.hide();
            this.navCtrl.setRoot("UpcomingShipmentListPage");
          },(err) => {
            this.loader.hide();
          });
        }
      }
    ]
    });
    alert.present();
  }
  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
}
  rejectBid(bid_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: 'Are you sure you are not interested in this bid?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.rejectBid(this.globalVar.current_user.company_id, this.shipment.id,bid_id).subscribe(data => {
            console.log(data);
            this.loader.hide()
            this.shipment = data;
          },(err) => {
            this.loader.hide()
          });
        }
      }
    ]
    });
    alert.present();
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

    this.ascending ?
      this.shipment.bids.sort((a, b) => a.amount !== b.amount ? a.amount < b.amount ? -1 : 1 : 0)
      :
      this.shipment.bids.sort((a, b) => a.amount !== b.amount ? a.amount < b.amount ? 1 : -1 : 0)

  }

  cancelShipment(){
    console.log("canel shipment")
    let addModal = this.modalCtrl.create('CancelShipmentModalPage', {shipment: this.shipment, cancel_by: "cargo_owner", state_event: "cancel"});
    addModal.onDidDismiss(item => {
      this.navCtrl.setRoot('PostedShipmentPage');
    })
    addModal.present();
  }

  openPosted(){
    this.navCtrl.setRoot("PostedShipmentPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    // this.navCtrl.setRoot("NewShipmentPage");
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  ionViewDidLeave(){
    console.log("view leave");
    this.subscriber.unsubscribe();
    clearInterval(this.x);
    clearInterval(this.t);
    clearInterval(this.source);

  }
}

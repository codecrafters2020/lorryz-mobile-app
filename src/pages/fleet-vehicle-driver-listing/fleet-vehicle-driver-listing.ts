import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController,ItemSliding,Item  } from 'ionic-angular';
import { VehicleListing,DriverListing} from '../pages'
import { CallNumber } from '@ionic-native/call-number';
import { User,VehicleProvider, GlobalVars, Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-fleet-vehicle-driver-listing',
  templateUrl: 'fleet-vehicle-driver-listing.html',
})
export class FleetVehicleDriverListingPage {
  public vehicles: any;
  public _vehicle: any;
  public ascending= true;

  public status: '';
  public from_pickup_date:'';
  public to_pickup_date:'';
  public toggle = true;
  public load = true;
  public disable_new_vehicle = true;
  activeItemSliding: ItemSliding = null;
  company_type: any;
  company_category: any;
  pickupnow: boolean;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public vehicle: VehicleProvider,public modalCtrl: ModalController,
              public toastCtrl: ToastController,public loader: Loader,
              private callNumber: CallNumber,
              public globalVar: GlobalVars) {
    this.vehicles = []
  }

  ionViewDidLoad() {
    this.loader.show("");
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
    this.vehicle.getVehicles().subscribe((resp) => {
      this.loader.hide();
      this.vehicles= resp;
      if(this.globalVar.current_user.company.company_type == 'individual' && this.vehicles.length < 1){
        this.disable_new_vehicle = false;
      }
      if(this.globalVar.current_user.company.company_type == 'company'){
        this.disable_new_vehicle = false;
      }

    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
    if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
    {
      this.pickupnow = true;
    }
  }

  loadVechile(){
  	this.navCtrl.setRoot('FleetVehicleDriverListingPage');
  }
  loadDriver(){
  	this.navCtrl.setRoot(DriverListing);
  }

  changeDriver(vehicle) {

    let addModal = this.modalCtrl.create('FleetChangeDriverModalPage',{vehicle: vehicle});
    addModal.onDidDismiss(item => {
      this.navCtrl.setRoot('FleetVehicleDriverListingPage');
    })
    addModal.present();
  }
  addNewVehicle(){
   this.navCtrl.push('FleetAddVehiclePage');
  }
  editVehicle(vehicle){
   this.navCtrl.push('FleetEditVehiclePage',{vehicle_id: vehicle.id});
  }

  editDriver(id){
    // event.preventDefault();
    console.log("editDriver",id);
   this.navCtrl.push('FleetEditVehiclePage',{vehicle_id: id});
  }

  deleteVehicle(id) {

    this.loader.show("");
    this.vehicle.deleteVehicle(id).subscribe((resp) => {
      this.loader.hide();
      this.navCtrl.setRoot('FleetVehicleDriverListingPage');


    }, (err) => {
      this.loader.hide();
    });
  }

  filter(){
    if (this.status && this.from_pickup_date && this.to_pickup_date) {
      this.applyFilter()
    }else if((this.status == undefined || this.status == '') && (this.from_pickup_date == undefined || this.from_pickup_date == '') && (this.to_pickup_date == undefined || this.to_pickup_date == '')){
      this.applyFilter()
      
    }else if(this.status == undefined || this.status == ''){
      this.showError('Vehicle state is required');
    }else if(this.from_pickup_date == undefined || this.from_pickup_date == ''){
      this.showError('Date range are required');
    }else if(this.to_pickup_date == undefined || this.to_pickup_date == ''){
      this.showError('Date range are required');
    }else{
      this.applyFilter()
    }
    
  }

  showError(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
     showCloseButton: true,
     closeButtonText: "x",
     cssClass: "toast-danger"
    });
    toast.present();
  }

  applyFilter(){
    this.loader.show("");

    let filter = {state: this.status,not_available_from: this.from_pickup_date ? this.from_pickup_date : '' , not_available_to: this.to_pickup_date ? this.to_pickup_date : '' ,company_id: this.globalVar.current_user.company_id };
    this.vehicle.getVehiclesByFilter(filter).subscribe((resp) => {
      this.loader.hide();
      this.vehicles = resp
    }, (err) => {
      this.loader.hide();
    });  
  }

  sortBy(){

    // event.preventDefault();
    this.ascending=this.ascending ? false : true;

   this.ascending ?
     this.vehicles.sort((a, b) => a.vehicle_type.name !== b.vehicle_type.name ? a.vehicle_type.name < b.vehicle_type.name ? -1 : 1 : 0)
     :
     this.vehicles.sort((a, b) => a.vehicle_type.name !== b.vehicle_type.name ? a.vehicle_type.name < b.vehicle_type.name ? 1 : -1 : 0)




  }

  showHide(){

    if (this.toggle) {
      this.load = false;
      this.toggle = false

    }else{
      this.toggle = true
      this.load = false;
    }
  }

  clear(){
    this.status='';

    this.from_pickup_date='';

    this.to_pickup_date='';

  }

  openOption(itemSlide: ItemSliding, item: Item) {
    console.log('opening item slide..');

    if(this.activeItemSliding!==null) //use this if only one active sliding item allowed
     this.closeOption();

    this.activeItemSliding = itemSlide;

    let swipeAmount = 240; //set your required swipe amount
    itemSlide.startSliding(swipeAmount);
    itemSlide.moveSliding(swipeAmount);

    itemSlide.setElementClass('active-options-right', true);
    itemSlide.setElementClass('active-swipe-right', true);

    item.setElementStyle('transition', null);
    item.setElementStyle('transform', 'translate3d(-'+swipeAmount+'px, 0px, 0px)');
   }

   closeOption() {
    console.log('closing item slide..');

    if(this.activeItemSliding) {
     this.activeItemSliding.close();
     this.activeItemSliding = null;
    }
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetVehicleDriverListingPage } from './fleet-vehicle-driver-listing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetVehicleDriverListingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetVehicleDriverListingPage),
     TranslateModule.forChild()
  ],
})
export class FleetVehicleDriverListingPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetAddDriverPage } from './fleet-add-driver';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    FleetAddDriverPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetAddDriverPage),
    TranslateModule.forChild()
  ],
})
export class FleetAddDriverPageModule {}

import { NgZone,Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController, AlertController, Platform  } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader, VehicleProvider} from '../../providers/providers';
import { FleetNewShipmentListing } from '../pages';
import * as moment from 'moment';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { interval } from 'rxjs/observable/interval';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Media,MediaObject } from '@ionic-native/media';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
declare var google;
declare var require: any;

@IonicPage()
@Component({
  selector: 'page-fleet-new-shipment-bidding',
  templateUrl: 'fleet-new-shipment-bidding.html',
})
export class FleetNewShipmentBiddingPage {
  @ViewChild('map') mapElement: ElementRef;
  public timeInterval = 10000;
  public subscriber: any;
	shipment: any;
	bid: any;
  lock: boolean;
  pickupTime;
  str;
  time ;
  public count:any;
  baseurl;
  company_type;
  company_category;
  pickupnow = false;
  toggle;
  x;
  baseUrl;
  buttonss;
  public headers : any;
  input: any;
  autoComplete: any;
  lat: any;
  lng: any;
  map: any;
  current_position: any;
  latLng: any;
  building: any;
  address: any;
  distance;
  shipment_ongoing;
  val: number;
  apiKey: string;
 options = {

  };
  shipments:any;
  countid: number;
  activeShipments: any;
  active_length: any;
  shipments_ongoing: any;
  shipments_ongoing_len: any;
  shipments_won: any;
  shipments_won_len: any;
  audioFile: MediaObject;
  audioPlay: boolean=false;
  src: any;
  src1: string;
  constructor( public http: HttpClient,
     public navCtrl: NavController,
     public navParams: NavParams,
  				public modalCtrl: ModalController,
          public platform: Platform,
          private diagnostic: Diagnostic,
          public shipmentProvider: FleetShipment,
          public globalVar: GlobalVars,
          public toastCtrl: ToastController,
          public loader: Loader,
           public alertCtrl: AlertController,
           public zone: NgZone,
           private locationAccuracy: LocationAccuracy,
           private geolocation: Geolocation,
           public vehicle: VehicleProvider,
           private media: Media,
           private sanitizer: DomSanitizer
           ) {
             this.shipments=[];
    this.shipment = {};
    this.activeShipments=[];
    this.shipments_ongoing=[];
    this.shipments_won=[];
    this.shipment_ongoing = {};
    this.count = [];
    this.bid = {}
    this.lock = false;
    this.audioPlay=false;
    this.shipment = this.navParams.get('shipment') || {};
    this.src=this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
    if(this.shipment.timer)
    {
      this.shipment=this.shipment.shipment
    }
    this.time = "0:00"
    this.baseurl = globalVar.server_link + "/api/v1/shipments/";
    this.baseUrl = globalVar.server_link + "/api/v1/users/";
    this.apiKey ='AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw';
    this.options = {

    };
    const self=this;
    this.shipment_ongoing = this.navParams.get('shipment_ongoing') || {};

    if(this.globalVar.lang==true){
      var googleTranslate = require('google-translate')(this.apiKey, this.options);
      googleTranslate.translate( self.shipment.drop_location, 'ur', function(err, translation) {
        self.shipment.drop_location = translation.translatedText; });
      googleTranslate.translate(self.shipment.pickup_location, 'ur', function(err, translation) {
        self.shipment.pickup_location = translation.translatedText; });
    googleTranslate.translate(self.shipment.cargo_description, 'ur', function(err, translation) {
     self.shipment.cargo_description = translation.translatedText; });
    googleTranslate.translate(self.shipment.payment_option, 'ur', function(err, translation) {
      self.shipment.payment_option = translation.translatedText; });

      console.log(this.shipment);
  }

  }
  ionViewDidEnter() {
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled) {
          this.askForHighAccuracy();
        } else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.askForHighAccuracy();
          });
        }
      })
    } else {
      this.loadMap();
    }
  }

  askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
        this.loadMap();
      });
  }

  ionViewWillEnter()
  {
    document.getElementById("timess").innerText = "0:00";
    var el;
    el = <HTMLInputElement>document.getElementById("butt")
    el.disabled = true;
    this.distance ="0"
    document.getElementById("dis").innerText = "0 mins";

  }


  loadMap() {

    this.geolocation.getCurrentPosition().then((position) => {

      this.current_position = position;
      console.log(this.current_position);
     this.lat=this.current_position.coords.latitude
     this.lng=this.current_position.coords.longitude
     console.log(this.current_position.coords.latitude);
      console.log(this.current_position.coords.longitude);

      this.calcRoute();

      });

    }

     calcRoute() {
      var directionsService = new google.maps.DirectionsService();
      var directionsRenderer = new google.maps.DirectionsRenderer();
      var haight = new google.maps.LatLng(this.current_position.coords.latitude, this.current_position.coords.longitude);
      var oceanBeach = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
      var request = {
          origin: haight,
          destination: oceanBeach,

          travelMode: 'DRIVING'
      };
      directionsService.route(request, function(response, status) {
        if (status == 'OK') {
//          directionsRenderer.setDirections(response);
          this.distance = response.routes[0].legs[0].duration.text;
          if(this.distance!=undefined)
          {
            document.getElementById("dis").innerText = this.distance;
          }
          else{
            document.getElementById("dis").innerText = "0 mins";
          }

        }
        else{
//          "ZERO_RESULTS"
            document.getElementById("dis").innerText = "0 mins";
        }
        // console.log(response);
        // debugger

    //    console.log(this.distance)
      });
    }


  ionViewDidLoad() {
    //this.setheaders();

    if (this.shipment.is_contractual || this.shipment.process == "prorate"){
      console.log(this.shipment)
      this.bid.amount = this.shipment.amount
      this.bid.detention = this.shipment.detention_per_vehicle
      this.lock = true
    }

    this.company_type = this.globalVar.current_user.company.company_type;
      this.company_category = this.globalVar.current_user.company.category;

      if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
      {
        this.pickupnow = true;
          this.timersec();

      }
      const source = interval(5000);
      this.subscriber = source.subscribe(val => {
          this.val = val
        this.bidscount();
        document.getElementById("count").innerText = this.count;
        if(  this.count > 7)
        {
          const  els = <HTMLInputElement>document.getElementById("butt");
          els.disabled=true;
        }

      });



}



timersec()
{

  console.log(this.shipment.is_pickup_now)

   const self =this;
  var date1 = moment.utc().format('YYYY-MM-DD');
  const  els = <HTMLInputElement>document.getElementById("butt")
      if(date1 == this.shipment.pickup_date)
      {
        this.pickupTime = this.shipment.pickup_time;
      this.str = this.pickupTime.split('T');
      var s = this.str[1].split('.')
      var shipmenttime = moment.duration(s[0]).asSeconds();
      console.log("pickup time "+shipmenttime);
      var h = this.str[1].split(':')
      var h1 = h[0];
      h1 = h1*3600;
      h1=parseInt(h1);
      var min = h[1];
      min = min*60;
      min=parseInt(min);
      var sec = h[2]
      sec=parseInt(sec);
      //.split('.');
//      var shipmenttime = h1+min+sec-60;

      var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');

      var local = moment(date).add(15, 'minutes').format(' HH:mm:ss');

      var localtime = moment.duration(local).asSeconds()
      var time = localtime-shipmenttime;
      time= 90-time;
      if(time >90 ){
        time = 90;

      }


      this.time = time;

      this.x = setInterval(function () {
      this.time = time;
      document.getElementById("timess").innerText = this.time;
      var el;
      el = <HTMLInputElement>document.getElementById("butt")
      this.buttonss = false;
      el.disabled = false;
      if( --time < 0 )
      {

        this.buttonss = true;
        clearInterval(this.x);
        // var el;
        el = <HTMLInputElement>document.getElementById("butt")
        if(self.shipment !=undefined)
        {
         if(self.shipment.is_pickup_now)
            el.disabled=true;
        }
         else
             el.disabled=false;
        document.getElementById("timess").innerText = "0:00";

      }
      },1000);

    }

    else
    {

  //    const  els = <HTMLInputElement>document.getElementById("butt")
      if(self.shipment !=undefined)
         if( this.shipment.is_pickup_now)
//            els.disabled=true;
        this.buttonss=true
         else
    //         els.disabled=false;
    this.buttonss=false
    //  el.disabled = true;
       // this.navCtrl.setRoot("FleetNewShipmentListingPage");
        document.getElementById("timess").innerText = "0:00";
        console.log("else clear interval")
         // this.subscriber.unsubscribe();

      }

}


bidscount()
{
  this.setheaders();
  this.http.get(this.baseurl+this.shipment.id+'/get_bids_count.json',
  {headers:this.headers}).subscribe((data: any) =>{
    this.count = data.bids_count;
    this.count =  7 - this.count ;

    });

  }
setheaders()
{

  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"

  })

}
  playAudio(src)
  {
    let that=this;
    var xhr = new XMLHttpRequest;
    xhr.open("get", src);
    xhr.responseType = "blob";
    debugger
    xhr.onload = function () {
      debugger
      var yourNewBlob = xhr.response;
      that.src1=xhr.response;
//       that.src = URL.createObjectURL(yourNewBlob)
    };
    xhr.send();
  }
  play(myFile) {
    debugger
    let audio:any=this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+myFile);
      this.audioFile = this.media.create(audio);
      this.audioFile.play();
      this.audioPlay=true;
  }
  getImgContent(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:audio/wav;base64,'+this.shipment.cargo_description_url);
}
  pauseAudio()
  {
    this.audioFile.pause();
    this.audioPlay=false;
  }
  openNew(){
    this.navCtrl.setRoot("FleetNewShipmentListingPage");
  }

  openActive(){
    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
  }

  openWon(){
    this.navCtrl.setRoot("FleetWonShipmentBiddingPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("FleetOngoingShipmentListingPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("FleetCompletedShipmentListingPage");
  }


  addItem() {

    if(this.pickupnow){

      this.bid.detention=0;

    }
    if(!(this.pickupnow)){
       if(parseFloat(this.bid.amount) <= 0 || this.bid.amount == null || isNaN(parseFloat(this.bid.amount)) ){

      let alert = this.alertCtrl.create({
      title: 'Alert',
      message: `Bid amount can't be zero, negative or empty`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
      });
      alert.present();
    }else
    if(parseFloat(this.bid.detention) <= 0 || this.bid.detention == null || isNaN(parseFloat(this.bid.amount)))
    {
      let alert = this.alertCtrl.create({
      title: 'Alert',
      message: `Detention can't be negative or empty`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
      });
      alert.present();
    }
    let addModal = this.modalCtrl.create('FleetNewShipmentBiddingModalPage',{shipment: this.shipment,
      bid: this.bid,lat:this.lat,lng:this.lng});
    addModal.onDidDismiss(item => {
      if(item != "cancel"){
        this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
      }
    })
    addModal.present();

  }

    else{
      this.loader.show("");
      this.shipmentProvider.active(this.globalVar.current_user.company_id,1).subscribe((data) => {
        this.activeShipments = data
        this.active_length = this.activeShipments.length;


        this.shipmentProvider.ongoing(this.globalVar.current_user.company_id,1).subscribe((data) => {
          this.shipments_ongoing = data;
            this.shipments_ongoing_len = this.shipments_ongoing.length;

            this.shipmentProvider.won(this.globalVar.current_user.company_id,1).subscribe((data) => {

              this.shipments_won = data;
            this.shipments_won_len = this.shipments_won.length;

            if(this.active_length==1||this.shipments_ongoing_len==1||this.shipments_won_len==1)
            {
              this.loader.hide();
              this.navCtrl.setRoot("FleetNewShipmentListingPage");
              let toast = this.toastCtrl.create({
                message: "Shipment Already in Progress",
                duration: 6000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-success"
              });
              toast.present();

            }
            else
            {
              this.loader.hide();
              if(parseFloat(this.bid.amount) <= 0 || this.bid.amount == null || isNaN(parseFloat(this.bid.amount)))
              {
                let alert = this.alertCtrl.create({
                title: 'Alert',
                message: `Amount can't be negative or empty`,
                buttons: [
                  {
                    text: 'OK',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
                });
                alert.present();
              }
              else
              {
                let addModal = this.modalCtrl.create('FleetNewShipmentBiddingModalPage',{shipment: this.shipment,
                  bid: this.bid,lat:this.lat,lng:this.lng});
                addModal.onDidDismiss(item => {
                  if(item != "cancel"){
                    this.navCtrl.setRoot("FleetActiveShipmentBiddingPage");
                  }
                })
                addModal.present();

              }

            }

            });

        });


       },(err) => {
        this.loader.hide();
      })



    }


  }

  postedShipmentDetail(){

  }

  notInterested(shipment_id){
    let alert = this.alertCtrl.create({
    title: 'Attention!',
    message: "Do you want to remove this shipment from your list?" ,
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.loader.show("");
          this.shipmentProvider.notInterested(shipment_id,this.globalVar.current_user.company_id).subscribe(data => {
            this.loader.hide();
            this.navCtrl.setRoot("FleetNewShipmentListingPage");
            let toast = this.toastCtrl.create({
              message: data["success"],
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();

          },(err) => {
            this.loader.hide();
          });
        }
      },
    ]
    });
    alert.present();
  }


  // bidonShipment(shipment_id){
  // 	let _bid = {amount: this.bid.amount,detention: this.bid.detention}
  // 	// debugger
  //   this.shipmentProvider.bidonShipment(shipment_id,this.globalVar.current_user.company_id,_bid).subscribe(data => {

  //     let toast = this.toastCtrl.create({
  //       message: "Bid Successfully Posted",
  //       duration: 3000,
  //       position: 'top'
  //     });
  //     toast.present();

  //     this.navCtrl.setRoot(FleetNewShipmentListing);

  //     // this.bids = data;
  //   },(err) => {

  //   });
  // }

  reject(shipment_id){
    // this.shipmentProvider.notInterested(this.globalVar.current_user.company_id,shipment_id).subscribe(data => {
    //   console.log(data);
    //   // this.shipments = data;
    // },(err) => {

    // });
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
  ionViewDidLeave()
  {
    console.log("view leave");
    let self = this
    if(this.val >0)
    {
      self.subscriber.unsubscribe();
    }
    clearInterval(this.x);

  }
  ngOnDestroy()
  {
    let self=this
    console.log("view leave");
      if (this.x) {
        clearInterval(this.x);
      }
      if(this.audioFile)
      {
        this.audioFile.stop();
      }

  }
}

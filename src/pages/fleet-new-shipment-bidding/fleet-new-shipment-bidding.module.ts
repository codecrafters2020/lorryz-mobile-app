import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetNewShipmentBiddingPage } from './fleet-new-shipment-bidding';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FleetNewShipmentBiddingPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetNewShipmentBiddingPage),
    TranslateModule.forChild()
  ],
})
export class FleetNewShipmentBiddingPageModule {}

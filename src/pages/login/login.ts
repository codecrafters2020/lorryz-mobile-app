import { Component, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, Platform, Slides, Select } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { User,GlobalVars, Loader } from '../../providers/providers';

import { Storage } from '@ionic/storage';
import { ActivateAccountPage,OwnerInformationPage,OrganizationInformationPage } from '../pages';
import {ForgotPasswordPage, NewShipmentPage,CompanyInformationPage,FleetNewShipmentListing,SignUpPage } from '../pages';
import { parseNumber } from 'libphonenumber-js';

// var countries = require('country-list')();

import list from 'country-list';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { interval } from 'rxjs/observable/interval';
let countries = list();
declare var google;


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  // account: { email: string, password: string } = {
  //   email: 'test@example.com',
  //   password: 'test'
  // };

  // account: {}
  // Our translated text strings
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;

  public timeInterval = 10000;
  public subscriber: any;

  public _email: string;
  public _password: string;
  public _country: string;
  private loginErrorString: string;
  public _countries: any;
  user_device_info= {};
  loaded: any;
  current_position: any;
  latLng: any;
  map: any;
  count:any;
  l: any;
  abc: any;
  len: any;
  _dummy: string;
  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public platform: Platform,
    private diagnostic: Diagnostic,
    public translateService: TranslateService,
    public storage: Storage,
    public globalVar: GlobalVars,
    public loader: Loader,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy
    ) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
    this._countries = [];
    console.log("login"+this.globalVar.device_id);
    this.user_device_info= {device_id: this.globalVar.device_id, os: this.globalVar.os};
 //   this.autopopulate();



    const source = interval(500);
    this.subscriber = source.subscribe(val => {


      this.autopopulate()

    });

  }



  ionViewDidLoad() {
    this.loader.show("");
    this.user.getCountries().subscribe(data => {
      this._countries = data;
      this.len = this._countries.countries.length;
      //this.loader.hide();
       this.initialize_autocomplete()
    },(err) => {
      this.loader.hide();
    });

  }

  autopopulate()
  {
    this._dummy="abc";
  }

   initialize_autocomplete() {
     if (this.platform.is('cordova')) {
       this.diagnostic.isLocationEnabled().then((enabled) => {
         if (enabled) {
           this.askForHighAccuracy();
         } else {
           this.locationAccuracy.canRequest().then((canRequest: boolean) => {
             this.askForHighAccuracy();
           });
         }
       })
     } else {
       this.loadMap();
     }
   }


   askForHighAccuracy() {
     this.locationAccuracy
       .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
         this.loadMap();
       });
   }

   loadMap() {

    if (this.map) {
      return this.map;
    }

    let self = this;
    // this.loader.show("Loading the Map")
    this.geolocation.getCurrentPosition().then((position) => {
      // this.loader.hide()

      this.current_position = position;
      console.log(this.current_position);
      this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      var geocoder = new google.maps.Geocoder;
      const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      geocoder.geocode({ 'location': this.latLng }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {

          if (results[1]) {
            var indice=0;
            for (var j=0; j<results.length; j++)
            {
                if (results[j].types[0]=='locality')
                    {
                      indice=j;
                        break;
                    }
                }

            for (var i=0; i<results[j].address_components.length; i++)
                {

                    if (results[j].address_components[i].types[0] == "country") {
                            //this is the object you are looking for
                          self.count = results[j].address_components[i];
                          self.abc = self.count.long_name;
                          console.log(self.abc);

                          break;

                        }




                      }
                      self.countree(self.abc)
        } else {
       //   window.alert('Geocoder failed due to: ' + status);
        }}
      });


      }); }


    countree(abc){
      let self =this;

      for(var i=0; i<this.len;i++)
        {
          if(abc == this._countries.countries[i].name)
          {
              this._country = this._countries.countries[i].dialing_code;
              console.log(this._country)
              console.log(this.abc)
           //   document.getElementById("country_name").nodeValue = this._country;
            //  document.getElementById("user_email").click();
              this.loader.hide();
              break;

            }
        }

        if(abc=="Pakistan")
        {
          document.getElementById("country_number").innerText = "For any assistance, Please call us at +92 337 7131317 Or send us email at contact@lorryz.com";
        }
        else{
          document.getElementById("country_number").innerText = "For any assistance, Please call us at +971 56 5466088 Or send us email at contact@lorryz.com";

        }

    }

    signUp(event){
    event.preventDefault();
    this.navCtrl.setRoot(SignUpPage);
  }

  validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  verifyPhone(){
    if (this.isNumeric(this._email)) {
      let _mobile =  this._email

      let that = this;

      let country = this._countries.countries.filter(obj => {
        return obj.dialing_code == that._country
      })[0]


      let country_initial = countries.getCode(country.name)

      let _parse_number = parseNumber('Phone: '+_mobile, country_initial, { extended: true })
      return _parse_number
    }else{

      return {email_not_valid: !this.validateEmail(this._email),valid: this.validateEmail(this._email)}
    }


  }

  doLogin() {
    let num = this.verifyPhone()

    if (!num["valid"]) {
      let msg = num["email_not_valid"] ? "Email is not valid" : "Phone number is not valid"

      let toast = this.toastCtrl.create({
        message: msg,
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    }else{
      console.log("valid")
      let _account = {};
      let login = this._email
      if (this.isNumeric(this._email)) {
        login = '+'+num["countryCallingCode"] + num["phone"]
      }
      _account['user'] = {email: login, password: this._password,country: this._country }

      // this.loader.show("");
      this.user.login(_account).subscribe((resp) => {
        this.storage.ready().then(() => {
          this.globalVar.current_user = resp["user"];
          this.storage.set('currentUser' , resp["user"]);
          let user = resp["user"];
          this.user_device_info = Object.assign({}, this.user_device_info, {id: this.globalVar.current_user.id});
          this.user.update(this.user_device_info).subscribe(res => {});
          if( user["mobile_verified"] != 'verified' ){
           this.navCtrl.setRoot(ActivateAccountPage);
          }
          else if(user["role"] == 'driver'){
            this.navCtrl.setRoot('DriverUpcomingShipmentListPage');
          }
          else if (user["role"] == null) {
            this.navCtrl.setRoot(OwnerInformationPage);
          }else if(user.company_id == null){
           this.navCtrl.setRoot(OwnerInformationPage);
          }else if(user.company_id && !user.information){
            if (user.company.company_type == 'individual') {
              this.navCtrl.setRoot('OwnerInformationPage');
            }else if(user.company.company_type == 'company'){
              this.navCtrl.setRoot('OwnerInformationPage');
            }else if(user.role=='fleet_owner') {
              this.navCtrl.setRoot(FleetNewShipmentListing)
            }else if(user.role=='cargo_owner') {
              this.navCtrl.setRoot(NewShipmentPage)
            }
          }else if(user["role"]=='fleet_owner') {
            this.navCtrl.setRoot(FleetNewShipmentListing);
          }else if(user["role"]=='cargo_owner') {
            this.navCtrl.setRoot(NewShipmentPage);
          }


        });


      }, (err) => {
        // this.loader.hide();
        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : this.loginErrorString,
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });
    }


  }

  forgotPassword(){
    // event.preventDefault();
    this.navCtrl.push(ForgotPasswordPage);
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentRequestPage } from './new-shipment-request';
//import { NewShipmentPage } from "../new-shipment/new-shipment";
//import { PagesVehicleTypePage } from "../pages-vehicle-type/pages-vehicle-type";

@NgModule({
  declarations: [
    NewShipmentRequestPage,
  //  NewShipmentPage
  ],

  imports: [
//    PagesVehicleTypePageModule,
    IonicPageModule.forChild(NewShipmentRequestPage),
    //IonicPageModule.forChild(PagesVehicleTypePage),
  ],
  // entryComponents: [
  //   PagesVehicleTypePage
  // ]
})
export class NewShipmentRequestPageModule {}

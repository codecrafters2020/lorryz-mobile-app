import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import { ChangeDetectorRef } from '@angular/core';
import {
  ModalController,
  IonicPage,
  Platform,
  ActionSheetController,
  NavController,
  NavParams,
  ToastController,
  AlertController,
  PopoverController,
  IonicApp,
  ViewController,
  App,
  LoadingController
} from "ionic-angular";
import { PostedShipmentPage } from "../pages";
import {
  Shipment,
  GlobalVars,
  Loader,
  CountryProvider
} from "../../providers/providers";
//import { NewShipmentPage } from "../pages";
import { NewShipmentPage } from "../new-shipment/new-shipment";
import { PagesVehicleTypePage } from "../pages-vehicle-type/pages-vehicle-type";
import * as moment from 'moment';
import { Keyboard } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
import { AwsProvider } from "../../providers/aws/aws";
import b64toBlob from "b64-to-blob";

const MEDIA_FILES_KEY = 'mediaFiles';

//import moment = require("moment");
declare var google;
@IonicPage()
@Component({
  selector: "page-new-shipment-request",
  templateUrl: "new-shipment-request.html",
  providers:[NewShipmentPage]
})
export class NewShipmentRequestPage {
  @ViewChild('map') mapElement: ElementRef;
  pickupnow;
  shipment: any;
  locations: any;
  vehicles: any;
  errors: any;
  fare: any;
  distance: any;
  lock: boolean;
  minDate: any;
  position: any;
  unformated_pickup_time: any;
  company_type ;
  company_category ;
  letsgo;
  id;
  vehiclename;
  vehiclevalue;
 time;
  hours;
  src;
  modalreq;
  pakistan;
  uae;
  lastTimeBackPress = 0;
userList;
lastkeydown1: number = 0;
  timePeriodToExit = 2000;
public input: string = '';
  public countries: string[] = [];
 public list:string[] = [
   "AC Airconditioning",
"Air conditioning",
"Bed & mattress",
  "Bedframes",
  "Bedroom set",
"Bed",
"Cabinets",
"Carpet",
"Center table with side tables",
"Chair",
"Clothes",
"Computer Table",
  "Cooking range",
  "Cupboard",
  "Curtains",
  "Desks",
  "Dining table",
  "Dishwasher",
  "Dressing table",
  "Fan",
  "Food Processor",
  "Freezer",
  "Fridge",
  "Furniture",
  "General",
  "General Cargo",
  "Geyser",
  "Home Theatre",
  "Hotel Scrap",
  "House Goods Shifting",
  "Iron Scrap",
  "Kitchen Appliance",
  "Mattress",
  "Microwave ovens",
  "Mirrors",
  "Plywood sheets",
  "Printer",
  "Printer & Scanner",
  "Refrigerators",
  "Sanitary",
  "Sewing Machine",
  "Slabs",
  "Sofas",
  "Table",
  "Television",
  "Tiles",
  "Tires",
  "TV",
  "Tyres",
  "Vacuum cleaners",
  "Vacuum pump",
  "Wardrobes",
  "Washing Machine",
  "Water Purifier & Dispenser",
  "Water tank (Plastic / Fibre)",
  "Wheel",
  "Wooden logs"
  ];
    sum;
    map: any;
    mediaFiles:any;
  recData: any;
  document: any;
  audioFile: MediaObject;
  blob: any;
  constructor(
    private platform: Platform,
    public keyboard:Keyboard,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: Shipment,
    public countryProvider: CountryProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loader: Loader,
    private viewCtrl: ViewController,
    public cdref:ChangeDetectorRef,
    public globalVar: GlobalVars,
    public appCtrl:App,
    //public moment:isMoment,
    private app: IonicApp,
    private cd: ChangeDetectorRef,
    public pickup: NewShipmentPage,
    private mediaCapture: MediaCapture,
    private storage: Storage,
    private file: File,
    private media: Media,
    private awsProvider: AwsProvider,
    private actionSheetController: ActionSheetController,
    private loadingController: LoadingController,
    public zone: NgZone,
  ) {
    this.shipment = { location_id: "abc" };
    this.locations = [];
    this.vehicles = [];
    this.fare = 0;
    this.distance = 0;
    this.lock = false;
    this.minDate = new Date().toISOString();
    this.position = this.navParams.get("position");
    this.id = this.navParams.get("id");
    this.vehiclename = this.navParams.get("vehicle_name");
    this.vehiclevalue = this.navParams.get("value");
    this.src = this.navParams.get("src");
    this.pickupnow = this.navParams.get('pickup_now');
    this.letsgo = this.navParams.get('lets_go');
    this.unformated_pickup_time = this.navParams.get('time');
    this.shipment.no_of_vehicles = this.navParams.get('no_vehicles');
    this.input = this.navParams.get('cargodesc');
    this.modalreq = false;
    this.sum=this.navParams.get('sum')

  }

  ionViewDidLoad() {
    this.pakistan = this.globalVar.current_user.country.name;
    console.log(this.pakistan);

    if (this.globalVar.current_user.company.is_contractual == true) {
      this.loader.show("");
      this.countryProvider
        .locations(this.globalVar.current_user.country_id)
        .subscribe(
          data => {
            this.loader.hide();
            this.locations = data;
            console.log(data);
          },
          err => {
            this.loader.hide();
          }
        );
        this.platform.registerBackButtonAction(() => {
          console.log("back button is pressed");
          const overlayView = this.app._overlayPortal._views[0];
          if (overlayView && overlayView.dismiss) {
          overlayView.dismiss();// it will close the modals, alerts
          }
         } );
    }
    // this.countryProvider
    //   .vehicles(this.globalVar.current_user.country_id)
    //   .subscribe(
    //     data => {
    //       this.vehicles = data;
    //       console.log(data);
    //     },
    //     err => {}
    //   );
//    console.log("here"+this.navParams.get("shipment"));
this.company_type = this.globalVar.current_user.company.company_type;
this.company_category = this.globalVar.current_user.company.category;

//this.uae = this.globalVar.current_user.country.name;
    this.shipment = this.navParams.get("shipment") || {};
    if((this.company_category == 'cargo')&&(this.company_type == 'individual')){
      this.shipment.cargo_packing_type = "other";
      this.shipment.payment_option = "before_pick_up";
       if(this.pickupnow == true){
     //   this.shipment.no_of_vehicles = 1;

    //this.shipment.payment_option = "before_pick_up";
  }}
  if((this.company_category == 'cargo')&&(this.company_type == 'company')){
    // this.shipment.cargo_packing_type = "other";
    this.shipment.payment_option = "before_pick_up";
    if(this.shipment.pickup_building_name=="")
    {
      this.shipment.pickup_building_name="e";
    }
//      this.shipment.no_of_vehicles = 1;

  //this.shipment.payment_option = "before_pick_up";
}

  this.load_map();
}
captureAudio() {
  this.mediaCapture.captureAudio().then(res => {
//    this.storeMediaFiles(res);
  this.mediaFiles = res[0];
  debugger
  this.recData=res[0]['fullPath'];
}, (err: CaptureError) => console.error(err));
}
play(myFile) {
  if (myFile.name.indexOf('.mp3') > -1) {
    debugger
    this.audioFile = this.media.create(myFile.fullPath);
    this.audioFile.play();
  } else {
    // let path = this.file.dataDirectory + myFile.name;
    // let url = path.replace(/^file:\/\//, '');
    // let video = this.myVideo.nativeElement;
    // video.src = url;
    // video.play();
  }
}
pause()
{
    this.audioFile.stop();
}
storeMediaFiles(files) {
  this.storage.get(MEDIA_FILES_KEY).then(res => {
    if (res) {
      let arr = JSON.parse(res);
      arr = arr.concat(files);
      this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
    } else {
      this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files))
    }
    this.mediaFiles = this.mediaFiles.concat(files);
  })
}

async uploadDocumentResource()
{
  let loading = this.loadingController.create({
  });
  loading.present();
    let that=this;
    if(this.recData)
    {
            that.zone.run(() => {
              let request = new XMLHttpRequest();
              request.open('GET', this.recData, true);
              request.responseType = 'blob';
              request.timeout = 360000;
              request.onload = function() {
                that.zone.run(() => {
                  let reader = new FileReader();
                  reader.readAsDataURL(request.response);
                  console.log("Step 1 reader",reader);
                  console.log("Step 1 got stuck in request.response",request)
                  reader.onload =  function(){
                    that.zone.run(async () => {
                      console.log("Step 2 Loaded",reader.result);
                      that.blob = reader.result.split(',')[1];
                      that.shipment.cargo_description_url=that.blob;
                      that.createShipment();
                          loading.dismiss();
                    });
                  }

                  reader.onerror =  function(e){
                    console.log('We got an error in file reader:::::::: ', e);
                  };

                  reader.onabort = function(event) {
                    console.log("reader onabort",event)
                  }
                })
              };

              request.onerror = function (e) {
                loading.dismiss();
                console.log("** I got an error in XML Http REquest",e);
              };

              request.ontimeout = function(event) {
                 loading.dismiss();
                 console.log("xmlhttp ontimeout",event)
              }

              request.onabort = function(event) {
                loading.dismiss();
                 console.log("xmlhttp onabort",event);
              }


              request.send();
            })
    }
    else
    {
      this.createShipment();
      loading.dismiss();
    }
}
load_map(){
      var directionsDisplay;
      var directionsService = new google.maps.DirectionsService();


      let source = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
      let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

      let mapOptions = {
        center: source,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false
      }
      let self = this;

      directionsDisplay = new google.maps.DirectionsRenderer();

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.addMarker(destination)
      this.addMarker(source)
      directionsDisplay.setMap(self.map);
      directionsDisplay.setOptions( { suppressMarkers: true } );

      var request = {
        origin: source,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
          directionsDisplay.setMap(self.map);
        } else {
          console.log("Directions Request failed: " + status);
        }
      });
    }
    addMarker(latlong){
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: latlong
      });
    }
  openModal() {
    this.modalreq = true;
    //  this.navCtrl.push('PagesVehicleTypePage', {pickupnow:this.pickupnow,letsgo:this.letsgo,
    //   position:this.position,shipment:this.shipment, vehcilevalue:this.vehiclevalue,edit:false,
    //   unformated_time:this.unformated_pickup_time});
    let addModal = this.modalCtrl.create('PagesVehicleTypePage', {pickupnow:this.pickupnow,letsgo:this.letsgo,
       position:this.position,shipment:this.shipment, vehcilevalue:this.vehiclevalue,edit:false,
         unformated_time:this.unformated_pickup_time,cargodesc:this.input,no_vehicles:this.shipment.no_of_vehicles,
         sum:this.sum
    });
    addModal.onDidDismiss(item => {
      // this.navCtrl.setRoot('NewShipmentRequestPage');
      // const startIndex = this.navCtrl.getActive().index - 2;
      // this.navCtrl.remove(startIndex, 2);

    })
    addModal.present();

  }
  add(item: string) {
    this.input = item;
    this.countries = [];
  }

  removeFocus() {
    this.keyboard.close();
  }

  search() {
    if (!this.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
    return;
    }
    this.countries = this.list.filter(item => item.toUpperCase().includes(this.input.toUpperCase()));
  }

  postedShipment() {
    console.log ("here"+this.shipment.loading_time );
    this.navCtrl.setRoot(PostedShipmentPage);

  }
  calculateFare() {
    if (
      this.globalVar.current_user.process == "prorate" &&
      this.shipment.vehicle_type_id
    ) {
      this.shipmentProvider
        .calculate_fare(this.shipment, this.globalVar.current_user.company_id)
        .subscribe(
          data => {
            this.fare = data["fare"];
            this.distance = data["distance"];
          },
          err => {}
        );
        console.log ("here"+this.shipment.loading_time );

    }
  }

  createShipment() {
       this.loader.show("");



      var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');

    var stillUtc = moment.utc(date).toDate();
    var local = moment(date).add(15, 'minutes').format(' HH:mm:ss')
    console.log(this.id)
    this.shipment.vehicle_type_id = this.id;
    this.shipment.loading_time = 0;
      this.shipment.unloading_time = 0;
      if((this.company_category == 'cargo')&&(this.company_type == 'company')){
        this.shipment.payment_option = 'credit_period';
      }
  if((this.company_category == 'cargo')&&(this.company_type == 'individual')){

    if(this.shipment.pickup_building_name=="")
    {
      this.shipment.pickup_building_name="e";
    }
    if(this.shipment.drop_building_name=="")
    {
      this.shipment.drop_building_name="e";
    }
   //   this.shipment.loading_time = 0;
   //   this.shipment.unloading_time = 0;
//      this.shipment.no_of_vehicles = 1;
    this.shipment.is_pickup_now = this.pickupnow;
//    this.shipment.currency=this.globalVar.current_user.country.currency;
    this.shipment.cargo_packing_type = "Other";
    this.shipment.payment_option = "before_pick_up";
    if(this.pickupnow == true){
    this.shipment.loading_time = 0;
      this.shipment.unloading_time = 0;
      this.shipment.payment_option = "before_pick_up";
 //  this.shipment.no_of_vehicles = 1;
        this.shipment.cargo_packing_type = "Other";
      this.unformated_pickup_time = local;
      this.shipment.pickup_time= this.unformated_pickup_time;
        this.shipment.pickup_date  = new Date();
        this.shipment.pickup_date .setDate( this.shipment.pickup_date .getDate() );
}
    }

    console.log("hsbxx"+this.shipment.pickup_date);
    console.log("hi1 pickup timme"+this.unformated_pickup_time +"shipment" +this.shipment.pickup_time);
    console.log("hi"+this.shipment.vehicle_type_id);
    console.log("hi111"+local);
    if (this.unformated_pickup_time && ((this.pickupnow == false)||(this.letsgo == true))) {
      this.shipment.pickup_time = this.format_pickup_time();

    }
    this.shipment = Object.assign({}, this.shipment, {
      state_event: "posted",
      company_id: this.globalVar.current_user.company_id,
      country_id: this.globalVar.current_user.country_id
    });
    this.shipment.cargo_description= this.input;
    if(!this.modalreq)
{
  if(this.shipment.pickup_time)
  {
      this.shipmentProvider
          .create(this.shipment, this.globalVar.current_user.company_id)
          .subscribe(
            data => {
              this.loader.hide();
              this.viewCtrl.dismiss().then(data=>{

              });
              // this.viewCtrl.dismiss().then(() => this.appCtrl.getRootNav().push('PostedShipmentPage',{reload: 1}));       //  this.navCtrl.push(PostedShipmentPage,{reload: 1});
          this.navCtrl.setRoot("PostedShipmentPage",{reload: 1});


            },
            err => {
              this.loader.hide();
              let toast = this.toastCtrl.create({
                message: err.error.errors[0],
                duration: 6000,
                position: "top",
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-danger"
              });
              toast.present();
            }
          );
    }
    else
        {
          this.loader.hide()
          let toast = this.toastCtrl.create({
            message: "Pickup Time Can't Be Blank",
            duration: 4000,
            position: "top",
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        }


      }
      else{
        this.loader.hide()
      }
}

  format_pickup_time() {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    const day = today.getDate();
    const hour = parseInt(this.unformated_pickup_time.split(":")[0]);
    const min = parseInt(this.unformated_pickup_time.split(":")[1]);
    this.time =  hour+min;
    return new Date(year, month, day, hour, min);
  }

  pad(n) {
    return n < 10 ? "0" + n : n;
  }

  ngAfterViewInit() {
    let input_pickup = <HTMLInputElement>(
      document.getElementById("auto_pickup").getElementsByTagName("input")[0]
    );
    let input_drop = <HTMLInputElement>(
      document.getElementById("auto_drop").getElementsByTagName("input")[0]
    );
    let options = {};
    let self = this;
    let autoComplete_pickup = new google.maps.places.Autocomplete(
      input_pickup,
      options
    );
    google.maps.event.addListener(autoComplete_pickup, "place_changed", () => {
      var place = autoComplete_pickup.getPlace();
      self.shipment.pickup_lat = place.geometry.location.lat();
      self.shipment.pickup_lng = place.geometry.location.lng();
      self.shipment.pickup_location = input_pickup.value;
      self.shipment.pickup_building_name = place.name;
      self.shipment.pickup_city = this.extractFromAdress(
        place.address_components,
        "locality"
      );
    });

    let autoComplete_drop = new google.maps.places.Autocomplete(
      input_drop,
      options
    );
    google.maps.event.addListener(autoComplete_drop, "place_changed", () => {
      var place = autoComplete_drop.getPlace();
      self.shipment.drop_lat = place.geometry.location.lat();
      self.shipment.drop_lng = place.geometry.location.lng();
      self.shipment.drop_location = input_drop.value;
      self.shipment.drop_building_name = place.name;
      self.shipment.drop_city = this.extractFromAdress(
        place.address_components,
        "locality"
      );
    });

    var geolocation = {
      lat: this.position.coords.latitude,
      lng: this.position.coords.longitude
    };
    var circle = new google.maps.Circle({
      center: geolocation,
      radius: this.position.coords.accuracy
    });

    autoComplete_pickup.setBounds(circle.getBounds());
    autoComplete_drop.setBounds(circle.getBounds());
  }

  extractFromAdress(components, type) {
    for (var i = 0; i < components.length; i++)
      for (var j = 0; j < components[i].types.length; j++)
        if (components[i].types[j] == type) return components[i].long_name;
    return "";
  }

  populateLocation(value): void {
    this.lock = true;
    this.shipment.pickup_building_name = value.pickup_building_name;
    this.shipment.pickup_location = value.pickup_location;
    this.shipment.pickup_lat = value.pickup_lat;
    this.shipment.pickup_lng = value.pickup_lng;
    this.shipment.pickup_city = value.pickup_city;

    this.shipment.drop_building_name = value.drop_building_name;
    this.shipment.drop_location = value.drop_location;
    this.shipment.drop_lat = value.drop_lat;
    this.shipment.drop_lng = value.drop_lng;
    this.shipment.drop_city = value.drop_city;

    this.shipment.amount = value.rate;
    //this.shipment.vehicle_type_id = this.id;
    this.shipment.vehicle_type_id = this.id;
    this.shipment.loading_time = value.loading_time;
    this.shipment.unloading_time = value.unloading_time;
    console.log("jkj"+this.shipment.vehicle_type_id);
  }

  unselectLocation() {
    this.lock = false;
    this.shipment.amount = "";

    this.shipment.pickup_building_name = "";
    this.shipment.pickup_location = "";
    this.shipment.pickup_lat = "";
    this.shipment.pickup_lng = "";
    this.shipment.pickup_city = "";

    this.shipment.drop_building_name = "";
    this.shipment.drop_location = "";
    this.shipment.drop_lat = "";
    this.shipment.drop_lng = "";
    this.shipment.drop_city = "";

    this.shipment.vehicle_type_id = "";
    this.shipment.loading_time = "";
    this.shipment.unloading_time = "";
  }

  openPosted() {
    this.navCtrl.setRoot('PostedShipmentPage');
  }

  openOngoing() {
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming() {
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew() {
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted() {
    this.navCtrl.setRoot("CompletedShipmentPage");
  }
  showNotification() {
    this.navCtrl.push("NotificationPage");
  }
  ionViewDidLeave()
  {
    if(this.audioFile)
    {
      this.audioFile.stop();
    }
  }
}

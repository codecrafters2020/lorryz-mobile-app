import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

declare var google;
@IonicPage()
@Component({
  selector: 'page-map-location',
  templateUrl: 'map-location.html',
})
export class MapLocationPage {
	@ViewChild('map') mapElement: ElementRef;
	public shipment: any;
	map: any;
  constructor(public navCtrl: NavController,
   	public navParams: NavParams,
   	private geolocation: Geolocation,
   	private diagnostic: Diagnostic,
   	private locationAccuracy: LocationAccuracy) {
  	this.shipment = this.navParams.get('shipment') || {};
  }

  ionViewDidLoad() {
    console.log(this.shipment)
    this.loadMap();
  }

  loadMap(){
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();

    
    let source = new google.maps.LatLng(this.shipment.pickup_lat, this.shipment.pickup_lng);
    let destination = new google.maps.LatLng(this.shipment.drop_lat, this.shipment.drop_lng);

    let mapOptions = {
      center: source,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }
    let self = this;
    
    directionsDisplay = new google.maps.DirectionsRenderer();

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(destination)
    directionsDisplay.setMap(self.map);
    directionsDisplay.setOptions( { suppressMarkers: true } );

    var request = {
      origin: source,
      destination: destination,
      travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(self.map);
      } else {
        console.log("Directions Request failed: " + status);
      }
    });
  }

  addMarker(latlong){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latlong
    });
  }

}

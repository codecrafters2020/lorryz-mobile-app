import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelShipmentPage } from './cancel-shipment';

@NgModule({
  declarations: [
    CancelShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelShipmentPage),
  ],
})
export class CancelShipmentPageModule {}

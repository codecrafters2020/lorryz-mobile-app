import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Shipment, GlobalVars, Loader} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-cancel-shipment',
  templateUrl: 'cancel-shipment.html',
})
export class CancelShipmentPage {
	shipment: any;
	shipment_obj: any;
	cancel_by: any;
	state_event: any;
  cancel_reason: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public shipmentProvider: Shipment,
    public loader: Loader,
    public toastCtrl: ToastController,
    public globalVar: GlobalVars,
  	private viewCtrl: ViewController) {
  	this.shipment = this.navParams.get('shipment')
  	this.cancel_by = this.navParams.get('cancel_by')
  	this.state_event = this.navParams.get('state_event')
    this.cancel_reason = "No bid selected"
  }

  ionViewDidLoad() {
  }

  cancelShipment(){
    this.shipment_obj = {id: this.shipment.id, cancel_by: this.cancel_by, state_event: this.state_event, cancel_reason: this.cancel_reason}
    this.loader.show("");
    this.shipmentProvider.update(this.shipment_obj, this.shipment.id, this.globalVar.current_user.company_id).subscribe(data => {
      this.loader.hide();
      this.viewCtrl.dismiss("cancel");
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err.error.errors,
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }
  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }

  showNotification(){
    this.navCtrl.push("NotificationPage")
  }

  repostShipment(){
    this.navCtrl.push("EditShipmentPage",{shipmentId:this.shipment.id}).then(() => {
          this.viewCtrl.dismiss("cancel");
    })
  }
}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ModalController,ViewController  } from 'ionic-angular';
import { OngoingShipmentPage } from '../pages';
import { Rating, GlobalVars, Loader} from '../../providers/providers';
import { Ionic2Rating } from "ionic2-rating";


@IonicPage()
@Component({
  selector: 'page-fleet-rate-shipment',
  templateUrl: 'fleet-rate-shipment.html',
})
export class FleetRateShipmentPage {
  public rating: 0;
  public shipment: any;
  public remarks: string;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private viewCtrl: ViewController,
   public ratingProvider: Rating,
   public loader: Loader,
   public toastCtrl: ToastController,
   public alertCtrl: AlertController,
   public modalCtrl: ModalController,
   public globalVar: GlobalVars) {
  }

  ionViewDidLoad() {
    this.rating = this.navParams.get("rate");
    this.shipment = this.navParams.get("shipment");
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  onModelChange(event){

  }

  submit(){
    // let receiver_id = this.globalVar.current_user.role == 'fleet_owner' ? this.shipment.company_id : this.shipment.fleet_id
    if(this.globalVar.current_user.role == "cargo_owner")
    {
      for(var i=0;i<this.shipment.vehicles.length;i++)
      {
        let company_rating= {
          shipment_id: this.shipment["id"],
          rating: this.rating,
          remarks: this.remarks,
          giver_id: this.globalVar.current_user.company_id,
          receiver_id: this.shipment.vehicles[i].company_id
        }

        this.loader.show("");
        this.ratingProvider.rateShipment(company_rating).subscribe(data => {
          this.loader.hide();
          this.viewCtrl.dismiss();

          if(this.shipment.vehicles.length==i)
          {
            let toast = this.toastCtrl.create({
              message: data['success'],
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();
        //  }, (err) => {
        //     this.loader.hide();
        //     let toast = this.toastCtrl.create({
        //       message: err.error.errors,
        //       duration: 6000,
        //       position: 'top',
        //       showCloseButton: true,
        //       closeButtonText: "x",
        //       cssClass: "toast-danger"
        //     });
        //     toast.present();
          }
        });

      }
    }
    else
    {
      let receiver_id = this.shipment.company_id
      let company_rating= {
        shipment_id: this.shipment["id"],
        rating: this.rating,
        remarks: this.remarks,
        giver_id: this.globalVar.current_user.company_id,
        receiver_id: receiver_id
      }

      this.loader.show("");
      this.ratingProvider.rateShipment(company_rating).subscribe(data => {
        this.loader.hide();
        this.viewCtrl.dismiss();

          let toast = this.toastCtrl.create({
            message: data['success'],
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success"
          });
          toast.present();
       }, (err) => {
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message: err.error.error,
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
       });

    }

  }




}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetRateShipmentPage } from './fleet-rate-shipment';
import { Ionic2RatingModule } from "ionic2-rating";
@NgModule({
  declarations: [
    FleetRateShipmentPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetRateShipmentPage),
    Ionic2RatingModule
  ],
})
export class FleetRateShipmentPageModule {}

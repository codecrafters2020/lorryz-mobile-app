import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User,GlobalVars, Loader } from '../../providers/providers';

import { Storage } from '@ionic/storage';
import { ActivateAccountPage,OwnerInformationPage,OrganizationInformationPage } from '../pages';
import {LoginPage, NewShipmentPage,CompanyInformationPage,FleetNewShipmentListing,SignUpPage } from '../pages';
import { parseNumber } from 'libphonenumber-js'

// var countries = require('country-list')();

import list from 'country-list';
let countries = list();
/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
	public _email: string;
  public _countries: any;
  public _country: string;
  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public storage: Storage,
    public globalVar: GlobalVars,
    public loader: Loader,) {
    this._countries = [];
  }

  ionViewDidLoad() {
    this.loader.show("");

    this.user.getCountries().subscribe(data => {
      this.loader.hide();
      this._countries = data;
    },(err) => {
      this.loader.hide();
    });
  }

  validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  verifyPhone(){
    if (this.isNumeric(this._email)) {
      let _mobile =  this._email
      
      let that = this;


      let country = this._countries.countries.filter(obj => {
        return obj.dialing_code == that._country
      })[0]
      

      let country_initial = countries.getCode(country.name)

      let _parse_number = parseNumber('Phone: '+_mobile, country_initial, { extended: true })
      return _parse_number
    }else{
      
      return {email_not_valid: !this.validateEmail(this._email),valid: this.validateEmail(this._email)}
    }
    
    
  }

 sendForgotPassword(){
   
   let num = this.verifyPhone()

   if (!num["valid"]) {
     let msg = num["email_not_valid"] ? "Email is not valid" : "Phone number is not valid" 

     let toast = this.toastCtrl.create({
       message: msg,
       duration: 6000,
       position: 'top',
      showCloseButton: true,
      closeButtonText: "x",
      cssClass: "toast-danger"
     });
     toast.present();
   }else{

     let _account = {};
     let login = this._email
     if (this.isNumeric(this._email)) {
       login = '+'+num["countryCallingCode"] + num["phone"]  
     }
     _account['user'] = {email: login }
     this.loader.show("")
     this.user.sendForgotPassword(_account).subscribe((resp) => {
       this.loader.hide()
       if (resp["errors"]) {
         let toast = this.toastCtrl.create({
            message: resp["errors"],
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
       }else{
         let toast = this.toastCtrl.create({
            message: resp["success"],
            duration: 6000,
            position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
          });
          toast.present();
          this.navCtrl.setRoot(LoginPage);
       }
       
       
     }, (err) => {
       let toast = this.toastCtrl.create({
         message: err && err.error && err.error.error ? err.error.error : "some thing went wrong",
         duration: 6000,
         position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
       });
       toast.present();
     });
   }

 }

 signIn(){
   // event.preventDefault();
   this.navCtrl.setRoot(LoginPage);
 }


}

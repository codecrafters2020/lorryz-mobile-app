import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyInformationPage } from './company-information';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyInformationPage),
    TranslateModule.forChild()
  ],
})
export class CompanyInformationPageModule {}

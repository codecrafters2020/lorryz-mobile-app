import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController,ActionSheetController } from 'ionic-angular';


import { User,GlobalVars, Loader } from '../../providers/providers';
import { NewShipmentPage } from '../pages';
import { Company } from '../../providers/company/company';
import { Storage } from '@ionic/storage';


import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AwsProvider } from './../../providers/aws/aws';


// var Buffer = require('').Buffer
import b64toBlob from "b64-to-blob";

@IonicPage()
@Component({
  selector: 'page-company-information',
  templateUrl: 'company-information.html',
})

export class CompanyInformationPage {

  public name: String;
  public address: String;
  public landline_no: String;
  public website_address: String;
  public license_number: String;
  public license_expiry_date: String;
  public secondary_contact_name: String;
  public secondary_mobile_no: String;
  public news_update: any;

  public password: String;
  public confirm_password: String;
  public current_password: String;
  selected_documents = [];
  documents = [];
  avatar: String
  selected_avatar: String;
  minDate: any;
  tax_registration_no: any;

  // public current_user: {firstname: '',lastname: ''}

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public company: Company,
              public storage: Storage,
              public toastCtrl: ToastController,
              public globalVar: GlobalVars,
              private awsProvider: AwsProvider,
              private file: File,
              private camera: Camera,
              private actionSheetCtrl: ActionSheetController,
              public loader: Loader,
              public zone: NgZone,
              public user: User,) {
    this.minDate = new Date().toISOString()
  }

  ionViewDidLoad() {


  }



  ionViewWillEnter(){
    this.loader.show("");
    this.updateCurrentUser()
    this.storage.get('currentUser').then(user => {
      this.globalVar.current_user = user;
      if (user) {
        this.company.getCompanyInformation().subscribe((resp)  => {
          this.loader.hide();
          if(resp){
              this.name = resp["name"];
              this.address = resp["address"];
              this.landline_no = resp["landline_no"];
              this.website_address = resp["website_address"];
              this.license_number = resp["license_number"];
              this.license_expiry_date = resp["license_expiry_date"];
              this.secondary_contact_name = resp["secondary_contact_name"];
              this.secondary_mobile_no = resp["secondary_mobile_no"];
              this.news_update =  true;
              this.avatar = resp["avatar"]
              this.selected_avatar = resp["avatar"]
              this.documents = resp["documents"].slice();
              this.selected_documents = resp["documents"].slice();
              this.tax_registration_no = this.globalVar.current_user.company.tax_registration_no
          }else{
            if (resp && resp["error"]) {
              let toast = this.toastCtrl.create({
                message: resp["error"],
                duration: 6000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-danger"
              });
              toast.present();
            }

          }
         },(err) => {
           this.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Some Went Wrong. Please Try Later",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        });
      }

    });
  }

  remove(doc){

   this.documents.splice( this.documents.indexOf(doc), 1 );
   this.selected_documents.splice( this.selected_documents.indexOf(doc), 1 );


  }
  disabled()
  {
    if(this.name)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  landToMain(){

      // if(this.validateDoc()){
      //   return false;
      // }
      let data={};
      if(this.avatar)
      {
        data = {
          company_informations: {
            name: this.name,
            address: "",
            landline_no: "",
            website_address: "",
            license_number: "",
            license_expiry_date: "",
            secondary_contact_name: "",
            secondary_mobile_no: "",
            news_update: "",
            password: '',
            confirm_password: '',
            current_password:"",
            avatar: this.avatar,
            documents: "",
            tax_registration_no: ""
          }
        }
      }
      else{
         data = {
          company_informations: {
            name: this.name,
            address: "",
            landline_no: "",
            website_address: "",
            license_number: "",
            license_expiry_date: "",
            secondary_contact_name: "",
            secondary_mobile_no: "",
            news_update: "",
            password: '',
            confirm_password: '',
            current_password: "",
            avatar: '',
            documents: "",
            tax_registration_no: ""
          }
        }
      }


    this.loader.show("");
    this.company.addCompanyInformation(data).subscribe((resp) => {
      this.loader.hide();
      if(resp && resp["success"]){

        this.storage.set('currentUser',resp["user"]);
        this.globalVar.current_user = resp["user"];

        // if(this.globalVar.current_user.role == 'fleet_owner'){
        //   this.navCtrl.setRoot('FleetNewShipmentListingPage');
        // }else{
        //   this.navCtrl.setRoot('NewShipmentPage');
        // }
        this.navCtrl.setRoot('NewShipmentPage');
        // let toast = this.toastCtrl.create({
        //   message: "Your account will be approved shortly by Lorryz Team",
        //   duration: 6000,
        //   position: 'top',
        //   showCloseButton: true,
        //   closeButtonText: "x",
        //   cssClass: "toast-success"
        // });
        // toast.present();

//        this.logout()
      }else{
       let toast = this.toastCtrl.create({
               message: resp["error"],
               duration: 6000,
               position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
             });
             toast.present();
      }

    }, (err) => {
      this.loader.show("");
      let toast = this.toastCtrl.create({
        message: "Some Went Wrong. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });


  }

  updateCurrentUser(){

    this.user.getCurrentUser().subscribe((resp) => {
      let user = resp["user"]
      this.storage.set('currentUser',user);
      this.globalVar.current_user = user;
    }, (err) => {

    });

  }

  logout() {
    this.loader.show("");
    this.user.logout(this.globalVar.current_user).subscribe(resp => {
      this.loader.hide();
    });
    this.globalVar.current_user = "";
    this.storage.remove("currentUser").then(() => {
      this.navCtrl.setRoot("LoginPage");
    });
  }

    takePicture(sourceType) {
      const options: CameraOptions = {
        quality: 100,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: sourceType
      }

      this.camera.getPicture(options).then((imageData) => {
        this.selected_avatar = imageData;

        if (this.globalVar.current_user.information){
          this.globalVar.current_user.information.avatar = imageData
        }

        let that = this;
        this.loader.show("uploading...");

        let ext = 'jpg';
        let type = 'image/jpeg'
        let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

        this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {
          console.log("Success in getSignedUploadRequest",data)
          that.avatar =  data["public_url"]
          console.log("i am in public url",data["public_url"])

            that.zone.run(() => {
              let request = new XMLHttpRequest();
              request.open('GET', imageData, true);
              request.responseType = 'blob';
              request.timeout = 360000;
              request.onload = function() {

                let reader = new FileReader();
                reader.readAsDataURL(request.response);

                reader.onload =  function(e){

                  that.zone.run(() => {
                    console.log("error",e);
                    let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                    that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                       that.loader.hide();

                      }, (err) => {
                        that.loader.hide();
                        console.log('err: ', err);
                    });
                  })

                }

                reader.onerror =  function(e){
                  console.log('We got an error in file reader:::::::: ', e);
                };

                reader.onabort = function(event) {
                  console.log("reader onabort",event)
                }

              };

              request.onerror = function (e) {
                that.loader.hide();
                console.log("** I got an error in XML Http REquest",e);
              };

              request.ontimeout = function(event) {
                 that.loader.hide();
                 console.log("xmlhttp ontimeout",event)
              }

              request.onabort = function(event) {
                 that.loader.hide();
                 console.log("xmlhttp onabort",event);
              }


              request.send();
            });
        }, (err) => {
          that.loader.hide();
          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
          console.log('getSignedUploadRequest timeout error: ', err);
        });

      }, (err) => {
        let toast = this.toastCtrl.create({
          message: "Unable to Access Gallery. Please Try Later",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();

        console.log('err: ', err);
      });
    }


    takeDocuments(sourceType) {

      const options: CameraOptions = {
        quality: 100,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: sourceType
      }

      let that = this;
      this.camera.getPicture(options).then((imageData) => {

      that.selected_documents.push(imageData);
      that.loader.show("uploading...");

        let ext = 'jpg';
        let type = 'image/jpeg'
        let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
        console.log("**new NAme **",newName);

        this.awsProvider.getSignedUploadRequest(newName, ext,type).timeout(360000).subscribe(data => {

          console.log("** I got success in getSignedUploadRequest",data);

          that.documents.push(data["public_url"]);
          that.zone.run(() => {
            let request = new XMLHttpRequest();
            request.open('GET', imageData, true);
            request.responseType = 'blob';
            request.timeout = 360000;
            request.onload = function() {
              that.zone.run(() => {
                let reader = new FileReader();
                reader.readAsDataURL(request.response);

                console.log("Step 1 reader",reader);
                console.log("Step 1 got stuck in request.response",request)

                reader.onload =  function(){
                  that.zone.run(() => {
                    console.log("Step 2 Loaded",reader.result);
                    let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                    console.log("blob",blob);
                    that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                      that.loader.hide();
                      console.log("result that i got after uploading to aws",_result,that.documents)
                    }, (err) => {
                      console.log('err in uploadFile:::::::: ', err);
                    });
                  });
                }

                reader.onerror =  function(e){
                  console.log('We got an error in file reader:::::::: ', e);
                };

                reader.onabort = function(event) {
                  console.log("reader onabort",event)
                }
              })
            };

            request.onerror = function (e) {
              that.loader.hide();
              console.log("** I got an error in XML Http REquest",e);
            };

            request.ontimeout = function(event) {
               that.loader.hide();
               console.log("xmlhttp ontimeout",event)
            }

            request.onabort = function(event) {
               that.loader.hide();
               console.log("xmlhttp onabort",event);
            }


            request.send();
          })
        }, (err) => {
            that.loader.hide();
            console.log('getSignedUploadRequest timeout error: ', err);

            let toast = this.toastCtrl.create({
              message: "Unable to send request. Check wifi connection",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

          });

      }, (err) => {
        let toast = this.toastCtrl.create({
          message: "Unable to Access Gallery. Please Try Later",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        console.log('err: ', err);
      });
    }

    validateDoc(){
      if (!this.avatar) {
        let toast = this.toastCtrl.create({
          message: "Company Logo is mandatory",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        return true
      }else if(this.documents.length == 0){
        let toast = this.toastCtrl.create({
          message: "License Documents is mandatory",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
        return true
      }
    }

    presentActionSheet() {
       let actionSheet = this.actionSheetCtrl.create({
         title: 'Select Image Source',
         buttons: [
           {
             text: 'Load from Library',
             handler: () => {
               this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
             }
           },
           {
             text: 'Use Camera',
             handler: () => {
               this.takePicture(this.camera.PictureSourceType.CAMERA);
             }
           },
           {
             text: 'Cancel',
             role: 'cancel'
           }
         ]
       });
       actionSheet.present();
     }

    presentActionSheetForDocuments() {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select Image Source',
        buttons: [
          {
            text: 'Load from Library',
            handler: () => {
              this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
          },
          {
            text: 'Use Camera',
            handler: () => {
              this.takeDocuments(this.camera.PictureSourceType.CAMERA);
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      actionSheet.present();
    }

    showNotification(){
      this.navCtrl.push("NotificationPage")
    }
    openDocument(doc){
      window.open(doc,'_system', 'location=yes');
      // const browser = this.iab.create(doc);
    }

    valid_format(file) {
      return file.includes(".jpg") || file.includes(".jpeg") || file.includes(".png")
    }


}

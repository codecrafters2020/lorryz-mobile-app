import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User,GlobalVars, Loader } from '../../providers/providers';
import { MyApp } from '../../app/app.component';
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  notifications: any;
  loading: any;
  public page = 1;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public user: User,
    public globalVar: GlobalVars,
    public loader: Loader,
    public myapp:MyApp,
    public storage: Storage,) {
    this.notifications = [];
    this.loading = false;
    this.globalVar.current_user.notification_count = 0
  }

  fetchNotification(infiniteScroll = null){
    this.user.notifications(this.globalVar.current_user.id,this.page).subscribe((data) => {

      this.loader.hide();
      this.loading = true;

      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.notifications.push(data[i]);
        }
      }else{
        this.notifications = data;
      }

      this.infiniteScrollComplete(infiniteScroll)
    },(err) => {
      this.infiniteScrollComplete(infiniteScroll)
    });
  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }
  doInfinite(infiniteScroll) {
    this.page = this.page+1;
    this.fetchNotification(infiniteScroll);
  }

  ionViewDidEnter() {
    this.loader.show("");
    this.page = 1
    this.fetchNotification();
  }

  doRefresh(refresher) {
    this.page = 1
    this.fetchNotification()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
  notificationDetail(notification)
  {
        if(notification.body.includes("has finished unloading cargo"))
        {
          this.navCtrl.setRoot('CompletedShipmentPage',{id:notification.notifiable_id,fromNotification:true,ongoing:true});
        }
        else if(notification.body.includes("is unloading cargo at delivery"))
        {
          this.navCtrl.push('OngoingShipmentDetailPage',{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("is loading cargo at pick up"))
        {
          this.navCtrl.push('OngoingShipmentDetailPage',{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("is on its way to delivery location"))
        {
          this.navCtrl.push('OngoingShipmentDetailPage',{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("is on its way to pick up the cargo"))
        {
          this.navCtrl.push('OngoingShipmentDetailPage',{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("new bid received"))
        {
          this.navCtrl.push("PostedShipmentDetailPage",{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("Shipment won"))
        {
          this.navCtrl.push("FleetWonShipmentBiddingPage",{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("Shipment won:"))
        {
          this.navCtrl.setRoot("FleetWonShipmentBiddingPage",{shipment_id:notification.notifiable_id,fromNotification:true});
        }
        else if(notification.body.includes("New shipment posted from"))
        {
          this.navCtrl.setRoot("FleetNewShipmentListingPage",{shipmentid:notification.notifiable_id,fromNotification:true});
        }

  }
  ionViewDidLeave()
  {
    this.user.getCurrentUser().subscribe(
      resp => {
        let user = resp["user"];
        this.storage.set("currentUser", user);
        this.globalVar.current_user = user;
      });
  }

}

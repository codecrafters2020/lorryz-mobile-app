import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { User,DriverProvider,Loader,GlobalVars } from '../../providers/providers';
import { Platform} from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@IonicPage()
@Component({
  selector: 'page-driver-upcoming-shipment-list',
  templateUrl: 'driver-upcoming-shipment-list.html',
})
export class DriverUpcomingShipmentListPage {
  public shipments : any
  public ascending= true;
  public loaded = false;
  public page = 1;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public driver: DriverProvider,
              public toastCtrl: ToastController,
               public platform: Platform,
               private diagnostic: Diagnostic,
               public globalVar: GlobalVars,
               private locationAccuracy: LocationAccuracy,
              public loader: Loader) {
    this.shipments = []
  }

  ionViewDidLoad(){
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled){
          this.locationAccuracy
           .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
             
          });
        }else{
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
              () => {
                
              },
              error => console.log('Error requesting location permissions', error)
            );
          });

        }
      })
    }
  }


  fetchShipments(infiniteScroll = null){
    
    this.driver.upcomingShipments(this.page).subscribe((data) => {
        this.loader.hide();
        
      if (infiniteScroll) {
       for(let i=0; i < Object.keys(data).length; i++) {
          this.shipments.push(data[i]);
        }
      }else{
        this.shipments = data;
      }

        this.page = this.page+1;
        this.loaded = true
        this.infiniteScrollComplete(infiniteScroll)
      }, (err) => {
        this.loader.hide();
        this.infiniteScrollComplete(infiniteScroll)
        let toast = this.toastCtrl.create({
          message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });

  }

  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }
  
  doInfinite(infiniteScroll) {
    this.fetchShipments(infiniteScroll);
  }
  doRefresh(refresher) {
    this.page = 1
    this.fetchShipments()
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
  ionViewDidEnter() {    
    this.loader.show("");
    this.page = 1
    this.fetchShipments();
  }

  upcoming(){
  	this.navCtrl.setRoot("DriverUpcomingShipmentListPage");
  }
  ongoing(){
  	this.navCtrl.setRoot("DriverOngoingShipmentListPage");
  }
  completed(){
  	this.navCtrl.setRoot("DriverCompletedShipmentListPage");	
  }

  openOngoingDetail(shipment){
    this.navCtrl.push("DriverUpcomingShipmentDetailPage", {shipment: shipment});  
  }

  sortBy(){

    this.ascending=this.ascending ? false : true;

      this.ascending ?
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? 1 : -1 : 0)
      :
      this.shipments.sort((a, b) => a.id !== b.id ? a.id < b.id ? -1 : 1 : 0)

    

  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }


}

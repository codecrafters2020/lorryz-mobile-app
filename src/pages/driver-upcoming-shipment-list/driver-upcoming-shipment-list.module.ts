import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverUpcomingShipmentListPage } from './driver-upcoming-shipment-list';

@NgModule({
  declarations: [
    DriverUpcomingShipmentListPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverUpcomingShipmentListPage),
  ],
})
export class DriverUpcomingShipmentListPageModule {}

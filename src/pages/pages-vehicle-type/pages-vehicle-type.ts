import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import{
Shipment,
GlobalVars,
Loader,
CountryProvider
} from "../../providers/providers";
import { NewShipmentRequestPage } from '../new-shipment-request/new-shipment-request';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the PagesVehicleTypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pages-vehicle-type',
  templateUrl: 'pages-vehicle-type.html',
})
export class PagesVehicleTypePage {

  value;
  vehicles:any;
  id;
  shipment;
  position;
  pickupnow;
  letsgo;
  vehicle_name;
  src;
  server;
  company_category
  company_type
  isedit = false;
  shipmentId;
  time;
  name;
  desc;
  public vehicleName:string[] = [];
  public vehicleDescription:string[] = [];
  cargodesc: any;
  no_vehicles: any;
  sum
  headers: any;
  baseurl: string;
  secondaryvehicles: any;
  primaryvehicleid: any;
  constructor(
    private viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public shipmentProvider: Shipment,
    public countryProvider: CountryProvider,
    public loader: Loader,
    public globalVar: GlobalVars,
    public http: HttpClient
) {
  this.baseurl = globalVar.server_link + "/api/v1/";
  this.server = this.globalVar.server_link;
  this.vehicles = [];
  this.secondaryvehicles=[];
  this.shipment = navParams.get('shipment');
  this.company_category = navParams.get('company_category');
  this.company_type = navParams.get('company_type');
 //console.log(this.company_type);
//console.log(this.company_category);


  this.shipmentId = navParams.get('shipmentId');
  //console.log(navParams.get('shipmentId'));
  this.isedit = navParams.get('edit');
  if (navParams.get('sum') != undefined)
  {
  this.sum = navParams.get('sum');
  this.sum = this.sum + "1"

  }
  else
  this.sum="11";






  }

  ionViewDidLoad() {
  var s,str;
    console.log('ionViewDidLoad PagesVehicleTypePage');
    debugger
    if(this.navParams.get('category')=='category')
    {
      this.countryProvider
      .primaryvehicles(this.globalVar.current_user.country_id)
      .subscribe(
        data => {
          this.vehicles = data;
        },
        err => {}
      );

    }
    else{
      this.countryProvider
      .subCategoryVehicles(this.navParams.get('primaryvehicleid'))
      .subscribe(
        data => {
          console.log(data);
          this.secondaryvehicles = data;
        },
        err => {}
      );
    }
      this.primaryvehicleid= this.navParams.get('primaryvehicleid');
      this.src=this.navParams.get("primarysrc");
      this.vehicle_name=this.navParams.get("vehiclename");
      this.pickupnow = this.navParams.get("pickupnow");
      this.letsgo = this.navParams.get("letsgo");

      this.no_vehicles = this.navParams.get("no_vehicles");
      this.cargodesc = this.navParams.get("cargodesc");

      this.time = this.navParams.get("unformated_time");
    // console.log(this.navParams.get("unformated_time"));
      this.shipment = this.navParams.get("shipment") || {};
      this.position = this.navParams.get("position") || {};
      //console.log(this.navParams.get("shipment"));
      console.log(this.sum)
  }

  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }

  vehicleid(id,name,src){
   this.src = src;
    this.id = id;
  this.vehicle_name = name;
     this.value = true;
     if(this.isedit == true)
     {
      this.navCtrl.push('EditShipmentPage',{
        shipment:this.shipment,id:this.id,vehicle_name:this.vehicle_name,src:this.src,
         company_category:this.company_category,
          company_type:this.company_type,shipmentId:this.shipmentId,is_edit:true
      }).then(() => {
        this.viewCtrl.dismiss("cancel");
        const startIndex = this.navCtrl.getActive().index - 2;
    this.navCtrl.remove(startIndex, 3);

      });
     }
     else{
      this.navCtrl.push('NewShipmentVehicleSelectPage',{
        shipment:this.shipment,id:this.id,position:this.position,
        pickup_now:this.pickupnow,lets_go:this.letsgo, vehicle_name:this.vehicle_name, value:this.value,src:this.src,
        time:this.time, cargodesc:this.cargodesc, no_vehicles:this.no_vehicles,sum:this.sum
       }).then(() => {
         let count = this.sum.length
        //  console.log(count)
        // this.viewCtrl.dismiss();
        // const startIndex = this.navCtrl.getActive().index - 2;

        // this.navCtrl.remove(startIndex,3);
      });

     }



  }
  secondaryvehicleid(id,name,src)
  {
    this.navCtrl.push('NewShipmentVehicleSelectPage',{
      shipment:this.shipment,id:this.primaryvehicleid,position:this.position,
      pickup_now:this.pickupnow,lets_go:this.letsgo, vehicle_name:this.vehicle_name, value:true,src:this.src,
      time:this.time, cargodesc:this.cargodesc, no_vehicles:this.no_vehicles,sum:this.sum,subcategory:true,
      secondarysrc:src,secondaryid:id,secondaryvehiclename:name
     }).then(() => {
       let count = this.sum.length
      //  console.log(count)
      // this.viewCtrl.dismiss();
      // const startIndex = this.navCtrl.getActive().index - 2;

      // this.navCtrl.remove(startIndex,3);
    });
  }
  setheaders()
{
  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"
  })

}

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagesVehicleTypePage } from './pages-vehicle-type';

@NgModule({
  declarations: [
    PagesVehicleTypePage,
  ],
  imports: [
    IonicPageModule.forChild(PagesVehicleTypePage),
  ],
  entryComponents: [
    PagesVehicleTypePage,
  ]
})
export class PagesVehicleTypePageModule {}

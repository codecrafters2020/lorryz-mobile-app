import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentDropOffPage } from './new-shipment-drop-off';

@NgModule({
  declarations: [
    NewShipmentDropOffPage,
  ],
  imports: [
    IonicPageModule.forChild(NewShipmentDropOffPage),
  ],
})
export class NewShipmentDropOffPageModule {}

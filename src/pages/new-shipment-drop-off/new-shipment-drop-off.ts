import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Keyboard } from '@ionic-native/keyboard';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { GeocoderProvider,GlobalVars } from '../../providers/providers';

declare var google;
@IonicPage()
@Component({
  selector: 'page-new-shipment-drop-off',
  templateUrl: 'new-shipment-drop-off.html',
})
export class NewShipmentDropOffPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('searchbar', {read: ElementRef}) searchbar: ElementRef;
  map: any;
  pickup_location: any;
  pickup_city : any;
  start_lat: any;
  start_lng: any;
  drop_location: any;
  latLng: any;
  drop_lat: any;
  drop_lng: any;
  marker: any;
  position: any;
  autoComplete: any;
  pickup_now: any;
  pickup_building_name: any;
  drop_building_name: any;
  drop_city: any;
  lets_go;
  company_type ;
  company_category;
  dropoff;
  drop_country: any;
  pickup_country:any;
  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   public toastCtrl: ToastController,
   public platform: Platform,
   private diagnostic: Diagnostic,
   public zone: NgZone,
   public geocoder: GeocoderProvider,
   public keyboard:Keyboard,
   public globalVar: GlobalVars,
   private locationAccuracy: LocationAccuracy)
  {
    this.drop_location = "";
    this.latLng = "";
    this.autoComplete = "";
    this.drop_building_name = "";
    this.drop_city = "";
    this.position = this.navParams.get('position');
    this.company_type = this.globalVar.current_user.company.company_type;
    this.company_category = this.globalVar.current_user.company.category;
//  console.log(this.company_type);
  //console.log(this.company_category);

  }

  ionViewDidLoad() {
    this.pickup_location = this.navParams.get('pickup_location');
    this.start_lat = this.navParams.get('start_lat');
    this.start_lng = this.navParams.get('start_lng');
    this.pickup_building_name = this.navParams.get('pickup_building_name');
    // this.pickup_now = this.navParams.get('pickup_now');
    // this.lets_go = this.navParams.get('lets_go');
    // console.log(this.navParams.get('lets_go'));
    this.pickup_city = this.navParams.get('pickup_city');
    this.pickup_country = this.navParams.get('pickup_country');
  }

  ngAfterViewInit() {
    let input = this.searchbar.nativeElement.querySelector('.searchbar-input');;
    let options = {}
    let self = this;
    this.autoComplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(this.autoComplete, 'place_changed', () => {
      let place =  this.autoComplete.getPlace();
      // self.cd.detectChanges();
      this.zone.run(() => {
        self.drop_lat = place.geometry.location.lat();
        self.drop_lng = place.geometry.location.lng();
        self.drop_location = input.value;
        self.drop_building_name = place.name;
        self.drop_city =  this.extractFromAdress(place.address_components, "locality")
        self.drop_country =  this.extractFromAdress(place.address_components, "country")
      });

      self.map.setCenter(place.geometry.location)
      self.marker.setPosition(place.geometry.location);
    });
    this.fetch_nearest_autocomplete(this.position)
  }

  ionViewDidEnter(){
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled){
          this.loadMap();
        }else{
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            // if(canRequest) {
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                () => {
                  this.loadMap();
                },
                error => console.log('Error requesting location permissions', error)
              );
            // }
          });

        }
      })
    }else{
      this.loadMap();
    }
  }

  loadMap(){
    if (this.map) {
      return this.map;
    }
    this.latLng = new google.maps.LatLng(this.position.coords.latitude, this.position.coords.longitude);
    let mapOptions = {
      center: this.latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let self = this;

    google.maps.event.addListenerOnce(this.map, 'idle', function(){
      //Load the markers
      self.marker = new google.maps.Marker({
        map: this,
        animation: google.maps.Animation.DROP,
        position: self.latLng,
      });

      google.maps.event.addListener(self.map, 'click', function (e) {
        if(e.placeId && e.placeId !== '') {
          var request = {
            placeId: e.placeId,
            fields: ['name', 'formatted_address', 'place_id', 'geometry']
          };


          var service = new google.maps.places.PlacesService(self.map);

          service.getDetails(request, function(place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              self.drop_building_name = place.name;
              self.dropoff = place.name;
              console.log(place.name);
            }
          });
        }


        self.keyboardCheck()
        console.log('place');
        console.log(e);
        console.log(self.fetch_address(e));
        console.log(self.geocoder)

        self.marker.setPosition(e.latLng); // set marker position to map center
        self.zone.run(() => {
          self.latLng = e.latLng;
          self.drop_lat = e.latLng.lat();
          self.drop_lng = e.latLng.lng();
          self.fetch_address(e);
        });
      });
      google.maps.event.addListener(self.map, 'drag', function (e) {
        self.keyboardCheck()
      });
    });
  }

  pickupnow(){
    this.pickup_now= true;
    console.log(this.pickup_now);
    this.newShipmentRequest();
  }

  pickuplater(){
    this.pickup_now= false;
    console.log(this.pickup_now);
    this.newShipmentRequest();
   }

   letsgo(){
    this.lets_go= true;
    this.newShipmentRequest();
   }

  keyboardCheck() {
    setTimeout(() => {
       //this.keyboard.hide()
     },500);
  }

  fetch_nearest_autocomplete(position){
    var geolocation = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    var circle = new google.maps.Circle({
      center: geolocation,
      radius: position.coords.accuracy
    });

    this.autoComplete.setBounds(circle.getBounds());
  }

  fetch_address(e){
    if(e.placeId){
      this.geocoder.fetch_address_placeId(e.placeId)
       .then((data : any) =>
       {
         this.drop_location = data;
       })
       .catch((error : any)=>
       {
          console.log(error);
       });

       this.geocoder.fetch_city_placeId(e.placeId)
       .then((data : any) =>
       {
        this.drop_city = this.extractFromAdress(data, "locality");
        this.drop_country = this.extractFromAdress(data, "country");
       })
    }else{
      this.geocoder.fetch_address_latlng(e.latLng)
       .then((data : any) =>
       {
         this.drop_location = data;
       })
       .catch((error : any)=>
       {
          console.log(error);
       });

       this.geocoder.fetch_city_latlng(e.latLng)
       .then((data : any) =>
       {
        this.drop_city = this.extractFromAdress(data, "locality");
        this.drop_country = this.extractFromAdress(data, "country");
       })
    }
  }

  extractFromAdress(components, type){
    for (var i=0; i<components.length; i++)
      for (var j=0; j<components[i].types.length; j++)
        if (components[i].types[j]==type) return components[i].long_name;
    return "";
  }

  newShipmentRequest(){
    this.navCtrl.push('NewShipmentVehicleSelectPage',{shipment: {pickup_lat: this.start_lat, pickup_lng: this.start_lng, pickup_location: this.pickup_location, drop_lat: this.drop_lat, drop_lng: this.drop_lng, drop_location: this.drop_location, pickup_building_name:  this.pickup_building_name, drop_building_name: this.drop_building_name, pickup_city: this.pickup_city, drop_city: this.drop_city,drop_country:this.drop_country,pickup_country:this.pickup_country}, position: this.position, pickup_now: this.pickup_now, lets_go:this.lets_go});
  }

  disabled(){
    if(this.drop_lat && this.drop_location){
      return false
    }else{
      return true
    }
  }
  newShipmentLocation(){
    this.navCtrl.push("NewShipmentLocationPage");
  }

  openPosted(){
    this.navCtrl.setRoot("PostedShipmentPage");
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { NewShipmentRequestPage } from '../pages';

declare var google;
@IonicPage()
@Component({
  selector: 'page-new-shipment-drop-off-location',
  templateUrl: 'new-shipment-drop-off-location.html',
})
export class NewShipmentDropOffLocationPage {
  pickup_location: any;
  start_lat: any;
  start_lng: any;
  drop_location: any;
  latLng: any;
  drop_lat: any;
  drop_lng: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private cd: ChangeDetectorRef,public alertCtrl: AlertController) {
    this.drop_location = "";
    this.latLng = "";
  }

  ionViewDidLoad() {
    this.pickup_location = this.navParams.get('pickup_location');
    this.start_lat = this.navParams.get('pickup_lat');
    this.start_lng = this.navParams.get('pickup_lng');
  }

  ngAfterViewInit() {
    let input = <HTMLInputElement>document.getElementById("auto1");
    let options = {
      componentRestrictions: {
        // country: 'ae',
        // types: ['(regions)']
      }
    }
    let self = this;
    let autoComplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autoComplete, 'place_changed', () => {
      var place =  autoComplete.getPlace();
      
      self.cd.detectChanges();
      self.drop_lat = place.geometry.location.lat();
      self.drop_lng = place.geometry.location.lng();
      self.drop_location = input.value;
      console.log('place_changed', place.geometry.location.lat(), place.geometry.location.lng());
    });
  }

  newShipmentRequest(){
    if(this.drop_lat && this.drop_lng && this.drop_location){
  	  this.navCtrl.push("NewShipmentRequestPage",{shipment: {pickup_lat: this.start_lat, pickup_lng: this.start_lng, pickup_location: this.pickup_location, drop_lat: this.drop_lat, drop_lng: this.drop_lng, drop_location: this.drop_location }});  
    }else{
      let alert = this.alertCtrl.create({
        title: 'ERROR',
        subTitle: 'Please add address.',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }

  newDropOffMap(){
    this.navCtrl.pop();  
  }

  openPosted(){
    this.navCtrl.setRoot("PostedShipmentPage");  
  }

  openOngoing(){
    this.navCtrl.setRoot("OngoingShipmentListPage");  
  }

  openUpcoming(){
    this.navCtrl.setRoot("UpcomingShipmentListPage");  
  }

  openNew(){
    this.navCtrl.setRoot("NewShipmentPage");  
  }

  openCompleted(){
    this.navCtrl.setRoot("CompletedShipmentPage");  
  }
  showNotification(){
    this.navCtrl.push("NotificationPage")
  }
}

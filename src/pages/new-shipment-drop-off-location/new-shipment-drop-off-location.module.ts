import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewShipmentDropOffLocationPage } from './new-shipment-drop-off-location';

@NgModule({
  declarations: [
    NewShipmentDropOffLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(NewShipmentDropOffLocationPage),
  ],
})
export class NewShipmentDropOffLocationPageModule {}

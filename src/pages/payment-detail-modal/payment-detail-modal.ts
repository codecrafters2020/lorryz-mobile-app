import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, ToastController,Platform } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { User,GlobalVars, Loader,InvoiceProvider } from '../../providers/providers';
@IonicPage()
@Component({
  selector: 'page-payment-detail-modal',
  templateUrl: 'payment-detail-modal.html',
})
export class PaymentDetailModalPage {
	public shipment: any;
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  private viewCtrl: ViewController,
  			  private document: DocumentViewer, 
  			  private file: File, 
  			  private transfer: FileTransfer, 
  			  private platform: Platform,
  			  public loader: Loader,
  			  public toastCtrl: ToastController,
          public globalVar: GlobalVars,
          public invoice: InvoiceProvider) {
  	this.shipment = {}
    console.log(this.globalVar.current_user)
  }


  


  ionViewDidLoad() {
    console.log(this.globalVar.current_user)
  	this.shipment = this.navParams.get('shipment') || {};
    console.log('ionViewDidLoad PaymentDetailModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }

  download(){
    this.loader.show("sending...");
    this.invoice.send_in_email(this.shipment.id).subscribe((resp) => {
      if (resp["success"]) {
        this.loader.hide();
        this.viewCtrl.dismiss("cancel");
        let toast = this.toastCtrl.create({
          message: resp["success"],
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        });  
        toast.present(); 
      }else{
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: resp["error"],
          duration: 3000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });

        toast.present(); 
      }


      
    });

  	// let path = null;
  	//   if (this.platform.is('ios')) {
  	//     path = this.file.documentsDirectory;
  	//   } else if (this.platform.is('android')) {
  	//     path = this.file.externalRootDirectory + '/Download/';
  	//   }
  		
  	//   const transfer = this.transfer.create();
  	//   this.loader.show("Downloading...");
  	//   transfer.download('http://staging.lorryz.com/invoices/'+this.shipment.id+'.pdf', path + 'Lorryz'+this.shipment.id+'Receipt.pdf').then(entry => {
  	//   	this.loader.hide();
  	//   	let toast = this.toastCtrl.create({
  	//   	  message: "Receipt Downloade! See Download directory to view it",
  	//   	  duration: 3000,
  	//   	  position: 'top'
  	//   	});
  	//   	toast.present();
  	//     // this.document.viewDocument(url, 'application/pdf', {});
  	//   });

  }

}

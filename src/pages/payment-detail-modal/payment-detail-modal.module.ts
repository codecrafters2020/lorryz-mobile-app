import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentDetailModalPage } from './payment-detail-modal';

@NgModule({
  declarations: [
    PaymentDetailModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentDetailModalPage),
  ],
})
export class PaymentDetailModalPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';
import { FleetShipment,VehicleProvider,GlobalVars, Loader } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-fleet-assign-vehicle-modal',
  templateUrl: 'fleet-assign-vehicle-modal.html',
})
export class FleetAssignVehicleModalPage {
	public shipment: any;
	public shipment_obj: any;
	public vehicles : any
  public loaded : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	private viewCtrl: ViewController,
  	public vehicle: VehicleProvider,
  	public shipmentProvider: FleetShipment,
		public toastCtrl: ToastController,
		public loader: Loader,
    public alertCtrl: AlertController,
    public globalVar: GlobalVars) {
  	this.shipment = {};
  	this.vehicles = [];
  	this.shipment_obj = {state_event: "vehicle_assigned", id: this.shipment.id};
    this.loaded = false;
  }

  ionViewDidLoad() {
    this.shipment = this.navParams.get('shipment') || {};

    let filter = {assignment: true}
    this.loader.show("");
    this.vehicle.getVehiclesForAssignment(filter).subscribe((resp) => {
      this.loader.hide();
      this.vehicles= resp;
      this.loaded = true
    }, (err) => {
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: err && err.error && err.error.error ? err.error.error : 'Something Went wrong',
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
    });
  }


  assignVehicle(){
    console.log(this.shipment_obj.vehicle_ids)
    if (this.shipment_obj.vehicle_ids && this.shipment_obj.vehicle_ids.length > 0){
      this.loader.show("");
      this.shipmentProvider.update(this.shipment_obj, this.shipment.id, this.globalVar.current_user.company_id)
      .subscribe(data => {
        this.loader.hide();
        this.viewCtrl.dismiss();
      }, (err) => {
        this.loader.hide();
        let toast = this.toastCtrl.create({
          message: err.error.errors,
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();
      });
    }else{
      let alert = this.alertCtrl.create({
      title: 'Attention!',
      message: 'Please select Vehicle',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
      });
      alert.present();
    }
  }

  dismiss() {
    this.viewCtrl.dismiss("cancel");
  }
}

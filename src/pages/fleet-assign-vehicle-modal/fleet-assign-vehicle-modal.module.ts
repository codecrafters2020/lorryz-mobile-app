import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetAssignVehicleModalPage } from './fleet-assign-vehicle-modal';

@NgModule({
  declarations: [
    FleetAssignVehicleModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetAssignVehicleModalPage),
  ],
})
export class FleetAssignVehicleModalPageModule {}

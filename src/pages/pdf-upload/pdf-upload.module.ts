import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PdfUploadPage } from './pdf-upload';

@NgModule({
  declarations: [
    PdfUploadPage,
  ],
  imports: [
    IonicPageModule.forChild(PdfUploadPage),
  ],
})
export class PdfUploadPageModule {}

import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, ToastController, LoadingController } from 'ionic-angular';
import { GlobalVars } from '../../providers/providers';
import { Shipment } from '../../providers/shipment/shipment';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { AwsProvider } from '../../providers/aws/aws';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import b64toBlob from "b64-to-blob";
@IonicPage()
@Component({
  selector: 'page-pdf-upload',
  templateUrl: 'pdf-upload.html',
})
export class PdfUploadPage {
  pdfDocuments: any;
  shipmentfrom: any;
	public headers : any;
  documentToUpload: any;
  filename: any;
  loader: any;
  document: any;
  uploadurl: any;
  image: any;
  transitionfrom: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private platform:Platform,
    public globalVar: GlobalVars,
    public shipment:Shipment,
    public http: HttpClient,
    public camera:Camera,
    private awsProvider: AwsProvider,
    private actionSheetController: ActionSheetController,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public zone: NgZone,
    )
  {
      this.pdfDocuments=[];
      this.uploadurl=[];
      this.image=[];
  }

  ionViewDidLoad() {
    this.pdfDocuments = this.navParams.get('pdfdoc');
    this.transitionfrom = this.navParams.get('transitionfrom');
    this.shipmentfrom=[];
    this.shipmentfrom = this.navParams.get('shipment');
    this.setHeader()
    console.log('ionViewDidLoad PdfUploadPage');
  }
  downloadAndOpenPdf(url)
  {
    if(this.platform.is('ios')){
      window.open( url , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( url , '_system','location=yes');
    }
  }
  delay() {
    return new Promise(resolve => setTimeout(resolve, 300));
  }
  async upload2aws()
  {
  this.loader = await this.loadingController.create({
      // content:"loading data..."
    });
    await this.loader.present();

    for(const im of this.image)
    {
        await this.uploadDocumentResource(im);
      }
  }
  async uploadingImagepart2(url,blob,public_url)
  {
    var arr=[];
    arr = this.shipmentfrom['documents'];
    await this.delay();
    let that =this;
    debugger;
    that.awsProvider.uploadFile(url, blob).subscribe(_result => {
      debugger;
      that.uploadurl.push(public_url);
     // that.pdfDocuments.push(public_url);
       console.log("result that i got after uploading to aws",_result)
       debugger
      if(that.uploadurl.length==that.image.length)
      {
//        this.loader.dismiss();
        if(arr!=undefined)
        {
          if(arr.length!=0)
          {
            for(var i=0; i<arr.length;i++)
            {
              that.uploadurl.push(arr[i]);
              if(i==(arr.length-1))
              {
                that.postPdf(that.uploadurl);
              }
            }
          }
          else{
            that.postPdf(that.uploadurl);
          }
        }
        else
        {
          that.postPdf(that.uploadurl);
        }

      }

        });

  }
  postPdf(pdfDoc)
  {
    if(this.globalVar.current_user.role=="cargo_owner")
    {

            this.http.patch(this.globalVar.server_link + '/api/v1/cargo/'+'companies/'+this.globalVar.current_user.company.id+'/shipments/'+this.shipmentfrom.id+'/update_documents.json' , {documents:pdfDoc},{headers: this.headers}).subscribe(res => {
              console.log(res);
              this.loader.dismiss();
              if(this.transitionfrom=='ongoing')
              {
                this.navCtrl.setRoot('OngoingShipmentListPage')
              }
              else
              {
                this.navCtrl.setRoot('CompletedShipmentPage')
              }
        },err=>{
          this.loader.dismiss();
          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        });
    }
    else{
      this.http.patch(this.globalVar.server_link + '/api/v1/fleet/'+'companies/'+this.globalVar.current_user.company.id+'/shipments/'+this.shipmentfrom.id+'/update_documents.json' , {documents:pdfDoc},{headers: this.headers}).subscribe(res => {
        console.log(res);
        this.loader.dismiss();
        if(this.transitionfrom=='ongoing')
        {
          this.navCtrl.setRoot('FleetOngoingShipmentListingPage')
        }
        else
        {
          this.navCtrl.setRoot('FleetCompletedShipmentListingPage')
        }

        },err=>{
          this.loader.dismiss();
          let toast = this.toastCtrl.create({
            message: "Unable to send request. Check wifi connection",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();
        });
    }
//    this.pdfDocuments.push('https://lorry-dev.s3.ap-south-1.amazonaws.com//users/avatar/f61c8602-dbc4-4a44-887c-07b3b142b630.pdf');

  }
  async uploadDocumentResource(im)
  {
    await this.delay();
    // this.loader = this.loadingController.create({
    // });
    // this.loader.present();
      let that=this;
      var fileName =this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      this.filename=fileName;
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/'+fileExtension;
        let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
        console.log("**new NAme **",newName);

        this.awsProvider.getSignedUploadRequest(newName, ext,type).subscribe(data => {

          console.log("** I got success in getSignedUploadRequest",data);
            this.document=data["public_url"];
          that.zone.run(() => {
            let request = new XMLHttpRequest();
            request.open('GET', that.documentToUpload, true);
            request.responseType = 'blob';
            request.timeout = 360000;
            request.onload = function() {
              that.zone.run(() => {
                let reader = new FileReader();
                debugger;
                let sizeInMB:any = (request.response.size / (1024*1024)).toFixed(2);
                 if(sizeInMB > 50)
                 {
                  that.loader.dismiss();
                  let toast = that.toastCtrl.create({
                      message: "Upload resource of size less than 50mb.",
                      duration: 4000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-danger"
                    });
                    toast.present();
                    // return;
                 }
                reader.readAsDataURL(request.response);

                console.log("Step 1 reader",reader);
                console.log("Step 1 got stuck in request.response",request)

                reader.onload =  function(){
                  that.zone.run(async () => {
                    console.log("Step 2 Loaded",reader.result);
                    let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                    debugger;
                    console.log("blob",blob);
                    await that.uploadingImagepart2(data["presigned_url"],blob,data["public_url"]);

                  //   that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                  //     console.log("result that i got after uploading to aws",_result)
                  //     that.pdfDocuments.push(that.document);
                  // }, (err) => {
                  //     console.log('err in uploadFile:::::::: ', err);
                  //   });
                  });
                }

                reader.onerror =  function(e){
                  console.log('We got an error in file reader:::::::: ', e);
                };

                reader.onabort = function(event) {
                  console.log("reader onabort",event)
                }
              })
            };

            request.onerror = function (e) {
              that.loader.dismiss();
              console.log("** I got an error in XML Http REquest",e);
            };

            request.ontimeout = function(event) {
               that.loader.dismiss();
               console.log("xmlhttp ontimeout",event)
            }

            request.onabort = function(event) {
               that.loader.dismiss();
               console.log("xmlhttp onabort",event);
            }


            request.send();
          })
        }, (err) => {
            this.loader.dismiss();
            console.log('getSignedUploadRequest timeout error: ', err);

            let toast = this.toastCtrl.create({
              message: "Unable to send request. Check wifi connection",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

          });


  }
  takeDocuments(sourceType) {

    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
    destinationType: this.camera.DestinationType.FILE_URI,
//      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: sourceType
    }

    let that = this;
    this.camera.getPicture(options).then((imageData) => {
      this.documentToUpload=imageData;
      var fileName =imageData.substr(imageData.lastIndexOf('/') + 1);
        this.filename=fileName;
        this.image.push(this.filename);
    }, (err) => {
      let toast = this.toastCtrl.create({
        message: "Unable to Access Gallery. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      toast.present();
      console.log('err: ', err);
    });
  }
  presentActionSheetForDocuments() {
    let actionSheet = this.actionSheetController.create({
      title: 'Select Document Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        // {
        //   text: 'Use Camera',
        //   handler: () => {
        //     this.takeDocuments(this.camera.PictureSourceType.CAMERA);
        //   }
        // },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
    }
    disbaled()
    {
      if(this.documentToUpload)
      {
        return false
      }
      else{
        return true
      }
    }
  setHeader(){
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| '',
      "Access-Control-Allow-Origin": "*"
    })
  }
}

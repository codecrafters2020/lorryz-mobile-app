import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WithdrawPaymentModePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-withdraw-payment-mode',
  templateUrl: 'withdraw-payment-mode.html',
})
export class WithdrawPaymentModePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WithdrawPaymentModePage');
  }

  openBalance(){
    this.navCtrl.setRoot("BalancePaymentModePage");
  }
  openWithDraw(){
    this.navCtrl.setRoot("WithdrawPaymentModePage");
  }
  openCreditCard(){
    this.navCtrl.setRoot("CreditCardPaymentModePage");
  }

}

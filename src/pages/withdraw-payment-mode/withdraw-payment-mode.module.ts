import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WithdrawPaymentModePage } from './withdraw-payment-mode';

@NgModule({
  declarations: [
    WithdrawPaymentModePage,
  ],
  imports: [
    IonicPageModule.forChild(WithdrawPaymentModePage),
  ],
})
export class WithdrawPaymentModePageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FleetNewShipmentBiddingModalPage } from './fleet-new-shipment-bidding-modal';

@NgModule({
  declarations: [
    FleetNewShipmentBiddingModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FleetNewShipmentBiddingModalPage),
  ],
})
export class FleetNewShipmentBiddingModalPageModule {}

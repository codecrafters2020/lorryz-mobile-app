import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController, ToastController, ViewController,AlertController  } from 'ionic-angular';
import { FleetShipment, GlobalVars, Loader} from '../../providers/providers';
import { FleetNewShipmentListing,FleetActiveShipmentBidding } from '../pages';

@IonicPage()
@Component({
  selector: 'page-fleet-new-shipment-bidding-modal',
  templateUrl: 'fleet-new-shipment-bidding-modal.html',
})
export class FleetNewShipmentBiddingModalPage {
	shipment: any;
  bid: any;
  lat: any;
  lng: any;
  redirect_to_active= false;
  pickupnow: boolean;
  company_category: any;
  company_type: any;
  shipments: any;
  countid: number;
  bidactive: any;
  constructor(public navCtrl: NavController,
					  	public navParams: NavParams,
					  	public shipmentProvider: FleetShipment,
					  	public toastCtrl: ToastController,
              public loader: Loader,
              private viewCtrl: ViewController,
              public alertCtrl: AlertController,
					  	public globalVar: GlobalVars) {
  	this.shipment = {};
  	this.shipments = [];
  	this.bid = {};
  }

  ionViewDidLoad() {
    this.company_type = this.globalVar.current_user.company.company_type;
      this.company_category = this.globalVar.current_user.company.category;

      if((this.company_category == 'fleet')&&(this.company_type == 'individual'))
      {
        this.pickupnow = true;

      }

    this.shipment = this.navParams.get('shipment') || {};
    this.bidactive = this.navParams.get('bidactive');
    this.lat = this.navParams.get('lat');
    this.lng = this.navParams.get('lng');
    console.log("i am in bidding",this.shipment.amount);
    this.bid = this.navParams.get('bid') || {};
    this.redirect_to_active = this.navParams.get('active') || false;

  }

  fleet_income(){
    let country = this.globalVar.current_user.country;
    return ((100 - country.commision)/100) * (this.bid.amount)
  }

  lorryz_share(){
    let country = this.globalVar.current_user.country;
    return (((country.commision)/100) * (this.bid.amount))
  }

  fleet_detention_income(){
    let country = this.globalVar.current_user.country;
    return (((100 - country.commision)/100) * (this.bid.detention))
  }

  lorryz_detention_share(){
    let country = this.globalVar.current_user.country;
    return (((country.commision)/100) * (this.bid.detention))
  }

  bidonShipment(shipment_id){
    this.loader.show("");
    let _bid = {amount: this.bid.amount,detention: this.bid.detention,lat:this.lat,lng:this.lng}
  if(this.pickupnow)
  {
    if(this.bidactive)
    {
      this.shipmentProvider.active(this.globalVar.current_user.company_id,1).subscribe((data) => {
        this.shipments=data;

        for(var i=0;i<this.shipments.length;i++)
        {
          if(this.shipment.id==this.shipments[i].id)
          {
              this.countid = 1;
              break;
          }

        }
        if(this.countid)
        {
          this.shipmentProvider.bidonShipment(shipment_id,this.globalVar.current_user.company_id,_bid,

            ).subscribe(data => {
            this.viewCtrl.dismiss();
            this.loader.hide();
            let toast = this.toastCtrl.create({
              message: "Bid Successfully Posted",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();
            // this.bids = data;
          },(err) => {
            this.loader.hide();
          });

        }
        else
        {
          // this.navCtrl.setRoot("FleetNewShipmentListingPage");
          this.viewCtrl.dismiss()
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message:"Shipment has been cancelled",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success"
          });
          toast.present();

        }

      });

    }
    else
    {
      this.shipmentProvider.posted(this.globalVar.current_user.company_id,1).subscribe((data) => {
        this.shipments=data;

        for(var i=0;i<this.shipments.length;i++)
        {
          if(this.shipment.id==this.shipments[i].id)
          {
              this.countid = 1;
              break;
          }

        }
        if(this.countid)
        {
          this.shipmentProvider.bidonShipment(shipment_id,this.globalVar.current_user.company_id,_bid,

            ).subscribe(data => {
            this.viewCtrl.dismiss();
            this.loader.hide();
            let toast = this.toastCtrl.create({
              message: "Bid Successfully Posted",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            toast.present();
            // this.bids = data;
          },(err) => {
            this.loader.hide();
          });

        }
        else
        {
          // this.navCtrl.setRoot("FleetNewShipmentListingPage");
          this.viewCtrl.dismiss()
          this.loader.hide();
          let toast = this.toastCtrl.create({
            message:"Shipment has been cancelled",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success"
          });
          toast.present();

        }

      });

    }


  }
  else
  {
    this.shipmentProvider.bidonShipment(shipment_id,this.globalVar.current_user.company_id,_bid,

      ).subscribe(data => {
      this.viewCtrl.dismiss();
      this.loader.hide();
      let toast = this.toastCtrl.create({
        message: "Bid Successfully Posted",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success"
      });
      toast.present();
      // this.bids = data;
    },(err) => {
      this.loader.hide();
    });

  }

  }

  dismiss() {
     this.viewCtrl.dismiss("cancel");
   }

}

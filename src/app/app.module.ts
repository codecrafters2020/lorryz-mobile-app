import { GlobalVars } from '../providers/global-vars';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Dialogs } from '@ionic-native/dialogs';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/settings/settings';
import { User } from '../providers/user/user';
import { Api } from '../providers/api/api';
import { MyApp } from './app.component';
import { Company } from '../providers/company/company';
import { Loader } from '../providers/loader';
import { ValidationError } from '../validators/validators';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Broadcaster } from '@ionic-native/broadcaster';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Shipment } from '../providers/shipment/shipment';
import { AwsProvider } from '../providers/aws/aws';
import { HttpModule } from '@angular/http';
import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { FleetShipment } from '../providers/fleet-shipment/fleet-shipment';
import { InvoiceProvider } from '../providers/invoice/invoice';
import { CountryProvider } from '../providers/country/country';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { DriverProvider } from '../providers/driver/driver';
import { VehicleProvider } from '../providers/vehicle/vehicle';
import { Rating } from '../providers/rating/rating';
// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
import { CallNumber } from '@ionic-native/call-number';
import { LocationTracker} from '../providers/location-tracker/location-tracker';

import { Ionic2RatingModule } from "ionic2-rating";
import { Payment } from '../providers/payment/payment';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { Insomnia } from '@ionic-native/insomnia';


import { FileTransfer } from '@ionic-native/file-transfer';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { Keyboard } from '@ionic-native/keyboard';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { Clipboard } from '@ionic-native/clipboard';
import { ForegroundService } from '@ionic-native/foreground-service/ngx';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media } from '@ionic-native/media';
import { AndroidPermissions } from '@ionic-native/android-permissions';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,

    HttpClientModule,
    Ionic2RatingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    GlobalVars,
    Api,
    Insomnia,
    ForegroundService,
    Items,
    User,
    ValidationError,
    Camera,
    SplashScreen,
    Loader,
    Company,
    StatusBar,
    Geolocation,
    Diagnostic,
    LocationAccuracy,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    AwsProvider,
     Camera,
     File,
     InAppBrowser,
     CallNumber,
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Shipment,
    FleetShipment,
    CountryProvider,
    Push,
    DriverProvider,
    VehicleProvider,
    InvoiceProvider,
    Rating,
    LocationTracker,
    BackgroundGeolocation,
    Payment,
    GeocoderProvider,
    DocumentViewer,
    FileTransfer,
    Keyboard,
    Network,
    NetworkProvider,
    Clipboard,
    Dialogs,
    BackgroundMode,
    Broadcaster,
    MediaCapture,
    Media,
    AndroidPermissions,
  ]
})
export class AppModule { }

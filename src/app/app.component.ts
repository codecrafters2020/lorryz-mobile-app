import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { TranslateService } from "@ngx-translate/core";
import { App } from "ionic-angular";
import { BackgroundMode } from '@ionic-native/background-mode';
import {
  Config,
  Nav,
  Platform,
  ToastController,
  AlertController,
  Events,
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { FirstRunPage, LoginPage } from "../pages/pages";
import { Settings } from "../providers/settings/settings";
import { GlobalVars } from "../providers/global-vars";
import { User } from "../providers/user/user";
import { Loader } from "../providers/loader";
import { Push, PushObject, PushOptions } from "@ionic-native/push";
import { Dialogs } from '@ionic-native/dialogs';
import { Broadcaster } from '@ionic-native/broadcaster';


import {
  ActivateAccountPage,
  OwnerInformationPage,
  OrganizationInformationPage
} from "../pages/pages";
import {
  ForgotPasswordPage,
  NewShipmentPage,
  CompanyInformationPage,
  FleetNewShipmentListing,
  DriverUpcomingShipmentListPage,
  NewShipmentRequestPage,
  IndividualInformationPage
} from "../pages/pages";
import { Network } from "@ionic-native/network";
import { NetworkProvider } from "../providers/network/network";
import { interval } from "rxjs/observable/interval";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Insomnia } from "@ionic-native/insomnia";
import { ForegroundService } from "@ionic-native/foreground-service/ngx";
import { LocationTracker } from '../providers/location-tracker/location-tracker';
import { AndroidPermissions } from '@ionic-native/android-permissions';
declare var cordova;

@Component({
  templateUrl: "myapp.html"
})
export class MyApp {
  // rootPage = FirstRunPage;
  rootPage: any;
  information: any;
  alert: any;
  notificationType: string
  notificationTypes: any = ['Shipment won', 'Pickup Now:' ,
  'on its way to pick up the cargo','new bid received','Hi','Hello','has finished unloading',
  'is unloading cargo','is on its way','is loading cargo','New shipment posted from'];
  @ViewChild(Nav) nav: Nav;
  cargo
  pages: any[] = [
    /* { title: 'Tutorial', component: 'TutorialPage' },
    { title: 'Welcome', component: 'WelcomePage' },
    { title: 'Owner Type', component: 'OwnerInformationPage' },
    { title: 'Company Type', component: 'OrganizationInformationPage' },
    { title: 'Dashboard', component: 'NewShipmentPage' },*/
    { title: "Login", component: "LoginPage" },
    { title: "Signup", component: "SignupPage" }
    /*{ title: 'Master Detail', component: 'ListMasterPage' },*/
    /*{ title: 'Menu', component: 'MenuPage' },*/
    /* { title: 'Settings', component: 'SettingsPage' },
    { title: 'Search', component: 'SearchPage' },
    { title: 'IndividualInformation', component: 'IndividualInformationPage' },
    { title: 'CompanyInformation', component: 'CompanyInformationPage' },
    { title: 'EditIndividualInformation', component: 'EditIndividualInformationPage' },
    { title: 'EditCompanyInformation', component: 'EditCompanyInformationPage' },
    { title: 'ForgotPassword', component: 'ForgotPasswordPage' },
    { title: 'NewShipment', component: 'NewShipmentPage' },
    { title: 'PostedShipment', component: 'PostedShipmentPage' },
    { title: 'FleetWonShipmentBidding',component: 'FleetWonShipmentBiddingPage'},
    { title: 'FleetOngoingShipmentListing',component: 'FleetOngoingShipmentListingPage'},
    { title: 'FleetOngoingShipmentDetail',component: 'FleetOngoingShipmentDetailPage'},
    { title: 'FleetAddDriver',component: 'FleetAddDriverPage'},
    { title: 'FleetAddVehicle',component: 'FleetAddVehiclePage'},
    { title: 'FleetVehicleDriverListing',component: 'FleetVehicleDriverListingPage'},
    { title: 'FleetCompletedShipmentListing',component: 'FleetCompletedShipmentListingPage'},
    { title: 'FleetCompletedShipmentDetail',component: 'FleetCompletedShipmentDetailPage'},

    { title: 'FleetNewShipmentBidding',component: 'FleetNewShipmentBiddingPage'},
    { title: 'FleetActiveShipmentBidding',component: 'FleetActiveShipmentBiddingPage'},
    { title: 'FleetNewShipmentListing',component: 'FleetNewShipmentListingPage'},
    { title: 'VehicleList',component: 'VehicleListPage'},
    { title: 'DriverUpcomingShipmentList',component: 'DriverUpcomingShipmentListPage'},
    { title: 'DriverOngoingShipmentList',component: 'DriverOngoingShipmentListPage'},
    { title: 'DriverCompletedShipmentList',component: 'DriverCompletedShipmentListPage'},*/
  ];
  pickupnow: boolean;
  bidwin: boolean;
  headers: any;
  interval: number;
  redirect: boolean;
  bid :boolean;
  dbdeviceid: any;
  rating: boolean;
  apk_version='1.2.8';
  adminAssignedDriverNotif:boolean=false;
  newShipmentFleetCompanyNotif=false;
  constructor(
    public http: HttpClient,
    private translate: TranslateService,
    public loader: Loader,
    public platform: Platform,
    public settings: Settings,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    public storage: Storage,
    public globalVar: GlobalVars,
    public user: User,
    public events: Events,
    private push: Push,
    public network: Network,
    public networkProvider: NetworkProvider,
    public alertCtrl: AlertController,
    public app: App,
    private insomnia: Insomnia,
    public toastCtrl: ToastController,
    private dialogs: Dialogs,
    private backgroundMode: BackgroundMode,
    private broadcaster: Broadcaster,
    public foregroundService: ForegroundService,
    public locationTracker: LocationTracker,
    private androidPermissions: AndroidPermissions
    ) {
    console.log("a");

    platform.ready().then(async () => {
      if (this.platform.is("cordova")) {
        console.warn(
          "Push notifications not initialized. Cordova is not available - Run in physical device"
        );


      this.insomnia.keepAwake();
      this.backgroundMode.enable();
      this.backgroundMode.on('activate').subscribe(() => {
        this.backgroundMode.disableWebViewOptimizations();
        cordova.plugins.backgroundMode.disableBatteryOptimizations();

        this.backgroundMode.setDefaults({
          title:"Lorryz application running in background",
          text:" ",
      });

        });
        const data = await this.androidPermissions.requestPermissions([
          "android.permission.ACCESS_BACKGROUND_LOCATION",
          "android.permission.ACCESS_COARSE_LOCATION",
          "android.permission.PERMISSION.ACCESS_FINE_LOCATION",
  ]);
      }
      //  this.foregroundService.start('GPS Running', 'Background Service', 'drawable/fsicon');



      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.initPushNotification();
      this.initTranslate();
      this.initializeApp();
      this.setPlatformListener();
      this.inialize_location_Fleet();
      this.checkAppVersion();
    });



  }


  // track_shipment(){
  //   this.backgroundMode.enable();
  //   this.backgroundMode.on('activate').subscribe(() => {
  //     this.backgroundMode.disableWebViewOptimizations();

  //     });
  //     }

      initTranslate() {
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang("en");
        const browserLang = this.translate.getBrowserLang();

          this.translate.use("en"); // Set your language

      }
      changeTranslationLanguage(){
        console.log("accessed");
      if(this.globalVar.lang == true){
          this.translate.setDefaultLang("ur");
          this.translate.use("ur");
          console.log("urdu" + this.globalVar.lang);
        }else{
          this.translate.setDefaultLang("en");
          this.translate.use("en");
          console.log("english" + this.globalVar.lang);
        }
      }
  initializeApp() {
    this.platform.ready().then(() => {
      this.storage.ready().then(() => {
        this.storage.get("introShown").then(result => {
          if (result) {
            this.storage.get("currentUser").then(user => {
              console.log(user);
              if (user) {
                this.globalVar.current_user = user;
                this.updateCurrentUser();
                console.log(user);
                this.inialize_location_Fleet();
                if(user.company.category == "cargo")
                  {
                    this.backgroundMode.disable();
                  }
              } else {
                //this.rootPage = 'LoginPage';
                this.rootPage = "LoginPage";
                this.backButtonHandler();

              }
              if (user) {
              }
            });
          } else {
            this.rootPage = "TutorialPage";
            this.storage.set("introShown", true);
          }
        });
      });

      this.backButtonHandler();
    });
//     let self=this;
//     if (this.platform.is('cordova')) {
//      this.interval = setInterval(function () {
//       var a = self.setPlatformListener();
//       console.log(a);
//     }, 5000);
//  }
  }
  checkAppVersion()
  {
    const updateUrl = this.globalVar.server_link+'/api/v1/api/apk_version.json';
    this.setheaders();
    this.http.get(this.globalVar.server_link+'/api/v1/api/apk_version.json').subscribe(data=>{
      console.log(data);
      if(this.apk_version==data['version'])
      {
        console.log('no update available');
      }
      else{
        this.alert = this.alertCtrl.create({
          title: "Update Available",
           //message: "Do you want to close the app?",
          message: "Update Available for Lorryz",
          buttons: [
            // {
            //   text: "Cancel",
            //   role: "cancel",
            //   handler: () => {
            //     this.alert = "";
            //   }
            // },
            {
              text: "Update Application",
              handler: () => {
                window.open("https://play.google.com/store/apps/details?id=com.LORRYZ","_system");
              }
            }
          ],
          enableBackdropDismiss: false
        });
        this.alert.present();
      }
    })
    // this.appUpdate.checkAppUpdate(updateUrl).then(() => { console.log('Update available') }).catch(()=>{
    //   console.log('no update available');
    // });
  }
  setPlatformListener() {
    // this.platform.pause.subscribe(() => {// background
    //   console.log('In Background');
    // return 1;
    // });
    this.platform.resume.subscribe(() => {// foreground
      console.log('In Foreground');
      this.logout_on();
    });

  }
  checkNetwork() {
    if (this.network.type == "none") {
      this.toastCtrl
        .create({
          message: "Sorry, no Internet connectivity detected",
          duration: 3000,
          position: "top"
        })
        .present();
    }
    this.networkProvider.initializeNetworkEvents();

    // Offline event
    this.events.subscribe("network:offline", () => {
      // alert('network:offline ==> '+this.network.type);
      this.toastCtrl
        .create({
          message: "Sorry, no Internet connectivity detected",
          duration: 3000,
          position: "top"
        })
        .present();
    });

    // Online event
    this.events.subscribe("network:online", () => {
      console.log("first tijme let checkNetwork", this.nav);
      // this.nav.setRoot("LoginPage");
      this.toastCtrl
        .create({
          message: `You are now online via ${this.network.type}`,
          duration: 3000,
          position: "top"
        })
        .present();
    });
  }

  backButtonHandler() {
    this.platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNavs()[0];
      console.log("Nav",this.nav.getActive().id)
      if (nav.canGoBack()) {
        //Can we go back?
        nav.pop();
      } else if (
        this.nav.getActive().id === DriverUpcomingShipmentListPage ||
        this.nav.getActive().id === FleetNewShipmentListing ||
        this.nav.getActive().id === NewShipmentPage ||
        this.nav.getActive().id === ActivateAccountPage ||
        this.nav.getActive().id === OwnerInformationPage ||
        this.nav.getActive().id === OrganizationInformationPage ||
        this.nav.getActive().id === CompanyInformationPage ||
        this.nav.getActive().id === IndividualInformationPage ||
        this.nav.getActive().id === LoginPage
      ) {
        this.showCloseAlert();
      }
      else {
        if (this.globalVar.current_user) {
          console.log("here in alert 3")
          this.setRootPage();
          this.nav.setRoot(this.rootPage);
        } else {
          console.log("here in alert 4")
          this.showCloseAlert();
        }
      }
    });
  }

  showCloseAlert() {
    console.log("here in alert 1")
    if (this.alert) {
      this.alert.dismiss();
      this.alert = "";
      return;
    }
    console.log("here in alert 2")
    if(this.globalVar.current_user)
    {
        if(this.globalVar.current_user.company.category=="fleet")
        {
          this.alert = this.alertCtrl.create({
            title: "App termination",
            //message: "Do you want to close the app?",
            message: "You are about to go in Offline mode and will not receive any notification for new shipments",
            buttons: [
              {
                text: "Cancel",
                role: "cancel",
                handler: () => {
                  this.alert = "";
                }
              },
              {
                text: "Close App",
                handler: () => {
                  this.setheaders();
                  this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_online_status.json',{'is_online' : false,'device_id': this.globalVar.device_id},
                  { headers:this.headers}).subscribe((data) => {
                    console.log("logout")
                this.platform.exitApp(); // Close this application
                  });
                }
              }
            ],
            enableBackdropDismiss: false
          });
          this.alert.present();

        }
        else{
          this.alert = this.alertCtrl.create({
            title: "App termination",
            message: "Do you want to close the app?",
            buttons: [
              {
                text: "Cancel",
                role: "cancel",
                handler: () => {
                  this.alert = "";
                }
              },
              {
                text: "Close App",
                handler: () => {
                  this.platform.exitApp(); // Close this application
                }
              }
            ],
            enableBackdropDismiss: false
          });
          this.alert.present();
        }
    }
    else
    {
      this.alert = this.alertCtrl.create({
        title: "App termination",
        message: "Do you want to close the app?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              this.alert = "";
            }
          },
          {
            text: "Close App",
            handler: () => {
              this.platform.exitApp(); // Close this application
            }
          }
        ],
        enableBackdropDismiss: false
      });
      this.alert.present();
    }

}
  inialize_location_Fleet()
  {
    let self=this;
    if(this.globalVar.current_user)
    {
      if(this.globalVar.current_user.company.category == 'fleet' && this.globalVar.current_user.company.company_type == 'individual')
      {
        this.globalVar.location_initiazed_fleet=true;
        if(self.globalVar.current_user.vehicle)
        {
          self.locationTracker.foregroundGeolocation(
            self.globalVar.current_user.vehicle.id
            );

        }
          this.interval = setInterval(function () {
            if(self.globalVar.current_user)
            {
              self.locationTracker.foregroundGeolocation(
                self.globalVar.current_user.vehicle.id
                );

            }
            else
            {
              clearInterval(self.interval);
              self.locationTracker.stopTracking();
              this.globalVar.location_initiazed_fleet=false;
            }
          }, 300000);

    }



    }
    else
    {
      this.locationTracker.stopTracking();
              this.globalVar.location_initiazed_fleet=false;

    }
  }
  initPushNotification() {
    if (!this.platform.is("cordova")) {
      console.warn(
        "Push notifications not initialized. Cordova is not available - Run in physical device"
      );
      return;
    }

    if (this.platform.is("ios")) {
      this.globalVar.os = "ios";
    } else if (this.platform.is("android")) {
      this.globalVar.os = "android";
    }
    const options: PushOptions = {
      android: {
        // senderID: "235106695213",
        // sound:"true"
      },
      ios: {
        alert: "true",
        badge: false,
        sound: "true"
      },
      windows: {},
      browser: {
        pushServiceURL: "http://push.api.phonegap.com/v1/push"
      }
    };
      this.push.createChannel({
        id: "TestChannel",
        description: "My first test channel",
        importance: 4,
        sound:"filhall_violin",
      }).then(() => console.log('Channel created'));

      this.push.createChannel({
        id: "TestChannel1",
        description: "My first test channel",
        importance: 4,
      }).then(() => console.log('Channel created'));
      // Delete a channel (Android O and above)
//      this.push.deleteChannel('LorryzChannel1').then(() => console.log('Channel deleted'));
    const pushObject: PushObject = this.push.init(options);

    pushObject.on("registration").subscribe((data: any) => {
      this.globalVar.device_id = data.registrationId;
      console.log("device token ->", data.registrationId);
    });

    pushObject.on("notification").subscribe((data: any) => {
      console.log('dataObject');
      console.log(data);
      const startTime = Math.floor(Date.now() / 1000);
      let beep = interval(1000).subscribe((res) => {
        this.dialogs.beep(1);
        const currentTime = Math.floor(Date.now() / 1000);
        console.log(currentTime - startTime);
        if(currentTime - startTime == 90) {
          beep.unsubscribe();
//          this.ringtones.stopRingtone('assets/filhall_violin.mp3');
        }
      });
      if(data.message.includes(this.notificationTypes[0])
      || data.message.includes(this.notificationTypes[1])
      || data.message.includes(this.notificationTypes[2])
      ||data.message.includes(this.notificationTypes[3] )
      || data.message.includes(this.notificationTypes[4])
      || data.message.includes(this.notificationTypes[5])
      || data.message.includes(this.notificationTypes[6])
      || data.message.includes(this.notificationTypes[7])
      || data.message.includes(this.notificationTypes[8])
      || data.message.includes(this.notificationTypes[9])
      || data.message.includes(this.notificationTypes[10]) ) {
        console.log('IN IF');
        if(data.message.includes(this.notificationTypes[0])) {
          console.log('Found bid wim');

          data.message = data.message.replace(this.notificationTypes[0], '');
          this.pickupnow=false;
          this.redirect=false;
          this.bidwin=true;
          this.bid=false;
          this.rating=false;
          this.adminAssignedDriverNotif=false;
          this.newShipmentFleetCompanyNotif=false;
        }
        else if(data.message.includes(this.notificationTypes[2])
        || data.message.includes(this.notificationTypes[6])
        || data.message.includes(this.notificationTypes[7])
        || data.message.includes(this.notificationTypes[8])
        || data.message.includes(this.notificationTypes[9])) {
          console.log('redirect to ongoing');

         // data.message = data.message.replace(this.notificationTypes[2], '');
          this.pickupnow=false;
          this.bidwin=false;
          this.redirect =true;
          this.bid=false;
          this.rating=false;
          this.adminAssignedDriverNotif=false;
          this.newShipmentFleetCompanyNotif=false;

        }

        else if(data.message.includes(this.notificationTypes[3])) {
          console.log('redirect to ongoing');

         // data.message = data.message.replace(this.notificationTypes[2], '');
          this.pickupnow=false;
          this.bidwin=false;
          this.redirect =false;
          this.bid=true;
          this.rating=false;
          this.adminAssignedDriverNotif=false;
          this.newShipmentFleetCompanyNotif=false;
        }
        else if(data.message.includes(this.notificationTypes[4])) {
          console.log('redirect to completed');
         // data.message = data.message.replace(this.notificationTypes[2], '');
          this.pickupnow=false;
          this.bidwin=false;
          this.redirect =false;
          this.bid=false;
          this.rating=true;
          this.adminAssignedDriverNotif=false;
          this.newShipmentFleetCompanyNotif=false;
       //     this.alert.dismiss();
        }
        else if(data.message.includes(this.notificationTypes[5])) {
          console.log('redirect to fleet won');
         // data.message = data.message.replace(this.notificationTypes[2], '');
          this.pickupnow=false;
          this.bidwin=false;
          this.redirect =false;
          this.bid=false;
          this.rating=false;
          this.adminAssignedDriverNotif=true;
          this.newShipmentFleetCompanyNotif=false;
       //     this.alert.dismiss();
        }
        else if(data.message.includes(this.notificationTypes[10])) {
          console.log('redirect to fleet new shipment');
         // data.message = data.message.replace(this.notificationTypes[2], '');
            if(data.additionalData.android_channel_id=='TestChannel1')
            {
              if(data.message.includes(this.notificationTypes[1]))
              {
                data.message = data.message.replace(this.notificationTypes[1], '');
                this.pickupnow=true;
                this.bidwin=false;
                this.redirect=false;
                this.bid=false;
                this.rating=false;
                this.adminAssignedDriverNotif=false;
                this.newShipmentFleetCompanyNotif=false;
              }
              else
              {
                this.bidwin=false;
                this.redirect =false;
                this.bid=false;
                this.rating=false;
                this.adminAssignedDriverNotif=false;
                this.pickupnow=false;
                this.newShipmentFleetCompanyNotif=true;
              }
            }
            else {
              console.log('Found PickUpnow');

              data.message = data.message.replace(this.notificationTypes[1], '');
              this.pickupnow=true;
              this.bidwin=false;
              this.redirect=false;
              this.bid=false;
              this.rating=false;
              this.adminAssignedDriverNotif=false;
              this.newShipmentFleetCompanyNotif=false;
            }

       //     this.alert.dismiss();
        }
        else {
          console.log('Found PickUpnow');

          data.message = data.message.replace(this.notificationTypes[1], '');
          this.pickupnow=true;
          this.bidwin=false;
          this.redirect=false;
          this.bid=false;
          this.rating=false;
          this.adminAssignedDriverNotif=false;
          this.newShipmentFleetCompanyNotif=false;
        }
       if (this.pickupnow && this.globalVar.current_user.is_online)
        {

        let confirmAlert = this.alertCtrl.create({
          title: "New Notification",
          message: data.message,
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                // this.newShipmentFleet();
                beep.unsubscribe();
           //     this.ringtones.stopRingtone('assets/filhall_violin.mp3');
              }
            },

            {
              text: "Bid",
              handler: () => {
                this.newShipmentFleet(data.additionalData.id);
                beep.unsubscribe();
             //   this.ringtones.stopRingtone('assets/filhall_violin.mp3');
              }
            },

          ]
        });
        confirmAlert.present();
      }
      if (this.newShipmentFleetCompanyNotif )
      {

      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "Exit",
            handler: () => {
              // this.newShipmentFleet();
              beep.unsubscribe();
         //     this.ringtones.stopRingtone('assets/filhall_violin.mp3');
            }
          },

          {
            text: "View",
            handler: () => {
              this.newShipmentFleetCompany();
           //   this.ringtones.stopRingtone('assets/filhall_violin.mp3');
            }
          },

        ]
      });
      confirmAlert.present();
      beep.unsubscribe();
    }
        if (this.pickupnow && !this.globalVar.current_user.is_online)
        {

        let confirmAlert = this.alertCtrl.create({
          title: "New Notification",
          message: data.message,
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                // this.newShipmentFleet();
                beep.unsubscribe();
           //     this.ringtones.stopRingtone('assets/filhall_violin.mp3');
              }
            },

            {
              text: "Bid",
              handler: () => {
                this.newShipmentFleet(data.additionalData.id);
             //   this.ringtones.stopRingtone('assets/filhall_violin.mp3');
              }
            },

          ]
        });
        confirmAlert.present();
        beep.unsubscribe();
      }
      if (this.rating && this.globalVar.current_user.company.category=="cargo")
      {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [

          {
            text: "OK",
            handler: () => {
              this.newRatingCargoNotification();
  //            beep.unsubscribe();
           //   this.ringtones.stopRingtone('assets/filhall_violin.mp3');
            }
          },

        ]
      });
      confirmAlert.present();
      beep.unsubscribe();
    }
    if (this.rating && this.globalVar.current_user.company.company_type=="individual" && this.globalVar.current_user.company.category=="fleet")
    {
    let confirmAlert = this.alertCtrl.create({
      title: "New Notification",
      message: data.message,
      enableBackdropDismiss: false,
      buttons: [

        {
          text: "OK",
          handler: () => {
            this.newRatingFleetNotification();
       //     beep.unsubscribe();
          }
        },

      ]
    });
    confirmAlert.present();
    beep.unsubscribe();
  }

      else if (this.bidwin && this.globalVar.current_user.company.company_type=="individual")
      {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [


          {
            text: "View Shipment",
            handler: () => {
              this.shipmentwon();
              beep.unsubscribe();
            }
          },

        ]
      });
      confirmAlert.present();
    }
    else if (this.adminAssignedDriverNotif)
    {
    let confirmAlert = this.alertCtrl.create({
      title: "New Notification",
      message: data.message,
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "View Shipment",
          handler: () => {
            this.shipmentwon();
          }
        },
      ]
    });
    confirmAlert.present();
    beep.unsubscribe();
  }
    else if (this.redirect && this.globalVar.current_user.company.company_type=="individual" && this.globalVar.current_user.company.category=="cargo" )
    {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [
         {
            text: "OK",
            handler: () => {
           this.OngoingShipmentListPage(data.additionalData.id);

            }
          },

        ]
      });
      confirmAlert.present();
      beep.unsubscribe();


    }
    else if (this.redirect && this.globalVar.current_user.company.company_type=="company" )
    {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [
         {
            text: "OK",
            handler: () => {
           this.OngoingShipmentListPage(data.additionalData.id);

            }
          },

        ]
      });
      confirmAlert.present();
      beep.unsubscribe();


    }
    else if (this.bid && this.globalVar.current_user.company.company_type=="individual" && this.globalVar.current_user.company.category=="cargo" )
    {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [
         {
            text: "OK",
            handler: () => {
       //    this.postedshipment();
            }
          },
          {
            text: "View Bid",
            handler: () => {
           this.postedshipment();
            }
          }

        ]
      });

      confirmAlert.present();
      beep.unsubscribe();


    }
    else if (this.bid && this.globalVar.current_user.company.company_type=="company" && this.globalVar.current_user.company.category=="cargo" )
    {
      let confirmAlert = this.alertCtrl.create({
        title: "New Notification",
        message: data.message,
        enableBackdropDismiss: false,
        buttons: [
         {
            text: "OK",
            handler: () => {
       //    this.postedshipment();
            }
          },


        ]
      });

      confirmAlert.present();
      beep.unsubscribe();


    }
    else if (this.bidwin && this.globalVar.current_user.company.company_type =="company")
    {
    let confirmAlert = this.alertCtrl.create({
      title: "New Notification",
      message: data.message,
      enableBackdropDismiss: false,
      buttons: [


        {
          text: "OK",
          handler: () => {
            this.shipmentwon();

          }
        },

      ]
    });
    confirmAlert.present();
    beep.unsubscribe();
  }







    }
      else {
        let confirmAlert = this.alertCtrl.create({
          title: "New Notification",
          message: data.message,
          enableBackdropDismiss: false,
          buttons: ['OK']
        });
        confirmAlert.present();
        beep.unsubscribe();
      }

      console.log('data.message');
      console.log(data.message);
      // } else {
      //   // this.nav.push(this.classes[data.additionalData.path], {id: data.additionalData.id});
      //   console.log("Push notification clicked");
      // }
    });

    pushObject
      .on("error")
      .subscribe(error => console.error("Error with Push plugin", error));
  }
  delay() {
    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  logout_on()
  {
    this.setheaders();
    var globalVarDeviceid=this.globalVar.device_id;
      if(this.headers!=null)
      {
        this.http.post( this.globalVar.server_link+'/api/v1/users/db_device_id.json',null,{headers: this.headers})
        .subscribe((data)=>{
          console.log(globalVarDeviceid)
          console.log(data["user_device_id"])
          if(data["user_device_id"]!=null)
          {
            if(globalVarDeviceid==data["user_device_id"])
            {
  //            this.setRootPage();
              // let toast = this.toastCtrl.create({
              //   message: "Device Id match",
              //   duration: 3000,
              //   position: "top"
              // });
              // toast.present();
            }
            else
            {
              // await this.delay();
              this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_online_status.json',{'is_online' : false,'device_id': this.globalVar.device_id},
              { headers:this.headers}).subscribe((data) => {
                console.log("logout")
    this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_device_id.json',{'device_id' : globalVarDeviceid,'id':this.globalVar.current_user.id},
        { headers:this.headers}).subscribe((data) => {
          console.log("logout");
          this.logout_when_same_device();
          let toast = this.toastCtrl.create({
            message: "User Id already logged in on another device",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
           });
          toast.present();
          });
        });
      }

          }
        });

      }

  }
  setHeaders()
  {
    this.headers = new HttpHeaders({
      Authorization: this.globalVar.current_user.auth_token|| ''

   })
  }
  updateCurrentUser() {
    this.setHeaders();
    this.user.getCurrentUser().subscribe(
      resp => {
        this.setheaders();
        let user = resp["user"];
        console.log(user);
        this.storage.set("currentUser", user);
        this.globalVar.current_user = user;
        console.log(this.globalVar.device_id);
        this.logout_on();
        this.setRootPage();
      },
      err => {
        if (err.status == 401 || err.status == 500) {
          this.logout();
        }
        let toast = this.toastCtrl.create({
          message: "You are unauthorized to access",
          duration: 3000,
          position: "top"
        });
        toast.present();
      }
    );
  }

  setRootPage() {
    debugger
    if (this.globalVar.current_user.mobile_verified != "verified") {
      this.rootPage = ActivateAccountPage;
    } else if (this.globalVar.current_user.role == "driver") {
      this.rootPage = DriverUpcomingShipmentListPage;
    } else if (this.globalVar.current_user.role == null) {
      this.rootPage = OwnerInformationPage;
    } else if (this.globalVar.current_user.company_id == null) {
      this.rootPage = OwnerInformationPage;
    } else if (
      this.globalVar.current_user.company_id &&
      !this.globalVar.current_user.information
    ) {
      if (this.globalVar.current_user.company.company_type == "individual") {
        this.rootPage = "OwnerInformationPage";
      } else if (
        this.globalVar.current_user.company.company_type == "company"
      ) {
        this.rootPage = "OwnerInformationPage";
      } else if (this.globalVar.current_user.role == "fleet_owner") {
        this.rootPage = FleetNewShipmentListing;
      } else if (this.globalVar.current_user.role == "cargo_owner") {
        this.rootPage = NewShipmentPage;
      } else {
        this.logout();
      }
    } else if (this.globalVar.current_user.role == "fleet_owner") {
      this.rootPage = FleetNewShipmentListing;
    } else if (this.globalVar.current_user.role == "cargo_owner") {
      this.rootPage = NewShipmentPage;
    } else {
      this.logout();
    }
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  login() {
    this.nav.setRoot("LoginPage");
  }
  signup() {
    this.nav.setRoot("SignupPage");
  }
  driverVehicleListing() {
    this.nav.push("FleetVehicleDriverListingPage");
  }
  driverTiming() {
    this.nav.setRoot("EditvehicletypePage");
  }
  editIndividual() {
    this.nav.push("EditIndividualInformationPage");
  }


  editDriver() {
    this.nav.push("DriverEditProfilePage");
  }
  setheaders()
  {
  this.headers = new HttpHeaders({
    Authorization: this.globalVar.current_user.auth_token|| '',
    "Access-Control-Allow-Origin": "*"
  })

  }

  editCompany() {
    this.nav.push("EditCompanyInformationPage");
  }
  newShipment() {
    this.nav.setRoot("NewShipmentPage");
  }
  newShipmentFleet(shipmentid) {
    this.nav.setRoot("FleetNewShipmentListingPage",{shipmentid:shipmentid});
  }
  newShipmentFleetCompany() {
    this.nav.setRoot("FleetNewShipmentListingPage");
  }
  OngoingShipmentListPage(shipmentid){
    console.log("ongoing shipment")

    this.nav.setRoot("OngoingShipmentDetailPage",{shipment_id:shipmentid});
  }
  postedshipment() {
    console.log("posted shipment")
    this.nav.setRoot("PostedShipmentPage");
  }

  shipmentwon() {
    this.nav.setRoot("FleetWonShipmentBiddingPage");
  }
  newRatingCargoNotification()
  {
    this.nav.setRoot("CompletedShipmentPage");
  }
  newRatingFleetNotification()
  {
    this.nav.setRoot("FleetCompletedShipmentListingPage");
  }

  logout() {
    this.setheaders();
    this.loader.show("");
    this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_online_status.json',{'is_online' : false,'device_id': this.globalVar.device_id},
    { headers:this.headers}).subscribe((data) => {
      console.log("logout")
      this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_device_id.json',{'device_id' : this.globalVar.device_id,'id':this.globalVar.current_user.id},
      { headers:this.headers}).subscribe((data) => {
        this.user.logout(this.globalVar.current_user).subscribe(resp => {
          this.loader.hide();
          this.globalVar.lang = false;
          this.globalVar.location_initiazed_fleet=false;
          this.changeTranslationLanguage();
            this.globalVar.current_user = "";
            this.storage.remove("currentUser").then(() => {
              this.nav.setRoot("LoginPage");

          });
        });
      });
    });
    // this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_device_id.json',{'device_id' : this.globalVar.device_id,'id':this.globalVar.current_user.id},
    // { headers:this.headers}).subscribe((data) => {
    //   console.log("logout");
    //   this.user.logout(this.globalVar.current_user).subscribe(resp => {
    //     this.loader.hide();
    //     this.globalVar.lang = false;
    //     this.changeTranslationLanguage();
    //     this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_online_status.json',{'is_online' : false,'device_id': this.globalVar.device_id},
    //     { headers:this.headers}).subscribe((data) => {
    //       console.log("logout")

    //       this.globalVar.current_user = "";
    //       this.storage.remove("currentUser").then(() => {
    //         this.nav.setRoot("LoginPage");
    //       });

    //     });
    //   });

    //   });

  }
  logout_when_same_device()
  {
        this.user.logout(this.globalVar.current_user).subscribe(resp => {
          this.loader.hide();
          this.globalVar.lang = false;
          this.globalVar.location_initiazed_fleet=false;
          clearInterval(this.interval);
          this.changeTranslationLanguage();
            this.globalVar.current_user = "";
            this.storage.remove("currentUser").then(() => {
              this.nav.setRoot("LoginPage");

          });
        });
   // this.user.logout(this.globalVar.current_user).subscribe(resp => {
  //     this.loader.hide();
  //     this.globalVar.lang = false;
  //     this.changeTranslationLanguage();
  //     this.http.patch(this.globalVar.server_link + "/api/v1/"+'users/change_online_status.json',{'is_online' : false},
  //     { headers:this.headers}).subscribe((data) => {
  //       console.log("logout")

  //       this.globalVar.current_user = "";
  //       this.storage.remove("currentUser").then(() => {
  //         this.nav.setRoot("LoginPage");
  //       });

  //     });
  //   });
  }
  withdrawPaymentMode() {
    this.nav.setRoot("WithdrawPaymentModePage");
  }

  CargoBalancePaymentMode() {
    this.nav.push("BalancePaymentModePage");
  }

  FleetBalancePaymentMode() {
    this.nav.push("FleetBalancePaymentModePage");
  }

  creditCardPaymentMode() {
    this.nav.push("CreditCardPaymentModePage");
  }

  tutorils() {
    this.nav.setRoot("TutorialPage");
  }

  ownerInfo() {
    this.nav.setRoot("OwnerInformationPage");
  }
  companyInfo() {
    this.nav.setRoot("OrganizationInformationPage");
  }

  driverDashboard() {
    this.nav.setRoot("DriverUpcomingShipmentListPage");
  }

  notifications() {
    this.nav.push("NotificationPage");
  }
}
